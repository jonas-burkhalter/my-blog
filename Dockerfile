FROM hugomods/hugo:exts as build

WORKDIR /src
COPY ./ /src

RUN hugo -d public

FROM nginx:alpine
COPY --from=build /src/public/ /usr/share/nginx/html/

EXPOSE 80