+++ 
draft = false
date = 2023-12-26T22:00:00+02:00
title = "WiLa-Postcard"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = []
categories = []
externalLink = ""
series = []
+++

{{< figure src="./images/postcard.jpg" alt="postcard" class="aligne-center size-larger" >}}

Während Geschichten in der Galerie entsprangen,
Durchstreifte ich Länder, anders doch prangen.
Südostasien, ein Mosaik so weit und breit,
In Thailands Zauber, begann mein Weg zur Zeit.

Tempel flüstern von vergang'nen Zeiten,
Im Tropenwald, wo Geheimnisse sich ausbreiten.
Laos, ein Land mit Wasserfällen sacht,
Natur's Symphonie, melodisch und schick, vollbracht.

Vietnams Schluchten, eine raue Pracht,
Ich wanderte durch die Schönheit, im Bann der Macht.
Reisfelder malten Landschaften in Grün,
Eine lebendige Leinwand, so kühn.

Tauchen in Tiefen, wo Korallenriffe blühen,
Kambodschas Inseln die Freude versprühen.
Köstliche Currys und Khaw Sois geniessen,
Doch leider keine Crepe verspiessen