+++  
draft = false  
date = 1997-07-31T00:00:00+02:00  
title = "Reisen"  
slug = ""  
+++

## Reisen durch Irland  
**Daten:** 18. Oktober 2024 - 11. November 2024  

*Ein Roadtrip durch die charmanten und malerischen Regionen Irlands. Highlights sind der lebendige Hafen von Dingle, die Cliffs of Moher in der Nähe von Doolin, Halloween in Galway und die atemberaubenden Landschaften von Mayo und Sligo. Die Reise führt ausserdem zu den historischen Stadtmauern von Derry und zur berühmten Destillerie in Bushmills.*  
[Weiterlesen](/de/series/reisen-irland/)

## Reisen durch Neuseeland  
**Daten:** 27. Dezember 2023 - 7. Februar 2024  

*Begib dich auf eine Reise durch die vielfältigen Landschaften und lebhaften Städte Neuseelands. Beginne in Auckland und erkunde das städtische Leben und die kulturellen Sehenswürdigkeiten. Eine Fähre führt nach Waiheke Island, bekannt für ihre malerischen Weingüter und ruhigen Strände. Die Reise geht mit einer umfassenden Rundreise durch das Land weiter, die durch unterschiedliche Terrains und malerische Routen führt. Diese Erkundung bietet einen tiefen Einblick in die natürliche Schönheit und das reiche Erbe Neuseelands.*  
[Weiterlesen](/de/series/reisen-neuseeland/)

## Reisen durch Südostasien  
**Daten:** 3. Oktober 2023 - 27. Dezember 2023  

*Begib dich auf eine Reise durch Südostasien, entdecke seine lebendige Kultur und atemberaubenden Landschaften. Beginne in Bangkok, Thailand, und besuche Ayutthaya und Kanchanaburi. Fahre nach Chiang Mai für seine Tempel und kulturellen Festivals, dann überquere nach Laos, entdecke Vang Vieng und Luang Prabang. Erlebe das Boun Lai Heua Fai Festival, dann reise nach Nong Khiaw und Sapa, Vietnam, für atemberaubende Landschaften. Schliesse deine Reise in Hanoi ab, Vietnams Hauptstadt, die reich an Geschichte und kolonialem Charme ist. Diese Reise offenbart das Herz der vielfältigen Schönheit und Traditionen von Südostasien.*  
[Weiterlesen](/de/series/reisen-südostasien/)
