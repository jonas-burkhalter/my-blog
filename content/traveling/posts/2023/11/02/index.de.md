+++  
draft = false  
date = 2023-11-02T22:00:00+02:00  
title = "Von Laos nach Vietnam"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++  

## Donnerstag: Ein Tag voller Reisen und Planänderungen  
<!--  
Mit dem Bus von Nong Khiaw nach Dien Bien Phu fahren  
7:30 Uhr: Tuk-Tuk zur Busstation  
9:30 Uhr: Im Bus sitzen  
18:30 Uhr: Ankunft in Dien Bien Phu  
Pläne ändern und sofort in den Nachtbus nach Sapa umsteigen  
3:30 Uhr: Ankunft in Sapa  

Nach einem Zimmer für die Nacht suchen  
-->  

Heute war ein Wirbelwind aus Reisen und schnellen Entscheidungen. Um 7:30 Uhr morgens stieg ich in ein Tuk-Tuk, um zur Busstation zu gelangen und meine Reise von Nong Khiaw nach Dien Bien Phu anzutreten. Die kurvigen Strassen und malerischen Landschaften sorgten für eine unvergessliche Fahrt.  

Um 9:30 Uhr sass ich bequem im Bus und freute mich auf die neuen Erlebnisse, die mich in Vietnam erwarteten. Die Stunden vergingen wie im Flug, begleitet von Landschaftsbildern und kurzen Einblicken in das Leben der Einheimischen.  

Als die Uhr 18:30 Uhr schlug, kam ich in Dien Bien Phu an, nur um festzustellen, dass sich meine Pläne erneut änderten.  
Anstatt einen zusätzlichen Tag in Dien Bien Phu zu verbringen, entschied ich mich, sofort nach Sapa weiterzufahren.  

Gegen 21:00 Uhr machte der Schlafbus einen unerwarteten Halt. Es stellte sich als willkommene Pause heraus, denn ich hatte die Gelegenheit, die erste richtige Mahlzeit des Tages zu geniessen. Die lokalen Aromen und das herzhafte Essen waren eine angenehme Überraschung.  

Um 3:30 Uhr morgens fand ich mich schliesslich in Sapa wieder, einer Stadt, die von atemberaubenden Terrassenreisfeldern umgeben ist. Während der Nachthimmel langsam dem Morgengrauen wich, machte ich mich auf die Suche nach einem gemütlichen Zimmer, um meinen Kopf für die Nacht auszuruhen.  
