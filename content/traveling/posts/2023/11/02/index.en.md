+++ 
draft = false
date = 2023-11-02T22:00:00+02:00
title = "Laos to Vietnam"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Thursday: A Day of Travel and Changing Plans
<!-- 
Taking the bus from nong khiaw to dien bien phu
7:30am the tuk tuk to the bus station
9:30 taking the bus
18:30 arraiving in dien bien phu
changing plans and immideatly changing to the night bus to sapa
3:30 arraving in sapa

searching for a room to sleep for the night
-->

Today was a whirlwind of travel and quick decisions. At 7:30 in the morning, I hopped on a tuk tuk to reach the bus station, embarking on a journey from Nong Khiaw to Dien Bien Phu. The winding roads and scenic landscapes made for a memorable ride.

By 9:30, I was comfortably settled on the bus, eagerly anticipating the new experiences that awaited me in Vietnam. The hours passed in a blur of countryside vistas and fleeting glimpses of local life.

As the clock struck 18:30, I arrived in Dien Bien Phu, only to find my plans shifting once again. 
Rather than taking an extra day in Dien Bien Phu, I decided to pressing forward to Sapa immediately.

Around 21:00, the sleeping bus made an unexpected stop. It turned out to be a welcome pause, as I had the opportunity to enjoy the first proper meal of the day. The local flavors and hearty fare were a pleasant surprise.

By 3:30 in the morning, I found myself in Sapa, a town nestled amidst stunning terraced rice fields. With the night sky slowly giving way to the dawn, I set out in search of a cozy room to rest my head for the night.
