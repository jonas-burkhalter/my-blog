+++ 
draft = false
date = 2023-11-10T22:00:00+02:00
title = "Cat Ba"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Freitag: Ankunft auf Cat Ba und Ruhe am Meer
<!--
Ankunft

spazieren gehen

den Sonnenuntergang geniessen

früh ins Bett gehen
-->

Der Tag ging nahtlos von der Reise in die einladende Umarmung der Insel Cat Ba über. Nach der Ankunft zog es mich an die Küste, um das Spektakel des Sonnenuntergangs zu geniessen. Die Farben am Himmel spiegelten die Ruhe des Meeres wider und schufen eine Leinwand der Gelassenheit.

Der Tag fand mit einem frühen Rückzug ins Bett ein harmonisches Ende. Eine erholsame Nacht war ein passender Abschluss für einen Tag voller Reisen, Erkundungen und der einfachen Freuden, die die Küsten Cat Bas zu bieten haben.

{{< figure src="./images/sunset.jpg" alt="Sonnenuntergang" class="aligne-center size-large" >}}

## Samstag: Gipfelstürmen und Gelassenheit auf Cat Ba
<!-- 
Deepwater Soloing (Bootsfahrt, Klettern, Mittagessen, Rückkehr)

Mittagsschlaf

Sonnenuntergang am Pier

Abendessen
-->
Der Tag begann mit einem aufregenden Abenteuer, geprägt von der spannenden Erfahrung des Deepwater Soloings. Mit einem Boot brach ich auf, um die zerklüfteten Klippen entlang der Küste von Cat Ba zu erkunden. Das Klettern an den Felsen, das Gefühl des Windes und die Herausforderung des Meeres unter mir – jeder Aufstieg war ein Tanz mit den Elementen. Nach dem Klettern erwartete mich ein wohlverdientes Mittagessen, ein Moment der Stärkung nach der körperlichen Anstrengung.

<div style="clear:both;"></div>
{{< figure src="./images/deep_water_solo.jpg" alt="Deepwater Soloing" class="aligne-left size-large" >}}
{{< figure src="./images/deep_water_solo_far.jpg" alt="Deepwater Soloing" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

Der Nachmittag bot eine willkommene Pause, als ich mich einem erholsamen Mittagsschlaf hingab. Die Ruhe der Insel und das rhythmische Rauschen der Wellen bildeten die perfekte Kulisse für einen Moment friedlicher Erholung.

Als die Sonne begann unterzugehen, machten sich Linn und ich auf den Weg zum Pier, um das Schauspiel des Sonnenuntergangs zu bewundern. Die Farben des Himmels spiegelten sich im ruhigen Wasser und schufen ein atemberaubendes Panorama. Der Tag endete stilvoll mit einem köstlichen Abendessen, bei dem wir die Aromen der Insel genossen.

{{< figure src="./images/sunset_2.jpg" alt="Sonnenuntergang" class="aligne-center size-medium" >}}

## Sonntag: Ein Tag der Reflexion und Gemeinschaft auf Cat Ba
<!-- 
lang schlafen

entspannen, Frühstück mit Linn

schlechtes Wetter

Bier trinken und Reisepläne für die Zukunft machen, erfolglos (Linn)
Schwierigkeiten, neue Pläne für die kommenden Tage/Wochen zu schmieden

Treffen mit Claire und Joe
mehr Bier trinken
Pizza und Burger essen
-->

{{< figure src="./images/rain.jpg" alt="Regen" class="aligne-right size-medium" >}}

Der Tag begann mit dem Luxus eines langen, ungestörten Schlafes – eine willkommene Erholung nach Tagen voller Abenteuer. Der Morgen verlief gemächlich, mit entspannten Momenten und einem gemeinsamen Frühstück mit Linn, das der ruhigen Atmosphäre eine Prise Kameradschaft verlieh.

Im Laufe des Tages umarmte das Wetter eine gemütliche Stimmung, ideal für Nachdenklichkeit. Gemeinsam mit Linn versuchte ich, die nächsten Reisepläne zu schmieden. Doch die Flüchtigkeit der Reiseplanung liess uns mehr Fragen als Antworten zurück, was ein Gefühl der Unsicherheit hervorrief.

{{< figure src="./images/burger.jpg" alt="Burger" class="aligne-left size-small" >}}

Unbeeindruckt davon entwickelte sich der Tag zu einem freudigen Wiedersehen mit Claire und Joe. Bei Bier wurden lebhafte Gespräche geführt, während wir versuchten, die Rätsel der Zukunft zu entschlüsseln. Trotz der Schwierigkeiten, konkrete Pläne zu formulieren, brachten das gemeinsame Lachen und die Kameradschaft Wärme in den Moment.

Der Abend setzte sich mit Pizza und Burger fort, ein Festmahl, das die Tiefe der Freundschaften widerspiegelte, die unterwegs geschlossen wurden. Als die Nacht hereinbrach, wichen die Herausforderungen der Planung der einfachen Freude, in der Gesellschaft von Gleichgesinnten zu sein.