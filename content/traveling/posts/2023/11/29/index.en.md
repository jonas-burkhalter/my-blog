+++ 
draft = false
date = 2023-11-29T22:00:00+02:00
title = "Saigon"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++


## Wednesday: From Mui Ne to Saigon—A Journey into History and Reunions
<!--
bus to saigon

war museeum 
(meeting thomas last seen in cambodia)

dinner (thomas, claire and I) 
 -->

The day commenced with the rhythmic hum of wheels on asphalt as we boarded a bus bound for Saigon—a journey that would unfold through the diverse landscapes of Vietnam.

{{< figure src="./images/war_museum_bomb.jpg" alt="war museum (bomb)" class="aligne-left size-smaller" >}}

Upon our arrival in Saigon, the day's exploration took on a poignant tone with a visit to the War Museum. The exhibits, a mosaic of memories and narratives, provided a stark glimpse into the historical chapters that shaped the nation. The echoes of the past resonated as we wandered through the museum, each artifact and photograph a testament to the resilience of the human spirit.

{{< figure src="./images/war_museum_helicopter.jpg" alt="war museum (helicopter)" class="aligne-center size-medium" >}}

In a serendipitous turn, the day also marked a reunion with Thomas—a familiar face last seen in Cambodia. The chance encounter added a layer of warmth to the day, weaving the threads of our separate journeys into a shared tapestry.

As the evening descended, Thomas, Claire, and I gathered for dinner. The culinary delights mirrored the diverse experiences of the day, each dish a fusion of flavors that echoed the cultural richness of Saigon. 

## Thursday: Cu Chi Tunnels, Market Vibes, and Culinary Delights
<!-- 
cu chi tunnels
market
dinner
-->

{{< figure src="./images/cu_chi_tunnels_trap.jpg" alt="Cu Chi tunnels (trap)" class="aligne-left size-small" >}}

Embarking on another day of exploration, Claire and I ventured into the historical depths of the Cu Chi Tunnels—a network of underground passages that bore witness to the resilience and ingenuity of the Vietnamese during times of conflict. The subterranean world unfolded before us, revealing the intricate tunnels, hidden chambers, and the stories of survival etched into the walls.

{{< figure src="./images/cu_chi_tunnels.jpg" alt="Cu Chi tunnels" class="aligne-right size-small" >}}

As we navigated through the narrow tunnels, the air thickened with a sense of history. These tunnels, stretching for miles beneath the earth, played a crucial role during the Vietnam War, serving as a clandestine network for communication, storage, and living quarters for the Viet Cong. The ingenuity behind their construction, with multiple layers and hidden entrances, showcased the resourcefulness of those who dwelled within.

Beyond their tactical significance, the Cu Chi Tunnels encapsulated the indomitable spirit of the Vietnamese people. The narratives of resilience and determination echoed through the passages, where daily life continued despite the surrounding chaos.

Transitioning from the historical to the vibrant present, we immersed ourselves in the lively atmosphere of a local market. The stalls brimmed with an array of colors, scents, and the hustle and bustle of daily life. 

As the day transitioned into evening, we indulged in the culinary delights that Saigon had to offer. The dinner table became a canvas of flavors—a culmination of the day's explorations and a celebration of the diverse gastronomic experiences that Vietnam presented.

## Friday: Bittersweet Farewells and a Journey to Phnom Penh
<!-- 
goodbye claire
bus to phnom penh
-->

{{< figure src="./images/claire.jpg" alt="Claire" class="aligne-right size-small" >}}

The day unfolded with a bittersweet farewell to Claire—a travel companion whose presence had enriched the tapestry of experiences since Bangkok. As our paths diverged, the echoes of shared adventures lingered, and the parting was accompanied by a sense of gratitude for the moments we had woven together.

**Goodbye Claire 😞😢**

Embracing the transient nature of travel, I boarded a bus bound for Phnom Penh. The wheels turned, carrying me through the landscapes that bridged Vietnam and Cambodia. The journey became a reflection of the diverse narratives that unfolded at each destination, a narrative penned by the ebb and flow of new encounters.
