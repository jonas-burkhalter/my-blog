+++  
draft = false  
date = 2023-11-29T22:00:00+02:00  
title = "Saigon"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Mittwoch: Von Mui Ne nach Saigon—Eine Reise in die Geschichte und Wiedersehen  
<!--  
Bus nach Saigon  
Kriegsmuseum  
(Treffen mit Thomas, zuletzt gesehen in Kambodscha)  
Abendessen (Thomas, Claire und ich)  
-->  

Der Tag begann mit dem gleichmässigen Summen der Räder auf Asphalt, als wir einen Bus nach Saigon bestiegen—eine Reise, die uns durch die vielfältigen Landschaften Vietnams führte.  

{{< figure src="./images/war_museum_bomb.jpg" alt="Kriegsmuseum (Bombe)" class="aligne-left size-smaller" >}}  

Nach unserer Ankunft in Saigon nahm der Tag eine nachdenkliche Wendung mit einem Besuch des Kriegsmuseums. Die Ausstellungen, ein Mosaik aus Erinnerungen und Erzählungen, boten einen eindrucksvollen Einblick in die historischen Kapitel, die die Nation geprägt haben. Die Echos der Vergangenheit hallten wider, als wir durch das Museum schlenderten. Jedes Artefakt und Foto zeugte vom unerschütterlichen menschlichen Geist.  

{{< figure src="./images/war_museum_helicopter.jpg" alt="Kriegsmuseum (Helikopter)" class="aligne-center size-medium" >}}  

In einer glücklichen Fügung war der Tag auch von einem Wiedersehen mit Thomas geprägt—einem bekannten Gesicht, das wir zuletzt in Kambodscha gesehen hatten. Diese Begegnung fügte dem Tag eine warme Note hinzu und verknüpfte die Fäden unserer getrennten Reisen zu einem gemeinsamen Geflecht.  

Am Abend versammelten sich Thomas, Claire und ich zum Abendessen. Die kulinarischen Köstlichkeiten spiegelten die vielfältigen Erfahrungen des Tages wider—jedes Gericht eine Verschmelzung von Aromen, die den kulturellen Reichtum Saigons widerspiegelten.  

## Donnerstag: Cu Chi Tunnel, Marktleben und kulinarische Genüsse  
<!--  
Cu Chi Tunnel  
Markt  
Abendessen  
-->  

{{< figure src="./images/cu_chi_tunnels_trap.jpg" alt="Cu Chi Tunnel (Falle)" class="aligne-left size-small" >}}  

Ein weiterer Tag der Erkundung begann, als Claire und ich in die historischen Tiefen der Cu Chi Tunnel eintauchten—ein Netzwerk unterirdischer Gänge, das von der Widerstandsfähigkeit und dem Einfallsreichtum der Vietnamesen während der Kriegszeiten zeugt. Die unterirdische Welt entfaltete sich vor uns und offenbarte die komplizierten Tunnel, versteckten Kammern und Geschichten des Überlebens, die in die Wände eingegraben waren.  

{{< figure src="./images/cu_chi_tunnels.jpg" alt="Cu Chi Tunnel" class="aligne-right size-small" >}}  

Während wir uns durch die engen Tunnel bewegten, verdichtete sich die Luft mit einem Gefühl von Geschichte. Diese Tunnel, die sich kilometerweit unter der Erde erstrecken, spielten während des Vietnamkriegs eine entscheidende Rolle. Sie dienten als geheimes Netzwerk für Kommunikation, Lagerung und Wohnräume für den Vietcong. Die Raffinesse ihrer Konstruktion, mit mehreren Ebenen und versteckten Eingängen, zeigte die Einfallsreichtum der Menschen, die dort lebten.  

Die Cu Chi Tunnel verkörperten nicht nur ihre taktische Bedeutung, sondern auch den unbezwingbaren Geist des vietnamesischen Volkes. Die Erzählungen von Widerstandsfähigkeit und Entschlossenheit hallten durch die Gänge, in denen das tägliche Leben trotz des Chaos weiterging.  

Vom Historischen in die lebendige Gegenwart wechselnd, tauchten wir in die geschäftige Atmosphäre eines lokalen Marktes ein. Die Stände waren gefüllt mit einer Fülle von Farben, Düften und dem lebhaften Treiben des täglichen Lebens.  

Als der Tag in den Abend überging, genossen wir die kulinarischen Köstlichkeiten, die Saigon zu bieten hatte. Der Esstisch wurde zur Leinwand der Aromen—ein Höhepunkt der Tageserkundungen und eine Feier der vielfältigen gastronomischen Erfahrungen, die Vietnam präsentierte.  

## Freitag: Abschiede und eine Reise nach Phnom Penh  
<!--  
Abschied von Claire  
Bus nach Phnom Penh  
-->  

{{< figure src="./images/claire.jpg" alt="Claire" class="aligne-right size-small" >}}  

Der Tag begann mit einem bittersüssen Abschied von Claire—einer Reisegefährtin, deren Anwesenheit die Erlebnisse seit Bangkok bereichert hatte. Als sich unsere Wege trennten, hallten die Echos gemeinsamer Abenteuer nach, und der Abschied wurde von einem Gefühl der Dankbarkeit für die gemeinsamen Momente begleitet.  

**Auf Wiedersehen, Claire 😞😢**  

Im Einklang mit der vergänglichen Natur des Reisens bestieg ich einen Bus nach Phnom Penh. Die Räder drehten sich und trugen mich durch die Landschaften, die Vietnam und Kambodscha miteinander verbinden. Die Reise wurde zu einer Reflexion über die vielfältigen Geschichten, die sich an jedem Zielort entfalteten—eine Erzählung, geschrieben durch das Kommen und Gehen neuer Begegnungen.  
