+++  
draft = false  
date = 2023-11-20T22:00:00+02:00  
title = "Nha Trang"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Montag: Ankunft in Nha Trang  
<!--  
Ankunft in Nha Trang (die Stadt ist nicht so schön)  

Abendessen (nicht besonders gut)  
-->  

Der Tag markierte die Ankunft von Linn und mir in der Küstenstadt Nha Trang. Als die Sonne am Horizont unterging, entfaltete sich die Stadt, doch ihr erster Eindruck liess zu wünschen übrig.  

Nha Trang präsentierte sich auf den ersten Blick ohne den Charme, der die vorherigen Reiseziele so auszeichnete. Statt faszinierender Anziehungskraft wirkte die Stadtlandschaft fade, und die Aussicht auf versteckte Schätze fühlte sich mehr wie Wunschdenken an.  

Der Abend setzte sich mit der enttäuschenden Suche nach einem Abendessen fort—eine Erkundung der lokalen Küche, die leider selbst bescheidene Erwartungen nicht erfüllte. Das kulinarische Erlebnis spiegelte die ernüchternde Atmosphäre der Stadt wider und liess die erhofften Highlights vermissen.  

## Dienstag: Ruhe und Langeweile am Strand  
<!--  
Kaffee trinken mit Linn  

Am Strand entspannen  
Die Sonne geniessen  
Ein Buch lesen  
Sich langweilen  
Ein Loch in den Sand graben  
-->  

Der Tag begann mit dem Versprechen von Ruhe, als Linn und ich bei einem köstlichen Kaffee am Strand sassen—ein Moment der Gelassenheit vor der rhythmischen Kulisse der Wellen. Die Sonne, ein wohlwollender Begleiter, hüllte uns in ihre warme Umarmung, während wir uns auf dem goldenen Sand niederliessen, um den Blick aufs Meer zu geniessen.  

{{< figure src="./images/beach.jpg" alt="Strand" class="aligne-right size-small" >}}  

Das Entspannen am Strand entfaltete sich wie eine Leinwand der Erholung—jede Welle ein beruhigender Pinselstrich, und die Sonne ein sanfter Farbton am Horizont. Ich vertiefte mich in die Seiten eines Buches, wobei sich die Erzählung mit der Melodie des Meeres verflocht. Doch im Laufe des Tages machte sich ein unerwarteter Begleiter breit—die Langeweile.  

Die einst fesselnden Wellen und die anziehenden Seiten des Buches begannen ihren Reiz zu verlieren. Die Ruhe verwandelte sich in eine subtile Monotonie und forderte eine Wendung im Tagesgeschehen. In einem spontanen Versuch, der aufkommenden Langeweile zu entkommen, entschied ich mich, ein Loch in den Sand zu graben—eine heitere Rebellion gegen die Routine.  

## Mittwoch: Ein Tag der Ruhe und gemeinsamen Momente  
<!--  
Lang schlafen  

Kaffee trinken mit Linn und Claire (endlich ein schönes Restaurant in Nha Trang)  

Ein weiterer entspannter Tag am Strand  
Langweilig  
Ein Loch graben  

Gemeinsames Abendessen geniessen  
-->  

Der Tag begann mit dem luxuriösen Genuss eines langen, ununterbrochenen Schlafs, eine erfrischende Einstimmung auf das, was kommen würde. Als die Morgensonne den Horizont erhellte, lockte das Versprechen eines neuen Tages, und Nha Trang zeigte eine Facette seines Charmes.  

Linn, Claire und ich begaben uns auf die Suche nach Kaffee—ein einfaches Ritual, das zu einer wunderbaren Entdeckung wurde. Endlich enthüllte sich ein Restaurant in Nha Trang als kulinarisches Juwel.  

Der Tag fand jedoch in den beruhigenden Rhythmen der Strandidylle seine Fortsetzung. Die strahlende Sonne schaffte es kaum, die anhaltende Monotonie zu vertreiben. Doch inmitten des sanften Auf und Ab der Wellen fand ich Trost.  

{{< figure src="./images/beach_evening.jpg" alt="Strand am Abend" class="aligne-left size-medium" >}}  

Ein Versuch, die aufkommende Langeweile zu überwinden, nahm die Form des Grabens eines Lochs im weichen Sand an—ein Akt, der ebenso heiter wie symbolisch für meine Suche nach Abwechslung war. Der Strand wurde zu einer Leinwand der Ruhe, auf der jedes Sandkorn Geschichten vom zeitlosen Tanz der Gezeiten erzählte.  

Als die Sonne am Horizont verschwand, fand der Tag seinen Höhepunkt im gemeinsamen Genuss eines Abendessens. Das kulinarische Erlebnis, bereichert durch die Gesellschaft von Linn und Claire, wurde zu einem Höhepunkt eines Tages, der ansonsten durch die subtilen Nuancen der Strandmonotonie geprägt war.  

{{< figure src="./images/chees_cake.jpg" alt="Käsekuchen" class="aligne-center size-medium" >}}  
