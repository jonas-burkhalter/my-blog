+++ 
draft = false
date = 2023-11-20T22:00:00+02:00
title = "Nha Trang"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Monday: Arrival in Nha Trang
<!-- 
arriving in nha trang (the city is not that beautiful)

getting dinner (not that good)
-->
The day marked Linn's and my arrival in the coastal city of Nha Trang. As the sun dipped below the horizon, the city unfurled itself, but its initial presentation left much to be desired.

Nha Trang, on first inspection, appeared devoid of the charm that had adorned previous destinations. The cityscape, rather than being immediately captivating, presented itself as lackluster, and the promise of hidden gems felt more like wishful thinking.

The evening continued with the disappointing quest for dinner—an exploration of local flavors that, unfortunately, failed to meet even the most modest expectations. The culinary experience mirrored the underwhelming aura of the city, lacking the anticipated delights.

## Tuesday: Serenity and Boredom by the Seashore
<!-- 
getting coffee with linn

chilling at the beach
enjoy the sun
reading a book
getting bored
digg a hole in the sand
 -->

The day dawned with the promise of serenity as Linn and I started with a delightful coffee by the beach—a moment of tranquility against the rhythmic backdrop of the waves. The sun, a benevolent companion, cast its warm embrace as we settled into the golden sands, seeking solace in the simplicity of the ocean view.

{{< figure src="./images/beach.jpg" alt="beach" class="aligne-right size-small" >}}

Chilling at the beach unfolded as a canvas of relaxation—each wave a soothing stroke, and the sun a gentle brushstroke against the horizon. I immersed myself in the pages of a book, the narrative intertwining with the melody of the sea. However, as the day wore on, an unexpected companion arrived—boredom.

The once captivating waves and the inviting pages of the book began to lose their allure. The serenity transformed into a subtle monotony, prompting a shift in the day's narrative. In a whimsical attempt to break free from the encroaching boredom, I decided to dig a hole in the sand—a lighthearted rebellion against the routine.

## Wednesday: A Day of Rest and Shared Moments
<!-- 
sleeping long

get a coffee with linn and claire (finally a nice restaurant in Nha Trang)

another chill day on the beach
boring
digging a hole

enjoy dinner together
 -->

The day unfolded with the luxurious embrace of a long, uninterrupted sleep, a rejuvenating prelude to what would transpire. As the morning sun graced the horizon, the promise of a new day beckoned, and Nha Trang unveiled a facet of its charm.

Linn, Claire, and I embarked on a quest for coffee—a simple ritual that blossomed into a delightful discovery. Finally, a restaurant in Nha Trang revealed itself as a culinary gem.

The day, however, settled into the soothing rhythm of beachside idleness. The sun, though radiant, struggled to dispel the lingering echoes of monotony. Yet, amidst the gentle ebb and flow of the waves, I found solace.

{{< figure src="./images/beach_evening.jpg" alt="beach evening" class="aligne-left size-medium" >}}

An attempt to break free from the spell of boredom took the form of digging a hole in the soft sand—an act as whimsical as it was symbolic of my quest for excitement. The beach became a canvas of tranquility, with each grain of sand telling stories of the tide's timeless dance.

As the sun dipped below the horizon, the day found its crescendo in the shared enjoyment of dinner. The culinary experience, enriched by the camaraderie of Linn and Claire, became a highlight in a day that otherwise meandered through the subtle nuances of seaside monotony.

{{< figure src="./images/chees_cake.jpg" alt="chees cake" class="aligne-center size-medium" >}}
