+++ 
draft = false
date = 2023-11-16T22:00:00+02:00
title = "Hue, Hoi An, Da Nang"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Thursday: A Day of Watery Journeys and Tranquil Slumbers
<!-- 
waking up  in the bus
seeing water, water and more water
hue is flooded
the bus takes 2 hours to get into the city

we take a taxi, bus, taxi to hoi an

get the first meel of the day (1600)

falling asleep for the next 16h
 -->

{{< figure src="./images/flood_with_linn.jpg" alt="flood" class="aligne-right size-medium" >}}

The day began with the blurry awakening in the bus, greeted by the sight of water stretching as far as the eye could see. Hue revealed itself as a city inundated, the floodwaters reshaping our plans. Originally intending to stay in Hue, the unforeseen challenges of the flood urged us to reevaluate our course.

Adapting to the circumstances, the bus, resilient in the face of adversity, took a staggering two hours to traverse the waterlogged terrain and finally reach the city center. Undeterred, the journey continued with a medley of taxis and buses, each leg inching closer to the new destination—Hoi An.

{{< figure src="./images/flood.jpg" alt="flood" class="aligne-left size-medium" >}}

Upon arrival, the first meal of the day took the form of a late late lunch, the clock ticking to 16:00. The flavors of the meal were a welcome reward after a day spent navigating the watery landscape and overcoming the challenges posed by nature's whims.

With the day's tribulations behind, the weariness settled in, and a 16-hour nap became a gentle descent into tranquil slumber. The rhythm of the road and the intermittent glimpses of water seemed to fade away, replaced by the serenity of rest.

## Friday: Embracing the Unexpected in Hoi An
<!-- 
what to do?
rain, cold and dirty beach

(howl day with linn)

chilling in a caffee
eating some intresting food (Banh dap, cao lau)

lantern making (peaceful, quiet)

bar (linn, joe, claire)(hot choclate for me)
 -->

{{< figure src="./images/beach.jpg" alt="beach" class="aligne-right size-medium" >}}

The day unfolded with a question hanging in the air—what to do when faced with rain, cold weather, and a less-than-ideal beach? The answer revealed itself in the simple joy of shared moments, as Linn and I navigated the day together.

The morning brought us to a cozy café. The day's culinary adventure commenced with the exploration of intriguing dishes—Banh dap and Cao Lau. Each bite offered a taste of Hoi An's unique gastronomic tapestry.

{{< figure src="./images/lantern_in_progress.jpg" alt="lantern" class="aligne-left size-small" >}}
{{< figure src="./images/lantern_done.jpg" alt="lantern" class="aligne-right size-small" >}}

The afternoon became an unexpected journey into artistic expression, as we engaged in lantern making. The atmosphere was peaceful, the only sounds being the snip of scissors and the occasional laughter, creating a moment of quiet creativity.

<div style="clear:both;"></div>

As evening descended, the day culminated in a bar rendezvous with Joe, and Claire. The warmth of companionship enveloped us, and a cup of hot chocolate became the perfect companion in the cozy ambiance.

## Saturday: Culinary Delights and Farewell in Hoi An and Da Nang
<!--
banh my whit claire, joe, linn and yuho
meeting yuho again before our paths spit up

taking a grab from hoi an to da nang

going out for dinner together (claire, joe, linn)

see the dragon bridg

grab some drinks
  -->

{{< figure src="./images/group (claire, joe, linn, yuho).jpg" alt="claire, joe, linn, yuho" class="aligne-left size-small" >}}

The day commenced with a delightful gathering, as Claire, Joe, Linn, and Yuho joined for a Banh Mi breakfast—a symphony of flavors shared among friends. The meal served not only as nourishment but also as a celebration of camaraderie before paths diverged.

The morning became a poignant moment of reunion as I met with Yuho once more, savoring the transient joy of shared experiences before bidding temporary farewells.

{{< figure src="./images/rice_in_pinapple.jpg" alt="rice in pinapple" class="aligne-right size-small" >}}

The journey then transitioned with a Grab ride from Hoi An to Da Nang, the landscapes shifting as the wheels carried us to a new destination. The day's adventures continued with a shared dinner, laughter echoing through the air as Claire, Joe, and Linn accompanied me through the city.

The Dragon Bridge, an iconic landmark. Its majestic form illuminated the night. The dragon came to life, breathing both fire and water from its mouth—a mesmerizing spectacle that added a touch of magic to the night.

{{< figure src="./images/dragon.jpg" alt="dragon" class="aligne-center size-medium" >}}

As the evening deepened, we found ourselves in the company of drinks, the clinking of glasses marking not just the end of a day but the celebration of friendships made along the way.

## Sunday: A Day of Bouldering and Culinary Delights
<!-- 
bouldering 
first time after a long time

getting my quang

going to the bikini bottom bar/restaurant
getting a burger and some fancy drinks
 -->

{{< figure src="./images/bouldering.jpg" alt="bouldering" class="aligne-left size-small" >}}

The day unfolded with the thrill of adventure as I embraced the challenge of bouldering—an activity I hadn't experienced in a "long time". The tactile sensation of holds beneath my fingers and the rush of pushing personal limits created a symphony of exhilaration, marking a triumphant return to the climbing world.

{{< figure src="./images/my_quang.jpg" alt="My Quang" class="aligne-right size-medium" >}}

The culinary journey continued with the quest for a personal new—Quang, a dish that encapsulates the essence of Vietnamese cuisine. The flavors danced on my taste buds, a savory celebration of local culinary expertise.

Shortly after, the journey led to the Bikini Bottom bar and restaurant—a haven for both burgers and fancy drinks. The evening became a culinary adventure, with the sizzling delight of a burger paired with the artistry of creatively crafted drinks.

{{< figure src="./images/drink_fancy.jpg" alt="fancy drink" class="aligne-center size-medium" >}}

## Monday: Farewell and Culinary Surprises on the Rails
<!-- 
goodbye to joe

taking train to nha trang
once again, strange food (banh loc [hue]) from a local with linn
 -->

{{< figure src="./images/group (claire, joe, linn).jpg" alt="claire, joe, linn" class="aligne-right size-smaller" >}}

The day began with a poignant farewell to Joe, a fellow traveler whose paths had intertwined for a time. The first person I meet in Bangkok two months earlier. Goodbyes are a bittersweet part of the journey, marking the ebb and flow of friendships made on the road.

{{< figure src="./images/joe.jpg" alt="joe" class="aligne-center size-medium" >}}

The next adventure unfolded with the rhythmic clatter of the train wheels as Linn and I embarked on a journey to Nha Trang. The landscapes outside the window transformed, revealing the ever-changing canvas of Vietnam.

{{< figure src="./images/train.jpg" alt="train" class="aligne-left size-smaller" >}}

The culinary exploration continued with a twist of surprise—Banh Loc, a specialty from Hue, shared by a local. The unfamiliar flavors added a dash of mystery to the day, proving that travel is not only about the known but also the delightful surprises encountered along the way.

As the train carved through the landscapes, the day became a fusion of goodbyes, new tastes, and the steady rhythm of the rails—a poetic metaphor for the transient nature of travel.
