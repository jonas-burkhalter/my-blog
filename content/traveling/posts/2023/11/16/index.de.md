+++  
draft = false  
date = 2023-11-16T22:00:00+02:00  
title = "Hue, Hoi An, Da Nang"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Donnerstag: Ein Tag voller Wasserwege und erholsamer Schlummer
<!--  
Aufwachen im Bus  
Wasser, Wasser und noch mehr Wasser  
Hue ist überflutet  
Der Bus braucht 2 Stunden, um in die Stadt zu kommen  

Taxi, Bus, Taxi nach Hoi An nehmen  

Das erste Essen des Tages (16:00 Uhr)  

16 Stunden schlafen  
-->

{{< figure src="./images/flood_with_linn.jpg" alt="Überschwemmung" class="aligne-right size-medium" >}}

Der Tag begann mit einem verschlafenen Aufwachen im Bus, begleitet vom Anblick von Wasser, soweit das Auge reichte. Hue zeigte sich als eine überschwemmte Stadt, und die Flut änderte unsere Pläne. Ursprünglich wollten wir in Hue bleiben, doch die Herausforderungen des Hochwassers zwangen uns, unsere Route neu zu bewerten.

Trotz der Umstände dauerte es zwei Stunden, bis der Bus durch das wasserüberflutete Gelände schliesslich das Stadtzentrum erreichte. Unbeirrt setzten wir unsere Reise mit einer Kombination aus Taxis und Bussen fort, um unser neues Ziel—Hoi An—zu erreichen.

{{< figure src="./images/flood.jpg" alt="Überschwemmung" class="aligne-left size-medium" >}}

Nach der Ankunft genossen wir das erste Essen des Tages, ein spätes Mittagessen um 16:00 Uhr. Die Aromen der Mahlzeit waren ein willkommener Lohn nach einem Tag voller Hindernisse und Herausforderungen durch die Launen der Natur.

Mit den Strapazen des Tages hinter uns stellte sich eine 16-stündige Ruhepause ein, die uns in einen tiefen, erholsamen Schlaf versetzte. Der Rhythmus der Strasse und die flüchtigen Wasserblicke verblassten, ersetzt durch die Ruhe der Erholung.

## Freitag: Das Unerwartete in Hoi An annehmen
<!--  
Was tun?  
Regen, Kälte und ein schmutziger Strand  

(ganzer Tag mit Linn)  

Chillen im Café  
Interessantes Essen (Banh Dap, Cao Lau)  

Laternenbasteln (friedlich, ruhig)  

Bar (Linn, Joe, Claire) (heisse Schokolade für mich)  
-->

{{< figure src="./images/beach.jpg" alt="Strand" class="aligne-right size-medium" >}}

Der Tag begann mit der Frage: Was tun bei Regen, Kälte und einem nicht gerade einladenden Strand? Die Antwort lag in der einfachen Freude gemeinsamer Momente, als Linn und ich den Tag zusammen verbrachten.

Am Morgen suchten wir ein gemütliches Café auf. Das kulinarische Abenteuer des Tages begann mit der Entdeckung von interessanten Gerichten—Banh Dap und Cao Lau. Jeder Bissen bot einen Einblick in Hoi Ans einzigartige gastronomische Vielfalt.

{{< figure src="./images/lantern_in_progress.jpg" alt="Laterne" class="aligne-left size-small" >}}  
{{< figure src="./images/lantern_done.jpg" alt="fertige Laterne" class="aligne-right size-small" >}}  

Der Nachmittag entwickelte sich zu einem unerwarteten Ausflug in die kreative Welt des Laternenbastelns. Die Atmosphäre war friedlich, begleitet von Scherenschnitten und gelegentlichem Lachen, was einen Moment der stillen Kreativität schuf.

<div style="clear:both;"></div>

Am Abend fand der Tag seinen Höhepunkt mit einem Barbesuch gemeinsam mit Joe und Claire. Die Wärme der Freundschaft umgab uns, und eine Tasse heisse Schokolade wurde zum perfekten Begleiter in der gemütlichen Atmosphäre.

## Samstag: Kulinarische Genüsse und Abschied in Hoi An und Da Nang
<!--  
Banh Mi mit Claire, Joe, Linn und Yuho  
Yuho vor der Trennung nochmals treffen  

Grab-Fahrt von Hoi An nach Da Nang  

Abendessen mit Claire, Joe, Linn  

Drachenbrücke anschauen  

Ein paar Drinks nehmen  
-->

{{< figure src="./images/group (claire, joe, linn, yuho).jpg" alt="Claire, Joe, Linn, Yuho" class="aligne-left size-small" >}}  

Der Tag begann mit einem herrlichen Treffen, als Claire, Joe, Linn und Yuho zum Banh Mi Frühstück zusammenkamen—eine Sinfonie der Aromen, die unter Freunden geteilt wurde. Das Frühstück war nicht nur Nahrung, sondern auch ein Fest der Kameradschaft, bevor sich die Wege trennten.

Am Morgen folgte ein emotionaler Moment des Wiedersehens mit Yuho, bei dem wir die flüchtige Freude gemeinsamer Erfahrungen genossen, bevor wir uns vorübergehend verabschiedeten.

{{< figure src="./images/rice_in_pinapple.jpg" alt="Reis in Ananas" class="aligne-right size-small" >}}  

Die Reise setzte sich mit einer Grab-Fahrt von Hoi An nach Da Nang fort, während sich die Landschaften unterwegs veränderten. Die Abenteuer des Tages gingen weiter mit einem gemeinsamen Abendessen, bei dem Claire, Joe und Linn mich durch die Stadt begleiteten.

Die Drachenbrücke, ein ikonisches Wahrzeichen, erstrahlte in der Nacht. Ihr majestätisches Erscheinungsbild wurde zum Leben erweckt, als der Drache Feuer und Wasser aus seinem Maul spie. Ein bezauberndes Schauspiel, das der Nacht einen Hauch von Magie verlieh.

{{< figure src="./images/dragon.jpg" alt="Drachenbrücke" class="aligne-center size-medium" >}}  

Der Abend endete mit einem gemeinsamen Anstossen, bei dem das Klirren der Gläser nicht nur den Abschluss eines Tages markierte, sondern auch die Feier von Freundschaften auf dem Weg.
