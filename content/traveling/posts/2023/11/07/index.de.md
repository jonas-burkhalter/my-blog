+++  
draft = false  
date = 2023-11-07T22:00:00+02:00  
title = "Hanoi"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++  

## Dienstag: Ankunft in Hanoi und Streetfood-Abenteuer  
<!--  
Ankunft in Hanoi  

Streetfood, Abzocke, schlechtes Essen, zu teuer  
-->  

Der Tag begann mit Vorfreude, als wir in Hanoi ankamen, einer Stadt voller Leben und Energie. Mit Begeisterung erkundeten wir die lebhaften Strassen, bereit, in die lokale Kultur einzutauchen – vor allem durch die berühmte Streetfood-Szene.  

Doch die Erfahrung nahm eine unerwartete Wendung. Was als kulinarisches Highlight begann, verwandelte sich in eine Reihe enttäuschender Ereignisse. Wir stiessen auf Streetfood-Anbieter, deren Preise wie Abzocke wirkten, und die Gerichte entsprachen nicht den Erwartungen. Die Enttäuschung über mittelmässiges Essen zu überhöhten Preisen überschattete das kulinarische Abenteuer.  

## Mittwoch: Hanois Vielfalt entdecken  
<!--  
Spaziergang durch das alte Viertel (mit Linn und Adam)  
Eierkaffee probieren  
Banh Mi von Banh Mi 25 holen  

Die ruhige Bücherstrasse geniessen  

Gefängnis Hoa Lo besuchen  

Jasmintee mit süssem Chili und gesalzenen tropischen Früchten  

Cocktail auf der Dachterrasse  

Bún Chả  
-->  

Der Tag begann mit einem entspannten Spaziergang durch das alte Viertel, begleitet von den vertrauten Gesichtern von Linn und Adam. Wir tauchten ein in das charmante Chaos der Strassen, ein Fest für die Sinne mit unzähligen Eindrücken. Ein Highlight war der ikonische Eierkaffee, eine lokale Spezialität, die die Intensität von Kaffee mit der Cremigkeit von Ei vereinte – eine köstliche Kombination.  

{{< figure src="./images/banh_mi.jpg" alt="Banh Mi" class="aligne-left size-small" >}}  

Gestärkt durch den Kaffee setzten wir unsere kulinarische Entdeckungsreise fort und besuchten Banh Mi 25. Dort genossen wir die vielfältigen Geschmacksnuancen eines Banh Mi, eines vietnamesischen Sandwiches, das mit seinen kontrastreichen Texturen begeistert.  

{{< figure src="./images/book_street.jpg" alt="Bücherstrasse" class="aligne-right size-smaller" >}}  

Auf der Suche nach einem ruhigen Moment zog es mich in die beschauliche Bücherstrasse. Zwischen den Regalen voller Wissen fand ich Inspiration und Ruhe in dieser friedlichen Atmosphäre.  

Mein nächstes Ziel war das Hoa Lo Gefängnis, ein Ort von tiefgreifender historischer Bedeutung. Dieses Gefängnis, ursprünglich von den Franzosen gebaut, um politische Gefangene festzuhalten, wurde später zu einem Symbol des unerschütterlichen Geistes der Freiheitskämpfer. Während ich durch die Gänge ging, schienen die Mauern die Geschichten der Gefangenen zu flüstern und mir einen eindringlichen Einblick in die Vergangenheit Vietnams zu geben.  

{{< figure src="./images/prison.jpg" alt="Hoa Lo Gefängnis" class="aligne-left size-small" >}}  
{{< figure src="./images/prison_art.jpg" alt="Hoa Lo Gefängnis" class="aligne-left size-medium" >}}  
<div style="clear:both;"></div>  

{{< figure src="./images/mocktail.jpg" alt="Mocktail" class="aligne-right size-small" >}}  

Am Abend fanden Linn und ich uns auf einer Dachterrasse wieder, wo wir den Sonnenuntergang genossen. Die Stadt verwandelte sich vor unseren Augen, die Farben des Himmels spiegelten die pulsierende Energie von Hanoi wider. Ein Prost auf Cocktails und einen Sonnenuntergang, der die Stadt in magischen Farben erstrahlen liess!  

Der kulinarische Höhepunkt des Tages war eine dampfende Schüssel Bún Chả, ein vietnamesisches Gericht mit gegrilltem Schweinefleisch und Reisnudeln. Die Mischung der Aromen war ein wahres Fest für die Sinne – der perfekte Abschluss eines Tages voller Entdeckungen und Genüsse.  

## Donnerstag: Kultur und kulinarische Entdeckungen  
<!--  
Tiramisu-Kaffee und Lesen  

Ho Chi Minh Mausoleum  

Literaturtempel besuchen und lesen  

Bia Hoi Ecke  

(Bánh Đống Xu)  
-->  

Der Tag begann mit dem Duft von Tiramisu-Kaffee, einem perfekten Begleiter für einen Morgen voller Ruhe und Lektüre. Der Genuss des Kaffees und das Umblättern der Seiten verschmolzen zu einer harmonischen Symphonie.  

{{< figure src="./images/coffee_tiramisu.jpg" alt="Tiramisu-Kaffee" class="aligne-left size-small" >}}  

Anschliessend besuchte ich das Ho Chi Minh Mausoleum, einen Ort, an dem Geschichte und Ehrfurcht aufeinandertreffen. Das Mausoleum ist ein Zeugnis für das Vermächtnis von Ho Chi Minh, dessen Einfluss die Geschichte Vietnams massgeblich geprägt hat.  

{{< figure src="./images/temple_of_literature.jpg" alt="Literaturtempel" class="aligne-right size-small" >}}  

Weiter ging es zum Literaturtempel, einem Ort voller akademischer Traditionen. Zwischen den alten Steinen fand ich eine ruhige Ecke, um in ein Buch einzutauchen – eine Brücke zwischen Vergangenheit und Gegenwart.  

Später zog es mich zur lebhaften Bia Hoi Ecke, einem Treffpunkt, an dem Einheimische und Reisende das frische Fassbier geniessen. Inmitten angeregter Gespräche und fröhlichem Lachen spürte ich die ansteckende Energie dieses Ortes.  

## Freitag: Ein Wiedersehen und die Reise nach Cat Ba  
<!--  
Kurzes Wiedersehen mit Claire  
Kaffee  

Abreise aus Hanoi  
Bus nach Haiphong, dann Fähre nach Cat Ba  
-->  

Der Tag begann mit einem kurzen, aber schönen Wiedersehen mit Claire. Inmitten der geschäftigen Strassen von Hanoi teilten wir bei einer Tasse Kaffee Momente der Verbundenheit und Freude.  

Im Laufe des Tages hiess es Abschied nehmen von Hanoi. Die nächste Etappe meiner Reise führte mich mit dem Bus nach Haiphong. Von dort ging es weiter mit einer Fähre nach Cat Ba Island, einem neuen Kapitel voller Abenteuer.  
