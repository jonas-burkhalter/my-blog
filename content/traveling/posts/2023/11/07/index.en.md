+++ 
draft = false
date = 2023-11-07T22:00:00+02:00
title = "Hanoi"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Tuesday: Arrival in Hanoi and Street Food Adventures
<!-- 
arriving in hanoi

steet food, rip of, bad food, to expensive 
-->

The day unfolded with anticipation as we arrived in Hanoi, a city pulsating with life and energy. Excitement filled the air as we ventured into the bustling streets, eager to immerse ourselfs in the local culture, particularly through its renowned street food scene.

However, the experience took an unexpected turn. What initially promised to be a gastronomic delight turned into a series of unfortunate events. We encountered street food vendors whose prices seemed to be a rip-off, offering dishes that didn't live up to expectations. The disappointment of encountering subpar food at exorbitant prices lingered, casting a shadow over the culinary adventure.

## Wednesday: Exploring Hanoi's Rich Tapestry
<!-- 
walking through the old quarter (with linn and adam)
getting a egg coffee
getting a banh mi from banh mi 25

enjoying the quiet book street

prison hoa lo

Jasmin tea with sweat chilli and salted tropical fruits

roof top cocktail

Bún chả
-->

The day began with a leisurely stroll through the old quarter, accompanied by the familiar faces of Linn and Adam. We delved into the charming chaos of the streets, a sensory feast of sights, sounds, and smells. A highlight was savoring the iconic egg coffee, a local delicacy that blended the richness of coffee with the silkiness of egg, creating a delightful fusion.

{{< figure src="./images/banh_mi.jpg" alt="Banh Mi" class="aligne-left size-small" >}}

Energized by caffeine, our culinary journey continued with a visit to Banh Mi 25, where we indulged in the flavorful goodness of a banh mi, a Vietnamese sandwich that captivates the taste buds with its diverse textures.

{{< figure src="./images/book_street.jpg" alt="book street" class="aligne-right size-smaller" >}}

Seeking a moment of quiet solitude, I ventured to the serene book street. Amidst the shelves of knowledge, I found solace and inspiration, enjoying the calm atmosphere in my own company.

In continuing my exploration, I visited Hoa Lo Prison, a site that carries a profound historical weight, serving as a poignant reminder of the city's tumultuous past. As I walked through the corridors, the walls seemed to echo with the stories of the prisoners, providing a somber yet essential glimpse into Vietnam's relentless struggle for independence. The prison, initially built by the French to detain political prisoners, later became a symbol of the indomitable spirit of those who fought for freedom. The experience was a stark immersion into the historical narrative that has shaped the resilience and determination of the Vietnamese people.

{{< figure src="./images/prison.jpg" alt="Hoa Lo Prison" class="aligne-left size-small" >}}
{{< figure src="./images/prison_art.jpg" alt="Hoa Lo Prison" class="aligne-left size-medium" >}}
<div style="clear:both;"></div>

{{< figure src="./images/mocktail.jpg" alt="mocktail" class="aligne-right size-small" >}}

As the sun began its descent, Linn and I found ourselves on a rooftop, enveloped in the warm hues of the sunset. The cityscape transformed, casting long shadows over the bustling streets of Hanoi. With each passing moment, the vibrant colors of the sky mirrored the lively energy of the city, creating a picturesque backdrop to our evening. Cheers to cocktails and a sunset that painted the town in enchanting hues!

The culinary adventure reached its zenith with a steaming bowl of Bún chả, a Vietnamese dish comprising grilled pork and vermicelli noodles. The mingling of flavors created a symphony in each bite, a fitting crescendo to a day filled with exploration and gastronomic delights.

## Thursday: A Day of Culture and Culinary Discoveries
<!-- 
tiramisu coffee
and reading 

ho chi minh mausoleum

temple of literatur
reading the book

bia hoi corner

(banh dong xu)
-->

The day unfolded with the rich aroma of tiramisu coffee, a perfect companion for a morning of quiet reading. The sip of coffee and the turn of pages created a harmonious symphony, blending the flavors of caffeine with the nourishment of literature.

{{< figure src="./images/coffee_tiramisu.jpg" alt="tiramisu coffee" class="aligne-left size-small" >}}

The journey then led me to the solemn grounds of the Ho Chi Minh Mausoleum, where history and reverence converged. The mausoleum stood as a testament to the legacy of Ho Chi Minh, a figure whose influence shaped the course of Vietnam's history.

{{< figure src="./images/temple_of_literature.jpg" alt="Temple of Literature" class="aligne-right size-small" >}}

The exploration continued at the Temple of Literature, a place steeped in centuries of academic tradition. Among the ancient stones, I found a quiet corner to immerse myself in a book, creating a bridge between past and present.

As the day progressed, I found myself drawn to the lively Bia Hoi Corner, a hub of social activity where locals and travelers alike gathered to enjoy the unique charm of fresh draft beer. Accompanied by fellow explorers, the animated conversations and laughter infused the air with a contagious energy.

## Friday: A Brief Reunion and the Journey to Cat Ba
<!-- 
short reunion with claire 
for a coffee

leaving hanoi 
bus to haiphong and then the ferry to cat ba
-->

The day began with a sweet reunion, as I met up briefly with Claire for a coffee. In the midst of the bustling Hanoi streets, we shared a moment of connection and laughter, savoring the joy that reunions bring.

As the day unfolded, it was time to bid farewell to Hanoi. The next leg of my adventure took me on a bus to Haiphong, from Haiphong, the journey continued with a ferry ride to Cat Ba Island. 
