+++ 
draft = false
date = 2023-11-23T22:00:00+02:00
title = "Da Lat"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Thursday: Culinary Adventures and a Journey to Da Lat
<!-- 
getting breakfast with linn and claire

taking the bus from Nha trang to Da Lat
 -->

The day began with the tantalizing aroma of breakfast, shared in the company of Linn and Claire. Our morning quest for flavors led us to delightful culinary discoveries, a harmonious start to the day.

Buoyed by the satisfaction of a shared meal, we embarked on the next leg of our adventure—the bus journey from Nha Trang to Da Lat. The landscape unfolded beyond the windows, a tapestry of changing scenes as we traversed the winding roads toward our destination.

{{< figure src="./images/dalat.jpg" alt="Da Lat" class="aligne-center size-medium" >}}

## Friday: Exploring Da Lat's Countryside by Bike
<!--
countryside bike tour with claire and linn 
we are in the back and have drivers
- Coffee Plantation
- Tea Plantation
- Linh An Pagoda and elephant waterfall
- K'Ho village with lunch
- Rice Wine making
- Weasle Coffe
- Flower Farm

Family Dinner

-->

{{< figure src="./images/driving_country_side.jpg" alt="driving" class="aligne-left size-smaller" >}}

The day unfolded with the promise of adventure as Claire, Linn, and I embarked on a countryside bike tour—a journey through the picturesque landscapes surrounding Da Lat. Seated comfortably in the back, our drivers guided us through a tapestry of breathtaking scenes.

{{< figure src="./images/coffee_plantation.jpg" alt="coffee_plantation" class="aligne-right size-small" >}}
Our first stop was a Coffee Plantation. From there, we continued to a Tea Plantation, savoring the tranquility of the lush, rolling hills.

{{< figure src="./images/tea_plantation.jpg" alt="tea_plantation" class="aligne-center size-medium" >}}

The Linh An Pagoda and the majestic Elephant Waterfall revealed themselves as gems along our route. The rhythmic sound of cascading water provided a soothing backdrop to the spiritual ambiance of the pagoda.

{{< figure src="./images/kho_village.jpg" alt="kho_village" class="aligne-center size-large" >}}

{{< figure src="./images/lunch.jpg" alt="lunch" class="aligne-right size-smaller" >}}
Our journey continued to a K'Ho village, where a delightful lunch awaited us. Immersed in the rich cultural tapestry of the village, we experienced a genuine connection with the local community.

{{< figure src="./images/view.jpg" alt="view" class="aligne-center size-medium" >}}

{{< figure src="./images/coffee.jpg" alt="coffee" class="aligne-right size-smaller" >}}

The exploration delved into the art of Rice Wine making, offering insights into traditional craftsmanship. The adventure continued with a visit to a Weasel Coffee plantation—an unusual and rather ordinary aromatic experience.

{{< figure src="./images/flower.jpg" alt="flower" class="aligne-left size-smaller" >}}

The final stop brought us to a Flower Farm, where vibrant blossoms painted the landscape in a riot of colors. As the day transitioned into evening, we gathered for a warm and inviting Family Dinner at the Hostel—a fitting conclusion to a day filled with shared discoveries and cultural immersion.

## Saturday: Adrenaline and Tranquility in Da Lat
<!-- 
canyoning tour
- abseiling
- water sliding (down a waterfall)
- floating down the river
- trekking in the river
- abseiling waterfalls
- trekking in the "jungle"

flower park

great fried rice for dinner
 -->

{{< figure src="./images/zip_line.jpg" alt="zip line" class="aligne-right size-small" >}}

The day unfolded with a surge of adrenaline as I embraced the thrills of a canyoning tour—a cascade of daring activities set against the backdrop of Da Lat's rugged terrain.

{{< figure src="./images/abseil.jpg" alt="abseil" class="aligne-left size-small" >}}

Our adventure included heart-pounding moments of abseiling down sheer cliffs and the exhilarating experience of water sliding down a waterfall. Floating down the river, trekking in the water, and abseiling waterfalls added layers of excitement to our journey.

Navigating the challenging terrain, we found ourselves trekking in the lush embrace of the "jungle," immersing ourselves in the natural wonders that Da Lat had to offer.

After the tour, I shifted gears from the exhilarating canyoning adventure to the tranquil beauty of a Flower Park. Vibrant blooms painted the landscape, offering a serene contrast to the morning's adrenaline-fueled escapades.

{{< figure src="./images/flower_park.jpg" alt="flower_park" class="aligne-center size-small" >}}


The day concluded on a flavorful note with a dinner featuring great fried rice—a satisfying end to a day that oscillated between heart-pounding thrills and moments of serene beauty.

## Sunday: A Scenic Journey from Da Lat to Mui Ne
<!-- 
bike tour with claire and linn 
we are in the back and have drivers
from dalat to muine
- saying goodbye to dalat from a viewpoint
- paradise lake
- mushrom farm
- rice paper making
- Pongour Waterfall
- coffee at a viewpoint
- lunch
- dragonfruit farm
- white sand dune

get food and a couple of beers and drinks
 -->

The day unfolded with the hum of engines as Claire, Linn, and I embarked on a bike tour, our trusty drivers navigating the scenic route from Da Lat to Mui Ne.

{{< figure src="./images/rice_paper.jpg" alt="rice paper" class="aligne-left size-medium" >}}

Our journey began with a heartfelt goodbye to Da Lat from a captivating viewpoint—a moment of reflection as we bid adieu to the misty mountain town.

The adventure continued with a visit to Paradise Lake, where the tranquil waters mirrored the serenity of the surrounding landscape. A Mushroom Farm unfolded as a testament to the agricultural richness of the region, and a demonstration of Rice Paper making offered insights into local craftsmanship.

{{< figure src="./images/waterfall.jpg" alt="waterfall" class="aligne-center size-large" >}}

Pongour Waterfall, with its cascading beauty, became a highlight of our journey—a natural spectacle that left us in awe. Pausing for coffee at a viewpoint, we absorbed the breathtaking vistas that unfolded before us.

{{< figure src="./images/viewpoint.jpg" alt="viewpoint" class="aligne-right size-small" >}}

A hearty lunch marked the midpoint of our day, fueling us for the exploration that lay ahead. Our journey continued with a visit to a Dragonfruit Farm, an immersion into the vibrant hues of this exotic fruit.

{{< figure src="./images/dune_red.jpg" alt="red sanddune" class="aligne-left size-smaller" >}}

The landscape transformed as we reached the White and Red Sand Dunes—a desert-like terrain that painted a stark contrast to the lush scenes of Da Lat. The day's adventure concluded with a leisurely evening of food, beers, and drinks—a fitting celebration of the diverse landscapes and experiences we encountered on our journey from Da Lat to Mui Ne.

{{< figure src="./images/driving.jpg" alt="driving" class="aligne-center size-medium" >}}
