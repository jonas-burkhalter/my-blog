+++  
draft = false  
date = 2023-11-23T22:00:00+02:00  
title = "Da Lat"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Donnerstag: Kulinarische Abenteuer und eine Reise nach Da Lat  
<!--  
Frühstück mit Linn und Claire  

Busfahrt von Nha Trang nach Da Lat  
-->  

Der Tag begann mit dem verlockenden Aroma eines Frühstücks, das ich zusammen mit Linn und Claire genoss. Unsere morgendliche Suche nach köstlichen Aromen führte uns zu einer harmonischen kulinarischen Entdeckung—ein perfekter Start in den Tag.  

Gestärkt durch das gemeinsame Essen machten wir uns auf den nächsten Abschnitt unserer Reise: die Busfahrt von Nha Trang nach Da Lat. Vor den Fenstern entfaltete sich eine wechselnde Landschaft, ein Teppich aus Szenen, der sich entlang der kurvigen Strassen zu unserem Ziel erstreckte.  

{{< figure src="./images/dalat.jpg" alt="Da Lat" class="aligne-center size-medium" >}}  

## Freitag: Da Lats Umland mit dem Fahrrad erkunden  
<!--  
Fahrradtour durch das Umland mit Claire und Linn  
Wir sitzen hinten, die Fahrer fahren.  
- Kaffeeplantage  
- Teeplantage  
- Linh-An-Pagode und Elefantenwasserfall  
- K’Ho-Dorf mit Mittagessen  
- Reisschnapsherstellung  
- Wieselkaffee  
- Blumenfarm  

Familienabendessen  
-->  

{{< figure src="./images/driving_country_side.jpg" alt="Fahren durchs Umland" class="aligne-left size-smaller" >}}  

Der Tag versprach Abenteuer, als Claire, Linn und ich eine Fahrradtour durch das malerische Umland von Da Lat unternahmen. Bequem hinten sitzend, führten unsere Fahrer uns durch eine atemberaubende Kulisse.  

{{< figure src="./images/coffee_plantation.jpg" alt="Kaffeeplantage" class="aligne-right size-small" >}}  

Unser erster Halt war eine Kaffeeplantage. Von dort aus ging es weiter zu einer Teeplantage, wo wir die Ruhe der üppigen, sanft geschwungenen Hügel genossen.  

{{< figure src="./images/tea_plantation.jpg" alt="Teeplantage" class="aligne-center size-medium" >}}  

Die Linh-An-Pagode und der majestätische Elefantenwasserfall entpuppten sich als wahre Juwelen entlang unserer Route. Das rhythmische Rauschen des Wassers bildete eine beruhigende Kulisse für die spirituelle Atmosphäre der Pagode.  

{{< figure src="./images/kho_village.jpg" alt="K'Ho-Dorf" class="aligne-center size-large" >}}  

{{< figure src="./images/lunch.jpg" alt="Mittagessen" class="aligne-right size-smaller" >}}  

Unsere Reise führte uns weiter in ein K’Ho-Dorf, wo ein köstliches Mittagessen auf uns wartete. Eingetaucht in die reiche kulturelle Vielfalt des Dorfes erlebten wir eine echte Verbindung zur lokalen Gemeinschaft.  

{{< figure src="./images/view.jpg" alt="Aussicht" class="aligne-center size-medium" >}}  

{{< figure src="./images/coffee.jpg" alt="Kaffee" class="aligne-right size-smaller" >}}  

Wir tauchten in die Kunst der Reisschnapsherstellung ein und bekamen Einblicke in traditionelles Handwerk. Danach besuchten wir eine Wieselkaffee-Plantage—eine ungewöhnliche, aber eher unspektakuläre aromatische Erfahrung.  

{{< figure src="./images/flower.jpg" alt="Blumenfarm" class="aligne-left size-smaller" >}}  

Unser letzter Halt führte uns zu einer Blumenfarm, wo leuchtende Blüten die Landschaft in ein Farbenmeer tauchten. Als der Tag in den Abend überging, versammelten wir uns zu einem gemütlichen Familienabendessen im Hostel—ein passender Abschluss für einen Tag voller gemeinsamer Entdeckungen und kultureller Erlebnisse.  

## Samstag: Adrenalin und Ruhe in Da Lat  
<!--  
Canyoning-Tour  
- Abseilen  
- Wasserrutschen (an einem Wasserfall)  
- Flussabwärts treiben  
- Trekking im Fluss  
- Abseilen an Wasserfällen  
- Trekking im „Dschungel“  

Blumenpark  

Leckeres gebratenes Reisgericht zum Abendessen  
-->  

{{< figure src="./images/zip_line.jpg" alt="Seilrutsche" class="aligne-right size-small" >}}  

Der Tag begann mit einem Adrenalinstoss, als ich mich den aufregenden Herausforderungen einer Canyoning-Tour stellte—eine Abfolge von waghalsigen Aktivitäten vor der Kulisse von Da Lats rauer Natur.  

{{< figure src="./images/abseil.jpg" alt="Abseilen" class="aligne-left size-small" >}}  

Unsere Abenteuer beinhalteten spannende Momente wie das Abseilen von steilen Klippen und das aufregende Gleiten über einen Wasserfall. Das Treiben auf dem Fluss, Trekking im Wasser und Abseilen an Wasserfällen bereicherten unsere Erlebnisse mit zusätzlichen Spannungshöhepunkten.  

Nach der Tour wechselte ich von den aufregenden Abenteuern des Canyonings zur ruhigen Schönheit eines Blumenparks. Lebendige Blüten tauchten die Landschaft in Farben, die einen friedlichen Kontrast zu den Adrenalinschüben des Vormittags bildeten.  

{{< figure src="./images/flower_park.jpg" alt="Blumenpark" class="aligne-center size-small" >}}  

Der Tag endete mit einem geschmackvollen Abendessen mit gebratenem Reis—ein zufriedenstellender Abschluss für einen Tag, der zwischen Nervenkitzel und Gelassenheit pendelte.  

## Sonntag: Eine malerische Reise von Da Lat nach Mui Ne  
<!--  
Fahrradtour mit Claire und Linn  
Wir sitzen hinten, die Fahrer fahren.  
Von Da Lat nach Mui Ne  
- Abschied von Da Lat von einem Aussichtspunkt aus  
- Paradiessee  
- Pilzfarm  
- Reispapierherstellung  
- Pongour-Wasserfall  
- Kaffee mit Aussicht  
- Mittagessen  
- Drachenfruchtfarm  
- Weisse Sanddünen  

Abendessen und ein paar Biere und Getränke  
-->  

Der Tag begann mit dem Summen der Motoren, als Claire, Linn und ich zu einer Fahrradtour aufbrachen, bei der unsere Fahrer uns entlang der malerischen Route von Da Lat nach Mui Ne chauffierten.  

{{< figure src="./images/rice_paper.jpg" alt="Reispapierherstellung" class="aligne-left size-medium" >}}  

Unsere Reise begann mit einem herzlichen Abschied von Da Lat von einem Aussichtspunkt aus—ein Moment der Reflexion, als wir der nebligen Bergstadt Lebewohl sagten.  

Die Reise führte uns weiter zum Paradiessee, wo die stillen Wasser die Ruhe der Umgebung widerspiegelten. Eine Pilzfarm offenbarte den landwirtschaftlichen Reichtum der Region, und eine Demonstration der Reispapierherstellung bot uns Einblicke in lokales Handwerk.  

{{< figure src="./images/waterfall.jpg" alt="Wasserfall" class="aligne-center size-large" >}}  

Der Pongour-Wasserfall, mit seiner eindrucksvollen Schönheit, wurde zum Höhepunkt unserer Reise—ein Naturschauspiel, das uns in Staunen versetzte.  

{{< figure src="./images/viewpoint.jpg" alt="Aussichtspunkt" class="aligne-right size-small" >}}  

Ein herzhaftes Mittagessen markierte die Mitte unseres Tages und gab uns Kraft für die weiteren Entdeckungen.  

Unsere Reise setzte sich fort mit einem Besuch einer Drachenfruchtfarm, deren lebendige Farben die Landschaft bereicherten.  

{{< figure src="./images/dune_red.jpg" alt="Rote Sanddünen" class="aligne-left size-smaller" >}}  

Die Landschaft verwandelte sich, als wir die weissen und roten Sanddünen erreichten—ein wüstenähnliches Terrain, das in scharfem Kontrast zu den grünen Szenen von Da Lat stand. Der Tag endete mit einem entspannten Abend voller Essen, Bier und Getränke—eine passende Feier für die vielfältigen Landschaften und Erfahrungen unserer Reise von Da Lat nach Mui Ne.  

{{< figure src="./images/driving.jpg" alt="Fahrt" class="aligne-center size-medium" >}}  
