+++ 
draft = false
date = 2023-11-03T22:00:00+02:00
title = "Sapa"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Friday: Trekking to Ta Van
<!-- 
Waking from Sapa to Ta Van to the hmong sister homestay hostel (3h 12:00-15:00)

enjoying a lazy afternoon 
blogging
-->
{{< figure src="./images/trek_to_ta_van.jpg" alt="rice fields" class="aligne-right size-medium" >}}

The day began with an invigorating trek from Sapa to Ta Van, leading me to the welcoming embrace of the Hmong Sister Homestay Hostel. The journey was a testament to the natural beauty that envelopes this region, with every step offering a new vista to behold. After three hours of immersive exploration, I arrived at the hostel, greeted by warm smiles and a sense of tranquility.

The afternoon unfolded in a blissful haze of leisure. I reveled in the luxury of simply being, allowing the surrounding mountains and serene atmosphere to wash over me. 

## Saturday: Embracing the Art of Doing Nothing
<!-- 
enjoying a lazy day 
blogging, reading, enjoying doing bare minimum
-->
{{< figure src="./images/hammock.jpg" alt="hammock" class="aligne-left size-medium" >}}

Today was a day dedicated to the exquisite art of doing nothing. I found myself cocooned in the comforting embrace of a hammock, surrounded by the gentle whispers of nature. The world seemed to slow down, allowing me to revel in the simplicity of the moment.

Blogging and reading became my companions, transporting me to different worlds and perspectives. The written word has a unique ability to bridge distances and ignite imagination.

## Sunday: The Bamboo Trek
<!-- 
bamboo trek
folling the trek thourgh the bamboo forrest to the waterfall
deciding to extend further and go up the hill
finding another waterfall
follow the river back down, off road and with no plan/map

meeting linn and adam again (kind of planned)
-->
Initially, I traced the bamboo trek, winding my way through the bamboo forest, alongside picturesque rice fields, and in the company of water buffaloes. This trail guided me to a popular yet tourist-frequented waterfall. Captivated by the beauty but desiring a more secluded experience, I chose to forge ahead and ascended a path uphill. The journey led me to the discovery of a less-known waterfall, offering a quieter and more intimate connection with nature. After relishing the serenity of this hidden gem, I descended by walking down the river, navigating the terrain by climbing and leaping from boulder to boulder. The entire experience was a harmonious blend of natural wonders and the joy of uncharted exploration.

{{< figure src="./images/bamboo_trek.jpg" alt="bamboo forrest" class="aligne-left size-small" >}}
{{< figure src="./images/rice_fields_with_buffaloes.jpg" alt="rice fields" class="aligne-left size-medium" >}}
{{< figure src="./images/river_trek.jpg" alt="river trek" class="aligne-left size-small" >}}
<div style="clear:both;"></div>

In a delightful twist of fate, I had the joy of reuniting with dear friends, Linn and [Adam](https://www.instagram.com/adamkadishman). The serendipity of our meeting added an extra layer of magic to the day, as we shared stories and laughter against the backdrop of this stunning landscape.

## Monday: A Day of Rest
<!-- 
chilling 

feeling not to well
-->

Today was a day dedicated to rest and self-care. I embraced the opportunity to simply chill, allowing my body and mind to find solace in the stillness. The surrounding tranquility provided a soothing balm for the soul.

However, it was also a day where I didn't feel quite myself. Sometimes, travel can take a toll on the body.

## Tuesday: Rainy Day
<!-- 
rainy day, nice but also a bit annoying because we wanted to go

booking the bus and hostel for later that day

blogging again

bus from sapa to hanoi 
-->

{{< figure src="./images/rain.jpg" alt="rain" class="aligne-left size-medium" >}}

Today dawned with a heavy rain, casting a soothing ambiance over the surroundings. While the raindrops were a welcome respite for the earth, they also brought a touch of inconvenience, as our initial plans were met with a wet embrace.

Undeterred, we used this interlude to take care of practicalities. We secured bookings for the bus and hostel, ensuring that the journey ahead would be smooth and seamless.

Once again, I turned to my trusty keyboard, finding solace in the act of blogging. 

As the day unfolded, it was time to bid farewell to Sapa. The bus ride to Hanoi promised a new chapter of experiences and discoveries. 
