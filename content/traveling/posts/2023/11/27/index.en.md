+++ 
draft = false
date = 2023-11-27T22:00:00+02:00
title = "Mui Ne"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Monday: Beachside Bliss in Mui Ne
<!-- 
relaxing on the "beach"
blogging
mocktails

 -->

{{< figure src="./images/beach.jpg" alt="beach" class="aligne-right size-smaller" >}}

The day unfolded in Mui Ne, a coastal haven that beckoned with the promise of relaxation. Our time was spent lounging on the beach, a tranquil retreat where the rhythmic sounds of the waves provided the soundtrack to our day.

{{< figure src="./images/mocktail.jpg" alt="mocktail" class="aligne-left size-smaller" >}}

Embracing the laid-back atmosphere, I delved into the world of blogging, capturing the essence of our adventures and the diverse landscapes we had encountered on our journey. The keyboard became a vessel for the stories that unfolded, a reflection of the experiences etched into each passing day.

The colors of the sunset painted the sky in hues of orange and pink, creating a picturesque backdrop to our moments of leisure.

{{< figure src="./images/sunset.jpg" alt="sunset" class="aligne-center size-medium" >}}

As the sun dipped below the horizon, we transitioned into a delightful evening of mocktails—refreshing concoctions that mirrored the easygoing vibe of Mui Ne. 

## Tuesday: Bittersweet Farewells and Seaside Contemplation
<!-- 
say goodbye to Linn

chilling at the pool and sea
planning the next few days
blogging

 -->

The day dawned with a bittersweet tinge as we bid farewell to Linn—a departure that left a void in our travel companionship. The echoes of shared adventures lingered, and as she embarked on a new journey, our paths diverged momentarily.

**Goodbye Linn 😞😢**

{{< figure src="./images/relaxing.jpg" alt="relaxing" class="aligne-right size-small" >}}
Seeking solace in the tranquility of Mui Ne, the day unfolded with moments of reflection. Chilling at the pool and by the sea became a meditative exercise—a chance to absorb the soothing symphony of waves and find serenity in the azure expanse.

The afternoon was spent blogging and planning the next few days, the horizon of possibilities stretching before us. The meticulous detailing of our itinerary became a roadmap for future adventures, each destination a potential canvas for new memories.

In an unexpected turn, the evening took a lively twist. Competitive rounds of pool against Claire infused a spirit of friendly rivalry, adding a delightful element of fun to the day's contemplative atmosphere.

{{< figure src="./images/pool.jpg" alt="pool" class="aligne-center size-medium" >}}
