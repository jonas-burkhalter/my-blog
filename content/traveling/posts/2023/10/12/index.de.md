+++ 
draft = false
date = 2023-10-12T22:00:00+02:00
title = "Chiang Mai"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Donnerstag: Von Phitsanulok nach Chiang Mai

Am frühen Morgen um 9:00 Uhr machte sich unser zuverlässiger Bus auf den Weg und trug uns auf den nächsten Abschnitt unserer Reise. Die Fahrt von Phitsanulok nach Chiang Mai dauerte etwa sieben Stunden. Als wir am späten Nachmittag im Hostel ankamen, verspürten wir ein starkes Hungergefühl und freuten uns auf ein herzhaftes Abendessen.

{{< figure src="./images/nigth_market.jpg" alt="Night market" class="aligne-right size-medium" >}}

Am Abend brachte uns die Aussicht auf ein Wiedersehen zusammen. Freunde aus Bangkok, Mitreisende, die uns ans Herz gewachsen waren, warteten auf dem lebhaften Nachtmarkt. Dies war kein gewöhnlicher Markt; er war ein lebendiges Gewebe aus Leben, mit Ständen, die Schätze aus nah und fern präsentierten. Live-Musik erfüllte die Luft und schuf eine Symphonie, die mit dem Puls von Chiang Mai in Einklang stand. Zwischen Lachen und der Melodie der Nacht versammelten wir uns und webten neue Erinnerungen in das Gewebe unserer gemeinsamen Abenteuer.

## Freitag: Tempel, Ruhe und kulinarische Erlebnisse

### Eine Wanderung zur Ruhe

{{< figure src="./images/hiking.jpg" alt="Wandern" class="aligne-left size-medium" >}}

Als die Sonne über Chiang Mai aufging, begaben wir uns auf eine Reise, die sowohl körperliche Anstrengung als auch spirituelle Erneuerung versprach. Der Weg führte uns auf den verehrten Monk Trail, der uns zum bezaubernden Wat Phra That Doi Suthep führte.

Die Wanderung war mehr als nur eine körperliche Herausforderung; sie war eine Übung in Achtsamkeit, jeder Schritt eine bewusste Verbindung zwischen Körper und Erde. Die grünen Umgebungen boten einen beruhigenden Hintergrund, ein lebendiges Zeugnis der natürlichen Schönheit Nordthailands.

{{< figure src="./images/chiang_mai.jpg" alt="Chiang Mai" class="aligne-right size-medium" >}}

An jeder Ecke des Weges wurden uns Schätze offenbart, von versteckten Lichtungen mit Panoramablick bis zu gelegentlichen Begegnungen mit anderen Suchenden. Der Duft alter Bäume vermischte sich mit dem erdigen Aroma von Blättern unter unseren Füssen und schuf eine sinnliche Symphonie, die uns begleitete.

Als wir uns dem Tempel näherten, stachen seine goldenen Türme in den Himmel, ein atemberaubender Anblick, eingebettet in den üppigen Wald. Wat Phra That Doi Suthep stand als Zeugnis jahrhundertelanger Hingabe, seine kunstvollen Details und lebendigen Farben spiegelten das reiche buddhistische Erbe wider.

{{< figure src="./images/wat_phra_that_doi_suthep.jpg" alt="Wat Phra That Doi Suthep" class="aligne-left size-medium" >}}

Nachdem wir den Tempel erreicht hatten, wurden wir von einer Ruhe empfangen, die die Luft durchdrang. Mönche in safranroben bewegten sich anmutig durch ihre Rituale, ihre Gesänge vereinten sich mit dem Rascheln der Blätter im sanften Wind.

Im Tempelinneren erhielten wir den buddhistischen Segen – eine Erfahrung, die auf einer tief spirituellen Ebene resonierte. Die alten Rituale trugen ein starkes Gefühl der Verbundenheit zu etwas Grösserem in sich, ein Faden, der durch Generationen von Suchenden zog.

In den ruhigen Ecken des Tempelgeländes setzten wir uns zur Meditation. Die Stille umhüllte uns und bot ein Zufluchtsort für innere Reflexion. Mit jedem Atemzug fühlten wir uns geerdet, eine Erinnerung daran, dass selbst in der geschäftigen Welt Momente der Ruhe gefunden werden können.

### Kulinarische Genüsse auf der Smile Organic Farm

{{< figure src="./images/cooking_class_garden.jpg" alt="Kochkurs" class="aligne-left size-medium" >}}

Als die Nachmittagssonne Chiang Mai in warmes Licht tauchte, tauschten wir die Ruhe des Tempels gegen die lebendige Energie der Smile Organic Farm. Der erste Halt war ein lokaler Markt, wo wir in das Herz der kommenden Kreationen eintauchten. Hier, zwischen den geschäftigen Ständen, lernten wir die vielfältigen Zutaten kennen, die bald in unseren Gerichten lebendig werden würden.

Vom Markt aus machten wir uns auf den Weg zur Farm, wo die Luft von der duftenden Symphonie aus Kräutern und Gewürzen erfüllt war. Es war eine sinnliche Reise, die uns half, unser Verständnis der Aromen zu vertiefen, die die thailändische Küche ausmachen. Unter der Anleitung unserer erfahrenen Lehrerin Nina begaben wir uns auf eine Reise der kulinarischen Entdeckung.

{{< figure src="./images/cooking_class_curry.jpg" alt="Kochkurs" class="aligne-right size-medium" >}}

Die Küche auf der Smile Organic Farm wurde zu einer Leinwand aus Aromen und Düften, ein Zeugnis der Vielfalt der thailändischen Kochtraditionen. Mit frischen Zutaten in der Hand, die entweder vom geschäftigen Markt stammten oder direkt von der Erde unter unseren Füssen gepflückt wurden, bereiteten wir unser erstes Gericht zu: Frühlingsrollen und Pad Thai.

Vor unserer zweiten Runde kulinarischer Kreationen tauchten wir in die Alchemie ein, unsere eigene Currypaste zu kreieren. Mit dieser aromatischen Basis fertig, machten wir uns daran, Khao Soi und eine duftende heisse Suppe zuzubereiten. Die Küche war lebendig, das Zischen der Pfannen und das betörende Aroma der köchelnden Gewürze erfüllten den Raum. Das Ergebnis war ein Fest für die Sinne und die Seele.

{{< figure src="./images/cooking_class.jpg" alt="Kochkurs" class="aligne-left size-medium" >}}

Als der Tag zu Ende ging, verliessen wir die Smile Organic Farm mit Herzen voller Dankbarkeit und Mägen voller köstlicher Erinnerungen. Es war ein kulinarisches Abenteuer, das nicht nur unsere Geschmacksknospen verwöhnt hatte, sondern auch unser Verständnis für die reiche Vielfalt der thailändischen Aromen vertieft hatte. Chiang Mai hatte uns erneut einen Einblick in das Herz der thailändischen Kultur gewährt und einen unauslöschlichen Eindruck auf unserer Reise hinterlassen.

<div style="clear:both;"></div>
{{< figure src="./images/cooking_class_group.jpg" alt="Kochkurs" class="aligne-center size-larger" >}}