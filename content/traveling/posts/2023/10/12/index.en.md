+++ 
draft = false
date = 2023-10-12T22:00:00+02:00
title = "Chiang mai"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Thursday: From Phitsanulok to Chiang Mai
<!-- 
travaling again
morning at 9 the bus took of
wiht the bus from Phitsanulok to chiang mai
for about 7h 
every ride takes so long

arriving in the hostel 
in the afternoon (16:30)
hungry

go for a good dinner

in the evening 
meeting up with friends from bangkok 
at the huge night market, with live music and stands all over the place
-->

At the brisk hour of 09:00, our trusty bus set forth, carrying us on the next leg of our journey. The journey from Phitsanulok to Chiang Mai spanned about seven hours. Arriving at the hostel in the late afternoon, there was a gnawing hunger, a hearty dinner was in order.

{{< figure src="./images/nigth_market.jpg" alt="Night market" class="aligne-right size-medium" >}}

As the evening unfolded, it brought with it the promise of reconnection. Friends from Bangkok, fellow travelers who had become kindred spirits, awaited at the bustling night market. This was no ordinary market; it was a vibrant tapestry of life, with stands adorned with treasures from near and far. Live music filled the air, creating a symphony that resonated with the beating heart of Chiang Mai. Amidst laughter and the melody of the night, we gathered, weaving new memories into the fabric of our shared adventures. 

## Friday: Temples, Tranquility, and Culinary Adventures

<!-- 
hike up the monk trail to the temple (Wat Phra That Doi Suthep)
nice excercice
beautiful temple in the middel
got buddist blessing
did some meditation 

cooking class
smile organic farm
fresh ingrediance from the market or from the farm
cooked pad thai, khao soi, hot soup and selfmade curry paste
 -->

### A Hike to Serenity

{{< figure src="./images/hiking.jpg" alt="Hiking" class="aligne-left size-medium" >}}

As the sun began its ascent over Chiang Mai, we embarked on a journey that promised both physical exertion and spiritual rejuvenation. The path ahead was none other than the revered Monk Trail, leading us to the enchanting Wat Phra That Doi Suthep.

The hike itself was more than a physical challenge; it was an exercise in mindfulness, each step a conscious connection between body and earth. The verdant surroundings provided a soothing backdrop, a living testament to the natural beauty of Northern Thailand.

{{< figure src="./images/chiang_mai.jpg" alt="Chiang mai" class="aligne-right size-medium" >}}

At every turn, the trail revealed its treasures, from hidden clearings offering panoramic views to the occasional encounter with fellow seekers. The scent of ancient trees mingled with the earthy aroma of leaves underfoot, creating a sensory symphony that accompanied us along the way.

As we approached the temple, its golden spires pierced the sky, a breathtaking sight nestled amidst the lush forest. Wat Phra That Doi Suthep stood as a testament to centuries of devotion, its intricate details and vibrant colors a reflection of the rich Buddhist heritage.

{{< figure src="./images/wat_phra_that_doi_suthep.jpg" alt="Wat Phra That Doi Suthep" class="aligne-left size-medium" >}}

Upon reaching the temple, we were met with a sense of serenity that permeated the air. Monks in saffron robes moved gracefully through their rituals, their chants merging with the rustle of leaves in the gentle breeze.

Inside the temple, we received the Buddhist blessing—an experience that resonated on a deeply spiritual level. The ancient rituals carried a profound sense of connection to something greater, a thread that wove through generations of seekers.

In the quiet corners of the temple grounds, we settled into meditation. The stillness enveloped us, offering a sanctuary for inner reflection. With each breath, we felt a sense of grounding, a reminder that even amidst the bustling world, moments of tranquility could be found.

### Culinary Delights at Smile Organic Farm

{{< figure src="./images/cooking_class_garden.jpg" alt="cooking class" class="aligne-left size-medium" >}}

As the afternoon sun bathed Chiang Mai in a warm glow, we traded the serenity of the temple for the vibrant energy of Smile Organic Farm. The first stop was a local market, where we delved into the heart of the evening's creations. Here, amidst the bustling stalls, we got to know the vibrant array of ingredients that would soon come to life in our dishes.

From the market, we made our way to the farm, where the air was alive with the fragrant symphony of herbs and spices. It was a sensory immersion, a chance to deepen our understanding of the flavors that define Thai cuisine. Under the guidance of our skilled instructor, Nina, we embarked on a journey of culinary discovery.

{{< figure src="./images/cooking_class_curry.jpg" alt="cooking class" class="aligne-right size-medium" >}}

The kitchen at Smile Organic Farm became a canvas of flavors and aromas, a testament to the richness of Thai culinary traditions. With fresh ingredients in hand, sourced either from the bustling market or plucked from the very earth beneath our feet, we crafted our first meal—Spring Rolls and Pad Thai. 

Before our second round of culinary creations, we delved into the alchemy of crafting our own curry paste. With this aromatic base ready, we set to work creating Khao Soi and a fragrant hot soup. The kitchen was alive with the sizzle of pans and the heady aroma of simmering spices. The result was a feast for both the senses and the soul.

{{< figure src="./images/cooking_class.jpg" alt="cooking class" class="aligne-left size-medium" >}}

As the day wound to a close, we left Smile Organic Farm with hearts full of gratitude and stomachs brimming with delicious memories. It was a culinary adventure that had not only tantalized our taste buds but also deepened our appreciation for the rich tapestry of Thai flavors. Chiang Mai had once again offered us a glimpse into the heart of Thai culture, leaving an indelible mark on our journey.

<div style="clear:both;"></div>
{{< figure src="./images/cooking_class_group.jpg" alt="cooking class" class="aligne-center size-larger" >}}
