+++ 
draft = false
date = 2023-10-22T22:00:00+02:00
title = "Vang Vieng"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Sunday: A Journey into the Future

<!-- 
Train, new building (china) 
Security check similar to airport
Check in
Speed 160km/h

sunset bar

night market
night market again (dessert)
 -->

As the train pulled into the station, a new landscape greeted me, dominated by towering, modern architecture—a testament to the evolving face of Laos, shaped by the ambitious efforts of its northern neighbor, China, who had played a significant role in constructing the railway network.

The journey had begun with a heightened sense of security, akin to airport checks, reflecting the efficiency and thoroughness of the system. The speed of the train was impressive, whisking through the terrain at a remarkable 160 km/h, affording glimpses of the countryside that seemed to blur and blend, creating a mesmerizing tableau.

{{< figure src="./images/sunset.jpg" alt="sunset" class="aligne-right size-medium" >}}

As the day progressed, I discovered a gem—'The Egg Sunset Bar'—a place where time seemed to slow, and the sky transformed into a canvas of vivid hues. It was a moment of tranquil reflection, a reminder that amidst the rush of travel, there are still pockets of serenity to be found.

Come evening, I strolled through the night market, which, while smaller than the one in Luang Prabang, still held its own charm. The stalls offered a delightful array of local delicacies.

## Monday

<!-- 
riding around

nam xay viewpoint

post office

Pha ngern viewpoint

restaurant with yuho
 -->

{{< figure src="./images/nam_xay_viewpoint.jpg" alt="Nam Xay viewpoint" class="aligne-left size-small" >}}

In the early afternoon, I made my way to the Nam Xay Viewpoint, a spot renowned for its captivating vistas. The landscape unfolded before me in a grand tapestry of greens and blues, stretching out as far as the eye could see. The viewpoint, however, was bustling with fellow explorers, all captivated by the same enchanting scenery.

{{< figure src="./images/pha_ngern_viewpoint.jpg" alt="Pha Ngern viewpoint" class="aligne-right size-medium" >}}

Later, as the sun began its descent, I ventured up to the Pha Ngern Viewpoint, eager to witness the breathtaking transformation of the landscape. The golden hour cast a spell on the surroundings, painting them in a warm, almost ethereal glow. It was a peaceful and quiet moment, with only one other person joining me to enjoy the sunset.

As the day drew to a close, I met with Yuho, a fellow traveler I'd had the pleasure of crossing paths with earlier. We shared stories over a delightful meal at a local restaurant, the sounds of Laos mingling with our laughter.

{{< figure src="./images/dinner_yuho.jpg" alt="dinner with Yuho" class="aligne-left size-medium" >}}

Afterward, Yuho and I made our way to the hostel's bar, where we had the pleasure of meeting some friends from Luang Prabang. It was a night of shared experiences and new connections, a perfect embodiment of the camaraderie that often graces the journey of a traveler.

Following this, I joined [Forrest](https://www.instagram.com/forresteliaslindsay/) for a meaningful evening at the night market. We had a beer, our conversation weaving through the experiences that had brought us both to this corner of the world

## Tuesday: Noë
<!-- 
coffee with huge dessert

waiting for noe

sunset at bar

dinner on terrace
 -->
{{< figure src="./images/cookie.jpg" alt="cookie" class="aligne-right size-medium" >}}

Around lunchtime, I settled into a quaint café for a cup of coffee, accompanied by an indulgent dessert that offered a sweet midday pick-me-up. It was a moment of quiet reflection, a chance to savor the simple pleasures that travel often brings.

Later, I found myself eagerly awaiting the arrival of Noë, my girlfriend, and Luisa. For some time we enjoyed to hold each other in the arms. After weeks without seeing us.

{{< figure src="./images/sunset_2.jpg" alt="sunset" class="aligne-left size-medium" >}}

As the afternoon sun began to wane, we made out way back to the Egg Sunset Bar, a place that had quickly become a favorite. The view, though veiled by clouds this time, still held a certain magic.

For dinner, we opted for a change of scenery, choosing to dine on the terrace. The ambiance was enchanting, with the evening breeze carrying with it the sounds and scents of Vang Vieng. It was a fitting end to a day filled with new experiences and cherished connections. 

## Wednesday: Sunset Views and Poolside Serenity
<!-- 

spaghetti for luisa, french fries for noe & honey lemon coffee for me

pool day

dinner on terrace
this time with a sunset view
 -->

The day unfurled with a sense of leisure, a chance to slow down and relish the moments. For lunch, Luisa opted for a comforting plate of spaghetti, while Noë indulged in a portion of crispy French fries. As for me, I decided to treat myself to a delightful honey lemon coffee. It was a delightful interplay of flavors that set the tone for the day.

The sun bore down with its characteristic intensity, and we sought refuge in the cool waters of the pool. It was a welcome respite, a chance to unwind and let the worries of the world slip away.

As evening approached, we made our way back to the terrace for dinner. This time, we were greeted by a spectacular sunset, painting the sky with hues of orange and pink. It was a moment of pure serenity, a reminder of the beauty that could be found in even the simplest of moments. 

## Thursday: A Rainy Trek and Culinary Delights
<!-- 
hiking the Pha ngern view point again
rain

night market food
roti for noe

 -->
This time, I ventured up to the Pha Ngern viewpoint with Luisa and Noë, eager to share the experience. As we arrived on top, the sky grew overcast, and soon enough, a heavy rain began to fall. Undeterred, we sought shelter in the small house of the viewpoint. The rain danced around us, a serene backdrop to the quietude of the mountains. After a while, the rain subsided, leaving behind a fresh, invigorating scent in the air. The familiar trail took on a new allure, each step a deliberate choice to move forward.

{{< figure src="./images/pha_ngern_viewpoint_noe.jpg" alt="Pha Ngern viewpoint" class="aligne-left size-small" >}}
{{< figure src="./images/pha_ngern_viewpoint_noe_2.jpg" alt="Pha Ngern viewpoint" class="aligne-left size-small" >}}
{{< figure src="./images/pha_ngern_viewpoint_rain.jpg" alt="Pha Ngern viewpoint" class="aligne-left size-small" >}}
{{< figure src="./images/pha_ngern_viewpoint_noe_3.jpg" alt="Pha Ngern viewpoint" class="aligne-left size-small" >}}

<div style="clear:both;"></div>

As the day drew to a close, we found ourselves at the night market, a place where culinary delights awaited.  

## Friday: Exploring Dead Ends and Cloudy Sunsets
<!-- 
chill morning

coffee
buying bread

walking around
dead end roads

sunset at bar
no sunset because of clouds

night market food
 -->
The morning in Vang Vieng unfolded with a sense of tranquil exploration. A steaming cup of coffee provided the perfect start, accompanied by the promise of a new day.

Later, a visit to the local bakery became a delightful ritual. The scent of freshly baked bread wafted through the air, promising a taste of comfort and home. With a loaf in hand, we felt a sense of contentment, knowing that simple pleasures could bring such joy.

{{< figure src="./images/dead_end_walk.jpg" alt="dead end walk" class="aligne-right size-medium" >}}

Venturing out, we meandered through the streets, stumbling upon intriguing alleys and pathways that led to dead ends. Each twist and turn revealed a different facet of this charming town, a place where every corner held a story.

In the afternoon, a trip to the Egg Sunset Bar with its promise of a vibrant sunset beckoned. However, nature had its own plans. Clouds gathered, shrouding the sun in a gentle haze. Though the fiery hues of a classic sunset eluded us, the moment was no less enchanting.

As evening fell, the night market once again became a hub of activity. The aroma of diverse cuisines mingled in the air, offering a tantalizing array of choices. It was a fitting end to a day of serendipitous discoveries and subtle beauty.
