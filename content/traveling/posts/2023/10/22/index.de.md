+++
draft = false
date = 2023-10-22T22:00:00+02:00
title = "Vang Vieng"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Sonntag: Eine Reise in die Zukunft

<!-- 
Zug, neues Gebäude (China)
Sicherheitskontrolle ähnlich wie am Flughafen
Check-in
Geschwindigkeit 160 km/h

Sonnenuntergangsbar

Nachtmarkt
Nachtmarkt nochmal (Dessert)
 -->

Als der Zug im Bahnhof einfuhr, wurde ich von einer neuen Landschaft empfangen, die von hoch aufragender, moderner Architektur dominiert wurde – ein Zeugnis für das sich wandelnde Gesicht von Laos, geprägt durch die ambitionierten Bemühungen seines nördlichen Nachbarn, China, das eine bedeutende Rolle beim Bau des Schienennetzes gespielt hatte.

Die Reise begann mit einem erhöhten Sicherheitsgefühl, ähnlich den Kontrollen an Flughäfen, was die Effizienz und Gründlichkeit des Systems widerspiegelte. Die Geschwindigkeit des Zuges war beeindruckend, er sauste mit bemerkenswerten 160 km/h durch das Terrain, und die ländliche Umgebung verschwamm in einem faszinierenden Anblick.

{{< figure src="./images/sunset.jpg" alt="Sonnenuntergang" class="aligne-right size-medium" >}}

Im Laufe des Tages entdeckte ich ein Juwel – die „The Egg Sunset Bar“ – ein Ort, an dem die Zeit langsamer zu vergehen schien und der Himmel sich in ein lebendiges Farbenmeer verwandelte. Es war ein Moment der ruhigen Reflexion, eine Erinnerung daran, dass es auch inmitten der Reisestrapazen noch Orte der Ruhe gibt.

Am Abend schlenderte ich durch den Nachtmarkt, der zwar kleiner war als der in Luang Prabang, aber dennoch seinen eigenen Charme hatte. Die Stände boten eine verlockende Auswahl an lokalen Delikatessen.

## Montag

<!-- 
Fahrt herum

Nam Xay Aussichtspunkt

Postamt

Pha Ngern Aussichtspunkt

Restaurant mit Yuho
 -->

{{< figure src="./images/nam_xay_viewpoint.jpg" alt="Nam Xay Aussichtspunkt" class="aligne-left size-small" >}}

Am frühen Nachmittag machte ich mich auf den Weg zum Nam Xay Aussichtspunkt, einem Ort, der für seine faszinierenden Ausblicke bekannt ist. Die Landschaft entfaltete sich vor mir wie ein grandioses Gemälde aus grünen und blauen Tönen, so weit das Auge reichte. Der Aussichtspunkt war jedoch gut besucht, und viele andere Entdecker schauten gebannt auf dieselbe bezaubernde Aussicht.

{{< figure src="./images/pha_ngern_viewpoint.jpg" alt="Pha Ngern Aussichtspunkt" class="aligne-right size-medium" >}}

Später, als die Sonne begann unterzugehen, machte ich mich auf den Weg zum Pha Ngern Aussichtspunkt, gespannt darauf, die atemberaubende Verwandlung der Landschaft zu erleben. Die goldene Stunde hüllte die Umgebung in ein warmes, fast ätherisches Licht. Es war ein friedlicher und ruhiger Moment, in dem nur noch eine weitere Person bei mir war, um den Sonnenuntergang zu geniessen.

Als der Tag sich dem Ende neigte, traf ich mich mit Yuho, einem Mitreisenden, den ich zuvor kennengelernt hatte. Wir tauschten Geschichten bei einem köstlichen Mahl in einem lokalen Restaurant aus, und die Geräusche von Laos mischten sich mit unserem Lachen.

{{< figure src="./images/dinner_yuho.jpg" alt="Abendessen mit Yuho" class="aligne-left size-medium" >}}

Danach machten Yuho und ich uns auf den Weg zur Bar des Hostels, wo wir einige Freunde aus Luang Prabang trafen. Es war eine Nacht voller geteilter Erlebnisse und neuer Verbindungen, ein perfektes Beispiel für die Kameradschaft, die oft die Reise eines Reisenden begleitet.

Anschliessend traf ich [Forrest](https://www.instagram.com/forresteliaslindsay/) zu einem bedeutungsvollen Abend auf dem Nachtmarkt. Wir tranken ein Bier und liessen unser Gespräch durch die Erfahrungen wandern, die uns beide in diese Ecke der Welt geführt hatten.

## Dienstag: Noë
<!-- 
Kaffee mit riesigem Dessert

Warten auf Noë

Sonnenuntergang in der Bar

Abendessen auf der Terrasse
 -->
{{< figure src="./images/cookie.jpg" alt="Keks" class="aligne-right size-medium" >}}

Gegen Mittag setzte ich mich in ein gemütliches Café für eine Tasse Kaffee, begleitet von einem üppigen Dessert, das mir einen süssen Energieschub am Mittag verschaffte. Es war ein Moment der stillen Reflexion, eine Gelegenheit, die einfachen Freuden zu geniessen, die das Reisen oft mit sich bringt.

Später wartete ich voller Vorfreude auf die Ankunft von Noë, meiner Freundin, und Luisa. Eine lange Zeit ohne uns ging zu Ende, und wir genossen es, uns in den Armen zu halten.

{{< figure src="./images/sunset_2.jpg" alt="Sonnenuntergang" class="aligne-left size-medium" >}}

Als die Nachmittags-Sonne langsam verblasste, machten wir uns auf den Weg zurück zur Egg Sunset Bar, einem Ort, der sich schnell zu einem Favoriten entwickelt hatte. Die Aussicht war diesmal von Wolken verhangen, aber sie hatte dennoch eine gewisse Magie.

Zum Abendessen entschieden wir uns für eine Änderung der Szenerie und wählten die Terrasse. Die Atmosphäre war bezaubernd, mit einer Brise, die die Klänge und Düfte von Vang Vieng mit sich trug. Es war ein passender Abschluss eines Tages voller neuer Erfahrungen und wertvoller Verbindungen.

## Mittwoch: Sonnenuntergang und Pool-Sommer
<!-- 

Spaghetti für Luisa, Pommes für Noë und Honig-Zitronen-Kaffee für mich

Pool-Tag

Abendessen auf der Terrasse
diesmal mit Sonnenuntergang
 -->

Der Tag verlief mit einer gewissen Gelassenheit, eine Gelegenheit, das Tempo zu verlangsamen und die Momente zu geniessen. Zum Mittagessen wählte Luisa ein herzhaftes Spaghetti-Gericht, während Noë sich an knusprigen Pommes erfreute. Für mich war es ein köstlicher Honig-Zitronen-Kaffee, der den Tag versüsste.

Die Sonne brannte mit ihrer charakteristischen Intensität, und wir suchten Zuflucht im kühlen Wasser des Pools. Es war eine willkommene Erholung, ein Moment, um die Sorgen des Alltags hinter sich zu lassen.

Als der Abend näher rückte, machten wir uns erneut auf den Weg zur Terrasse für das Abendessen. Diesmal wurden wir von einem spektakulären Sonnenuntergang begrüsst, der den Himmel in Orange- und Rosatönen malte. Es war ein Moment purer Ruhe, eine Erinnerung an die Schönheit, die auch in den einfachsten Momenten zu finden ist.

## Donnerstag: Ein Regenspaziergang und kulinarische Köstlichkeiten
<!-- 
Wanderung zum Pha Ngern Aussichtspunkt nochmal
Regen

Essen auf dem Nachtmarkt
Roti für Noë
 -->
Diesmal machte ich mich mit Luisa und Noë auf den Weg zum Pha Ngern Aussichtspunkt, gespannt darauf, die Erfahrung zu teilen. Als wir oben ankamen, verdunkelte sich der Himmel und bald begann starker Regen zu fallen. Unbeeindruckt suchten wir Zuflucht im kleinen Haus des Aussichtspunkts. Der Regen tanzte um uns, eine ruhige Kulisse für die Stille der Berge. Nach einer Weile liess der Regen nach und hinterliess einen frischen, belebenden Duft in der Luft. Der vertraute Weg nahm eine neue Anziehungskraft an, jeder Schritt eine bewusste Wahl, weiterzugehen.

{{< figure src="./images/pha_ngern_viewpoint_noe.jpg" alt="Pha Ngern Aussichtspunkt" class="aligne-left size-small" >}}
{{< figure src="./images/pha_ngern_viewpoint_noe_2.jpg" alt="Pha Ngern Aussichtspunkt" class="aligne-left size-small" >}}
{{< figure src="./images/pha_ngern_viewpoint_rain.jpg" alt="Pha Ngern Aussichtspunkt" class="aligne-left size-small" >}}
{{< figure src="./images/pha_ngern_viewpoint_noe_3.jpg" alt="Pha Ngern Aussichtspunkt" class="aligne-left size-small" >}}

<div style="clear:both;"></div>

Als der Tag sich dem Ende näherte, fanden wir uns wieder auf dem Nachtmarkt, einem Ort, an dem kulinarische Köstlichkeiten auf uns warteten.

## Freitag: Erkundung von Sackgassen und bewölkten Sonnenuntergängen
<!-- 
Chilliger Morgen

Kaffee
Brot kaufen

Herumlaufen
Sackgassen

Sonnenuntergang in der Bar
Kein Sonnenuntergang wegen Wolken

Essen auf dem Nachtmarkt
 -->
Der Morgen in Vang Vieng begann mit einer entspannten Erkundung. Eine dampfende Tasse Kaffee war der perfekte Start in den Tag, begleitet von der Aussicht auf einen neuen Tag.

Später wurde der Besuch in der örtlichen Bäckerei zu einem kleinen Ritual. Der Duft von frisch gebackenem Brot lag in der Luft und versprach einen Hauch von Komfort und Heimeligkeit. Mit einem Laib in der Hand fühlten wir uns zufrieden, denn es sind oft die einfachen Freuden, die solche Freude bereiten.

{{< figure src="./images/dead_end_walk.jpg" alt="Sackgasse-Spaziergang" class="aligne-right size-medium" >}}

Wir machten uns auf den Weg und schlenderten durch die Strassen, entdeckten interessante Gassen und Wege, die in Sackgassen führten. Jede Wendung und jede Ecke zeigte eine neue Facette dieser charmanten Stadt, einem Ort, an dem jede Ecke eine Geschichte erzählt.

Am Nachmittag lockte uns ein Ausflug zur Egg Sunset Bar mit dem Versprechen eines lebendigen Sonnenuntergangs. Doch die Natur hatte ihre eigenen Pläne. Wolken zogen auf und verhüllten die Sonne in einem sanften Nebel. Obwohl uns die klassischen Sonnenuntergangsfarben diesmal verwehrt blieben, war der Moment nicht weniger bezaubernd.

Am Abend wurde der Nachtmarkt erneut zu einem Mittelpunkt des Geschehens. Die Aromen unterschiedlicher Küchen vermischten sich in der Luft und boten eine verlockende Auswahl an Speisen. Es war ein passender Abschluss eines Tages voller zufälliger Entdeckungen und subtiler Schönheit.