+++ 
draft = false
date = 2023-10-08T22:00:00+02:00
title = "Kanchanaburi"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Sunday: A Tranquil Retreat by the Waters of Kanchanaburi
<!-- 
leaving bangkok
nice bus ride - writing blog posts
nice hostel, looking like it is in the wild with small bungalos floating on the water
death railway, learning some history of asias part in world war 2
restaurant, super nice food (khao soi)
 -->

Today marked [Alex](https://www.instagram.com/alexander.bich/) and my departure from the vibrant bustle of Bangkok. A comfortable mini van ride awaited, providing not just transportation but also a serene environment for some focused writing. With my laptop at hand, I took the opportunity to translate the day's experiences into words, capturing the essence of our journey.

{{< figure src="./images/hostel.jpg" alt="hostel" class="aligne-left size-small" >}}

As we approached Kanchanaburi, the landscape transformed into a picturesque haven. The hostel, nestled in nature's embrace, appeared as though it had sprung organically from the wild. Small bungalows, perched on the water's edge, offered a unique and peaceful escape from the urban clamor.

{{< figure src="./images/death_railway.jpg" alt="death railway" class="aligne-right size-medium" >}}

The day held a somber yet enlightening visit to the Death Railway, a poignant reminder of Asia's role in the tumultuous years of World War II. The echoes of history resonated through the railway, urging reflection on the sacrifices made and the resilience shown during those trying times.

{{< figure src="./images/khao_soi.jpg" alt="Khao soi" class="aligne-left size-small" >}}

With history etched in our minds, we gathered at a local restaurant, eager to indulge in the flavors of Thailand. Khao Soi, a fragrant and flavorful noodle soup, graced my plate, a true culinary masterpiece that left my taste buds dancing in delight. The meal was a fitting tribute to a day of transition, where the past and present wove together seamlessly, creating memories that would linger long after the final spoonful was savored.

## Monday: A Day of Natural Wonders at Erawan National Park
<!--
early wake up (at 8am)
advetures bus ride (1.5h)
erawan nation park with beautiful waterfalls and fish that try to eat you alive
relaxing day 
bus ride back
nice dinner (green curry)
-->

{{< figure src="./images/waterfall_2.jpg" alt="waterfall" class="aligne-right size-medium" >}}

Monday arrived with an early wake-up call, the clock striking 8 a.m., signaling the start of a day filled with promise and adventure. A rugged bus awaited, promising a 1.5-hour journey through winding roads and scenic landscapes, setting the stage for our rendezvous with nature.

As we arrived at Erawan National Park, a hidden gem of Thailand, a symphony of rushing water and vibrant flora greeted us. The park is famed for its stunning waterfalls, and we wasted no time in exploring each tier.

<div style="clear:both;"></div>
{{< figure src="./images/waterfall_6.jpg" alt="waterfall" class="aligne-left size-large" >}}
{{< figure src="./images/waterfall_7.jpg" alt="waterfall" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

{{< figure src="./images/alex_with_fish.jpg" alt="Alex with fish" class="aligne-right size-small" >}}

Cascading down like nature's own stairway, the waterfalls held an almost mystical allure. The crystal-clear pools below were home to an surprise – friendly, albeit overenthusiastic, fish. They playfully nipped at our toes, turning a leisurely swim into an amusing underwater encounter.

After the invigorating exploration, we settled in for a day of pure relaxation. The lush green surroundings and the soothing sound of nature provided the perfect backdrop for unwinding. It was a day to lose track of time, letting the worries of the world fade into the background.

The return bus ride was a quiet reflection of our day's adventures, each of us lost in our own thoughts, carrying with us the memories of Erawan's majestic beauty.

As the day came to a close, we gathered for a delightful dinner. The aroma of green curry filled the air, a testament to the rich flavors that Thailand is celebrated for. It was a fitting end to a day that had unfolded like a dream, leaving us eager for the next adventure that awaited in this enchanting land.

{{< figure src="./images/green_curry.jpg" alt="green curry" class="aligne-center size-large" >}}
