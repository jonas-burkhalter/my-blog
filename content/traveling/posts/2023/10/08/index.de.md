+++ 
draft = false
date = 2023-10-08T22:00:00+02:00
title = "Kanchanaburi"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Sonntag: Ein ruhiger Rückzugsort am Wasser von Kanchanaburi

Heute markierte den Abschied von [Alex](https://www.instagram.com/alexander.bich/) und mir aus dem lebhaften Trubel von Bangkok. Eine angenehme Minivan-Fahrt erwartete uns, die nicht nur Transport, sondern auch eine ruhige Umgebung zum konzentrierten Schreiben bot. Mit meinem Laptop griff ich die Gelegenheit, die Erlebnisse des Tages in Worte zu fassen und das Wesen unserer Reise einzufangen.

{{< figure src="./images/hostel.jpg" alt="Hostel" class="aligne-left size-small" >}}

Als wir uns Kanchanaburi näherten, verwandelte sich die Landschaft in ein malerisches Paradies. Das Hostel, eingebettet in die Umarmung der Natur, schien, als sei es organisch aus der Wildnis hervorgegangen. Kleine Bungalows, die am Rand des Wassers thronten, boten einen einzigartigen und friedlichen Rückzugsort aus dem städtischen Lärm.

{{< figure src="./images/death_railway.jpg" alt="Death Railway" class="aligne-right size-medium" >}}

Der Tag beinhaltete einen ernsten, aber aufschlussreichen Besuch der Death Railway, eine eindrucksvolle Erinnerung an Asiens Rolle in den turbulenten Jahren des Zweiten Weltkriegs. Die Echos der Geschichte hallten durch die Eisenbahn, was uns zum Nachdenken über die Opfer und die Widerstandsfähigkeit der Menschen in diesen schwierigen Zeiten anregte.

{{< figure src="./images/khao_soi.jpg" alt="Khao Soi" class="aligne-left size-small" >}}

Mit der Geschichte in unseren Gedanken versammelten wir uns in einem lokalen Restaurant, gespannt darauf, die Aromen Thailands zu geniessen. Khao Soi, eine duftende und würzige Nudelsuppe, schmückte meinen Teller – ein wahres kulinarisches Meisterwerk, das meine Geschmacksknospen zum Tanzen brachte. Das Mahl war eine passende Hommage an einen Übergangstag, an dem die Vergangenheit und die Gegenwart nahtlos miteinander verschmolzen und Erinnerungen schufen, die noch lange nach dem letzten Löffel nachklingen würden.

## Montag: Ein Tag voller natürlicher Wunder im Erawan Nationalpark

{{< figure src="./images/waterfall_2.jpg" alt="Wasserfall" class="aligne-right size-medium" >}}

Der Montag begann mit einem frühen Wecker, der um 8 Uhr schlug und den Beginn eines Tages voller Versprechungen und Abenteuer signalisierte. Ein robuster Bus wartete, der uns eine 1,5-stündige Fahrt durch gewundene Strassen und malerische Landschaften versprach und die Bühne für unser Rendezvous mit der Natur bereitete.

Als wir den Erawan Nationalpark erreichten, ein verstecktes Juwel Thailands, wurden wir von einer Symphonie aus plätscherndem Wasser und lebendiger Flora begrüsst. Der Park ist berühmt für seine atemberaubenden Wasserfälle, und wir versäumten es nicht, jede Stufe zu erkunden.

<div style="clear:both;"></div>
{{< figure src="./images/waterfall_6.jpg" alt="Wasserfall" class="aligne-left size-large" >}}
{{< figure src="./images/waterfall_7.jpg" alt="Wasserfall" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

{{< figure src="./images/alex_with_fish.jpg" alt="Alex mit Fisch" class="aligne-right size-small" >}}

Die Wasserfälle, die wie eine Treppe der Natur herabstürzten, besassen eine fast mystische Anziehungskraft. Die kristallklaren Becken darunter beherbergten eine Überraschung – freundliche, wenn auch überenthusiastische Fische. Sie knabberten spielerisch an unseren Zehen, was ein gemütliches Schwimmen in eine amüsante Unterwasserbegegnung verwandelte.

Nach der belebenden Erkundung liessen wir uns für einen Tag der puren Entspannung nieder. Die üppig grünen Umgebungen und das beruhigende Geräusch der Natur boten den perfekten Hintergrund, um abzuschalten. Es war ein Tag, an dem die Zeit unmerklich verflog und die Sorgen der Welt in den Hintergrund traten.

Die Rückfahrt im Bus war eine stille Reflexion über unsere Abenteuer des Tages. Jeder von uns versunken in seine Gedanken, trugen wir die Erinnerungen an die majestätische Schönheit von Erawan mit uns.

Als der Tag sich dem Ende zuneigte, versammelten wir uns zu einem köstlichen Abendessen. Das Aroma von grünem Curry erfüllte die Luft, ein Beweis für die reichen Aromen, für die Thailand berühmt ist. Es war ein passendes Ende für einen Tag, der sich wie ein Traum entfaltete und uns neugierig auf das nächste Abenteuer machte, das in diesem verzauberten Land auf uns wartete.

{{< figure src="./images/green_curry.jpg" alt="Grünes Curry" class="aligne-center size-large" >}}