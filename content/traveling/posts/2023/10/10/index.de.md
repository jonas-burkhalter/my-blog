+++ 
draft = false
date = 2023-10-10T22:00:00+02:00
title = "Ayutthaya"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Dienstag: Von Kanchanaburi nach Ayutthaya

Die Reise von Kanchanaburi nach Ayutthaya war eine Übung in Spontaneität. Wir entschieden uns, es Schritt für Schritt anzugehen und die Verbindungen unterwegs herauszufinden. Die Ungewissheit fügte ein Element des Abenteuers hinzu, wodurch die blosse Handlung des Reisens zu einer echten Entdeckung wurde. Wir nahmen den Bus von Kanchanaburi nach Suphan Buri, einem Ort, von dem keiner von uns je gehört hatte, und hofften auf einen Anschlussbus nach Ayutthaya. Der Anschlussbus oder Minivan war direkt da, und die Reise konnte nicht reibungsloser verlaufen.

Bei unserer Ankunft in Ayutthaya war die Kürze unseres Aufenthalts kaum zu fassen, aber sie hinterliess einen bleibenden Eindruck in unseren Herzen. Die reiche Geschichte der Stadt flüsterte durch die antiken Ruinen, ein Zeugnis ihrer einstigen Grösse. Unsere Zeit hier war zwar kurz, aber ihre Echos würden noch lange nachhallen.

{{< figure src="./images/hospital.jpg" alt="Krankenhaus" class="aligne-left size-medium" >}}

Im Laufe des Tages fanden wir uns an einem Ort wieder, der eine andere Art von Geschichte beherbergte – das lokale Krankenhaus. Hier, mitten im Strudel der Emotionen, erlebten wir die täglichen Kämpfe der Menschen, die ihre eigenen Schlachten kämpften. Ich war einfach als Freund da, begleitete Alex durch diese schwierige Zeit, bot moralische Unterstützung und stand ihm bei, während er diesen komplexen Weg navigierte.

Nach einer schnellen Planänderung beschlossen wir, unseren Aufenthalt in Ayutthaya zu verkürzen. Alle Online-Nachtbusse waren ausgebucht, und der Nachtzug der dritten Klasse schien keine attraktive Option zu sein. Wir buchten einen Zug für den nächsten Tag am frühen Nachmittag und beschlossen, einen Halt in Phitsanulok einzulegen, bevor wir nach Chiang Mai weiterreisten. Mit allen getroffenen Vorkehrungen war der neue Plan in Bewegung.

## Mittwoch: Eine Geschichte von Erkundung und Entdeckung

### Verloren in der Zeit: Ayutthayas rätselhafter Labyrinth

Als die Sonne die antike Stadt Ayutthaya in Gold- und Rottöne tauchte, begaben wir uns auf eine Suche nach den Überresten eines einst grossen Königreichs. Doch was uns erwartete, war ein Labyrinth aus engen Gassen und verborgenen Schätzen, die sich als schwieriger zu finden erwiesen als erwartet. An jeder Ecke und in jeder Gasse schienen die Tempel und Ruinen ein verführerisches Versteckspiel zu spielen.

### Wat Phra Si Sanphet: Ein Blick in Ayutthayas glorreiche Vergangenheit

{{< figure src="./images/wat_phra_si_snaphet_stupa.jpg" alt="Wat Phra Si Snaphet (Stupa)" class="aligne-left size-medium" >}}

Gerade als die Hoffnung zu schwinden begann und die Zeit knapp wurde, stiessen wir auf den majestätischen Wat Phra Si Sanphet. Hoch aufragend auf dem weitläufigen Gelände zeugte dieser Tempel von der Grandeur des Ayutthaya-Königreichs.

{{< figure src="./images/wat_phra_si_snaphet.jpg" alt="Wat Phra Si Snaphet" class="aligne-right size-small" >}}

Eine kurze Geschichtsstunde offenbarte, dass Ayutthaya, gegründet im Jahr 1350, als zweite Hauptstadt von Siam erblühte. Sein Wohlstand war in der Pracht von Bauwerken wie Wat Phra Si Sanphet deutlich sichtbar, das als königliches Kloster und spirituelles Herz des Königreichs diente. Die drei Chedis des Tempels waren ein Zeugnis für die Macht und Verehrung, die den Ayutthaya-Königen entgegengebracht wurden.

### Eine Zugreise ins Unbekannte

{{< figure src="./images/train.jpg" alt="Zug" class="aligne-right size-small" >}}

Als der Tag sich dem Ende neigte, begaben wir uns auf eine 5,5-stündige Zugfahrt nach Phitsanulok. Wir entschieden uns für die dritte Klasse und nahmen die Ungewissheit an, die vor uns lag. Das rhythmische Klackern der Räder auf den Gleisen wurde zu einem beruhigenden Wiegenlied, während die vorbeiziehende Landschaft ein lebendiges Porträt von Thailands vielfältiger Topografie malte.

Dieser Abschnitt unseres Abenteuers war eine eindrucksvolle Erinnerung daran, dass manchmal die bedeutungsvollsten Erfahrungen aus unvorhergesehenen Umwegen entstehen. Inmitten der Ungewissheit fanden wir eine einzigartige Art von Schönheit, von der wir wussten, dass sie die Kapitel unserer Reisegeschichten noch viele Jahre lang färben würde.

{{< figure src="./images/train_ride.jpg" alt="Zugfahrt" class="aligne-left size-medium" >}}

Die Reise war eine Symphonie von Erfahrungen – die Kameradschaft mit anderen Reisenden, die sich ständig ändernde Landschaft und das Gefühl der Freiheit, das nur eine Zugfahrt bieten kann. Mit jeder vergehenden Minute tauchte ich tiefer in ein neues Kapitel dieses Abenteuers ein.

## Die Geschichte von Ayutthaya

Das Ayutthaya-Königreich, gegründet 1350, war ein mächtiges und prosperierendes siamesisches Königreich, das im heutigen Thailand lag. Es wurde von König Ramathibodi I. gegründet und stieg schnell zu einer bedeutenden Macht auf, die nach Sukhothai zur zweiten Hauptstadt des siamesischen Reiches wurde.

Unter der Herrschaft einer Reihe fähiger Monarchen erlebte Ayutthaya eine Blütezeit in kultureller und wirtschaftlicher Hinsicht. Die strategische Lage des Königreichs an der Mündung mehrerer grosser Flüsse ermöglichte ausgedehnte Handelsnetzwerke mit benachbarten Regionen, darunter China, Indien, Persien und Arabien.

Eine der bemerkenswertesten Errungenschaften Ayutthayas war seine kulturelle Synthese. Das Königreich nahm verschiedene kulturelle und religiöse Einflüsse auf, darunter den Theravada-Buddhismus, den Hinduismus und den Islam. Dieser integrative Ansatz trug zum reichen künstlerischen und architektonischen Erbe des Königreichs bei, mit prächtigen Tempeln und Palästen, die die Hauptstadt schmückten.

Ayutthayas Wohlstand zog jedoch auch das Interesse fremder Mächte auf sich, was zu gelegentlichen Konflikten führte. 1569 wehrte das Königreich eine grosse Invasion der Birmanen erfolgreich ab. Doch dieser Sieg markierte den Beginn einer angespannten Beziehung zwischen den beiden Königreichen, die 1767 in einer verheerenden birmanischen Invasion gipfelte.

Der Fall von Ayutthaya kam nach einer brutalen Belagerung, die mit der Plünderung und Verbrennung der Stadt endete. Dieses Ereignis markierte das Ende der Dominanz Ayutthayas in der Region und führte zur Zerstreuung seiner Bevölkerung.

### Architektur

Ein markantes Merkmal der Architektur von Ayutthaya sind die monumentalen Tempel, bekannt als "Wat" in Thai. Diese Tempel verfügen oft über eine "Prang", einen hohen, kunstvoll verzierten Turm, der den mythischen Mittelpunkt des Universums, den Meru-Berg, in der hinduistischen Kosmologie symbolisiert. Bekannte Beispiele sind der Wat Phra Si Sanphet und der Wat Phra Mahathat, von denen jeder einzigartige Prangs mit Stuckreliefs und skulpturalen Details aufweist.

Der Einfluss der Khmer-Architektur, insbesondere im Prang-Design, ist ein bedeutender Aspekt des architektonischen Erbes von Ayutthaya. Dieser Einfluss zeigt sich in gestaffelten Strukturen und Lotusknospen-Finalen, die eine harmonische Integration religiöser Symbolik und künstlerischer Ausdrucksweise widerspiegeln.

Ausserdem folgt die Anordnung der Tempel in Ayutthaya oft einem mandalaähnlichen Layout, das geometrische Harmonie und spirituelle Bedeutung betont. Die Hallen, Kapellen und Klaustergänge, die den zentralen Stupa umgeben, schaffen einen heiligen Raum für religiöse Zeremonien und Meditation.

Da Ayutthaya eine kosmopolitische Stadt war, die in grossem Umfang Handel trieb, sind europäische Einflüsse, insbesondere portugiesische und niederländische, in einigen architektonischen Elementen erkennbar. Dies zeigt sich in der Verwendung bestimmter Baumaterialien sowie in der Integration westlicher Stile in traditionelle thailändische Designs.

{{< figure src="./images/phra_ti_nang_yen.jpg" alt="Phra Ti Nang Yen" class="aligne-center size-medium" >}}