+++ 
draft = false
date = 2023-10-10T22:00:00+02:00
title = "Ayutthaya"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Tuesday: From Kanchanaburi to Ayutthaya
<!-- 
kanchanaburi to ayutthaya (just going to the bus and figure the connection out as we go)
short wake in ayutthaya
hospital (mental support for alex and seeing the daily stuggle of people here)
change of plan, shorten ayutthaya and taking the train to Phitsanulok (because everything was booked)
 -->

{{< figure src="./images/bus.jpg" alt="bus" class="aligne-right size-medium" >}}

The journey from Kanchanaburi to Ayutthaya was an exercise in spontaneity. We decided to take it one step at a time, opting to figure out the connections on the go. The uncertainty added an element of adventure, transforming the mere act of travel into a true exploration. We took the bus from Kanchanaburi to Suphan Buri, a place neither of us had ever heard of, and hoped for a connecting bus to Ayutthaya. The connecting bus or minivan was right there, and the journey couldn't have been smoother.

Upon our arrival in Ayutthaya, the brevity of our stay belied the impact it would leave on our hearts. The city's rich history whispered through the ancient ruins, a testament to the grandeur it once held. Our time here may have been short, but its echoes would resonate long after we moved on.

{{< figure src="./images/hospital.jpg" alt="hospital" class="aligne-left size-medium" >}}

As the day unfolded, we found ourselves drawn to a place that held a different kind of history—the local hospital. Here, amidst the whirlwind of emotions, we witnessed the daily struggles of individuals fighting their own battles. I was there just as a friend, accompanying Alex through this challenging time, offering moral support and standing by his side as he navigated this complex journey. 

In a swift change of plans, we decided to shorten our stay in Ayutthaya. All online night buses were sold out, and the third-class night train didn't seem like an appealing option. We booked a train for the next day in the early afternoon and decided to make a stop in Phitsanulok before continuing on to Chiang Mai. With all arrangements in place, the new plan was set in motion.

## Wednesday: A Tale of Exploration and Discovery
<!-- 
wondering around in ayutthaya not finding any tempel, ruins or anything to see
finally finding the wat ... temple (short history story about the history of the kingdom ayutthaya)
train ride (5.5h, 3 class, not knowing what will come, nice experience, beautiful view) 
exhausted from travelling
 -->

### Lost in Time: Ayutthaya's Enigmatic Labyrinth

As the sun painted the ancient city of Ayutthaya with hues of gold and crimson, we embarked on a quest to uncover the remnants of a once-great kingdom. However, what awaited us was a maze of narrow streets and hidden treasures that proved more elusive than anticipated. With every corner turned and every alley explored, the temples and ruins seemed to play a tantalizing game of hide-and-seek. 

### Wat Phra Si Sanphet: A Glimpse into Ayutthaya's Glorious Past

{{< figure src="./images/wat_phra_si_snaphet_stupa.jpg" alt="Wat Phra Si Snaphet (stupa)" class="aligne-left size-medium" >}}

Just as hope was beginning to wane and the time started running out, we stumbled upon the majestic Wat Phra Si Sanphet. Standing tall amidst the sprawling grounds, this temple bore witness to the grandeur of the Ayutthaya kingdom.

{{< figure src="./images/wat_phra_si_snaphet.jpg" alt="Wat Phra Si Snaphet" class="aligne-right size-small" >}}

A brief history lesson revealed that Ayutthaya, established in 1350, flourished as the second capital of Siam. Its prosperity was evident in the opulence of structures like Wat Phra Si Sanphet, which served as the royal monastery and the spiritual heart of the kingdom. The temple's three chedis were a testament to the might and reverence commanded by the Ayutthayan kings.

### A Train Journey into the Unknown

{{< figure src="./images/train.jpg" alt="train" class="aligne-right size-small" >}}

As the day drew to a close, we embarked on a 5.5-hour train ride to Phitsanulok. Opting for third class, I embraced the uncertainty that lay ahead. The rhythmic clatter of wheels on tracks became a comforting lullaby, while the passing landscapes painted a vivid portrait of Thailand's diverse topography.

This leg of our adventure was a poignant reminder that sometimes, the most meaningful experiences arise from the unplanned detours. In the midst of uncertainty, we found a unique kind of beauty—one that we knew would color the chapters of our travel stories for years to come.

{{< figure src="./images/train_ride.jpg" alt="train ride" class="aligne-left size-medium" >}}

The journey was a symphony of experiences – the camaraderie with fellow travelers, the ever-changing scenery, and the sense of liberation that only a train ride can offer. With every passing minute, I found myself immersed in a new chapter of this adventure.

## The history of Ayutthaya

The Ayutthaya Kingdom, established in 1350, was a powerful and prosperous Siamese kingdom located in what is now modern-day Thailand. Founded by King Ramathibodi I, Ayutthaya quickly rose to prominence, becoming the second capital of the Siamese realm after Sukhothai.

Under the rule of a succession of capable monarchs, Ayutthaya flourished both culturally and economically. The kingdom's strategic location at the confluence of several major rivers allowed for extensive trade networks with neighboring regions, including China, India, Persia, and Arabia.

One of Ayutthaya's most notable achievements was its cultural synthesis. The kingdom embraced and integrated various cultural and religious influences, including Theravada Buddhism, Hinduism, and Islam. This inclusive approach contributed to the kingdom's rich artistic and architectural heritage, with magnificent temples and palaces adorning the capital city.

However, Ayutthaya's prosperity also attracted the attention of foreign powers, leading to occasional conflicts. In 1569, the kingdom successfully repelled a major invasion by the Burmese. Nevertheless, this victory marked the beginning of a strained relationship between the two kingdoms, culminating in a devastating Burmese invasion in 1767.

The fall of Ayutthaya came after a brutal siege, resulting in the sacking and burning of the city. This event marked the end of Ayutthaya's dominance in the region and led to the dispersal of its population. .

### Architcture

One of the defining features of Ayutthaya architecture is its monumental temples, known as "wat" in Thai. These temples often feature a "prang," a towering, intricately decorated spire that symbolizes Mount Meru, the mythical center of the universe in Hindu cosmology. Notable examples include Wat Phra Si Sanphet and Wat Phra Mahathat, each showcasing unique prangs adorned with stucco reliefs and sculptural details.

The influence of Khmer architecture, particularly evident in the prang design, is a significant aspect of Ayutthaya's architectural heritage. This influence is characterized by tiered structures and lotus-bud finials, reflecting a harmonious integration of religious symbolism and artistic expression.

Furthermore, the layout of Ayutthaya's temples often follows a mandala-like arrangement, emphasizing geometric harmony and spiritual significance. The halls, chapels, and cloisters surrounding the central stupa create a sacred space for religious ceremonies and meditation.

As Ayutthaya was a cosmopolitan city engaged in extensive trade, European influences, particularly Portuguese and Dutch, are discernible in some architectural elements. This is evident in the use of certain building materials, as well as the integration of Western styles into traditional Thai designs.

{{< figure src="./images/phra_ti_nang_yen.jpg" alt="Phra Ti Nang Yen" class="aligne-center size-medium" >}}
