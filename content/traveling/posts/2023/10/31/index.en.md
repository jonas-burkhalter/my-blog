+++ 
draft = false
date = 2023-10-31T22:00:00+02:00
title = "Nong Khiaw"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Tuesday: Nong Khiaw
<!-- 
spontaniously taking the bus to Nong Khiaw

booking my bus to vietnam and a trek for claire

walking up a viewpoint
-->
Today unfolded with a tinge of frustration, as I still found myself navigating the intricacies of travel bureaucracy. Since I couldn't leave on the 1st of November, I decided to embark on a bus journey to Nong Khiaw. This quaint town, nestled amidst awe-inspiring scenery, promised a unique experience, even if my stay was to be shorter than anticipated.

Once there I finally booked a bus ticket to Vietnam, taking a step closer to leaving the country. 

{{< figure src="./images/nong_khiaw.jpg" alt="nong khiaw" class="aligne-left size-small" >}}

In Nong Khiaw, the landscape unfolded before us in breathtaking fashion. We embarked on a hike to a viewpoint, where the panoramic vistas left us in awe. The rugged mountains and the lazy flow of the Nam Ou River below painted a scene of serene beauty.

{{< figure src="./images/sunset.jpg" alt="sunset" class="aligne-center size-large" >}}

## Wednesday: Jungle Adventures in Nong Khiaw
<!-- 
Walking up the viewpoint
feels like in the jungle
scary but nice feeling

meeting someone on top and walking togheter back down

enjoying an afternoon doing not much 
-->
Today was a day of exhilarating exploration as I ventured deep into the lush terrain surrounding Nong Khiaw. The trek to the viewpoint felt like a journey through a verdant jungle, with every step bringing me closer to the heart of nature. There was a mix of excitement and trepidation, but the experience was undeniably invigorating.

{{< figure src="./images/bridge.jpg" alt="bamboo bridge" class="aligne-left size-small" >}}
{{< figure src="./images/jungle.jpg" alt="jungle trek" class="aligne-left size-small" >}}
{{< figure src="./images/viewpoint.jpg" alt="view from viewpoint" class="aligne-left size-medium" >}}
<div style="clear:both;"></div>

At the top, I was greeted by a fellow adventurer, and together, we began our descent back down. The shared journey added a wonderful sense of camaraderie, as we navigated the terrain and exchanged stories along the way.

The afternoon was a time of quiet reflection and leisure, a chance to revel in the beauty of doing nothing.
