+++  
draft = false  
date = 2023-10-31T22:00:00+02:00  
title = "Nong Khiaw"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++  

## Dienstag: Nong Khiaw

<!--  
Spontan mit dem Bus nach Nong Khiaw fahren  

Bus nach Vietnam buchen und eine Trekkingtour für Claire organisieren  

Zu einem Aussichtspunkt wandern  
-->

Der heutige Tag begann mit einem Hauch von Frustration, da ich immer noch versuchte, die Tücken der Reisebürokratie zu überwinden. Da ich am 1. November nicht abreisen konnte, entschloss ich mich, spontan mit dem Bus nach Nong Khiaw zu fahren. Diese malerische Stadt, eingebettet in atemberaubende Landschaften, versprach eine einzigartige Erfahrung, auch wenn mein Aufenthalt kürzer ausfiel als ursprünglich geplant.

Kaum angekommen, buchte ich endlich mein Busticket nach Vietnam und kam meinem geplanten Abreisedatum ein Stück näher.

{{< figure src="./images/nong_khiaw.jpg" alt="Nong Khiaw" class="aligne-left size-small" >}}

In Nong Khiaw entfaltete sich die Landschaft in beeindruckender Weise. Wir machten eine Wanderung zu einem Aussichtspunkt, von dem uns ein atemberaubendes Panorama erwartete. Die zerklüfteten Berge und der ruhige Verlauf des Nam Ou Flusses darunter malten ein Bild von friedlicher Schönheit.

{{< figure src="./images/sunset.jpg" alt="Sonnenuntergang" class="aligne-center size-large" >}}

## Mittwoch: Dschungel-Abenteuer in Nong Khiaw

<!--  
Wanderung zum Aussichtspunkt  
Fühlt sich an wie im Dschungel  
Erschreckend, aber schönes Gefühl  

Jemanden auf dem Gipfel treffen und zusammen wieder hinunter gehen  

Einen Nachmittag geniessen, in dem wir nicht viel taten  
-->

Heute war ein Tag voller aufregender Erkundungen, als ich tief in das üppige Terrain rund um Nong Khiaw vordrang. Die Wanderung zum Aussichtspunkt fühlte sich an wie eine Reise durch einen grünen Dschungel, bei der jeder Schritt mich näher an das Herz der Natur brachte. Es war eine Mischung aus Aufregung und Nervosität, aber die Erfahrung war zweifellos belebend.

{{< figure src="./images/bridge.jpg" alt="Bambusbrücke" class="aligne-left size-small" >}}  
{{< figure src="./images/jungle.jpg" alt="Dschungelwanderung" class="aligne-left size-small" >}}  
{{< figure src="./images/viewpoint.jpg" alt="Aussicht vom Aussichtspunkt" class="aligne-left size-medium" >}}  
<div style="clear:both;"></div>

Oben angekommen, traf ich auf einen weiteren Abenteurer, und gemeinsam begannen wir den Abstieg. Die gemeinsame Wanderung verlieh dem Erlebnis eine wunderbare Kameradschaft, während wir das Gelände navigierten und unterwegs Geschichten austauschten.

Der Nachmittag war eine Zeit der stillen Reflexion und des Geniessens, eine Gelegenheit, das schöne Gefühl des Nichtstuns zu erleben.
