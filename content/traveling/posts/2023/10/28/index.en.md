+++ 
draft = false
date = 2023-10-28T22:00:00+02:00
title = "Luang Prabang (Boun Lai Heua Fai)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Saturday: Vang Vieng back to Luang Prabang
<!-- 
bumpy bus ride back to luang prabang

running into anne, a traveler i meet before
going together to the night market again, for a short dinner
 -->
{{< figure src="./images/bumpy_road.jpg" alt="bumpy road" class="aligne-right size-small" >}}

After a bumpy bus ride from Vang Vieng, I found myself back in the charming town of Luang Prabang. Despite the jolts and bumps along the way, the scenic views from the bus window made the journey worthwhile. The lush greenery and winding roads of Laos never fail to captivate.

As if by fate, I ran into [Anne](https://www.instagram.com/annemonot/), a fellow traveler I had met before. It was a delightful surprise, and we decided to spend the evening together. We made our way to the lively night market, sharing stories and savoring a short dinner. The aroma of local delicacies were just as enchanting as my first visit.

## Sunday: Enchanting Encounters and Unexpected Festivals
<!-- 
meeting with claire

walking the riverside / old town

enjoying to do not much

watching light in monastry (evening on the way to the night market) 

night market

unexpected festival in monastry on way home
playing music
food
sepak takraw
-->
Today was a day filled with serendipity and shared moments. I had the pleasure of reuniting with my dear friend Claire, and together, we explored the picturesque riverside and old town of Luang Prabang. The gentle flow of the Mekong River provided a soothing soundtrack to our leisurely stroll.

{{< figure src="./images/lights_in_monastry.jpg" alt="lights in monastry" class="aligne-left size-small" >}}

As evening descended, we made our way to a monastery, drawn by the allure of flickering lights. The serene ambiance and play of shadows on ancient stones created a mesmerizing scene.

{{< figure src="./images/night_market.jpg" alt="night market" class="aligne-right size-small" >}}

The night market, with its vibrant stalls and delectable street food, beckoned us once again. The colors and flavors were as enchanting as ever.

To our surprise, our journey home led us to an unexpected part of the festival in another monastery. 
The surroundings were adorned with beautiful lanterns, casting a warm and enchanting glow over the festivities. 

The highlight of the celebration was undoubtedly the feast of local delicacies. As if that wasn't enough, we also had the chance to witness an exhilarating game of sepak takraw. Sepak takraw is a traditional Southeast Asian sport similar to volleyball, where players use their feet, knees, and head to hit a rattan ball over a net. The agility and skill of the players left us in awe.

## Monday: Boun Lai Heua Fai
<!-- 
breakfast

planning my way to vietnam (frustrating) 
waiting for answers
no solution found

Massage with claire and anne

light festival
watching the parade
and the river side
letting the floating flower thing go
-->
The day began with a leisurely breakfast, a perfect way to fuel up for the adventures and challenges that lay ahead.

I spent a considerable part of the day trying to plan my route to Vietnam, but it proved to be a rather frustrating endeavor. Waiting for answers and searching for a solution left me with more questions than answers. I had to leave Laos on the 1st of November, but unforeseen circumstances had other plans. It turned out that no buses would be available on the 1st of November.

In the midst of this planning quandary, I found solace in the company of friends. Claire and Anne joined me for a relaxing massage, a much-needed respite from the day's frustrations.

{{< figure src="./images/parade.jpg" alt="parade" class="aligne-left size-medium" >}}

As the sun began to set, Luang Prabang came alive with the mesmerizing glow of the light festival. The streets were adorned with colorful lanterns, and I joined the crowd to watch the vibrant parade winding its way through the town. The illuminated floats and traditional costumes were a sight to behold.

{{< figure src="./images/floating_flower.jpg" alt="floating flower" class="aligne-right size-small" >}}

The highlight of the evening, however, was the serene moment by the riverside, where I released a floating flower into the water. It was a symbolic gesture of letting go, a reminder to embrace the flow of life and trust in the journey ahead.
