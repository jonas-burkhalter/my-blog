+++  
draft = false  
date = 2023-10-28T22:00:00+02:00  
title = "Luang Prabang (Boun Lai Heua Fai)"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++  

## Samstag: Vang Vieng zurück nach Luang Prabang

<!--  
holprige Busfahrt zurück nach Luang Prabang  

Zufällig Anne getroffen, eine Reisende, die ich schon vorher getroffen hatte  
Gemeinsam zum Nachtmarkt gegangen, um ein kurzes Abendessen zu geniessen  
-->

{{< figure src="./images/bumpy_road.jpg" alt="holprige Strasse" class="aligne-right size-small" >}}

Nach einer holprigen Busfahrt von Vang Vieng fand ich mich wieder in der charmanten Stadt Luang Prabang. Trotz der Erschütterungen und des holprigen Weges lohnten sich die landschaftlichen Ausblicke aus dem Fenster des Busses. Das üppige Grün und die kurvigen Strassen von Laos haben ihren ganz eigenen Zauber.

Wie vom Schicksal gefügt, traf ich [Anne](https://www.instagram.com/annemonot/), eine Reisende, die ich schon früher getroffen hatte. Es war eine wunderbare Überraschung, und wir beschlossen, den Abend zusammen zu verbringen. Wir machten uns auf den Weg zum lebhaften Nachtmarkt, teilten Geschichten und genossen ein kurzes Abendessen. Der Duft der lokalen Köstlichkeiten war genauso verführerisch wie bei meinem ersten Besuch.

## Sonntag: Verzaubernde Begegnungen und unerwartete Festivals

<!--  
Treffen mit Claire  

Spaziergang am Flussufer / Altstadt  

Geniessen des Nichtstuns  

Licht im Kloster beobachten (Abend auf dem Weg zum Nachtmarkt)  

Nachtmarkt  

Unerwartetes Festival im Kloster auf dem Heimweg  
Musik spielen  
Essen  
Sepak Takraw  
-->

Heute war ein Tag voller Zufälle und geteilter Momente. Ich hatte das Vergnügen, meine liebe Freundin Claire wiederzutreffen, und gemeinsam erkundeten wir das malerische Flussufer und die Altstadt von Luang Prabang. Der sanfte Fluss des Mekong bot den beruhigenden Klang für unseren gemütlichen Spaziergang.

{{< figure src="./images/lights_in_monastry.jpg" alt="Licht im Kloster" class="aligne-left size-small" >}}

Als der Abend hereinbrach, machten wir uns auf den Weg zu einem Kloster, angezogen von dem verlockenden Schein der flimmernden Lichter. Die ruhige Atmosphäre und das Spiel der Schatten auf den alten Steinen schufen eine faszinierende Szenerie.

{{< figure src="./images/night_market.jpg" alt="Nachtmarkt" class="aligne-right size-small" >}}

Der Nachtmarkt, mit seinen lebhaften Ständen und köstlichen Streetfoods, lockte uns ein weiteres Mal. Die Farben und Aromen waren ebenso verführerisch wie beim ersten Mal.

Zu unserer Überraschung führte uns unser Heimweg zu einem unerwarteten Teil des Festivals in einem anderen Kloster. Die Umgebung war mit wunderschönen Laternen geschmückt, die ein warmes und bezauberndes Licht auf die Feierlichkeiten warfen.

Das Highlight des Festes war zweifellos das Festmahl mit lokalen Köstlichkeiten. Als wäre das nicht genug gewesen, hatten wir auch die Gelegenheit, ein spannendes Spiel Sepak Takraw zu erleben. Sepak Takraw ist ein traditioneller südostasiatischer Sport, der dem Volleyball ähnelt, bei dem die Spieler mit ihren Füssen, Knien und Köpfen einen Rattanball über ein Netz schlagen. Die Agilität und das Können der Spieler beeindruckten uns sehr.

## Montag: Boun Lai Heua Fai

<!--  
Frühstück  

Planung meiner Reise nach Vietnam (frustrierend)  
Warten auf Antworten  
Keine Lösung gefunden  

Massage mit Claire und Anne  

Lichterfest  
Parade anschauen  
Am Flussufer  
Das schwimmende Blumending loslassen  
-->

Der Tag begann mit einem gemütlichen Frühstück, der perfekte Start in den Tag voller Abenteuer und Herausforderungen, die noch vor mir lagen.

Einen Grossteil des Tages verbrachte ich damit, meine Route nach Vietnam zu planen, aber es war eine ziemlich frustrierende Angelegenheit. Das Warten auf Antworten und das Suchen nach einer Lösung liessen mich mit mehr Fragen als Antworten zurück. Ich musste Laos am 1. November verlassen, aber unvorhergesehene Umstände hatten andere Pläne. Es stellte sich heraus, dass am 1. November keine Busse verfügbar sein würden.

Mitten in dieser Planungsnot fand ich Trost in der Gesellschaft von Freunden. Claire und Anne begleiteten mich zu einer entspannenden Massage, eine dringend benötigte Erholung von den Frustrationen des Tages.

{{< figure src="./images/parade.jpg" alt="Parade" class="aligne-left size-medium" >}}

Als die Sonne unterging, erwachte Luang Prabang mit dem faszinierenden Glanz des Lichterfestivals zum Leben. Die Strassen waren mit bunten Laternen geschmückt, und ich schloss mich der Menge an, um die lebendige Parade zu verfolgen, die durch die Stadt zog. Die erleuchteten Wagen und traditionellen Kostüme waren ein wahrer Anblick.

{{< figure src="./images/floating_flower.jpg" alt="schwimmende Blume" class="aligne-right size-small" >}}

Das Highlight des Abends war jedoch der ruhige Moment am Flussufer, als ich eine schwimmende Blume ins Wasser setzte. Es war eine symbolische Geste des Loslassens, eine Erinnerung daran, den Fluss des Lebens zu akzeptieren und dem bevorstehenden Weg zu vertrauen.