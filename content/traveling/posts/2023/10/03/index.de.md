+++ 
draft = false
date = 2023-10-03T22:00:00+02:00
title = "Letzte und erste Tage"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

{{< figure src="./images/work_present.jpg" alt="Arbeit Geschenk" class="aligne-right size-smaller" >}}

Die Luft war erfüllt von Vorfreude, eine spürbare Elektrizität, die schien, jede Faser meines Seins zu berühren. Die Uhr tickte lauter, jede vergehende Sekunde fügte sich der zunehmenden Aufregung hinzu. Es war das Gefühl, das durch deine Adern fliesst – eine Mischung aus Nervosität und Euphorie, die den Beginn eines grossen Abenteuers ins Unbekannte ankündigte.

{{< figure src="./images/wedding.jpg" alt="Hochzeit" class="aligne-left size-small" >}}

Mit den vergehenden Tagen wuchs auch die Spannung. Die Realität der bevorstehenden Reise lastete schwer auf meinen Schultern. Die Abschiede waren bittersüss; Umarmungen hielten etwas länger an, und Worte trugen eine unausgesprochene Tiefe. Über die Tage hinweg verabschiedete ich mich von meinen Grosseltern, Eltern und meiner Schwester. Ich spielte und gewann ein letztes unglaubliches Floorball-Spiel und sagte meinen Teamkollegen Lebewohl. Dann verabschiedete ich mich von anderen Freunden und meinen Brüdern, jeder von ihnen hinterliess einen unauslöschlichen Eindruck, übermittelte ihre guten Wünsche, ihre Gesichter ein Mosaik aus Stolz und Besorgnis.

Schliesslich war es soweit. Mit jedem Schritt in Richtung Flughafen spürte ich, wie das Gewicht der Situation auf mich herabfiel. Das war es – die Schwelle zu einem neuen Kapitel, ein Sprung ins Unbekannte. Nachdem es sich wie eine Ewigkeit angefühlt hatte, waren alle Sicherheitskontrollen abgeschlossen, der Rucksack als mein Gepäck aufgegeben und ich sass im Flugzeug. Bereit zum Abflug.

{{< figure src="./images/airplane.jpg" alt="Flugzeug" class="aligne-center size-medium" >}}

Nach dem zehnstündigen Flug nach Bangkok fühlten sich meine Knochen schwer an, und mein schläfriger Geist war überwältigt. Die Reise vom Flughafen zum Hostel war eine Odyssee für sich. Um fünf Uhr morgens nach Bangkoker Zeit rauschten Züge und Busse vorbei, die eine Vielzahl von Leben mit sich führten, jedes eine Geschichte, die darauf wartete, gehört zu werden. Die unbekannten Strassen, das rhythmische Schwingen der Fahrt – es war sowohl desorientierend als auch befreiend.

Endlich kam ich im [Jam Hostel](https://jamhostelbangkok.com/) an, einer Oase des Komforts mitten im urbanen Dschungel. Für einen kurzen Moment stand ich allein da, noch war niemand wach, und auch die Rezeption hatte noch nicht geöffnet.

Doch das hielt nicht lange an. Innerhalb weniger Minuten fand ich mich in Gesprächen mit Fremden wieder, die sich zu sofortigen Gefährten entwickelten. Da war ein Reisender aus den USA und einer aus Australien. In ihren Geschichten fand ich Trost, eine Beruhigung, dass es in diesem Meer der Unbekanntheit auch verwandte Seelen gab, die ihre eigenen Reisen antraten.

Als die anfängliche Besorgnis der Freude über die Verbindung wich, wurde mir klar, dass das die Essenz des Reisens ist. Es geht nicht nur um die Orte, die man sieht, sondern um die Menschen, die man trifft. Das gemeinsame Lachen, der ausgetauschte Rat, die Kameradschaft, die keine Grenzen kennt – es ist die Magie, die Fremde zu Freunden macht.

In diesen Momenten fand ich ein Gefühl der Zugehörigkeit an den unwahrscheinlichsten Orten. Die Spannungen, die mich auf dieser Reise begleitet hatten, begannen sich zu lösen, ersetzt durch ein tiefes Gefühl der Dankbarkeit. Dankbarkeit für den Weg, der mich hierher geführt hatte, für die Gesichter, die meine Begleiter wurden, und für das Versprechen unzähliger weiterer Begegnungen auf dieser Reise ins Unbekannte.

Und so, mit einem Herzen voller Hoffnung und einem Geist, der auf das, was vor mir liegt, begierig war, trat ich vor, bereit, das Unbekannte mit offenen Armen zu empfangen. Die Reise hatte gerade erst begonnen, aber bereits verwandelte sie sich in ein Abenteuer meines Lebens.