+++ 
draft = false
date = 2023-10-03T22:00:00+02:00
title = "Last and first days"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

{{< figure src="./images/work_present.jpg" alt="work present" class="aligne-right size-smaller" >}}

The air was charged with anticipation, a tangible electricity that seemed to touch every fiber of my being. The clock ticked louder, each passing second adding to the mounting excitement. It was the kind of thrill that courses through your veins, a mixture of anxiety and exhilaration, signaling the beginning of a grand adventure into the unknown.

{{< figure src="./images/wedding.jpg" alt="wedding" class="aligne-left size-small" >}}

As the days unfolded, so did the tension. The reality of the impending journey weighed heavily on my shoulders. The farewells were bittersweet; hugs lingered a little longer, and words carried an unspoken depth. Over the days, I said my goodbyes to my grandparents, parents, and sister. Played and won a last incredible game of floorball, said goodbye to my teammates. Then to other friends and my brothers, each one leaving an indelible mark, offered their well-wishes, their faces a mosaic of pride and concern.

Finally, it was time. With every step towards the airport, I could feel the gravity of the situation sink in. This was it – the threshold of a new chapter, a leap into the uncharted. After what felt an eternety all securty checks were done, the backpack given up as my luggage and was sitting in the airplane. Ready for take off.

{{< figure src="./images/airplane.jpg" alt="airplane" class="aligne-center size-medium" >}}

After the ten-hour flight to Bangkok, my bones felt heavy, and my sleepy mind was overwhelmed. The journey from the airport to the hostel was an odyssey in itself. At five o'clock Bangkok time, trains and buses whirred by, carrying with them a mosaic of lives, each one a story waiting to be heard. The unfamiliar streets, the rhythmic sway of the journey – it was both disorienting and liberating.

At last, I arrived at [Jam Hostel](https://jamhostelbangkok.com/), an oasis of comfort amid the urban sprawl. For a brief moment, I stood alone, noone was awake yet and even the reception didn't open yet.

But it didn't last. Within minutes, I found myself immersed in conversations with strangers turned instant companions. There was a traveler from the USA and one from Australia. In their stories, I found solace, a reassurance that in this sea of unfamiliarity, there were kindred spirits navigating their own journeys.

As the initial apprehension gave way to the joy of connection, I realized that this is the essence of travel. It's not just about the places you see, but the people you meet. The shared laughter, the exchanged wisdom, the camaraderie that knows no borders – it's the magic that turns strangers into friends.

In those moments, I found a sense of belonging in the unlikeliest of places. The knots of tension that had accompanied me on this journey began to unravel, replaced by a profound sense of gratitude. Gratitude for the path that led me here, for the faces that became my companions, for the promise of countless more encounters on this voyage into the unknown.

And so, with a heart brimming with hope and a spirit eager for what lies ahead, I stepped forward, ready to embrace the uncharted with open arms. The journey had only just begun, but already, it was transforming into an adventure of a lifetime.
