+++ 
draft = false
date = 2023-10-02T22:00:00+02:00
title = "Vorbereitung"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

# Vorbereitung

## Checkliste

- [x] Flug
- [x] Reisepass
- [x] Impfungen
- [x] Internationaler Führerschein
- [x] Unfallversicherung (1 + 6 Monate mit Zwischenversicherung)
- [x] Alters- und Hinterbliebenenversicherung (sollte den Mindestbeitrag erreichen)
- [x] Rentenversicherung (gegen die Risiken von Tod oder Invalidität)
- [x] Packen
<!-- - [x] ~~Budget~~ -->
<!-- - [x] ~~Visum~~ -->
<!-- - [x] ~~Krankenversicherung~~ -->
<!-- - [x] ~~Reiseversicherung~~ -->

<details>
<summary>Packliste</summary>

- [x] Kleidung und Schuhe
    - [x] Kappe
    - [x] Lange, leichte Hose
    - [x] Flip-Flops
    - [x] Wäschebeutel
    - [x] Shorts
    - [x] Sneaker
    - [x] Socken
    - [x] Sonnenbrille
    - [x] Badebekleidung & Handtuch
    - [x] Dünnes Langarmshirt
    - [x] *Dünne Regenjacke*
    - [x] T-Shirts
    - [x] Unterwäsche
- [x] Elektronik
    - [x] Kamera
    - [x] *Ladegeräte*
    - [x] Taschenlampe
    - [x] *Kopfhörer*
    - [x] *Laptop*
    - [x] *Handy*
    - [x] Powerbank
    - [x] Reise-Steckdosenadapter
    <!-- - [ ] Steckdosenleiste -->
- [x] Persönlicher Komfort
    - [x] *Rucksack*
    - [x] Bücher
    - [x] Kartenspiele
    - [x] Dry Bag
    - [x] *Ohrstöpsel*
    - [x] Seiden-Schlafsack
    - [x] Schloss
    - [x] *Schlafmaske*
- [x] Kulturbeutel
    - [x] After-Sun
    - [x] Kontaktlinsen (40 + 16) -> Linsenmittel
    - [x] Deo
    - [x] Nagelpflege
    - [x] Rasierer
    - [x] Duschgel
    - [x] Ersatzbrille
    - [x] Sonnenschutz
    - [x] Zahnbürste
    - [x] Zahnpasta
- [x] Reisedokumente
    - [x] Kopie der Reisedokumente (digital gespeichert)
    - [x] *Kreditkarte*
    - [x] Führerschein & internationaler Führerschein
    - [x] *Flugtickets*
    - [x] ***Gültiger Reisepass***
- [x] Reiseapotheke
    - [x] Durchfallmittel (z.B. Imodium Akut, Kohletabletten)
    - [x] Mückenspray (z.B. Nobite, Autan, mit DEET* oder ähnlichem Inhaltsstoff)
    - [x] Pflaster
    - [x] Handdesinfektionsmittel
    - [x] Malariaprophylaxe
    - [x] Pinzette
    
</detail>