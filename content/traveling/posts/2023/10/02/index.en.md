+++ 
draft = false
date = 2023-10-02T22:00:00+02:00
title = "Preperation"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

# Preperation

## Checklist

- [x] flight
- [x] passport
- [x] vaccination
- [x] international driving licence
- [x] accident insurance (1 + 6 months with interim insurance) 
- [x] old-age and survivors's insurance (should reach minimum contribution)
- [x] pension fund (against the risks of death or disability)
- [x] packing
<!-- - [x] ~~budget~~ -->
<!-- - [x] ~~visa~~ -->
<!-- - [x] ~~health insurance~~ -->
<!-- - [x] ~~travel insurance~~ -->

<details>
<summary>Packing list</summary>

- [x] Clothing and Shoes
    - [x] Cap
    - [x] Long, lightweight pants
    - [x] Flip Flops
    - [x] Laundry bag
    - [x] Shorts
    - [x] Sneakers
    - [x] Socks
    - [x] Sunglasses
    - [x] Swimwear & Towel
    - [x] Thin long-sleeve shirt
    - [x] *Thin rain jacket*
    - [x] T-Shirts
    - [x] Underwear
- [x] Electronics
    - [x] Camera
    - [x] *Chargers*
    - [x] Flashlight
    - [x] *Headphones*
    - [x] *Laptop*
    - [x] *Mobile phone*
    - [x] Power bank
    - [x] Travel plug adapter
    <!-- - [ ] Power strip -->
- [x] Personal Comfort
    - [x] *Backpack*
    - [x] Books
    - [x] Card games 
    - [x] Dry bag
    - [x] *Earplugs*
    - [x] Silk sleeping bag
    - [x] Lock
    - [x] *Sleep mask*
- [x] Toiletry bag
    - [x] After sun
    - [x] Contact lenses (40 + 16) -> linsenmittel
    - [x] Deodorant
    - [x] Nail care
    - [x] Razor
    - [x] Shower gel
    - [x] Spare glasses
    - [x] Sunscreen
    - [x] Toothbrush
    - [x] Toothpaste
- [x] Travel Documents
    - [x] Copy of Travel Documents (digitally stored)
    - [x] *Credit card*
    - [x] Driver's license & international driver's license
    - [x] *Flight ticket*
    - [x] ***Valid passport***
- [x] Travel Pharmacy
    - [x] Anti-diarrheal medication (e.g., Imodium Akut, charcoal tablets)
    - [x] Anti-mosquito spray (e.g., Nobite, Autan, with DEET* or similar ingredient)
    - [x] Band-aids
    - [x] Hand sanitizer
    - [x] Malaria prophylaxis
    - [x] Tweezers
    
</detail>
