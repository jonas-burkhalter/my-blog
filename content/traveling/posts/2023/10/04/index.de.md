+++ 
draft = false
date = 2023-10-04T22:00:00+02:00
title = "Bangkok"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Mittwoch: Den Jetlag überwinden

Die Ankunft am Mittwoch – der Jetlag legte sich wie ein schwerer Mantel über mich und zog mich in eine Welt der schläfrigen Verwunderung. Vom Flughafen brachte mich ein Wirbelwind aus Zügen und Bussen ins Herz der Stadt, jede Station ein Kontrollpunkt auf der Landkarte des Abenteuers.

Nach einer kurzen Pause im Hostel und noch etwas benommen, begann ich, die Stadt allein zu erkunden.

{{< figure src="./images/big_budda.jpg" alt="Grosser Buddha" class="aligne-left size-small" >}}

Der Chao Phraya Fluss begrüsste mich mit offenen Armen, und ich machte eine Bootstour, die das lebendige Gewebe von Bangkok enthüllte. Die verschiedenen Buddha-Statuen ragten hoch empor und verströmten eine Aura der Ruhe, die schien, in die Luft selbst einzusickern.

{{< figure src="./images/tuk_tuk.jpg" alt="Tuk Tuk" class="aligne-right size-smaller" >}}

Mit dem Untergang der Sonne kam der Nervenkitzel meiner ersten Tuk-Tuk-Fahrt, die mich durch die belebten Strassen schickte, jede Kurve ein Versprechen neuer Entdeckungen. Tempel flüsterten Geheimnisse längst vergangener Zeiten, ihre kunstvolle Schönheit ein Zeugnis der Kunstfertigkeit antiker Hände.

{{< figure src="./images/kra_pao.jpg" alt="Kra Pao" class="aligne-left size-smaller" >}}

Der Tag gipfelte in einem Festmahl aus authentischer thailändischer Küche, geteilt mit neuen Freunden aus dem Hostel. Lachen und Geschichten vermischten sich mit dem Aroma von Gewürzen und schufen eine Erinnerung, die noch lange nach dem Abräumen der Teller nachhallen würde.

## Donnerstag: Nachtmärkte und Lichter der Stadt

Donnerstag brach an, der Jetlag war nun nur noch ein leises Flüstern im Hintergrund. Wir machten uns auf, die Stadt zu erkunden, gingen einfach durch die Strassen, ohne einen klaren Plan, was wir sehen wollten.

{{< figure src="./images/pad_thai.jpg" alt="Pad Thai" class="aligne-left size-medium" >}}

Als der Hunger kam, entschieden wir uns, uns mit einem Michelin-Stern-Pad-Thai zu verwöhnen, einem kulinarischen Meisterwerk, das Street Food zu einer Kunstform erhob.

Das MBK Shopping Center war ein Mekka für Shopping-Fans, seine lebhaften Gänge boten alles, von trendiger Mode bis zu kuriosen Souvenirs. Es fühlte sich wie ein endloses Gebäude mit Shop an Shop an, bevor wir uns entschlossen, das Einkaufen aus einer anderen Perspektive zu betrachten.

{{< figure src="./images/algae.jpg" alt="Algen" class="aligne-right size-small" >}}

Der Nachtmarkt lockte, ein Kaleidoskop aus Farben und Geräuschen, das den Rahmen für einen Abend der Entdeckungen bildete. Viele neue Lebensmittel wurden probiert, wie grüner Mango, Algen und Durian. 

Als der Tag sich dem Ende neigte, rief die Dachterrassenbar des Hostels nach uns, mit Bier und einigen lustigen Runden "Wahrheit oder trinken" mit Leuten, mit denen ich vorher nie gesprochen hatte.

## Freitag: Durch die Zeit und Tradition

{{< figure src="./images/thompson_house.jpg" alt="Jim Thompson Haus" class="aligne-left size-medium" >}}

Der Freitag begann mit einem Besuch des Slam Markts, ein Eintauchen in den Kern des lokalen Handels. Das Jim Thompson Haus war ein Portal in die Vergangenheit, ein Blick in das Leben des amerikanischen Unternehmers, der Thailand in der Seidenindustrie einen unauslöschlichen Stempel aufdrückte.

Chinatown entfaltete sich wie ein Wandteppich aus Traditionen, seine engen Gassen gesäumt von Ständen, die ein Fest für die Sinne boten. Eine Bootsfahrt auf dem Fluss gab uns eine andere Perspektive auf die Stadt, ein Blick auf Bangkoks Verschmelzung von Alt und Neu.

Der Abend brachte die donnernde und brutale Welt des Muay Thai, eine Schau von Können und Geist, die mich in Ehrfurcht vor den Athleten zurückliess, die diese jahrhundertealte Tradition verkörperten. Es war eine rohe und unverschämte Demonstration körperlicher Kraft, ein Zeugnis der unnachgiebigen Natur des Kampfsports.

{{< figure src="./images/muay_thai_arena.jpg" alt="Muay Thai Arena" class="aligne-left size-medium" >}}
{{< figure src="./images/muay_thai_fight.jpg" alt="Muay Thai Kampf" class="aligne-right size-medium" >}}

## Samstag: Balance zwischen Erkundung und Reflexion

{{< figure src="./images/floating_market.jpg" alt="Schwimmender Markt" class="aligne-right size-medium" >}}

Der Samstag war ein Tag der Balance, des Planens für die kommenden Abenteuer und des Geniessens des gegenwärtigen Moments. Ein Besuch des schwimmenden Marktes war ein Sprung in das Herz des thailändischen Handels. Mit köstlichem Street Food und einer Bootsfahrt durch die Kanäle.

Mit dem stetigen Klicken der Tasten begann ich, meine Notizen der vergangenen Tage niederzuschreiben. Diese wurden dann an OpenAI übergeben, um die Geschichten zu formen, die schliesslich diese digitalen Seiten zieren würden.