+++ 
draft = false
date = 2023-10-04T22:00:00+02:00
title = "Bangkok"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Wednesday: Navigating the Jet Lag Haze
<!--
- jet lag
- flughafen zu hostel (zug und bus fahren)
- boot tour
- big budda
- lucky budda
- first tuktuk ride
- tempel
- really good thai food dinner with the people from the hostel 
-->

Arriving on Wednesday, jet lag wrapped its arms around me, pulling me into a world of drowsy wonder. From the airport, a whirlwind of trains and buses carried me to the heart of the city, each station a checkpoint on the map of adventure.

After a short break at the hostel and still a little stunned I started exploring the city on my own. 

{{< figure src="./images/big_budda.jpg" alt="big budda" class="aligne-left size-small" >}}

The Chao Phraya River welcomed me with open arms, and I embarked on a boat tour that unveiled the vibrant tapestry of Bangkok. The various Buddha statues stood tall, emanating an aura of tranquility that seemed to seep into the very air.

{{< figure src="./images/tuk_tuk.jpg" alt="Tuk Tuk" class="aligne-right size-smaller" >}}

With the setting sun came the thrill of my first tuktuk ride, weaving through the bustling streets, each turn a promise of new discoveries. Temples whispered secrets of centuries gone by, their ornate beauty a testament to the artistry of ancient hands. 

{{< figure src="./images/kra_pao.jpg" alt="kra pao" class="aligne-left size-smaller" >}}

The day culminated in a feast of authentic Thai cuisine, shared with newfound friends from the hostel. Laughter and stories mingled with the aroma of spices, creating a memory that would linger long after the plates were cleared.

## Thursday: Night Markets and City Lights
<!--
- night market
- mbk shopping center
- public boat 
- roof top bar of hostel
- Michelin-star pad thai 
-->

Thursday dawned, the jet lag now a mere whisper in the background. We wonderd of to explore the city, walking around without a clear plan what to see. 

{{< figure src="./images/pad_thai.jpg" alt="pad thai" class="aligne-left size-medium" >}}

When the hunger came we decided to give us a thread with a Michelin-star Pad Thai, a culinary masterpiece that elevated street food to an art form.

The MBK Shopping Center was a mecca for shopaholics, its bustling aisles offering everything from trendy fashion to quirky souvenirs. What felt like a never ending buildings with shop after shop, just before going to see shopping from an other side.

{{< figure src="./images/algae.jpg" alt="algae" class="aligne-right size-small" >}}

The night market beckoned, a kaleidoscope of colors and sounds that set the scene for a evening of exploration. Lots of new food was tasted, like green mango, algae and durian. 
As the day came to an end the rooftop bar of the hostel called for a with beers and some funny rounds of truth or drink with people I had never spoken to before.

## Friday: Traversing Time and Tradition
<!-- 
- slam market
- jim thompsen house
- china town
- river boat
- muay thai 
-->

{{< figure src="./images/thompson_house.jpg" alt="Jim Thompson house" class="aligne-left size-medium" >}}

Friday beckoned with a visit to the Slam Market, an immersion into the heart of local commerce. The Jim Thompson House was a portal to the past, a glimpse into the life of the American entrepreneur who left an indelible mark on Thailand's silk industry. 

Chinatown unfolded like a tapestry of traditions, its narrow alleys lined with stalls offering a sensory feast. A river boat ride provided a different perspective of the city, a view of Bangkok's juxtaposition of ancient and modern.

The evening brought the thunderous and brutal world of Muay Thai, a display of skill and spirit that left me in awe of the athletes who embodied this centuries-old tradition. It was a raw and unapologetic showcase of physical prowess, a testament to the unforgiving nature of combat sports.

{{< figure src="./images/muay_thai_arena.jpg" alt="muay thai arena" class="aligne-left size-medium" >}}
{{< figure src="./images/muay_thai_fight.jpg" alt="muay thai fight" class="aligne-rigth size-medium" >}}

## Saturday: Balancing Exploration and Reflection
<!-- 
- planning the next days
- floating market
- start writing blog posts 
-->

{{< figure src="./images/floating_market.jpg" alt="floating market" class="aligne-right size-medium" >}}

Saturday was a day of balance, of planning the adventures that lay ahead and basking in the present moment. A visit to the floating market was a plunge into the heart of Thai commerce. With delicuse street food and a boat ride through the canals.

With the steady click of keys, I began to write my notes down of the past days. These were then given to OpenAI to form the tales that would eventually grace these digital pages.
