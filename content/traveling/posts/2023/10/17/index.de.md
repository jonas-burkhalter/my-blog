+++ 
draft = false
date = 2023-10-17T22:00:00+02:00
title = "Thailand nach Laos"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Dienstag: Pai nach Chiang Rai

Die Berge von Pai hinter uns lassend, begann unsere Reise nach Chiang Rai in einem voll besetzten Minivan. Die Gruppe wuchs unerwartet, was zu etwas Improvisation bei den Sitzplatzarrangements führte, wobei sich eine abenteuerlustige Person für den Boden entschied.

Bei unserer Ankunft in Chiang Rai war unsere erste Aufgabe, uns im Hostel einzurichten und Tickets für das bevorstehende Slowboat-Abenteuer zu sichern. Nachdem wir diese Aufgaben erledigt hatten, machten wir uns auf den Weg zum faszinierenden Blauen Tempel.

Später, als der Hunger einsetzte, trafen wir eine unkonventionelle, aber bequeme Entscheidung zum Abendessen – einen Abstecher zu den vertrauten Gefilden von Pizza Hut.

## Mittwoch: Grenzübergang

Unser Tag begann mit dem sanften Schein der Morgendämmerung, als wir in einen Minivan stiegen, dessen Motor das frühe Morgenstillen durchbrach. Es war ein frühes Aufstehen, genau um 5 Uhr morgens, als wir uns auf den Weg zum Grenzübergang machten.

Der Grenzübertritt nach Laos hielt sein eigenes Abenteuer bereit. Während einige unserer Begleiter den Visumprozess durchliefen, hatte ich das unerwartete Glück, dass kein Visum erforderlich war. Dieser Moment der Erleichterung war jedoch nur von kurzer Dauer, da das, was als Nächstes passierte, unerwartet war.

Ein plötzlicher Ruck und ein Quietschen brachten uns zum abrupten Halt – eine kleinere Kollision mit einem anderen Fahrzeug nur 10 Minuten nach der Einreise nach Laos. Es war eine Erinnerung an die unvorhersehbare Natur des Reisens, ein scharfer Kontrast zu den ruhigen Landschaften, die uns erwarteten.

{{< figure src="./images/slowboat.jpg" alt="Slowboat" class="aligne-left size-small" >}}

Nachdem wir das Vorfall hinter uns gelassen hatten, machten wir uns auf den Weg zum Einschiffungspunkt für das Slowboat. Das Schiff stand hoch, mit einem Holzaussen, das von zahllosen Reisen auf dem Mekong gezeichnet war. Als wir uns niederliessen, breitete sich ein Gefühl der Kameradschaft über unsere vielfältige Gruppe aus. Gespräche flossen, Geschichten vermischten sich, und Lachen hallte über den weiten Fluss.

Die Slowboat-Fahrt war buchstäblich eine gemächliche. Etwa sieben Stunden drifteten wir dahin, die vorbeiziehende Landschaft ein faszinierendes Tableau aus üppigem Grün und vereinzeltem Flussleben. Unter den Reisenden bot ein Fremder uns einen Bissen von einer seltsamen Frucht an. Zusammen mit [Linn](https://www.instagram.com/linnokolo), einer Mitreisenden, die ebenfalls kulinarische Abenteuer liebte, kosteten wir die Frucht, deren Geschmack eine einzigartige Mischung aus Säure und Süsse war.

Als der Abend hereinbrach, erreichten wir Pak Paeng, unsere gewählte Unterkunft für die Nacht. Das Hostel, eine weite Unterkunft, empfing uns mit offenen Türen. Ein 16-Bett-Schlafsaal wartete, jedes Bett ein Versprechen auf Ruhe nach einem Tag voller unerwarteter Wendungen.

{{< figure src="./images/pak_paeng.jpg" alt="Pak Paeng" class="aligne-center size-large" >}}

## Donnerstag: Die Slowboat-Fahrt geht weiter

Am nächsten Morgen kehrten wir wieder in die sanfte Umarmung des Slowboats zurück, bereit, unsere Reise fortzusetzen. Der vertraute Rhythmus des Flusses und das beruhigende Surren des Motors begrüssten uns. Ich nahm meinen Platz neben Claire ein, fand Trost in der stillen Reflexion und führte lebhafte Gespräche gleichermassen. Wieder einmal gaben wir uns dem Fluss des Mekongs hin, unsere Sinne auf die sich ständig verändernde Landschaft ausgerichtet.

Als der Tag sich dem Ende zuneigte, lockte Luang Prabang am Horizont. Die antike Stadt, tief verwurzelt in Geschichte und Tradition, versprach neue Abenteuer und Erfahrungen. Unsere Reise war lang, aber sie war ein Gewebe aus Fäden der Kameradschaft, Entdeckung und der anhaltenden Schönheit Südostasiens.