+++ 
draft = false
date = 2023-10-17T22:00:00+02:00
title = "Thailand to Laos"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Tuesday: Pai to Chiang Rai
<!-- 
mini van ride from pai to chiang rai
private group
10 people
5 to go to laos 

blue temple
-->

{{< figure src="./images/blue_temple.jpg" alt="blue temple" class="aligne-right size-small" >}}

Leaving behind the tranquil mountains of Pai, our journey to Chiang Rai unfolded in a packed minivan. The group grew unexpectedly, leading to a bit of improvisation for seating arrangements, with one adventurous soul opting for the floor.

Upon our arrival in Chiang Rai, our first mission was to settle into the hostel and secure tickets for the forthcoming slowboat adventure. With these tasks checked off, we ventured to the captivating Blue Temple. 

Later, as hunger pangs set in, we made an unconventional yet comforting choice for dinner - a detour to the familiar confines of Pizza Hut.

## Wednesday: Crossing boarder
<!-- 
early driving to the boarder with a minivan (start at 5am) 
crossing to laos (waiting for the others to get a visa, since i didn't need one)

Car crash 10min in laos

entering the slowboat
chilling, talking enjoying the scenery
about 7 hour ride
Strange fruite from stranger

hostel in pak paeng
16 bed room
-->

Our day started with the soft glow of dawn as we boarded a minivan, the engine's hum punctuating the early morning stillness. It was an early rise, 5 AM sharp, as we set forth towards the border.

The border crossing into Laos held its own sense of adventure. While some of our companions navigated the visa process, I found myself with an unforeseen stroke of luck - no visa required. This moment of reprieve was short-lived, as what unfolded next was unexpected.

A sudden jolt and screech brought us to an abrupt halt, a minor collision with another vehicle just 10 minutes into Laos. It was a reminder of the unpredictable nature of travel, a stark contrast to the serene landscapes that awaited us.

{{< figure src="./images/slowboat.jpg" alt="slowboat" class="aligne-left size-small" >}}

With the incident behind us, we made our way to the slowboat embarkation point. The vessel stood tall, its wooden exterior weathered by countless journeys down the Mekong. As we settled in, a sense of camaraderie settled over our diverse group. Conversations flowed, stories intertwined, and laughter echoed against the backdrop of the sprawling river.

The slowboat journey was, quite literally, a leisurely one. For about seven hours, we drifted along, the passing scenery a mesmerizing tableau of lush greenery and occasional river life. Amidst the travelers, a stranger offered a taste of a peculiar fruit. Together with [Linn](https://www.instagram.com/linnokolo), a fellow foodie delighting in shared culinary adventures, we tasted the fruit, its flavor a unique blend of tartness and sweetness.

As evening descended, we reached Pak Paeng, our chosen haven for the night. The hostel, a sprawling abode, welcomed us with open doors. A 16-bed dorm awaited, each bunk a promise of rest after a day filled with unexpected turns.

{{< figure src="./images/pak_paeng.jpg" alt="pak paeng" class="aligne-center size-large" >}}

## Thursday: The Slowboat continues
<!-- 
on the slowboat again
chilling, talking enjoying the scenery, sleeping
again about 7 hour ride

arraiving in luang prabang

was a nice but long experiance
 -->
The next morning, we returned to the gentle embrace of the slowboat, ready to resume our voyage. The familiar rhythm of the river and the soothing hum of the engine greeted us. I took my seat next to Claire, finding solace in the quiet reflection and engaging in lively conversation in equal measure. Once again, we surrendered to the flow of the Mekong, our senses attuned to the ever-changing scenery

As the day drew to a close, Luang Prabang beckoned on the horizon. The ancient town, steeped in history and tradition, promised new adventures and experiences. Our journey had been long, but it had been a tapestry woven with threads of camaraderie, discovery, and the enduring beauty of Southeast Asia.
