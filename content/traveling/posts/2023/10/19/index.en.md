+++ 
draft = false
date = 2023-10-19T22:00:00+02:00
title = "Luang Prabang"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Thursday: Arriving in Luang Prabang
<!-- 
arriving in luang prabang after 2 day slowboat trip

night market, the common source of the dinner
 -->

The sun had barely begun its descent when we arrived in the serene town of Luang Prabang, following a two-day journey along the mystical Mekong River. Nestled between emerald hills and flanked by the gentle flow of the river, Luang Prabang felt like a place where time slowed down.

{{< figure src="./images/night_marked.jpg" alt="night marked" class="aligne-left size-medium" >}}

Eager to stretch our legs after the slowboat's embrace, we ventured into the vibrant night market. Here, Laotian crafts and delicacies unfolded in a kaleidoscope of colors and aromas. It became our communal meeting point, a place where shared experiences of the day found culmination in the delight of local cuisine.

## Friday: Unexpected Turns and Serene Cascades
<!-- 
getting inspired by others plans

waterfall
tuk tuk breaks down
nice view and even better to enjoy in the lagune on top

night market, the common source of the dinner
-->
<!-- todo -->
{{< figure src="./images/tuk_tuk.jpg" alt="tuk tuk breakdown" class="aligne-left size-small" >}}

Today, our path led us to a breathtaking waterfall, a testament to nature's grandeur. However, fate had its own quirky sense of timing. Our trusty tuk-tuk chose the middle of nowhere to stage a protest, leaving us stranded. With no immediate solution, we waited for another tuk-tuk to pick us up.

We made the best of the situation, as we always did on this journey. Claire and I decided to climb on top of the tuk-tuk, taking in the surrounding scenery while we waited for another tuk-tuk to arrive. Our unexpected pit stop turned into a unique adventure, a story to add to the ever-growing tapestry of our travels.

{{< figure src="./images/waterfall.jpg" alt="waterfall" class="aligne-right size-small" >}}

Undeterred, we embraced the setback, finding resilience in the face of adversity. Soon, we reached the pinnacle of the Kuang Si waterfall, where a crystalline lagoon awaited, inviting us to surrender to its tranquil depths. The view was spectacular, but it was the feeling of being immersed in this natural wonder that truly took our breath away.

As dusk painted the sky, we returned to the welcoming embrace of the night market. Here, amidst the vibrant stalls and the hum of local life, we shared stories of the day's adventures, grateful for the twists and turns that had enriched our journey.

{{< figure src="./images/ice_cream.jpg" alt="ice cream" class="aligne-center size-medium" >}}

## Saturday: A Day of Insight and Farewells
<!-- 
uxo 

ethical art and culture museum

a sandwich from home (with proper bread)

chilling

traditional laos massage

big brother mouse

night market
saying goodbye to some good fiends i made
 -->

### UXO Information Center

As Saturday dawned, I embarked on a journey that would leave an indelible mark on my understanding of Laos' history. My first destination was the UXO Information Center, a place of somber reflection and a testament to the nation's resilience. Laos holds the painful distinction of being one of the most heavily bombed countries in the world, with a staggering 270 million bombs dropped between 1964 and 1973.

{{< figure src="./images/uxo.jpg" alt="uxo" class="aligne-left size-medium" >}}

Inside the center, I was confronted with the stark reality: around 80 million unexploded bomblets still linger in the Laotian landscape, casting a shadow over daily life, particularly in rural areas where agriculture sustains livelihoods. Tragically, this lethal legacy claims nearly one life every two weeks due to accidental encounters.

The UXO Information Center served as an eye-opening experience, enlightening me about this critical issue. Through informative exhibits and poignant stories, it shed light on the tireless efforts of organizations and individuals dedicated to clearing these deadly remnants. 

### Traditional Arts and Ethnology Centre

{{< figure src="./images/art_and_ethnic.jpg" alt="traditional arts and ethnology centre" class="aligne-right size-medium" >}}

From there, I delved into the Traditional Arts and Ethnology Centre, a treasure trove of Laotian heritage, where each exhibit seemed to breathe life into centuries past. The centre provided a vivid tapestry of Laos' rich cultural diversity, showcasing the artistic expressions and traditions that have shaped the nation's identity. It offered a fascinating glimpse into the lives and customs of Laos' four main ethnic groups: the Akha, (H)mong, Tai Dam, and Kmhmu. Each group brings its own unique traditions, clothing, and ways of life, contributing to the diverse and harmonious mosaic of Laotian culture.

### Feel good afternoon

{{< figure src="./images/sandwich.jpg" alt="sandwich" class="aligne-left size-small" >}}

A touch of familiarity came in the form of a sandwich, crafted with care and featuring the finest, freshly baked bread. The afternoon drifted by in a leisurely haze. I met up with Claire and [Yuho](https://www.instagram.com/operation_ho_ho_ho/), and together we headed to a traditional Laos massage. This ancient practice was a soothing balm for body and soul, providing a perfect interlude to our day. As the sun began its descent, we found ourselves at 'Big Brother Mouse', a heartwarming initiative that emphasized the transformative power of literacy in local communities. Our purpose for visiting was to interact with local individuals, engaging them in English conversations to help them practice the language. This not only fosters language skills but also promotes cultural exchange and mutual understanding. Big Brother Mouse, with its focus on education and community-building, provided an ideal platform for this meaningful interaction.

### Night market

As night fell, the bustling night market provided a fitting backdrop for farewells. Newfound friends bid each other goodbye, their hearts brimming with gratitude for the connections forged during our time in Luang Prabang.

## Sunday: Reflections and Onward Journeys
<!-- 
chill day

family bakery
good bye

vietnam visa

taking the train to Vang Vieng
 -->

Sunday granted us a slower pace, a day for quiet reflection and fond farewells. The family bakery, a quaint establishment, offered comfort in the form of warm, freshly baked goods. With a heavy heart, I said my goodbyes, knowing that the memories and friendships would forever hold a place in my journey.

Practical matters beckoned, and a visit to secure a Vietnam visa was in order. In the afternoon, I boarded a train bound for Vang Vieng, eager for the next chapter of adventures to unfold.

{{< figure src="./images/train.jpg" alt="train" class="aligne-center size-medium" >}}
