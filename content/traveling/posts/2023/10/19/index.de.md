+++ 
draft = false
date = 2023-10-19T22:00:00+02:00
title = "Luang Prabang"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien"]
+++

## Donnerstag: Ankunft in Luang Prabang

Die Sonne hatte kaum ihren Abstieg begonnen, als wir in der ruhigen Stadt Luang Prabang ankamen, nach einer zweitägigen Reise entlang des mystischen Mekong. Eingebettet zwischen smaragdgrünen Hügeln und flankiert vom sanften Fluss, fühlte sich Luang Prabang wie ein Ort an, an dem die Zeit langsamer verging.

{{< figure src="./images/night_marked.jpg" alt="Nachtmarkt" class="aligne-left size-medium" >}}

Voller Vorfreude, unsere Beine nach der Umarmung des Slowboats zu dehnen, begaben wir uns auf den lebhaften Nachtmarkt. Hier entfalten sich laotische Handwerkskunst und Delikatessen in einem Kaleidoskop aus Farben und Aromen. Der Markt wurde zu unserem gemeinsamen Treffpunkt, einem Ort, an dem die geteilten Erlebnisse des Tages ihren Höhepunkt in der Freude über die lokale Küche fanden.

## Freitag: Unerwartete Wendungen und ruhige Wasserfälle

Heute führte uns unser Weg zu einem atemberaubenden Wasserfall, einem Zeugnis der Grösse der Natur. Doch das Schicksal hatte seinen eigenen, eigenwilligen Sinn für Timing. Unser treues Tuk-Tuk entschied sich mitten im Nirgendwo, gegen uns zu protestieren, und liess uns gestrandet zurück. Ohne eine sofortige Lösung warteten wir darauf, dass ein anderes Tuk-Tuk uns abholte.

{{< figure src="./images/tuk_tuk.jpg" alt="Tuk Tuk Panne" class="aligne-left size-small" >}}

Wir machten das Beste aus der Situation, wie wir es auf dieser Reise immer taten. Claire und ich entschieden uns, auf das Tuk-Tuk zu klettern und die umliegende Landschaft zu geniessen, während wir auf das nächste Tuk-Tuk warteten. Unsere unerwartete Pause verwandelte sich in ein einzigartiges Abenteuer, eine Geschichte, die zu dem immer grösser werdenden Gewebe unserer Reisen hinzukam.

{{< figure src="./images/waterfall.jpg" alt="Wasserfall" class="aligne-right size-small" >}}

Unbeeindruckt nahmen wir den Rückschlag an und fanden Resilienz im Angesicht der Widrigkeiten. Bald erreichten wir den Gipfel des Kuang Si Wasserfalls, wo uns eine kristallklare Lagune empfing, die uns einlud, uns in ihre ruhigen Tiefen zu begeben. Der Blick war spektakulär, aber es war das Gefühl, in diesem Naturwunder eingetaucht zu sein, das uns wirklich den Atem raubte.

Als die Dämmerung den Himmel malte, kehrten wir in die einladende Umarmung des Nachtmarkts zurück. Hier, zwischen den lebhaften Ständen und dem Summen des lokalen Lebens, teilten wir Geschichten über die Abenteuer des Tages und waren dankbar für die Wendungen, die unsere Reise bereichert hatten.

{{< figure src="./images/ice_cream.jpg" alt="Eiscreme" class="aligne-center size-medium" >}}

## Samstag: Ein Tag der Erkenntnisse und Abschiede

### UXO Informationszentrum

Als der Samstag anbrach, begab ich mich auf eine Reise, die einen bleibenden Eindruck auf mein Verständnis der Geschichte von Laos hinterlassen sollte. Mein erstes Ziel war das UXO Informationszentrum, ein Ort der ernsten Reflexion und ein Zeugnis der Widerstandskraft des Landes. Laos trägt die schmerzhafte Auszeichnung, eines der am stärksten bombardierten Länder der Welt zu sein, mit erschreckenden 270 Millionen Bomben, die zwischen 1964 und 1973 abgeworfen wurden.

{{< figure src="./images/uxo.jpg" alt="UXO" class="aligne-left size-medium" >}}

Im Inneren des Zentrums wurde ich mit der erschreckenden Realität konfrontiert: Etwa 80 Millionen nicht explodierte Bomben liegen noch immer in der laotischen Landschaft und werfen ihren Schatten auf das tägliche Leben, besonders in ländlichen Gebieten, in denen die Landwirtschaft das Leben sichert. Tragischerweise fordert dieses tödliche Erbe fast alle zwei Wochen ein Leben aufgrund unbeabsichtigter Begegnungen.

Das UXO Informationszentrum war eine aufschlussreiche Erfahrung, die mich über dieses kritische Thema aufklärte. Durch informative Ausstellungen und bewegende Geschichten wurde das unermüdliche Engagement von Organisationen und Einzelpersonen hervorgehoben, die sich der Beseitigung dieser tödlichen Überreste widmen.

### Zentrum für traditionelle Kunst und Ethnologie

{{< figure src="./images/art_and_ethnic.jpg" alt="Zentrum für traditionelle Kunst und Ethnologie" class="aligne-right size-medium" >}}

Von dort aus begab ich mich ins Zentrum für traditionelle Kunst und Ethnologie, einen Schatz der laotischen Erbe, wo jedes Exponat das Leben vergangener Jahrhunderte zum Leben erweckte. Das Zentrum bot ein lebendiges Bild der kulturellen Vielfalt von Laos und zeigte die künstlerischen Ausdrucksformen und Traditionen, die die Identität des Landes geprägt haben. Es gab einen faszinierenden Einblick in das Leben und die Bräuche der vier wichtigsten ethnischen Gruppen von Laos: die Akha, (H)mong, Tai Dam und Kmhmu. Jede Gruppe bringt ihre eigenen einzigartigen Traditionen, Kleidung und Lebensweisen mit, die zur vielfältigen und harmonischen Mosaik der laotischen Kultur beitragen.

### Wohlfühl-Nachmittag

{{< figure src="./images/sandwich.jpg" alt="Sandwich" class="aligne-left size-small" >}}

Ein Hauch von Vertrautem kam in Form eines Sandwichs, das mit Sorgfalt zubereitet und mit dem besten frisch gebackenen Brot serviert wurde. Der Nachmittag verflog in einer gemütlichen Haze. Ich traf mich mit Claire und [Yuho](https://www.instagram.com/operation_ho_ho_ho/), und zusammen gingen wir zu einer traditionellen laotischen Massage. Diese alte Praxis war ein beruhigendes Heilmittel für Körper und Seele und bot eine perfekte Unterbrechung unseres Tages. Als die Sonne ihren Abstieg begann, fanden wir uns bei „Big Brother Mouse“, einer herzergreifenden Initiative, die die transformative Kraft der Alphabetisierung in den lokalen Gemeinschaften betonte. Unser Ziel war es, mit den lokalen Menschen zu interagieren und sie in englischen Gesprächen zu unterstützen, um ihre Sprachkenntnisse zu üben. Dies fördert nicht nur die Sprachfähigkeiten, sondern auch den kulturellen Austausch und das gegenseitige Verständnis. „Big Brother Mouse“, mit seinem Fokus auf Bildung und Gemeinschaftsaufbau, bot eine ideale Plattform für diese bedeutungsvolle Interaktion.

### Nachtmarkt

Als die Nacht hereinbrach, bot der lebhafte Nachtmarkt den perfekten Hintergrund für Abschiede. Neu gewonnene Freunde verabschiedeten sich, ihre Herzen erfüllt von Dankbarkeit für die Verbindungen, die während unserer Zeit in Luang Prabang geknüpft wurden.

## Sonntag: Reflexionen und Weiterreisen

Sonntag schenkte uns ein langsameres Tempo, einen Tag für stille Reflexion und herzliche Abschiede. Die Familienbäckerei, ein malerisches Geschäft, bot Trost in Form von warmen, frisch gebackenen Leckereien. Mit schwerem Herzen verabschiedete ich mich, wohl wissend, dass die Erinnerungen und Freundschaften immer einen Platz in meiner Reise haben würden.

Praktische Dinge riefen, und so stand der Besuch zur Sicherung eines Vietnam-Visums auf dem Plan. Am Nachmittag stieg ich in einen Zug nach Vang Vieng, gespannt auf das nächste Kapitel meiner Abenteuer.

{{< figure src="./images/train.jpg" alt="Zug" class="aligne-center size-medium" >}}