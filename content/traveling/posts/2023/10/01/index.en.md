+++ 
draft = false
date = 2023-10-01T22:00:00+02:00
title = "Traveling south east asia"
description = "Overview and ideas"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

# Overview
<iframe 
    src="https://www.google.com/maps/d/embed?mid=11LXriO5dxSju08ggypv8xdBmCtttknA&ehbc=2E312F&noprof=1" 
    width="100%"
    height="500">
</iframe>

# What to do in south east asia

## General ideas
- climbing
- diving
- jungle track
- massage
- streed food
- surfing
- yoga

## Cambodia
- Battambang
  - bamboo train
- Kampot
  - pepper plantations
- Kep
  - beaches
  - Killing Caves
- Koh Poulo Wai
- Koh Rong
  - beaches
  - diving
  - nightlife
- Koh Rong Samloem
  - beaches
  - diving
- Koh Ta Kiev
- - beaches
- Phnom Penh
  - killing fields
  - tuol sleng genocide museum
- Sihanoukville
- Siem Reap
  - Angkor Wat
  - Tonle Sap Lake

## Indonesia
<!-- - Bali -->
- Bunkaten
- Lombok
  - volcano
- Raja Ampat
  - diving

## Laos
- Luang Namtha
  - trekking
- Nong Khiaw
  - limestone cliffs
- Pakse
  - Bolaven Plateau
  - coffee plantations
- Phonsavan
  - Plain of Jars
- Si Phan Don
  - four thousand islands
- Vang Vieng
  - climbing
  - trekking

## Philippines
- Palawan

## Thailand
- Ayutthaya
  - temples
- Bangkok
  - muay thai
  - river boat
- Chiang Rai
  - golden triangle
- Kanchanaburi
  - death railway
  - Erawan national park
- Ko Tao
  - diving
- Krabi 
  - [krabi climbing](https://krabirockclimbing.com/)
  <!-- - koko hostel overnight tour -->
- Pai
  - canyon
  - Tha Pai hot springs
- Phuket
  - surfing
- Sangkhlaburi
  - mountain

## Vietnam
- Da Lat
- Da Nang
- Ha Long
  - beach
  - limestone islands
- Hanoi
- Ho Chi Minh City (Saigon)
  - Cu Chi tunnels
  - war remnants museum
- Hoi An
  - ancient town
  - tailor shops
- Hue
- Long Xuyen
  - Mekong delta
- Mui Ne
  - sand dunes
  - kitesurfing
  - windsurfing
- Nha Trang
  - beach
- Ninh Binh
- Phong Nha-Ke Bang
  - Hang Son Doong cave
- Sa Pa
  - rice fields

<!-- 
## India
- Hampi's boulder

## Mongolia
## Nepal
## Pakistan
 -->


## New Zealand

### North Island
- Auckland
  - Sky Tower
  - diving (Goat Island Marine Reserve)
  - War Memorial Museum 
- Coromandel Peninsula
  - diving (Bay of Islands)
  - relaxe (Cathedral Cove) 
  - Hot Water Beach
  - hiking (Coromandel Coastal Walkway)
  - kayaking (Whitianga)
- Gisborne
  - East Cape Lighthouse
  - hiking (Motu Trails)
- Rotorua
  - Maori culture
  - diving (Lake Rotoiti)
  - Polynesian Spa
  - hiking (Redwoods Forest)
- Taupo
  - Skydive
  - hiking (Tongariro Alpine Crossing)
  - relaxe (Lake Taupo)
  - Orakei Korako Geothermal Park

### South Island
- Nelson / Abel Tasman National Park
  - diving
  - kayaking
  - hiking
  - relaxe (Kaiteriteri Beach)
  - Beach (Wharariki)
- Fiordland National Park
  - hiking (Dusky Track or Hollyford) 
  - cruise (Doubtful Sound)
  - wildlife-watching
- Manapouri
  - relaxe (Lake Manapouri)
- Milford Sound
  - diving
- Queenstown
  - Adventure activities (bungee jumping, skiing)
  - relaxe (Lake Wakatipu)
  - diving (Lake Wakatipu)
  - hiking (Moke Lake)
- Te Anau
  - glowworm cave tour
- Wanaka
  - cruise (Lake Wanaka)
  - hiking (Roy's Peak)
  - diving (Lake Wanaka)
  - hiking (Rob Roy Glacier)

### Central South Island
- Mount Cook National Park
  - hiking (Hooker Valley)
  - hiking (Mueller Hut)
  - Tasman Valley
  - Stargazing at Aoraki/Mount Cook Observatory
- Christchurch
  - cultural attractions
  - relaxe (Christchurch Botanic Gardens)
  - diving (Akaroa)
  - hiking (Godley Head Coastal)

# Comments
You have more ideas, add them as a comment

{{< chat cactus-jonas-burkhalter.gitlab.io-2023-10-01 >}}

<!-- Certainly! Excluding the places already mentioned, here are some other must-see attractions in Thailand


5. **Hua Hin**: This beach resort town is known for its royal palace, night markets, and water parks. It's a popular weekend getaway for locals and tourists alike.

6. **Krabi Province**: While Krabi town itself is a transportation hub, the surrounding area is stunning. Railay Beach, Ao Nang, and the Phi Phi Islands are famous for their crystal-clear waters, dramatic limestone cliffs, and vibrant nightlife.

8. **Ko Samet**: This island, close to Bangkok, is known for its white sandy beaches, clear waters, and vibrant nightlife. It's a popular destination for locals and expats.

9. **Sukhothai**: Another UNESCO World Heritage site, Sukhothai is the site of the first capital of Thailand. The historical park is home to well-preserved ruins from the 13th century.

10. **Trang Province**: This less-visited province is known for its stunning islands, including Ko Muk, Ko Kradan, and Ko Libong. It's a great destination for snorkeling and diving.

11. **Nan**: This province in Northern Thailand is known for its scenic landscapes, including mountains, waterfalls, and lush forests. It's a great destination for trekking and outdoor activities.

12. **Pattaya**: Known for its lively nightlife and entertainment, Pattaya also offers beautiful beaches, water sports, and attractions like the Sanctuary of Truth.

These are just a few highlights, and there are many more hidden gems and cultural experiences to discover throughout Thailand. -->


<!-- Indonesia is a vast and diverse country with a multitude of attractions. Here are some of the must-see places and experiences:

1. **Bali**: Known for its stunning beaches, rich culture, and vibrant nightlife, Bali is a popular destination. Visit Ubud for its art scene and rice terraces, explore ancient temples like Tanah Lot, and relax on beaches like Kuta and Seminyak.

2. **Komodo Island**: This UNESCO World Heritage site is famous for the Komodo dragons, the world's largest lizards. It's also a great destination for diving, with vibrant coral reefs and diverse marine life.

3. **Yogyakarta**: This cultural hub in Java is known for its historic temples, including the UNESCO-listed Borobudur and Prambanan. The city is also a center for traditional Javanese arts and crafts.

4. **Jakarta**: The capital city offers a mix of modern skyscrapers, historic districts, and vibrant markets. Visit the National Monument, explore the Old Town (Kota Tua), and indulge in the city's diverse cuisine.

5. **Ubud, Bali**: This town in the uplands of Bali is known as the cultural heart of the island. It's surrounded by rice paddies, lush forests, and temples. Explore art galleries, visit the Monkey Forest, and immerse yourself in traditional Balinese arts.

6. **Lombok**: This neighboring island to Bali offers stunning beaches, including the famous Pink Beach, as well as the challenging trek to Mount Rinjani, an active volcano.

7. **Raja Ampat**: Located in West Papua, this archipelago is a paradise for divers and snorkelers. It's known for having some of the most biodiverse marine life in the world.

8. **Borobudur**: This ancient Buddhist temple complex in Central Java is one of the most impressive archaeological sites in Southeast Asia. It's particularly stunning at sunrise.

9. **Tana Toraja, Sulawesi**: This region is known for its unique funeral traditions and distinctive architecture. The Toraja people are known for their elaborate burial ceremonies.

10. **Gili Islands**: These three small islands off the coast of Lombok are known for their clear waters, excellent snorkeling and diving, and laid-back atmosphere.

11. **Bunaken Marine Park, Sulawesi**: Another top destination for divers, this marine park is home to an incredible diversity of coral reefs and marine life.

12. **Mount Bromo, Java**: An active volcano and part of the Bromo Tengger Semeru National Park. The sunrise view over the caldera is breathtaking.

13. **Flores**: This island is known for its stunning landscapes, including Kelimutu, a volcano with tri-colored crater lakes.

14. **Ubud Monkey Forest, Bali**: This sacred sanctuary is home to hundreds of long-tailed macaques. It's a fascinating place to observe primates in a natural environment.

Remember, Indonesia is a vast country with over 17,000 islands, each with its own unique attractions and cultural experiences. Be sure to explore beyond the popular destinations to discover hidden gems. -->

<!-- Mongolia, known for its vast landscapes, nomadic culture, and rich history, offers a unique travel experience. Here are some must-see places and experiences in Mongolia:

1. **Gobi Desert**: Explore the vast, arid expanse of the Gobi Desert, known for its unique landscapes, sand dunes, and the famous Flaming Cliffs where dinosaur fossils were discovered.

2. **Khustain Nuruu National Park (Hustai National Park)**: This park is home to the endangered Przewalski's horse, also known as takhi. It's a great place to witness the beauty of Mongolia's natural scenery.

3. **Terelj National Park**: Located close to Ulaanbaatar, Terelj is known for its stunning rock formations, including the famous Turtle Rock. It's a popular spot for hiking and camping.

4. **Ulaanbaatar**: The capital city of Mongolia, Ulaanbaatar, offers a mix of modernity and tradition. Visit Gandantegchinlen Monastery, the National Museum of Mongolia, and Sukhbaatar Square.

5. **Karakorum (Kharkhorin)**: This ancient capital of the Mongol Empire is steeped in history. Explore the Erdene Zuu Monastery, which is one of the oldest Buddhist monasteries in Mongolia.

6. **Lake Khövsgöl**: Often referred to as the "Blue Pearl of Mongolia," this freshwater lake is one of the largest in the country and offers stunning scenery and opportunities for boating and hiking.

7. **Altai Tavan Bogd National Park**: Located in the far west of Mongolia, this park is home to the country's highest peaks and is known for its glaciers, lakes, and diverse wildlife.

8. **Orkhon Valley**: Another UNESCO World Heritage Site, the Orkhon Valley is rich in archaeological remains and contains historical sites of the Mongol Empire.

9. **Elsen Tasarkhai Sand Dunes**: These small sand dunes are located not far from Karakorum and offer a taste of the Gobi Desert's beauty.

10. **Khorgo-Terkhiin Tsagaan Nuur National Park**: This park in the Arkhangai Province is known for its volcanic landscapes, including the extinct Khorgo volcano and the beautiful Terkhiin Tsagaan Nuur lake.

11. **Tsenkher Hot Springs**: Relax in natural hot springs in the Arkhangai Province, which are known for their therapeutic properties.

12. **Gorkhi-Terelj National Park**: This park offers opportunities for hiking, rock climbing, and visiting the famous Aryabal Meditation Temple.

13. **Ongi Monastery Ruins**: Explore the remnants of one of Mongolia's largest monasteries, located along the Ongi River.

14. **Mongol Els Sand Dunes**: These dramatic sand dunes, located in the Gobi Gurvansaikhan National Park, are surrounded by stunning desert landscapes.

15. **Nadaam Festival**: If you visit during July, don't miss the Nadaam Festival, which celebrates Mongolia's "Three Manly Games" of wrestling, horse racing, and archery.

Mongolia's vast and diverse landscapes, combined with its rich cultural heritage, make it a truly unique destination for travelers seeking adventure and cultural experiences. Remember to respect local customs and traditions during your visit. -->

<!-- Nepal is a country of remarkable natural beauty and rich cultural heritage. Here are some of the must-see places and experiences in Nepal:

1. **Mount Everest and the Everest Region**: Whether you're a serious mountaineer or just want to trek to the base camp, this region offers some of the most breathtaking views on Earth.

2. **Kathmandu Valley**: This UNESCO World Heritage Site is home to three ancient cities - Kathmandu, Bhaktapur, and Patan. Explore their Durbar Squares, temples, and vibrant markets.

3. **Bhaktapur Durbar Square**: Known as the "City of Devotees," Bhaktapur is famed for its well-preserved medieval architecture and intricate artwork.

4. **Patan Durbar Square**: This ancient city showcases exquisite Newar architecture, including palaces, temples, and statues.

5. **Boudhanath Stupa**: One of the largest stupas in the world, this sacred site is a focal point for Tibetan Buddhism.

6. **Swayambhunath (Monkey Temple)**: Perched on a hill, this ancient stupa offers panoramic views of Kathmandu Valley.

7. **Chitwan National Park**: A UNESCO World Heritage Site, this park is known for its diverse wildlife, including Bengal tigers, one-horned rhinoceros, and a wide variety of bird species.

8. **Pokhara**: This city is known for its stunning lakes, including Phewa Lake with its reflection of the Annapurna and Machapuchare mountains.

9. **Annapurna Region**: A trekking paradise, the Annapurna Circuit offers diverse landscapes, from subtropical forests to high-altitude plateaus.

10. **Langtang National Park**: A less crowded trekking destination, Langtang offers beautiful scenery and a chance to experience local Tamang and Sherpa culture.

11. **Lumbini**: Birthplace of Siddhartha Gautama, who later became Buddha, Lumbini is a pilgrimage site for Buddhists from around the world.

12. **Nagarkot**: A hill station near Kathmandu, Nagarkot offers some of the best views of the Himalayas, especially at sunrise.

13. **Gorkha**: This is the birthplace of King Prithvi Narayan Shah, the founder of modern Nepal. The Gorkha Durbar offers a glimpse into Nepal's history.

14. **Tansen**: This town in the hills of Western Nepal is known for its well-preserved Newari architecture and stunning views of the Himalayas.

15. **Rara Lake**: Located in the remote far-western region, Rara is the largest lake in Nepal and offers pristine natural beauty.

Remember, Nepal's diverse geography offers everything from high-altitude treks to jungle safaris. The best sights to see will depend on your interests and the time of year you visit. -->

<!-- Pakistan is a country with a rich cultural heritage, stunning natural beauty, and a diverse range of attractions. Here are some of the must-see places and experiences in Pakistan:

1. **Karachi**: The largest city in Pakistan, Karachi offers a mix of modernity and tradition. Visit Clifton Beach, explore the bustling markets, and take in the vibrant culture.

2. **Lahore**: Known as the cultural capital of Pakistan, Lahore is steeped in history. Visit the Lahore Fort, Badshahi Mosque, and the Shalimar Gardens.

3. **Islamabad**: The capital city of Pakistan is known for its clean, well-planned streets and beautiful scenery. Don't miss Faisal Mosque, Pakistan Monument, and Daman-e-Koh viewpoint.

4. **Hunza Valley**: Often referred to as heaven on Earth, this region in Gilgit-Baltistan offers jaw-dropping scenery, including Rakaposhi and Ultar Sar peaks.

5. **Skardu**: Known as the gateway to some of the highest peaks in the Karakoram Range, Skardu is a starting point for treks to K2 and the Baltoro Glacier.

6. **Fairy Meadows**: This lush plateau provides one of the most mesmerizing views of Nanga Parbat, the world's ninth-highest mountain.

7. **Swat Valley**: Often called the "Switzerland of Pakistan", Swat Valley is known for its scenic beauty, lush green valleys, and cultural richness.

8. **Murree**: A popular hill station, Murree offers relief from the heat with its cool climate, dense forests, and scenic beauty.

9. **Multan**: Known as the "City of Saints" and the "City of Mangoes", Multan is one of the oldest cities in the Asian subcontinent, with a rich cultural and historical heritage.

10. **Thar Desert**: Experience the unique landscape of the Thar Desert in Sindh. Explore the desert culture, visit ancient ruins, and go on camel safaris.

11. **Mohenjo-Daro**: An ancient archaeological site of the Indus Valley Civilization, Mohenjo-Daro is one of the world's earliest major urban settlements.

12. **Taxila**: This UNESCO World Heritage Site is one of the most important archaeological sites in Pakistan, containing the remains of Gandhara, the ancient Buddhist kingdom.

13. **Hiran Minar**: Located near Lahore, this historic site features a uniquely designed minaret set amidst a large water tank, offering a serene and picturesque view.

14. **Karimabad**: This town in Hunza Valley offers stunning views of Rakaposhi, Ultar Sar, and Hunza Peak, along with historical landmarks like the Baltit Fort.

15. **Khyber Pass**: This historic mountain pass has been an important trade and invasion route for centuries. It holds significant historical and cultural value.

Pakistan's diverse landscapes, from mountains to deserts to historical cities, offer a wide range of experiences for travelers. Remember to respect local customs and traditions during your visit. -->