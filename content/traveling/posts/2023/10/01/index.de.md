+++  
draft = false  
date = 2023-10-01T22:00:00+02:00  
title = "Reisen durch Südostasien"  
description = "Übersicht und Ideen"  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

# Übersicht
<iframe 
    src="https://www.google.com/maps/d/embed?mid=11LXriO5dxSju08ggypv8xdBmCtttknA&ehbc=2E312F&noprof=1" 
    width="100%"
    height="500">
</iframe>

# Was tun in Südostasien

## Allgemeine Ideen  
- Klettern  
- Tauchen  
- Dschungelwandern  
- Massage  
- Strassenessen  
- Surfen  
- Yoga

## Kambodscha  
- Battambang  
  - Bambuszug  
- Kampot  
  - Pfefferplantagen  
- Kep  
  - Strände  
  - Killing Caves  
- Koh Poulo Wai  
- Koh Rong  
  - Strände  
  - Tauchen  
  - Nachtleben  
- Koh Rong Samloem  
  - Strände  
  - Tauchen  
- Koh Ta Kiev  
  - Strände  
- Phnom Penh  
  - Killing Fields  
  - Tuol Sleng Genozid-Museum  
- Sihanoukville  
- Siem Reap  
  - Angkor Wat  
  - Tonle Sap See  

## Indonesien  
- Bunkaten  
- Lombok  
  - Vulkan  
- Raja Ampat  
  - Tauchen  

## Laos  
- Luang Namtha  
  - Trekking  
- Nong Khiaw  
  - Kalksteinfelsen  
- Pakse  
  - Bolaven-Plateau  
  - Kaffeeplantagen  
- Phonsavan  
  - Plain of Jars  
- Si Phan Don  
  - 4000 Inseln  
- Vang Vieng  
  - Klettern  
  - Trekking  

## Philippinen  
- Palawan  

## Thailand  
- Ayutthaya  
  - Tempel  
- Bangkok  
  - Muay Thai  
  - Flussboot  
- Chiang Rai  
  - Goldener Dreieck  
- Kanchanaburi  
  - Todesbahn  
  - Erawan-Nationalpark  
- Ko Tao  
  - Tauchen  
- Krabi  
  - [krabi climbing](https://krabirockclimbing.com/)  
  <!-- - koko hostel overnight tour -->  
- Pai  
  - Canyon  
  - Tha Pai heissen Quellen  
- Phuket  
  - Surfen  
- Sangkhlaburi  
  - Berge  

## Vietnam  
- Da Lat  
- Da Nang  
- Ha Long  
  - Strand  
  - Kalksteininseln  
- Hanoi  
- Ho-Chi-Minh-Stadt (Saigon)  
  - Cu Chi Tunnel  
  - Kriegsremi-Museum  
- Hoi An  
  - Altstadt  
  - Massschneiderläden  
- Hue  
- Long Xuyen  
  - Mekong-Delta  
- Mui Ne  
  - Sanddünen  
  - Kitesurfen  
  - Windsurfen  
- Nha Trang  
  - Strand  
- Ninh Binh  
- Phong Nha-Ke Bang  
  - Hang Son Doong-Höhle  
- Sa Pa  
  - Reisterrassen  

## Neuseeland

### Nordinsel  
- Auckland  
  - Sky Tower  
  - Tauchen (Goat Island Marine Reserve)  
  - Kriegsdenkmalmuseum  
- Coromandel Peninsula  
  - Tauchen (Bay of Islands)  
  - Entspannen (Cathedral Cove)  
  - Hot Water Beach  
  - Wandern (Coromandel Coastal Walkway)  
  - Kajaken (Whitianga)  
- Gisborne  
  - Ostkap-Leuchtturm  
  - Wandern (Motu Trails)  
- Rotorua  
  - Maori-Kultur  
  - Tauchen (Lake Rotoiti)  
  - Polynesian Spa  
  - Wandern (Redwoods Forest)  
- Taupo  
  - Fallschirmspringen  
  - Wandern (Tongariro Alpine Crossing)  
  - Entspannen (Lake Taupo)  
  - Orakei Korako Geothermal Park  

### Südinsel  
- Nelson / Abel Tasman-Nationalpark  
  - Tauchen  
  - Kajaken  
  - Wandern  
  - Entspannen (Kaiteriteri Beach)  
  - Strand (Wharariki)  
- Fiordland-Nationalpark  
  - Wandern (Dusky Track oder Hollyford)  
  - Kreuzfahrt (Doubtful Sound)  
  - Tierbeobachtung  
- Manapouri  
  - Entspannen (Lake Manapouri)  
- Milford Sound  
  - Tauchen  
- Queenstown  
  - Abenteueraktivitäten (Bungee-Jumping, Skifahren)  
  - Entspannen (Lake Wakatipu)  
  - Tauchen (Lake Wakatipu)  
  - Wandern (Moke Lake)  
- Te Anau  
  - Glowworm-Höhlentour  
- Wanaka  
  - Kreuzfahrt (Lake Wanaka)  
  - Wandern (Roy's Peak)  
  - Tauchen (Lake Wanaka)  
  - Wandern (Rob Roy Glacier)  

### Zentral-Südinsel  
- Mount Cook-Nationalpark  
  - Wandern (Hooker Valley)  
  - Wandern (Mueller Hut)  
  - Tasman Valley  
  - Sternegucken am Aoraki/Mount Cook Observatory  
- Christchurch  
  - kulturelle Attraktionen  
  - Entspannen (Christchurch Botanic Gardens)  
  - Tauchen (Akaroa)  
  - Wandern (Godley Head Coastal)  

# Kommentare  
Du hast noch mehr Ideen? Füge sie als Kommentar hinzu  

{{< chat cactus-jonas-burkhalter.gitlab.io-2023-10-01 >}}  

<!-- Sicher! Ausser den bereits genannten Orten gibt es in Thailand noch einige weitere Sehenswürdigkeiten:

5. **Hua Hin**: Dieser Strandort ist bekannt für seinen königlichen Palast, Nachtmärkte und Wasserparks. Er ist ein beliebtes Wochenendziel für Einheimische und Touristen.

6. **Krabi-Provinz**: Während Krabi-Stadt selbst ein Verkehrsknotenpunkt ist, ist die Umgebung wunderschön. Railay Beach, Ao Nang und die Phi Phi-Inseln sind berühmt für ihre kristallklaren Gewässer, dramatischen Kalksteinfelsen und lebendige Nachtleben.

8. **Ko Samet**: Diese Insel, in der Nähe von Bangkok, ist bekannt für ihre weissen Sandstrände, klaren Gewässer und lebhaften Nachtleben. Sie ist ein beliebtes Ziel für Einheimische und Expats.

9. **Sukhothai**: Ein weiteres UNESCO-Weltkulturerbe, Sukhothai ist der Standort der ersten Hauptstadt Thailands. Der historische Park ist Heimat gut erhaltener Ruinen aus dem 13. Jahrhundert.

10. **Trang-Provinz**: Diese weniger besuchte Provinz ist bekannt für ihre wunderschönen Inseln, darunter Ko Muk, Ko Kradan und Ko Libong. Es ist ein grossartiges Ziel zum Schnorcheln und Tauchen.

11. **Nan**: Diese Provinz im Norden Thailands ist bekannt für ihre malerischen Landschaften, einschliesslich Bergen, Wasserfällen und dichten Wäldern. Es ist ein grossartiges Ziel zum Wandern und Outdoor-Aktivitäten.

12. **Pattaya**: Bekannt für sein lebhaftes Nachtleben und Unterhaltung bietet Pattaya auch schöne Strände, Wassersport und Sehenswürdigkeiten wie den Sanctuary of Truth.

Das sind nur einige Highlights, und es gibt noch viele weitere versteckte Juwelen und kulturelle Erlebnisse in ganz Thailand zu entdecken. -->

<!-- Indonesien ist ein riesiges und vielfältiges Land mit einer Vielzahl von Sehenswürdigkeiten. Hier sind einige der Orte und Erlebnisse, die Sie unbedingt sehen sollten:

1. **Bali**: Bekannt für seine atemberaubenden Strände, seine reiche Kultur und sein lebhaftes Nachtleben ist Bali ein beliebtes Reiseziel. Besuchen Sie Ubud für seine Kunstszene und Reisterrassen, erkunden Sie alte Tempel wie Tanah Lot und entspannen Sie an Stränden wie Kuta und Seminyak.

2. **Komodo-Insel**: Diese UNESCO-Weltkulturerbestätte ist bekannt für die Komodo-Drachen, die grössten Echsen der Welt. Es ist auch ein grossartiges Ziel zum Tauchen mit bunten Korallenriffen und vielfältigem Meeresleben.

3. **Yogyakarta**: Dieses kulturelle Zentrum auf Java ist bekannt für seine historischen Tempel, darunter der UNESCO-geförderte Borobudur und Prambanan. Die Stadt ist auch ein Zentrum für traditionelle javanesische Kunst und Handwerk.

4. **Jakarta**: Die Hauptstadt bietet eine Mischung aus modernen Wolkenkratzern, historischen Bezirken und lebhaften Märkten. Besuchen Sie das Nationaldenkmal, erkunden Sie die Altstadt (Kota Tua) und lassen Sie sich die Vielfalt der Stadtküche schmecken.

5. **Ubud, Bali**: Diese Stadt in den Uplands von Bali wird als kulturelles Herz der Insel angesehen. Sie ist umgeben von Reisterrassen, üppigen Wäldern und Tempeln. Erkunden Sie Kunstgalerien, besuchen Sie den Affenwald und tauchen Sie ein in die traditionelle balinesische Kunst.

6. **Lombok**: Diese Nachbarinsel von Bali bietet wunderschöne Strände, darunter den berühmten Pink Beach, sowie die herausfordernde Wanderung zum Mount Rinjani, einem aktiven Vulkan.

7. **Raja Ampat**: Im Westen Neuguineas gelegen, ist diese Archipel ein Paradies für Taucher und Schnorchler. Es ist bekannt für seine unglaublich biodiverse Meereswelt.

8. **Borobudur**: Dieser alte buddhistische Tempelkomplex auf Java ist eines der beeindruckendsten archäologischen Stätten in Südostasien. Besonders atemberaubend ist er bei Sonnenaufgang.

9. **Tana Toraja, Sulawesi**: Diese Region ist bekannt für ihre einzigartigen Bestattungsrituale und ihre auffällige Architektur. Die Toraja-Leute sind bekannt für ihre aufwendigen Bestattungszeremonien.

10. **Gili-Inseln**: Diese drei kleinen Inseln vor der Küste von Lombok sind bekannt für ihre klaren Gewässer, ausgezeichnetes Schnorcheln und Tauchen sowie ihre entspannte Atmosphäre.

11. **Bunaken Marine Park, Sulawesi**: Ein weiteres Top-Ziel für Taucher, dieser Meerespark ist Heimat einer unglaublichen Vielfalt an Korallenriffen und Meereslebewesen.

12. **Mount Bromo, Java**: Ein aktiver Vulkan und Teil des Bromo-Tengger-Semeru-Nationalparks. Der Blick auf den Krater bei Sonnenaufgang ist atemberaubend.

13. **Flores**: Diese Insel ist bekannt für ihre atemberaubenden Landschaften, einschliesslich des Kelimutu, eines Vulkans mit trikolorigen Kraterseen.

14. **Ubud-Affenwald, Bali**: Dieses heilige Heiligtum ist Heimat von Hunderten von Langschwanzmakaken. Es ist ein faszinender Ort, um Affen in einem natürlichen Lebensraum zu beobachten.

Indonesien ist ein riesiges Land mit über 17.000 Inseln, jede mit ihren eigenen einzigartigen Sehenswürdigkeiten und kulturellen Erlebnissen. Entdecken Sie über die bekannten Ziele hinaus auch versteckte Juwelen. -->

<!-- Mongolei, bekannt für ihre weiten Landschaften, die nomadische Kultur und ihre reiche Geschichte, bietet ein einzigartiges Reiseerlebnis. Hier sind einige der Orte und Erlebnisse, die Sie in der Mongolei sehen sollten:

1. **Gobi-Wüste**: Erkunden Sie die weite, trockene Fläche der Gobi-Wüste, bekannt für ihre einzigartigen Landschaften, Sanddünen und die berühmten Flaming Cliffs, wo Dinosaurierfossilien entdeckt wurden.

2. **Khustain Nuruu Nationalpark (Hustai-Nationalpark)**: Dieser Park ist Heimat des bedrohten Przewalski-Pferdes, auch bekannt als Takhi. Es ist ein grossartiger Ort, um die Schönheit der mongolischen Natur zu erleben.

3. **Terelj-Nationalpark**: In der Nähe von Ulaanbaatar gelegen, ist Terelj bekannt für seine beeindruckenden Felsformationen, darunter das berühmte Turtle Rock. Es ist ein beliebter Ort zum Wandern und Campen.

4. **Ulaanbaatar**: Die Hauptstadt der Mongolei, Ulaanbaatar, bietet eine Mischung aus Modernität und Tradition. Besuchen Sie das Gandantegchinlen-Kloster, das Nationalmuseum der Mongolei und den Sukhbaatar-Platz.

5. **Karakorum (Kharkhorin)**: Diese alte Hauptstadt des Mongolenreiches ist voller Geschichte