+++  
draft = false  
date = 2023-12-14T22:00:00+02:00  
title = "Die dunklen Seiten des Reisens (Präsentation)"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "dunkle Seite", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Die dunklen Seiten des Reisens"]  
+++  

## OOH MEIN GOTT!! ES WAR **DAS** BESTE!!

<!--  
Nur das Schöne zeigen  
Hohe Erwartungen  
Jeder sagt dir, es war das Beste  
-->

Im Zeitalter der sozialen Medien werden unsere Reiseerfahrungen zu einer kuratierten Erzählung – einem Highlight-Video, das das Aussergewöhnliche betont und das Alltägliche oder Herausfordernde herunterspielt. Der Druck, sich der Erzählung zu beugen, dass jede Reise das „beste Erlebnis aller Zeiten“ sein sollte, kann überwältigend sein.

{{< figure src="./images/influencer.jpg" alt="Influencer" class="aligne-left size-medium" >}}

Freunde, Influencer und sogar Fremde erzählen uns von ihren unvergleichlichen Abenteuern, sodass es scheint, als müsste jede Reiseerfahrung eine euphorische Reise sein. Doch die Wahrheit ist, dass nicht jeder Moment auf der Strasse bildschön ist.

Dieser Druck zur Konformität kann dazu führen, dass man die weniger perfekten Seiten des Reisens nicht teilt. Die Angst, als undankbar oder negativ wahrgenommen zu werden, lässt uns unsere Geschichten bearbeiten und die Momente der Frustration, Erschöpfung oder Enttäuschung weglassen, die ein unvermeidlicher Teil der Reiseerfahrung sind.

Selbst mein Blog konzentriert sich auf die Dinge, die ich getan habe, geschrieben mit einem nostalgischen und verherrlichenden Ton von Chat GPT. Umrahmt nur von den ikonischsten, lustigsten oder beeindruckendsten Bildern.
