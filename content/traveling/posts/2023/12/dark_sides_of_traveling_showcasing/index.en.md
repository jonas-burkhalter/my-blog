+++ 
draft = false
date = 2023-12-14T22:00:00+02:00
title = "Dark sides of traveling (showcasing)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "dark side", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["dark side of traveling"]
+++

## OOH MY GOD!! IT WAS **THE** BEST!!
<!-- 
only showing the beautiful
high expectations
everyone tells you it was the best
-->

In the age of social media, our travel experiences become a curated narrative—a highlight reel that emphasizes the extraordinary and downplays the mundane or challenging. The pressure to conform to the narrative that every trip should be the "best ever" can be overwhelming.

{{< figure src="./images/influencer.jpg" alt="influencer" class="aligne-left size-medium" >}}

Friends, influencers, and even strangers regale us with tales of their unparalleled adventures, making it seem like every travel experience should be a euphoric journey. However, the truth is, not every moment on the road is picture-perfect. 

This pressure to conform can lead to a reluctance to share the less-than-ideal aspects of travel. The fear of being seen as ungrateful or negative may cause us to edit our stories, leaving out the moments of frustration, exhaustion, or disappointment that are an inevitable part of the travel experience.

Even my blog focuses on the things I did, written with a nostalgic and glorifying tone from Chat GPT. Framed with only the most iconic, funny or stunning pictures.
