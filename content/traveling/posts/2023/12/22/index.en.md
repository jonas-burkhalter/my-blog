+++ 
draft = false
date = 2023-12-22T22:00:00+02:00
title = "Koh Rong"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Friday: Battling Adversity - A Day of Endurance
<!-- 
(Everthing sucks)

Being sick
Taking the nightbus (Thursday evening 10pm)
Night bus cold as fuck
At 0545 waiting (in phnom penh) an hour for the connecting mini van
Almost get kicked out of it, they didn't belief I paid, luckily stil had the ticket
Waiting another hour
driving to sihanoukville
Hurry to ferry, need cash. ATM broken
Getting the wrong ferry ticket (not to M'Pai bay)
Boot is over an 1.5 hour late
Can't get to M'pai bay
Need to rebook hostel (party hostel last minute)
Big waives during the boat ride
Almost 20h journey
Power cut at the hostel

worst traveling day i had so far

on the phone with noe. something nice and relaxing after a hard and stressful day

getting some dinner
dizzy and falling asleep while eating
people helping me 
-->
The journey began with the unwelcome companion of illness—a shadow that lingered throughout the day, casting a pall over every subsequent trial and tribulation. Despite the physical discomfort, the journey pressed on.

The night bus, a vessel of cold discomfort, became the stage for the initial acts of endurance. Departing at 10 pm (Thursday), the frosty air within the confines of the bus mirrored the external chill, setting the tone for a journey that seemed destined for adversity.

At 5:45 am, the scene shifted to Phnom Penh—a transitional point that should have marked a seamless connection to a minivan. However, what followed was a saga of skepticism and disbelief, as I almost faced eviction from the minivan due to doubts about payment—an ordeal only salvaged by the fortuitous retention of the ticket.

The subsequent hour of waiting became a punctuation mark in a narrative of delays, leading to a bumpy ride to Sihanoukville. The urgency to catch a ferry to M'Pai Bay met an unexpected hurdle—an ATM refusal at the precise moment when cash was imperative.

To compound the day's challenges, the wrong ferry ticket was procured, diverting the journey away from the intended destination. The wait for the boat, prolonged by over an hour and a half, stretched patience thin, amplifying the fatigue that had settled in.

As the boat finally arrived, excitement turned to disappointment when I learned that the pier at M'Pai Bay was defective and unreachable. The prospect of reaching there seemed bleak. Swiftly, I had to alter my plans, opting for a new hostel on Koh Rong—underscoring the surreal nature of the day's misadventures.

The boat ride, plagued by sizable waves, added a final chapter to a day that could only be described as the worst traveling experience so far. Even at the hostel, the travails persisted with a power cut, plunging the evening into darkness.

Navigating through the haze of exhaustion, dinner became a surreal experience, with dizziness threatening to claim the moment. Yet, the kindness of strangers emerged, offering assistance during a vulnerable moment—a testament to the humanity that often shines through in the bleakest hours.

{{< figure src="./images/sea.jpg" alt="sea" class="aligne-center size-medium" >}}

## Saturday: Battling the Ills of Travel
<!-- 
still sick (flue, Headache, cough, Shortness of breath, Tiredness and weakness, Runny and stuffy nose, Sore throat, Eye pain, the complete package)

breakfast
watching forrest gump

planning and booking the trip to saigon

long, nice phone call with noe

dinner

talking with the people from the hostel (lost boys)
one year anniversary

going to bed

-->
The specter of illness continued to cast its long shadow, manifesting as a relentless array of symptoms—a complete package of discomfort that tested the boundaries of endurance. The flu, accompanied by headaches, cough, shortness of breath, tiredness, weakness, a runny and stuffy nose, sore throat, and even eye pain, held its grip on the day.

Amidst the persistent ailment, the day commenced with a tentative breakfast—a modest attempt at nourishment in the face of bodily resistance. Seeking solace in the familiar, Forrest Gump became a cinematic companion, providing a momentary escape from the physical trials.

Despite the challenges, the day unfolded with a blend of productivity and connection. The planning and booking of the journey to Saigon became a strategic endeavor, a testament to the determination to push forward despite the prevailing circumstances.

As the day drew to a close, the prospect of rest held the promise of relief. The sanctuary of a bed beckoned, providing a temporary escape from the rigors of the ongoing journey. The battle against illness persisted, yet the indomitable spirit to navigate through the trials of travel remained undeterred.

{{< figure src="./images/sunset.jpg" alt="sunset" class="aligne-center size-medium" >}}
