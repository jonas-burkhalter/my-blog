+++ 
draft = false
date = 2023-12-22T22:00:00+02:00
title = "Koh Rong"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Freitag: Kämpfen gegen die Widrigkeiten – Ein Tag der Ausdauer
<!-- 
(Everything sucks)

Krank sein
Nachtbus nehmen (Donnerstagabend 22 Uhr)
Nachtbus kalt wie Sau
Um 05:45 Uhr warten (in Phnom Penh) eine Stunde auf den anschliessenden Minivan
Fast rausgeschmissen, sie glaubten mir nicht, dass ich bezahlt habe, zum Glück hatte ich noch den Ticketbeleg
Warten weitere Stunde
Fahrt nach Sihanoukville
Beeilung zur Fähre, Bargeld nötig. ATM kaputt
Falsches Fährticket (nicht nach M'Pai Bay)
Boot ist über 1,5 Stunden zu spät
Kann nicht nach M'Pai Bay
Hostel kurzfristig neu buchen (Partyhostel in letzter Minute)
Grosse Wellen während der Bootsfahrt
Fast 20 Stunden Reise
Stromausfall im Hostel

schlimmster Reisetag, den ich bisher hatte

Telefonat mit Noe. Etwas Schönes und Entspannendes nach einem harten und stressigen Tag

Abendessen
Schwindel und Einschlafen während des Essens
Menschen helfen mir
-->

Die Reise begann mit dem unwillkommenen Begleiter der Krankheit – ein Schatten, der den ganzen Tag über anhielt und jede anschliessende Probe und Schwierigkeit überschattete. Trotz körperlicher Unannehmlichkeiten ging die Reise unermüdlich weiter.

Der Nachtbus wurde zum Schauplatz für die ersten Prüfungen der Ausdauer – kalt und ungemütlich, mit einer eisigen Atmosphäre, die den Ton für eine Reise setzte, die wie gemacht war für Widrigkeiten.

Um 5:45 Uhr änderte sich die Szenerie in Phnom Penh – ein Übergangspunkt, der eigentlich einen nahtlosen Anschluss an einen Minivan hätte markieren sollen. Doch was folgte, war eine Saga von Skepsis und Unglauben, als ich fast aus dem Minivan geworfen wurde – nur gerettet durch das Glück, den Ticketbeleg noch zu haben.

Die anschliessende Stunde des Wartens wurde zu einem Höhepunkt einer Erzählung aus Verzögerungen, die in eine holprige Fahrt nach Sihanoukville überging. Die Eile, eine Fähre nach M'Pai Bay zu erwischen, traf auf unerwartete Hindernisse – ein defekter Geldautomat zum exakt falschen Zeitpunkt, als Bargeld unbedingt nötig war.

Um die Herausforderungen des Tages zu verstärken, wurde das falsche Fährticket gezogen, das die Reise von der geplanten Richtung abbrachte. Die Wartezeit auf das Boot, die über eineinhalb Stunden andauerte, zerrte an den Nerven und trug zur Erschöpfung bei, die sich bereits breitgemacht hatte.

Als das Boot endlich eintraf, wich die Aufregung Enttäuschung, als ich erfuhr, dass der Kai in M'Pai Bay defekt und unerreichbar war. Die Aussicht, dorthin zu gelangen, erschien trostlos. Schnell musste ich meine Pläne ändern und ein neues Hostel auf Koh Rong buchen – eine ironische Wendung für einen Tag, der nur so von Missgeschicken gesättigt war.

Die Bootsfahrt, die von grossen Wellen geprägt war, setzte dem Ganzen die Krone auf – ein Tag, der nur als der schlimmste Reisetag, den ich bisher erlebt habe, beschrieben werden kann. Selbst im Hostel hielten die Qualen weiter an mit einem Stromausfall, der den Abend in Dunkelheit hüllte.

Mitten im Nebel der Erschöpfung wurde das Abendessen zu einer surrealen Erfahrung, bei der der Schwindel drohte, den Moment zu übernehmen. Doch die Freundlichkeit von Fremden erwachte in dieser schwierigen Stunde und bot Hilfe – ein Zeugnis der Menschlichkeit, die oft in den dunkelsten Stunden zum Vorschein kommt.

{{< figure src="./images/sea.jpg" alt="Meer" class="aligne-center size-medium" >}}

## Samstag: Kämpfen gegen die Übel des Reisens
<!-- 
immer noch krank (Grippe, Kopfschmerzen, Husten, Atemnot, Müdigkeit und Schwäche, laufende und verstopfte Nase, Halsschmerzen, Augenschmerzen, das volle Programm)

Frühstück
Forrest Gump schauen

Reise nach Saigon planen und buchen

langer, schöner Telefonanruf mit Noe

Abendessen

Gespräche mit den Menschen aus dem Hostel (Lost Boys)
Einjähriges Jubiläum

Ins Bett gehen
-->

Die Geissel der Krankheit setzte sich mit einem langen Schatten fort – eine unaufhörliche Reihe von Symptomen, die die Grenzen der Ausdauer auf die Probe stellte. Die Grippe, begleitet von Kopfschmerzen, Husten, Atemnot, Müdigkeit, Schwäche, laufender und verstopfter Nase, Halsschmerzen und sogar Augenschmerzen, hatte einen festen Griff auf den Tag.

Mittendrin in dieser anhaltenden Unwohlsein begann der Tag mit einem zaghaften Frühstück – ein vorsichtiger Versuch, Nahrung zu sich zu nehmen, trotz des Widerstands des Körpers. Trost in der Vertrautheit fand ich in Forrest Gump, der mich für einen kurzen Moment aus den physischen Prüfungen entführte.

Trotz der Herausforderungen verlief der Tag mit einer Mischung aus Produktivität und Verbindung. Das Planen und Buchen der Reise nach Saigon wurde zu einem strategischen Vorhaben – ein Zeugnis der Entschlossenheit, trotz der vorherrschenden Umstände weiterzumachen.

Am Tagestreffen, das mit einem schönen Telefonanruf mit Noe begann, fand ich Entspannung und Trost – ein erlösender Moment nach den Strapazen des Vortages.

{{< figure src="./images/sunset.jpg" alt="Sonnenuntergang" class="aligne-center size-medium" >}}
