+++ 
draft = false
date = 2023-12-08T22:00:00+02:00
title = "Dark sides of traveling (being local)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "dark side", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["dark side of traveling"]
+++

## REALITY CHECK
<!-- 
- craving for the "real" "local" experience
    - feeling/being the tourist
    - is everything you see just for the tourists? (feeling ausgebeutet)
    - beeing mostly with other travelers
-->
In our quest for the quintessential travel experience, the desire to immerse ourselves in the "real" and "local" becomes a guiding force. Yet, as we traverse new territories, a paradox unfolds, challenging our pursuit of authenticity.

The very essence of our yearning introduces a dual identity—one where we simultaneously seek to be both the observer and the observed. As we strive for the "real" local experience, the awareness of our status as tourists becomes an inescapable reality, shaping our interactions and perceptions.

Amidst the vibrant markets and bustling streets, a disquieting question lingers: Is everything we encounter designed solely for tourists? The unsettling feeling of being "ausgebeutet" (exploited) creeps in, challenging the authenticity of our experiences. The tension between genuine cultural encounters and commercialized offerings tests the boundaries of our quest for the authentic.

{{< figure src="./images/tourists.jpg" alt="trash" class="aligne-right size-medium" >}}

In the pursuit of the "real" local experience, an unintended consequence emerges—finding comfort in the company of fellow travelers. The shared experiences and common quest for authenticity create a bond, but inadvertently, we may find ourselves isolated from the very communities we yearn to connect with. The balance between forging connections with fellow travelers and authentically engaging with locals becomes a delicate act.
