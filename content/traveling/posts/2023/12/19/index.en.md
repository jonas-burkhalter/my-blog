+++ 
draft = false
date = 2023-12-19T22:00:00+02:00
title = "Battambang"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Tuesday: Battambang's Tranquil Embrace
<!--
arrive in Battambang

chill the afternoon, watch tv series
-->

The bus journey unfolded, bringing me to the doorstep of Battambang—a town with a charm that whispered tales of French colonial architecture and the unhurried rhythm of Cambodian life.

Upon arrival, the afternoon bathed in the warmth of Battambang's ambiance, beckoning me to embrace a tranquil interlude. The day's agenda took a leisurely turn as I opted to immerse myself in the simple pleasures of downtime.

As the afternoon sun dipped towards the horizon, the allure of relaxation drew me indoors. The comforting glow of a screen and the familiar narratives of a TV series became my companions. In the midst of this peaceful hiatus, the day unfolded in unhurried moments—each episode a chapter in the narrative of an afternoon spent in quiet reflection.

{{< figure src="./images/dinner.jpg" alt="dinner" class="aligne-center size-medium" >}}

## Wednesday: Battambang's Tapestry of Experiences
<!-- 
tuk tuk tour
Bamboo sticky rice
Wat samroung knong
killing fields
Rice paper making
Rice wine making and tasting
bamboo train (highlight)
bat cave (6.5 Million bat flying out of it)

tired, hungry but happy
-->

{{< figure src="./images/bamboo_sticky_rice.jpg" alt="bamboo sticky rice" class="aligne-right size-small" >}}

The day commenced with a Tuk Tuk tour—a rhythmic dance through the town's streets that revealed the hidden gems nestled within its corners. The journey began with a culinary delight—the tasting of Bamboo Sticky Rice, a local delicacy that tantalized the taste buds with its unique flavors and textures.

{{< figure src="./images/temple.jpg" alt="temple" class="aligne-left size-smaller" >}}

Wat Samroung Knong emerged, a place of worship steeped in history and architectural elegance. The echoes of sacred moments resonated within its walls, offering a glimpse into Cambodia's spiritual heritage.

The somber yet poignant visit to the Killing Fields unfolded—a place of profound historical significance where the scars of Cambodia's past were laid bare. The air hung heavy with the weight of remembrance, a solemn tribute to lives lost and a testament to the resilience of the human spirit.

A shift in ambiance brought the lively hum of activity at a Rice Paper Making workshop—a celebration of Cambodia's culinary traditions. The artistry of hands at work transformed simple ingredients into delicate sheets, a testament to the culinary heritage passed down through generations.

{{< figure src="./images/rice_wine.jpg" alt="rice wine" class="aligne-right size-medium" >}}

The journey continued with a visit to a Rice Wine Making facility, where the alchemy of fermentation and aging produced a tasting experience that resonated with the essence of Cambodia's rural traditions.

The day's highlight unfolded on the Bamboo Train—a unique mode of transport that traversed Battambang's picturesque landscapes. The rhythmic clatter of the bamboo platform on the tracks became a melody of travel, offering an exhilarating ride that merged the contemporary with the timeless.

{{< figure src="./images/bamboo_train.jpg" alt="bamboo train" class="aligne-center size-medium" >}}

{{< figure src="./images/bat_cave.jpg" alt="bat cave" class="aligne-left size-small" >}}
As the day drew to a close, the Bat Cave beckoned—an awe-inspiring spectacle as millions of bats took to the skies in a breathtaking display of nature's wonders.

The day's adventures left me tired, hungry, but undeniably happy. 

## Thursday: Culinary Adventures and the Struggle with Sickness
<!-- 
feeling a bit sick

Cooking class at Smokin'pot
amazing
one on one
going to the market, buying the ingrediance for my menus
going to the country side home
cooking the traditional way
fish amok, lok lak, Stir Fry beef with Lemon gras
(amazing food and interesting insides in the culinary history of the country)

back feeling real sick
waiting for the bus
-->
The day dawned with a hint of weariness as a touch of sickness cast a subtle shadow over the horizon. Undeterred, I embarked on a culinary escapade that would unfold as a testament to the resilience of the spirit, even in the face of physical discomfort.

The stage was set at Smokin'Pot—a cooking class that promised not just a journey into Cambodian cuisine but an intimate exploration guided by the hands-on expertise of a one-on-one session.

The adventure began with a trip to the market—a sensory delight where vibrant colors and fragrant aromas painted the canvas of culinary exploration. We dove into the bustling market, selecting the freshest produce that would soon grace my culinary creations.

{{< figure src="./images/cooking_class.jpg" alt="cooking class" class="aligne-left size-medium" >}}

The journey continued to the countryside home—a tranquil retreat where the traditions of Cambodian cooking unfolded in the most authentic setting. The traditional methods became my guide as I immersed myself in the preparation of three iconic dishes—Fish Amok, Lok Lak, and Stir Fry Beef with Lemongrass.

The culinary odyssey not only yielded incredible flavors but also offered intriguing insights into the culinary history of the country. Each stir of the pot echoed the tales of generations, a rich tapestry woven with the threads of Cambodia's gastronomic heritage.

{{< figure src="./images/cooking_class_result.jpg" alt="cooking class result" class="aligne-center size-small" >}}

Returning to the hostel, a sudden surge of sickness gripped me—a stark contrast to the earlier culinary highs. As I grappled with the discomfort, the afternoon unfolded with a different tone. Waiting for the night bus became a test of endurance, made slightly more bearable by a nap on the hostel's couch, despite having already checked out.
