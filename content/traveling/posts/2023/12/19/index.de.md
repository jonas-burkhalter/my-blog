+++ 
draft = false
date = 2023-12-19T22:00:00+02:00
title = "Battambang"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Dienstag: Battambangs ruhige Umarmung
<!--
ankommen in Battambang

Entspanne den Nachmittag, schaue Serien
-->

Die Busfahrt führte mich zur Tür von Battambang – einer Stadt mit einem Charme, der Geschichten von französischer Kolonialarchitektur und dem gemächlichen Rhythmus des kambodschanischen Lebens flüsterte.

Bei der Ankunft erstrahlte der Nachmittag in der Wärme der Atmosphäre von Battambang und lockte mich, eine ruhige Pause zu umarmen. Der Tagesablauf nahm einen gemächlichen Verlauf, da ich mich entschied, die einfachen Freuden des Ausruhens zu geniessen.

Als die Nachmittagssonne zum Horizont sank, zog die Anziehungskraft der Entspannung mich ins Innere. Der beruhigende Schein eines Bildschirms und die vertrauten Erzählungen einer Serie wurden zu meinen Gefährten. Inmitten dieser friedlichen Auszeit entwickelte sich der Tag in gemächlichen Momenten – jede Episode ein Kapitel in der Erzählung eines Nachmittags, der in ruhiger Reflexion verbracht wurde.

{{< figure src="./images/dinner.jpg" alt="Abendessen" class="aligne-center size-medium" >}}

## Mittwoch: Battambangs Tapisserie aus Erlebnissen
<!-- 
Tuk Tuk-Tour
Bambus-Klebriger Reis
Wat Samroung Knong
Killing Fields
Reispapierherstellung
Reisweinherstellung und -verkostung
Bambusbahn (Highlight)
Fledermaushöhle (6,5 Millionen Fledermäuse, die herausfliegen)

müde, hungrig, aber glücklich
-->

{{< figure src="./images/bamboo_sticky_rice.jpg" alt="Bambus-Klebriger Reis" class="aligne-right size-small" >}}

Der Tag begann mit einer Tuk Tuk-Tour – einem rhythmischen Tanz durch die Strassen der Stadt, der die verborgenen Schätze in den Ecken enthüllte. Die Reise begann mit einem kulinarischen Genuss – der Verkostung von Bambus-Klebrigem Reis, einer lokalen Delikatesse, die die Geschmacksnerven mit ihren einzigartigen Aromen und Texturen ansprach.

{{< figure src="./images/temple.jpg" alt="Tempel" class="aligne-left size-smaller" >}}

Wat Samroung Knong erschien als nächstes – ein Heiligtum voller Geschichte und architektonischer Eleganz. Die Echos heiliger Momente hallten innerhalb seiner Mauern wider und boten einen Einblick in Cambodias spirituelles Erbe.

Der traurige, aber bedeutungsvolle Besuch der Killing Fields folgte – ein Ort von tiefgründiger historischer Bedeutung, an dem die Narben der kambodschanischen Vergangenheit offengelegt wurden. Die Luft hing schwer von der Last des Erinnerns, ein ehrendes Denkmal für die verlorenen Leben und ein Zeugnis der Widerstandsfähigkeit des menschlichen Geistes.

Ein Wandel in der Atmosphäre brachte das lebendige Treiben in einem Reispapierherstellungs-Workshop – eine Feier der kulinarischen Traditionen Kambodschas. Die Handwerkskunst der Arbeitshände verwandelte einfache Zutaten in zarte Blätter, ein Zeugnis der kulinarischen Tradition, die über Generationen weitergegeben wurde.

{{< figure src="./images/rice_wine.jpg" alt="Reiswein" class="aligne-right size-medium" >}}

Die Reise führte weiter zu einer Reisweinherstellungsstätte, wo die Alchemie der Gärung und Reifung ein Verkostungserlebnis hervorbrachte, das mit dem Wesen der ländlichen Traditionen Kambodschas resonierte.

Das Highlight des Tages entwickelte sich auf der Bambusbahn – eine einzigartige Transportmethode, die die malerischen Landschaften von Battambang durchquert. Das rhythmische Klappern der Bambusplattform auf den Gleisen wurde zu einer Melodie der Reise, die ein berauschendes Erlebnis bot, das das Zeitlose mit dem Zeitgenössischen verband.

{{< figure src="./images/bamboo_train.jpg" alt="Bambusbahn" class="aligne-center size-medium" >}}

{{< figure src="./images/bat_cave.jpg" alt="Fledermaushöhle" class="aligne-left size-small" >}}
Als der Tag zu Ende ging, lockte die Fledermaushöhle – ein atemberaubendes Spektakel, als Millionen von Fledermäusen in einem atemberaubenden Display der Naturwunder in den Himmel stiegen.

Die Abenteuer des Tages liessen mich müde, hungrig, aber unbestreitbar glücklich.

## Donnerstag: Kulinarische Abenteuer und der Kampf mit der Krankheit
<!-- 
sich etwas krank fühlen

Kochkurs bei Smokin'Pot
wunderbar
eins zu eins
Marktbesuch, Zutaten für meine Menüs kaufen
Fahrt zum Landleben
Traditionell kochen
Fisch Amok, Lok Lak, Rindfleisch-Stir Fry mit Zitronengras
(wunderbares Essen und interessante Einblicke in die kulinarische Geschichte des Landes)

Zurück fühle ich mich wirklich krank
Warten auf den Bus
-->

Der Tag begann mit einem Hauch von Abgeschlagenheit, als ein leichter Schatten von Krankheit den Horizont überzog. Unerschrocken begab ich mich auf ein kulinarisches Abenteuer, das ein Zeugnis für die Widerstandskraft des Geistes war – auch im Angesicht körperlicher Unannehmlichkeiten.

Die Bühne war gesetzt bei Smokin'Pot – ein Kochkurs, der nicht nur eine Reise in die kambodschanische Küche versprach, sondern eine intime Erkundung unter der Anleitung der praktischen Expertise eines Eins-zu-Eins-Session.

Das Abenteuer begann mit einem Besuch auf dem Markt – ein sinnlicher Genuss, bei dem lebendige Farben und duftende Aromen das Panorama der kulinarischen Erkundung malten. Wir tauchten ein in den geschäftigen Markt und wählten die frischesten Zutaten, die bald meine kulinarischen Kreationen bereichern sollten.

{{< figure src="./images/cooking_class.jpg" alt="Kochkurs" class="aligne-left size-medium" >}}

Die Reise führte weiter ins ländliche Zuhause – ein ruhiger Rückzugsort, wo die Traditionen des kambodschanischen Kochens in der authentischsten Umgebung entfalteten. Die traditionellen Methoden wurden zu meinem Führer, als ich mich in die Zubereitung von drei ikonischen Gerichten vertiefte – Fisch Amok, Lok Lak und Rindfleisch-Stir Fry mit Zitronengras.

Die kulinarische Odyssee brachte nicht nur unglaubliche Aromen hervor, sondern bot auch faszinierende Einblicke in die kulinarische Geschichte des Landes. Jeder Rührlöffel wiederholte die Geschichten von Generationen, ein reich gewebtes Tapisserie aus Kambodschas gastronomischem Erbe.

{{< figure src="./images/cooking_class_result.jpg" alt="Kochkurs-Ergebnis" class="aligne-center size-small" >}}

Zurück im Hostel griff plötzlich eine neue Welle der Krankheit nach mir – ein scharfer Kontrast zu den vorherigen kulinarischen Höhen. Während ich mit dem Unwohlsein kämpfte, entwickelte sich der Nachmittag mit einem anderen Ton. Warten auf den Nachtbus wurde zu einem Test der Ausdauer, der durch ein Nickerchen auf dem Couch des Hostels, trotz des Auscheckens, etwas erträglicher gemacht wurde.
