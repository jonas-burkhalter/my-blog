+++ 
draft = false
date = 2023-12-15T22:00:00+02:00
title = "Siem Reap"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Friday: Siem Reap's Dawn Embrace
<!-- 
arriving early morning in siem reap

go to a coffee until the room is ready
do some research and planning

relaxe in the room (cuddle)

get dinner 

tired from the traveling
-->

{{< figure src="./images/night_bus.jpg" alt="night bus" class="aligne-left size-small" >}}

The wheels of the night bus carried us through the quiet predawn hours, depositing us in the heart of Siem Reap as the first light painted the sky.

Arriving in the early morning hours, the city was a canvas awakening to the hues of a new day. Eager to embrace the unfolding adventure, we found solace in a local coffee shop, where the comforting aroma of freshly brewed coffee accompanied the anticipation of the day's explorations.

As midday unfolded, the room became a sanctuary of relaxation—a haven where fatigue from the night's journey dissipated, replaced by the gentle embrace of quiet moments and shared cuddles.

As the day progressed, the culinary delights of Siem Reap beckoned. Dinner became a sensory journey, as local flavors mingled with the weariness of travel, creating a unique tapestry of tastes and experiences.

Tiredness settled in, a gentle reminder of the miles traversed and the diverse landscapes encountered. Yet, within the weariness, there lingered a quiet satisfaction—the contentment of arriving in a new city, the promise of discoveries yet to unfold, and the comforting thought of a restful night ahead.

## Saturday: A Tapestry of Temples and Sunset Hues
<!-- 
afternoon at angkor wat (with a tuk tuk)
banteay kdei
sras srang
ta prohm
victory gate
bayton
angkor wat (at sunset)

nice dinner
-->

The sun ascended in Siem Reap, casting its golden glow on a city steeped in history. Eager to explore the fabled temples of Angkor Wat, we embarked on an afternoon odyssey with a trusty tuk-tuk as our chariot.

{{< figure src="./images/banteay_kdei.jpg" alt="Banteay Kdei" class="aligne-right size-small" >}}

The first stop on this archaeological journey was Banteay Kdei, a temple veiled in the shadows of ancient trees. Here, the echoes of centuries past resonated through the weathered stones, and the intricate carvings whispered tales of a bygone era.

Sras Srang, the "Royal Bath," emerged next—a tranquil reservoir where the ancient Khmer kings once sought respite. The still waters reflected the remnants of a once-grand platform, inviting contemplation on the passage of time.

{{< figure src="./images/ta_prohm.jpg" alt="Ta Prohm" class="aligne-left size-medium" >}}

The jungle-clad embrace of Ta Prohm awaited—a cinematic setting where colossal roots intertwined with ancient stones. The dance of light and shadow created a surreal ambiance, transporting us to a realm where nature and architecture coexisted in harmonious entropy.

{{< figure src="./images/victory_gate.jpg" alt="Victory Gate" class="aligne-right size-small" >}}

Passing through the Victory Gate, a triumphal arch of symmetry and grandeur, we arrived at Bayon. The enigmatic faces carved into towering stone towers gazed down, their serene expressions evoking a sense of timeless wisdom.

{{< figure src="./images/angkor_wat.jpg" alt="Angkor Wat" class="aligne-left size-medium" >}}

As the day drew to a close, the grandeur of Angkor Wat unfolded before us—an iconic silhouette against the canvas of the setting sun. The temple's reflection shimmered in the surrounding waters, casting a spell that transcended the temporal realm.

{{< figure src="./images/angkor_wat_inside.jpg" alt="Angkor Wat" class="aligne-center size-large" >}}

With the day's archaeological explorations behind us, we ventured to a local eatery for a dinner that mirrored the richness of my cultural encounters. The flavors of Khmer cuisine mingled with the lingering enchantment of the temples, creating a culinary tapestry that mirrored the day's journey.

## Sunday: Whispers of Change and Shared Horizons
<!-- 
last day together with noe

planning our next upcoming (january) traveling together  

cuddling
enjoying the time together

change of plans, noe is unhappy with our current plan
what can we do?
-->
As the sun painted the Siem Reap sky with hues of dawn, the day unfolded, marking the last moments shared with Noë—a chapter of companionship and shared adventures.

The morning was dedicated to the art of planning, as we huddled over maps and itineraries, weaving the threads of our upcoming January travels. The possibilities stretched before us like an open book, each page waiting to be written with the tales of exploration that lay ahead.

Cuddling became a language of its own—an unspoken dialogue that transcended words and deepened the connection. In these quiet moments, the warmth of shared experiences lingered, creating a sanctuary in which time seemed to slow.

The day unfolded in the simple joy of enjoying each other's company, embracing the transient beauty of shared time. Laughter and conversation filled the air, becoming the soundtrack to a day that reveled in the present.

However, as the day progressed, a subtle shift occurred. A change of plans emerged, as Noë expressed discontent with our current trajectory. In the face of uncertainty, questions hung in the air—what adjustments could be made, and how could we ensure that our journey continued to align with shared aspirations?

## Monday: A Day of Decisions, Tests, and New Horizons
<!-- 
conclusion is we meet a week later)

early morning decision (extra day together, do some medical test)
an extra day with noe
bad reason but a nice day

doing some medical tests
releave after the result

waffles again

book flights to new zealand
try to book a camper van
plans are made 
 -->
The Siem Reap dawn cast its gentle light on a day marked by unexpected decisions and the subtle undercurrent of change.

In the early morning hours, a decision unfolded—a choice to extend our shared journey by an extra day. A blend of circumstances and desire converged, leading to an additional day with Noë. The reason behind the extension carried a weight, but the day itself unfolded as a testament to the resilience of shared moments.

As the day commenced, a practical yet necessary turn took us to a medical facility. Anticipation hung in the air as we awaited the results—a delicate dance between hope and pragmatism. The eventual relief that followed painted the canvas of the day in hues of reassurance, affirming that, for now, the journey could continue unburdened.

Waffles, a familiar indulgence, provided a sweet interlude—an echo of shared breakfasts that had become a cherished ritual. Amidst the swirl of flavors, plans for the future began to crystallize.

The day evolved into a pivotal moment—a juncture where flights to New Zealand were booked, and the dream of traversing its landscapes in a camper van took shape. The clicking of keys and the glow of screens became the soundtrack to a day marked by the crafting of new horizons.

Plans, once abstract, solidified into tangible itineraries, and the excitement of upcoming adventures infused the air with a renewed energy. In the dance of booking confirmations and itinerary details, the day reached its crescendo—a conclusion that hinted at the vast landscapes awaiting exploration.

## Tuesday: Farewells and Solo Sojourns
<!--
good bye noe
we will meet again in new zealand!!

bus to Battambang (traveling alone again)
-->

As the Siem Reap sun heralded a new day, emotions lingered in the air—a blend of farewells and the anticipation of solo adventures on the horizon.

The morning unfolded with a bittersweet goodbye to Noë. Promises of future reunions echoed in the air, with New Zealand as the envisioned backdrop for the next chapter of shared exploration. The parting embrace held the weight of transient separation, tempered by the optimism that new encounters awaited.

Alone once more, I embarked on a bus journey to Battambang—a solo sojourn that marked a return to the rhythm of independent exploration. The landscapes outside the window transformed, weaving tales of Cambodia's rural beauty and cultural tapestry.
