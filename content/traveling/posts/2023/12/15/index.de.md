+++  
draft = false  
date = 2023-12-15T22:00:00+02:00  
title = "Siem Reap"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++  

## Freitag: Siem Reaps Morgenumarmung  
<!--  
ankommen am frühen Morgen in Siem Reap  

geh zu einem Kaffee bis das Zimmer fertig ist  
Forschung und Planung machen  

Erholen im Zimmer (kuscheln)  

Abendessen  

müde vom Reisen  
-->

{{< figure src="./images/night_bus.jpg" alt="night bus" class="aligne-left size-small" >}}

Die Räder des Nachtbusses trugen uns durch die stillen Morgenstunden, bis wir im Herzen von Siem Reap abgesetzt wurden, während das erste Licht den Himmel färbte.

Am frühen Morgen war die Stadt ein wachsendes Canvas, das sich langsam den Farben eines neuen Tages öffnete. Eifrig, das Abenteuer zu beginnen, fanden wir Zuflucht in einem kleinen Kaffee, dessen duftender Kaffeedampf die Erwartung auf die Erkundung des heutigen Tages begleitete.

{{< figure src="./images/night_bus.jpg" alt="night bus" class="aligne-left size-small" >}}

Mittags wurde das Zimmer zu einem Rückzugsort der Erholung – ein Zufluchtsort, an dem die Müdigkeit der letzten Reise schwand und durch die sanfte Umarmung ruhiger Momente und gemeinsamer Kuscheleinheiten ersetzt wurde.

Als der Tag voranschritt, lockten die kulinarischen Köstlichkeiten von Siem Reap. Das Abendessen wurde zu einer sinnlichen Reise, bei der lokale Aromen mit der Erschöpfung der Reise verschmolzen und ein einzigartiges Tapestry der Eindrücke und Erlebnisse erschufen.

Die Erschöpfung legte sich wie ein sanfter Schleier über uns – eine stille Erinnerung an die zurückgelegten Kilometer und die vielfältigen Landschaften, die wir erkundet hatten. Doch in der Erschöpfung lag eine stille Zufriedenheit – die Zufriedenheit, in einer neuen Stadt angekommen zu sein, die Versprechen von Entdeckungen, die noch bevorstanden, und die tröstliche Vorstellung einer erholsamen Nacht.

## Samstag: Ein Teppich aus Tempeln und Sonnenuntergangshimmeln  
<!--  
Nachmittag in Angkor Wat (mit einem Tuk Tuk)  
Banteay Kdei  
Sras Srang  
Ta Prohm  
Victory Gate  
Bayon  
Angkor Wat (bei Sonnenuntergang)  

schönes Abendessen  
-->

Die Sonne erhob sich in Siem Reap und warf ihr goldenes Licht auf eine Stadt, die voller Geschichte ist. Eifrig, die berühmten Tempel von Angkor Wat zu erkunden, begaben wir uns mit einem verlässlichen Tuk-Tuk auf eine nachmittägliche Odyssee.

{{< figure src="./images/banteay_kdei.jpg" alt="Banteay Kdei" class="aligne-right size-small" >}}

Der erste Halt dieser archäologischen Reise war Banteay Kdei, ein Tempel, der von uralten Bäumen beschattet wird. Hier hallten die Echoes vergangener Jahrhunderte durch die gealterten Steine, und die detaillierten Schnitzereien flüsterten Geschichten einer vergangenen Ära.

{{< figure src="./images/ta_prohm.jpg" alt="Ta Prohm" class="aligne-left size-medium" >}}

Der von Dschungel umgebene Ta Prohm erwartete uns – ein cinematisches Setting, in dem riesige Wurzeln mit alten Steinen verschmelzen. Das Spiel von Licht und Schatten erschuf eine surreale Atmosphäre, die uns in eine Welt versetzte, in der Natur und Architektur in harmonischer Zerfallskunst miteinander verschmolzen.

{{< figure src="./images/victory_gate.jpg" alt="Victory Gate" class="aligne-right size-small" >}}

Durch das Victory Gate, ein triumphaler Bogen der Symmetrie und Pracht, erreichten wir Bayon. Die rätselhaften Gesichter, die in hohe Steintürme geschnitzt sind, starrten herab und liessen ihre ruhigen Ausdrücke Weisheit ausstrahlen.

{{< figure src="./images/angkor_wat.jpg" alt="Angkor Wat" class="aligne-left size-medium" >}}

Als der Tag sich seinem Ende neigte, öffnete sich die Pracht von Angkor Wat vor uns – eine ikonische Silhouette gegen die Leinwand des Sonnenuntergangs. Die Reflexion des Tempels schimmerte in den umgebenden Gewässern und legte einen Zauber über, der die zeitliche Dimension überschritt.

{{< figure src="./images/angkor_wat_inside.jpg" alt="Angkor Wat" class="aligne-center size-large" >}}

Mit den archäologischen Erkundungen des Tages hinter uns, begaben wir uns in ein lokales Restaurant, dessen Abendessen die Vielfalt meiner kulturellen Erlebnisse widerspiegelte. Die Aromen der Khmer-Küche vermischten sich mit dem anhaltenden Zauber der Tempel und erschufen ein kulinarisches Tapestry, das die Erlebnisse des Tages widerspiegelte.

## Sonntag: Flüsternde Veränderungen und geteilte Horizonte  
<!--  
letzter Tag zusammen mit Noë  

unsere nächsten Reisen (Januar) zusammen planen  

kuscheln  
die Zeit zusammen geniessen  

Änderungen der Pläne, Noë ist mit unserem aktuellen Plan unzufrieden  
was können wir tun?  
-->

{{< figure src="./images/breakfast.jpg" alt="breakfast" class="aligne-left size-medium" >}}

Als die Sonne den Siem Reap-Himmel mit Dämmerungshimmel einfärbte, entfaltet sich der Tag und markiert die letzten Momente, die ich mit Noë geteilt habe – ein Kapitel von Kameradschaft und gemeinsamen Abenteuern.

Der Morgen war dem Planen gewidmet, da wir Karten und Routen ausbreiteten und die Fäden unserer kommenden Januar-Reisen knüpften. Die Möglichkeiten erstreckten sich vor uns wie ein offenes Buch, das darauf wartete, mit den Erzählungen der bevorstehenden Erkundungen geschrieben zu werden.

Kuscheln wurde zu einer Sprache – ein ungesprochenes Gespräch, das über Worte hinausging und die Verbindung vertiefte. In diesen ruhigen Momenten schwebte die Wärme gemeinsamer Erlebnisse, erschuf einen Rückzugsort, in dem die Zeit zu verweilen schien.

Der Tag entwickelte sich im einfachen Genuss der Gesellschaft des anderen – die Schönheit der geteilten Zeit geniessen. Lachen und Gespräche erfüllten die Luft und wurden zum Soundtrack eines Tages, der sich im Jetzt ergoss.

Doch im Laufe des Tages trat eine subtile Veränderung auf. Eine Planänderung trat in Kraft, da Noë mit unserem aktuellen Kurs unzufrieden war. Vor der Unsicherheit hingen Fragen in der Luft – welche Anpassungen könnten gemacht werden, und wie könnten wir sicherstellen, dass unsere Reise weiterhin mit gemeinsamen Bestrebungen übereinstimmt?

## Montag: Ein Tag der Entscheidungen, Tests und neuen Horizonte  
<!--  
Entscheidung früh am Morgen (zusätzlicher Tag zusammen, medizinische Tests machen)  
ein zusätzlicher Tag mit Noë  
schlechter Grund, aber ein schöner Tag  

medizinische Tests machen  
Erleichterung nach dem Ergebnis  

Waffeln wieder  

Flüge nach Neuseeland buchen  
versuchen, ein Camper zu buchen  
Pläne werden gemacht  
-->

Die Siem Reap-Dämmerung warf ihr sanftes Licht auf einen Tag, der von unerwarteten Entscheidungen und einem leisen Wandel geprägt war.

In den frühen Morgenstunden entfaltet sich eine Entscheidung – eine Wahl, die unsere gemeinsame Reise um einen zusätzlichen Tag verlängerte. Ein Zusammenspiel aus Umständen und Wunsch vereinte sich und führte zu einem weiteren Tag mit Noë. Der Grund hinter der Verlängerung hatte Gewicht, aber der Tag selbst zeigte sich als Beweis für die Widerstandsfähigkeit gemeinsamer Momente.

{{< figure src="./images/breakfast.jpg" alt="breakfast" class="aligne-left size-medium" >}}

Am Tag begab sich eine praktische, aber notwendige Wende, als wir ein medizinisches Zentrum aufsuchen mussten. Erwartung lag in der Luft, als wir auf die Ergebnisse warteten – ein zarter Tanz zwischen Hoffnung und Pragmatismus. Die letztendliche Erleichterung, die folgte, malte den Tag in Farben der Gewissheit, bestätigte, dass die Reise nun ohne Belastung fortgesetzt werden konnte.

Waffeln, eine vertraute Genussweise, boten ein süsses Zwischenspiel – ein Echo gemeinsamer Frühstücksroutinen, die zu einem geschätzten Ritual geworden waren. Mittendrin begannen Pläne für die Zukunft Gestalt anzunehmen.

Der Tag entwickelte sich zu einem Schlüsselmoment – ein Wendepunkt, an dem Flüge nach Neuseeland gebucht wurden und der Traum, seine Landschaften in einem Camper zu durchqueren, Form annahm. Das Klicken von Tasten und das Leuchten von Bildschirmen wurden zum Soundtrack eines Tages, der von der Gestaltung neuer Horizonte geprägt war.

Pläne, die einst abstrakt waren, solidifizierten sich zu greifbaren Reiserouten, und die Aufregung über bevorstehende Abenteuer durchzog die Luft mit einer neuen Energie. In dem Tanz der Buchungsbestätigungen und Reiseplanungsdetails erreichte der Tag seinen Höhepunkt – eine Schlussfolgerung, die auf die weiten Landschaften hinwies, die auf Erkundung warteten.

## Dienstag: Abschiede und Solo-Entdeckungen  
<!--  
Lebewohl, Noë  
Wir sehen uns wieder in Neuseeland!!  

Bus nach Battambang (wieder allein reisen)  
-->

{{< figure src="./images/beach.jpg" alt="beach" class="aligne-right size-medium" >}}

Als die Siem Reap-Sonne einen neuen Tag begrüsste, schwebten Emotionen in der Luft – eine Mischung aus Abschieden und der Erwartung auf Solo-Abenteuer in der Zukunft.

Der Morgen begann mit einem bittersüssen Abschied von Noë. Versprechungen künftiger Wiedersehen hallten in der Luft, mit Neuseeland als dem beabsichtigten Rahmen für das nächste Kapitel gemeinsamer Erkundungen. Die Umarmung des Abschieds hatte das Gewicht vorübergehender Trennung, wurde aber durch die Zuversicht gemildert, dass neue Begegnungen bevorstehen.

Allein einmal mehr, begab ich mich auf eine Busreise nach Battambang – eine Solo-Odyssee, die eine Rückkehr zum Rhythmus unabhängiger Erkundung markierte. Die Landschaften ausserhalb des Fensters verwandelten sich, webten Geschichten von Kambodschas ländlicher Schönheit und kultureller Tapisserie.  
