+++  
draft = false  
date = 2023-12-01T22:00:00+02:00  
title = "Phnom Penh"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Freitag: Ankunft in Kambodscha—Begegnungen und Vorfreude  
<!--  
Ankunft in Kambodscha  
jemand schlägt mit einem riesigen Stock auf den Bus (ein weiterer toller Start in einem neuen Land)  
Geld abheben  
einchecken  
Essen holen (Shara treffen)  
auf Noë warten  
endlich ankommen  
-->  

Der Tag begann mit dem Überqueren der Grenze und markierte meine Ankunft in Kambodscha. Mit den ersten Schritten auf neuem Boden begrüsste ich die Vorfreude auf kulturelle Eigenheiten und die Geschichten, die dieses Land zu erzählen hatte.  

{{< figure src="./images/bus_ride.jpg" alt="Busreifenwechsel" class="aligne-right size-small" >}}  

Der Start in Kambodscha begann buchstäblich mit einem Knall, als der Bus eine unerwartete Begrüssung erhielt—ein lautes Klopfen, als jemand mit einem riesigen Stock auf den Bus schlug. Ein weiterer exzentrischer Empfang in einem neuen Land und ein Zeugnis für die unvorhersehbare Natur des Reisens.  

Als erstes stand eine praktische Aufgabe an—Geld abheben, um die lokalen Märkte und Erlebnisse zu erkunden. Die Suche nach kambodschanischen Riel fügte dem Tag eine Note der Alltagsorganisation hinzu.  

Auf der Suche nach lokaler Küche machte ich mich auf, etwas zu essen, und traf dabei Shara—eine Begegnung, die dem Tag eine menschliche Note verlieh und mich daran erinnerte, wie viele Verbindungen an neuen Orten entstehen können.  

Der Tag mündete in eine Phase des Wartens, erfüllt von der Vorfreude auf die Ankunft von Noë—ein Wiedersehen, das die Aussicht auf gemeinsame Erlebnisse in einer neuen Stadt mit sich brachte.  

Schliesslich endete das Warten mit der Freude des Wiedersehens, als Noë ankam und die Distanz mit der Wärme vertrauter Gesichter in einem fremden Land überbrückte. Das Lachen und der Austausch gemeinsamer Geschichten füllten die Luft, während der Tag nahtlos in die Geselligkeit der Nacht überging.  

## Samstag: Ein Tag voller Gemütlichkeit und italienischer Genüsse  
<!--  
Kuschel-Tag  
zusammen im Bett entspannen  
Frühstück holen  
zurück ins Bett  
noch ein paar Stunden kuscheln  
Abendessen (Pasta Pesto & Pizza)  
in romantischem italienischen Restaurant  
zurück ins Bett  
eine Serie zusammen anfangen (Derry Girls)  
-->  

Der Tag war eine Oase der Gemütlichkeit—eine bewusste Pause, um die einfache Freude der Zweisamkeit zu geniessen.  

Der Morgen begann mit einem gemächlichen Start, eingehüllt in die wohlige Wärme gemeinsamer Stunden. Ein Ausflug zum Frühstück brachte eine Prise Genuss in den Tag, eine Einstimmung auf die entspannten Stunden, die vor uns lagen.  

Zurück in die Umarmung von Decken kehrend, gaben wir uns der Verlockung des Ausruhens hin und verbrachten Stunden in Gesellschaft des Anderen, genossen die Schönheit des gemeinsamen Nichtstuns.  

{{< figure src="./images/dinner.jpg" alt="Abendessen mit Noë" class="aligne-left size-smaller" >}}  

Als der Abend näher rückte, lockte der Duft italienischer Küche. Das Abendessen wurde zu einem Erlebnis aus Pasta Pesto und Pizza, genossen in einem romantischen italienischen Restaurant—eine exquisite Verschmelzung von Aromen in einer charmanten Atmosphäre.  

Mit gesättigten Mägen zogen wir uns in die Vertrautheit unseres Rückzugsortes zurück und tauchten in ein gemeinsames Hobby ein. Die Nacht begann mit der ersten Folge einer neuen Serie—*Derry Girls*—eine humorvolle Reise in die Welt des irischen Charmes.  

## Sonntag: Reflexionen über Geschichte und kulinarische Genüsse  
<!--  
lange im Bett bleiben  
Frühstück holen (Reis und Pad Thai)  
S21/Tuol Sleng besuchen (kurzer historischer Absatz über die Khmer Rouge schreiben)  
leckeres indisches Abendessen  
-->  

Der Tag begann als ein gemächlicher Wohlklang, mit der Morgensonne, die einen sanften Glanz auf einen Tag der Ruhe warf. Es war ein Tag, an dem wir lange im Bett blieben und die Stille des Augenblicks genossen.  

Aus unserem Kokon auftauchend, machten wir uns auf den Weg zum Frühstück—eine Mischung aus Reis und Pad Thai, eine Fusion von Aromen, die die kulinarische Vielfalt Kambodschas andeutete.  

{{< figure src="./images/tuol_sleng_noe.jpg" alt="Tuol Sleng" class="aligne-right size-smaller" >}}  

Am Nachmittag begaben wir uns auf eine nachdenkliche Reise in die Vergangenheit Kambodschas. Das S21/Tuol Sleng Genozid-Museum zeugte von einem dunklen Kapitel—der Ära der Khmer Rouge. Die Wände dieser ehemaligen Schule flüsterten von den Gräueltaten, eine eindringliche Erinnerung an die menschlichen Kosten ideologischer Extreme.  

### Tuol Sleng  
Tuol Sleng, einst eine Schule, die während des Khmer-Rouge-Regimes in ein Folter- und Verhörzentrum umgewandelt wurde, steht als erschreckendes Zeugnis der tragischen Geschichte Kambodschas. Die Wände, geschmückt mit erschütternden Fotografien der Opfer, erzählen von der Brutalität und dem Leid, das sich innerhalb dieser Mauern abspielte. Jede Aufnahme erzählt eine erschütternde Geschichte, und die erhaltenen Räume zeugen von den begangenen Gräueltaten.  

{{< figure src="./images/tuol_sleng.jpg" alt="Tuol Sleng" class="aligne-center size-small" >}}  

Der Abend nahm eine andere kulturelle Wendung—ein köstliches indisches Abendessen. Die Aromen von Curry und Gewürzen boten einen wohlschmeckenden Kontrast zu den am Nachmittag erfahrenen schweren Eindrücken.  

## Montag: Von kulinarischen Genüssen zum Charme von Kep  
<!--  
Frühstück in der schicken Paris Baguette  
Mini-Van nach Kep  
-->  

Der Tag begann mit einer kulinarischen Erkundung, die den Ton für die bevorstehenden Abenteuer angab. Das Frühstück lockte in der schicken Paris Baguette mit köstlichem Gebäck und Gourmet-Leckereien.  

Gestärkt und bereit für den Tag begaben wir uns auf die nächste Etappe unserer Reise—eine Fahrt im Mini-Van nach Kep. Die Landschaft entfaltete sich jenseits der Fenster, eine sich bewegende Leinwand der ländlichen Schönheit Kambodschas.  

{{< figure src="./images/mini_van.jpg" alt="Mini-Van" class="aligne-center size-larger" >}}  
