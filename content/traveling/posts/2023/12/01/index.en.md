+++ 
draft = false
date = 2023-12-01T22:00:00+02:00
title = "Phnom Penh"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Friday: Arriving in Cambodia—Encounters and Anticipation
<!--
arriving in cambodia
somebody hitting the bus with a huge stick (another great beginning in a new country)

getting cash

checking in

get some food (meet Shara)

waiting for Noë
finally arraving
 -->

The day unfolded with the crossing of borders, marking my arrival in Cambodia. Stepping onto new soil, I embraced the anticipation of cultural nuances and the stories that this land held.

{{< figure src="./images/bus_ride.jpg" alt="bus tire change" class="aligne-right size-small" >}}

The start in Cambodia began with a literal bang as the bus faced an unexpected greeting—a resounding thud as someone, with what seemed like a colossal stick, struck the bus. It marked yet another eccentric welcome to a new country, a testament to the unpredictable nature of travel.

First on the agenda was a practical necessity—getting cash to navigate the local markets and experiences that awaited. The quest for Cambodian riels added a touch of practicality to the day's adventures.

Eager to savor the local flavors, I ventured out to get some food and, in a stroke of serendipity, met Shara—an encounter that added a human touch to the journey, a reminder of the connections waiting to be forged in new places.

The day unfolded into an interlude of waiting, with the anticipation of meeting Noë—a reunion that held the promise of shared experiences in a new city. Time ticked away as I lingered in the twilight of expectation.

Finally, the wait culminated in the joy of reunion as Noë arrived, bridging the gap of distance with the warmth of familiar faces in a foreign land. The echoes of laughter and the chatter of shared tales filled the air as the day seamlessly transitioned into the camaraderie of the night.

## Saturday: A Day of Cozy Bliss and Italian Delights
<!-- 
cuddle day

relax together in bed

get some breakfast

go back to bed
cuddle for some more hours

get dinner (pasta pesto & pizza) 
at romantice italian restaurant

back to bed
start to watch a serie together (derry girls)
-->

The day unfolded as a sanctuary of coziness—a respite from the perpetual motion of travel. It was a cuddle day, a deliberate pause to bask in the simple joy of togetherness.

The morning began with a leisurely start, cocooned in the comfort of shared warmth. A venture out for breakfast added a touch of indulgence to the day, a prelude to the languid hours that lay ahead.

Returning to the cocoon of blankets, we surrendered to the allure of rest, spending hours in each other's company, savoring the beauty of doing nothing together.

{{< figure src="./images/dinner.jpg" alt="dinner with Noë" class="aligne-left size-smaller" >}}

As the day transitioned into evening, the aroma of Italian cuisine beckoned. Dinner became an experience of pasta pesto and pizza, enjoyed at a romantic Italian restaurant—an exquisite fusion of flavors in a charming setting.

With satiated appetites, we retreated to the familiar embrace of our sanctuary, diving into a shared pastime. The night unfolded with the beginning of a new series—Derry Girls—a delightful journey into the world of laughter and Irish humor.

## Sunday: Reflections on History and Culinary Delights
<!-- 
stay in bed for long

get some breakfast (rice and pad thai)

go to the S21 / tuol sleng (write a short historic paragraph about the khmer rough)

get delicouse indian dinner
-->

The day unfolded as a languid symphony, with the morning sun casting a gentle glow on a day of leisure. It was a stay-in-bed-for-long kind of day, where time seemed to linger, allowing us to savor the quietude of the moment.

Emerging from our cocoon, we ventured out for breakfast—an ensemble of rice and pad Thai, a fusion of flavors that hinted at the culinary diversity awaiting exploration in Cambodia.

{{< figure src="./images/tuol_sleng_noe.jpg" alt="Tuol Sleng" class="aligne-right size-smaller" >}}

In the afternoon, we embarked on a sobering journey into Cambodia's past. The S21/Tuol Sleng Genocide Museum bore witness to a dark chapter—the Khmer Rouge era. The walls of this former school whispered tales of atrocities, a haunting reminder of the human cost of ideological extremism. It was a sobering experience, inviting reflection on the resilience of the human spirit amid adversity.

### Tuol Sleng
Tuol Sleng, once a high school turned into a torture and interrogation center during the Khmer Rouge regime, stands as a chilling testament to Cambodia's tragic history. The walls, adorned with haunting photographs of victims, convey the brutality and suffering that transpired within its confines. Each image tells a harrowing story, and the preserved rooms bear witness to the atrocities committed against innocent lives. 

{{< figure src="./images/tuol_sleng.jpg" alt="Tuol Sleng" class="aligne-center size-small" >}}

The evening transitioned into a different cultural exploration—a delicious Indian dinner. The flavors of curry and spices became a palate-pleasing contrast to the weight of history encountered earlier in the day.

## Monday: From Culinary Delights to the Charm of Kep
<!-- 
get breakfast at the fancy paris baguette

Mini Van to Kep
 -->

The day began with a culinary escapade, setting the tone for the adventures that lay ahead. Breakfast beckoned at the fancy Paris Baguette, a feast for the senses with delectable pastries and gourmet delights.

Satiated and fueled for the day, we embarked on the next leg of our journey—an adventure in a mini-van bound for Kep. The landscape unfolded beyond the windows, a moving canvas of Cambodia's rural beauty.

{{< figure src="./images/mini_van.jpg" alt="mini van" class="aligne-center size-larger" >}}
