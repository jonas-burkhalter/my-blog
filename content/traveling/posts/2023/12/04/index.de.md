+++  
draft = false  
date = 2023-12-04T22:00:00+02:00  
title = "Kep"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++  

## Montagabend: Den Sonnenuntergang und kulinarische Genüsse in Kep geniessen  
<!--  
Ankunft  
Sonnenuntergang am Pier  
mit Noë  
Abendessen (Lok Lak)  
-->  

Die Ankunft in Kep markierte den Beginn eines Abends, durchzogen von der Küstenromantik dieses malerischen Ortes. Die Abenteuer des Tages mündeten nahtlos in die Erkundung der landschaftlichen Schönheiten von Kep.  

Als die Sonne begann, zu sinken, zog es uns zum Pier, einem malerischen Aussichtspunkt, um den Tag würdevoll ausklingen zu lassen. Der Sonnenuntergang, gemalt in den Farben von Bernsteintönen und Korallen, zauberte eine ruhige Stimmung und bot einen Moment der Reflexion in Gesellschaft von Noë.  

{{< figure src="./images/sunset_1_noe.jpg" alt="Sonnenuntergang" class="aligne-right size-small" >}}  
{{< figure src="./images/sunset_1_noe_and_i.jpg" alt="Sonnenuntergang" class="aligne-left size-medium" >}}  
{{< figure src="./images/sunset_1_noe_2.jpg" alt="Sonnenuntergang" class="aligne-right size-large" >}}  
<div style="clear:both;"></div>  

Mit dem Tag, der Lebewohl sagte, wurde das Abendessen zu einer Feier der kambodschanischen Aromen. Lok Lak, eine kulinarische Köstlichkeit, zierte den Tisch—eine Verkörperung des reichen gastronomischen Teppichs Kambodschas. Die vermischenden Aromen und die lebendigen Farben spiegelten die kulturelle Vielfalt wider, die Kep zu bieten versprach.  

{{< figure src="./images/lok_lak.jpg" alt="Lok Lak" class="aligne-center size-medium" >}}  

## Dienstag: Ein Strandtag und kulinarische Erkundungen  
<!--  
Strandtag  
nicht in Stimmung  
spazieren gehen  
Krabbenmarkt  
Abendessen  
-->  

Der Tag begann mit dem Versprechen eines Strandtages—eine Leinwand aus Sonne, Meer und Sand erstreckte sich vor uns. Doch manchmal diktiert die Stimmung ein anderes Tempo. Nicht in Stimmung für den Strand, entschied ich mich für einen gemächlichen Spaziergang, um mich in der Küstenatmosphäre zu verlieren, ohne den sandigen Einschub.  

{{< figure src="./images/house.jpg" alt="Haus" class="aligne-left size-medium" >}}  
{{< figure src="./images/boat.jpg" alt="Boot" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

{{< figure src="./images/crab_market.jpg" alt="Krabbenmarkt" class="aligne-left size-smaller" >}}  
Die Erkundung der lebhaften gastronomischen Landschaft von Kep führte uns zum Krabbenmarkt—einem geschäftigen Zufluchtsort der maritimen Schätze. Der Markt, ein Kaleidoskop aus Sinnen, präsentierte eine Vielzahl frisch gefangener Meeresfrüchte, wobei jede Verkaufsbude ein Schaufenster der ozeanischen Kostbarkeiten war. Obwohl wir die lebendige Atmosphäre des Marktes schätzten, führte unser persönlicher Geschmack uns davon, uns auf die Meeresfrüchte-Angebote einzulassen.  

Mit dem Tag, der sich zum Abend hin neigte, wurde das Abendessen zur Fortsetzung unserer kulinarischen Entdeckungen. Die Aromen von Kep erfüllten unsere Essenszeit, und schufen eine Symphonie von Geschmacksrichtungen, die die Küstenerträge feierte.  

{{< figure src="./images/sunset_2_noe.jpg" alt="Sonnenuntergang" class="aligne-center size-medium" >}}  
