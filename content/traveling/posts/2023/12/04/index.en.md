+++ 
draft = false
date = 2023-12-04T22:00:00+02:00
title = "Kep"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Monday Evening: Embracing Sunset and Culinary Delights in Kep
<!-- 
arriving 

sunset at the pier
with noe

dinner (Lok lak)
 -->

{{< figure src="./images/sunset_1_noe.jpg" alt="sunset" class="aligne-right size-small" >}}

Arriving in Kep marked the beginning of an evening infused with the coastal charm of this quaint destination. The day's adventures unfolded seamlessly into an exploration of Kep's scenic delights.

As the sun began its descent, we found ourselves drawn to the pier, a picturesque vantage point to witness the day gracefully bowing out. The sunset, painted in hues of amber and coral, cast a tranquil spell, offering a moment of reflection in the company of Noë.

{{< figure src="./images/sunset_1_noe_and_i.jpg" alt="sunset" class="aligne-left size-medium" >}}
{{< figure src="./images/sunset_1_noe_2.jpg" alt="sunset" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

With the day bidding adieu, dinner became a celebration of Cambodian flavors. Lok Lak, a culinary delight, adorned the table—an embodiment of Cambodia's rich gastronomic tapestry. The mingling aromas and vibrant colors mirrored the cultural diversity that Kep promised to unveil.

{{< figure src="./images/lok_lak.jpg" alt="Lok lak" class="aligne-center size-medium" >}}

## Tuesday: A Beach Day and Culinary Explorations
<!-- 
beach day
not in the mood
walking aroud

crab market

dinner 
 -->

The day unfolded with the promise of a beach day—a canvas of sun, sea, and sand stretched out before us. However, sometimes, the mood dictates a different rhythm. Not in the mood for the beach, I opted for a leisurely stroll around, immersing myself in the coastal atmosphere without the sandy interlude.

{{< figure src="./images/house.jpg" alt="house" class="aligne-left size-medium" >}}
{{< figure src="./images/boat.jpg" alt="boat" class="aligne-right size-medium" >}}

<div style="clear:both;"></div>

{{< figure src="./images/crab_market.jpg" alt="crab market" class="aligne-left size-smaller" >}}
Exploring the vibrant gastronomic landscape of Kep, we found ourselves immersed in the lively atmosphere of the Crab Market—a bustling haven of maritime wonders. The market, a sensory kaleidoscope, presented an assortment of freshly caught seafood, each stall a showcase of oceanic treasures. Although we appreciated the market's lively ambiance, our personal tastes led us away from indulging in the seafood offerings.

As the day transitioned into evening, dinner became a continuation of our culinary explorations. The flavors of Kep infused our dining experience, creating a symphony of tastes that celebrated the coastal bounty.

{{< figure src="./images/sunset_2_noe.jpg" alt="sunset" class="aligne-center size-medium" >}}
