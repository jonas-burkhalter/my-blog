+++ 
draft = false
date = 2023-12-29T22:00:00+02:00
title = "Auckland"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

## Friday: Auckland Adventures
<!--
landing in auckland at 05:30 (absolutly exhausted)

taking a taxi to the hostel but arriving to early to check in
waiting in a coffee until the hostel opens 

dropping backpack in hostel, no checkin yet

chilling in "The Shelf" a coffee

finally checkin
chilling in the hostel

sleeping for long
-->
{{< figure src="./images/breakfast.jpg" alt="breakfast" class="aligne-right size-medium" >}}

Touching down in Auckland at the ungodly hour of 05:30, I resembled a sleep-deprived zombie more than a seasoned traveler. With energy levels hovering somewhere near zero, the prospect of navigating a new city felt daunting.

Dragging myself into a taxi, I mumbled something resembling an address to the driver, who thankfully seemed to understand my delirious state. Yet, even at this early hour, fate had a twisted sense of humor in store for me—arriving at the hostel, I was met with locked doors and a sign mocking my eagerness to collapse into bed.

Desperate for caffeine-infused salvation, I stumbled into the nearest coffee joint, where time seemed to stand still as I waited for the world to wake up along with my chosen elixir. The minutes stretched into eternity until finally, the hostel opened its doors, offering a haven for weary travelers like myself.

With backpack dropped but still no check-in in sight, I sought refuge in yet another café, aptly named "The Shelf." Here, amidst the aroma of freshly brewed coffee and the hum of morning chatter, I found solace in the simple pleasure of just being still.

Eventually, blessed relief came in the form of a room key, and I wasted no time in retreating to the sanctuary of my dormitory. With no agenda other than to recover from the whirlwind of travel, I surrendered to the call of sleep, bidding farewell to Auckland's dawn in exchange for the sweet embrace of dreams.

## Saturday: A Day of Discovery in Auckland
<!--
going to the coffee

call with Muschle

museum 
maori history
auckland and new zealand history/life
vulcanos
war
-->
Waking up on Saturday felt like emerging from a fog, but with the promise of a new day ahead, I summoned the energy to kickstart it with one essential ritual: coffee. Breakfast became a sacred ceremony at a cozy café, where the aroma of freshly ground beans mingled with the anticipation of the day's adventures.

A phone call with Muschle added a personal touch to the day, bridging distances and fostering a sense of camaraderie. The warmth of friendship echoed through the wires, transcending geographical boundaries.

{{< figure src="./images/maori.jpg" alt="maori" class="aligne-left size-small" >}}

Buoyed by the camaraderie, I set out to explore Auckland's rich tapestry of history and culture, making a beeline for the Auckland War Memorial Museum. Here, time became a fluid concept as I delved into the depths of Maori history, tracing the intricate threads of tradition and identity that have woven through the fabric of New Zealand's narrative.

{{< figure src="./images/kiwi.jpg" alt="kiwi" class="aligne-right size-small" >}}

The museum's exhibits offered a kaleidoscopic glimpse into Auckland's past, from its humble beginnings to its evolution into a vibrant metropolis. I marveled at the tales of resilience and triumph, woven amidst the backdrop of volcanic landscapes and the echoes of war.

## Sunday: Coffee, Rugby, and New Year's Revelry
<!--
going to a coffee

all blacks expirience

new year
beers, watching the countdown on the skytower
-->
Fueling up with my morning fix of coffee, I embarked on a journey of quintessential Kiwi experiences.

{{< figure src="./images/haka.jpg" alt="haka" class="aligne-right size-small" >}}

Next on the agenda was an immersive All Blacks experience—a deep dive into the heart of New Zealand's rugby culture. From the thunderous haka to the strategic brilliance on the field, the encounter with All Blacks' legacy offered insights into the nation's sporting pride.

{{< figure src="./images/new_year.jpg" alt="new_year" class="aligne-left size-small" >}}

As the sun dipped below the Auckland skyline, New Year's Eve took center stage. The evening was set against the backdrop of camaraderie and celebration. Beers in hand, the countdown to midnight unfolded against the iconic silhouette of the Sky Tower. The night sky sparkled with both fireworks and the collective enthusiasm of those gathered to bid farewell to the year gone by and welcome the promises of the new one.
