+++ 
draft = false
date = 2023-12-29T22:00:00+02:00
title = "Auckland"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Neuseeland"]
+++

## Freitag: Abenteuer in Auckland
<!--
Ankunft in Auckland um 05:30 (absolut erschöpft)

Taxi zum Hostel, aber zu früh zum Einchecken
Warten in einem Café, bis das Hostel öffnet

Rucksack im Hostel abgestellt, noch kein Einchecken möglich

Chillen in "The Shelf", einem Café

Schliesslich Einchecken
Chillen im Hostel

Schlafen für lange Zeit
-->
{{< figure src="./images/breakfast.jpg" alt="Frühstück" class="align-right size-medium" >}}

Um 05:30 landete ich in Auckland zur unchristlichen Uhrzeit von 05:30. Absolut erschöpft ähnelte ich mehr einem schlafentwöhnten Zombie als einem erfahrenen Reisenden. Mit Energielevel irgendwo im Minusbereich fühlte sich das Navigieren einer neuen Stadt wie eine fast unüberwindbare Aufgabe an.

Mühsam liess ich mich in ein Taxi sinken und murmelte etwas in Richtung eines Adressangebots, das zum Glück der Fahrer halbwegs zu verstehen schien. Doch selbst zu dieser frühen Stunde hatte das Schicksal einen recht bitteren Humor—am Hostel angekommen, wurde ich von verschlossenen Türen empfangen und ein Schild verspottete mein hastiges Verlangen, ins Bett zu fallen.

Auf der Suche nach einer rettenden Koffeinladung taumelte ich in das nächstgelegene Café, wo die Zeit still zu stehen schien, während ich auf die Welt wartete, um mit meinem gewählten Elixier aufzuwachen. Die Minuten zogen sich in die Länge, bis schliesslich das Hostel seine Türen öffnete und eine Zuflucht für müde Reisende wie mich bot.

Mit meinem Rucksack abgestellt, aber noch immer ohne einchecken zu können, suchte ich Schutz in einem weiteren Café, passend benannt "The Shelf." Hier, mitten im Aroma frisch gebrühten Kaffees und dem Summen des morgendlichen Gesprächs, fand ich Trost in der einfachen Freude, einfach still zu sein.

Schliesslich kam die ersehnte Erleichterung in Form eines Zimmerschlüssels, und ich verschwendete keine Zeit damit, mich in die Zuflucht meines Schlafsaals zurückzuziehen. Ohne Agenda ausser der Erholung von dem Reisewirbel hingab ich mich dem Ruf des Schlafes, verabschiedete mich von Auckland's Morgen und liess mich in den süssen Griff der Träume sinken.

## Samstag: Ein Tag der Entdeckung in Auckland
<!--
zuerst in ein Café gehen

Telefonat mit Muschle

Museum 
Maori-Geschichte
Auckland und Neuseeland Geschichte/Leben
Vulkane
Krieg
-->
Am Samstag aufzuwachen fühlte sich an wie aus einem Nebel hervorzutreten, aber mit dem Versprechen eines neuen Tages vor mir fand ich genug Energie, um ihn mit einer essentiellen Morgenroutine zu starten: Kaffee. Frühstück wurde zu einer heiligen Zeremonie in einem gemütlichen Café, wo der Duft frisch gemahlener Bohnen mit der Vorfreude auf die Abenteuer des Tages vermischte.

Ein Telefonat mit Muschle fügte eine persönliche Note hinzu und überbrückte Distanzen, wodurch ein Gefühl der Kameradschaft entstand. Die Wärme der Freundschaft hallte durch die Drähte und überwand geografische Grenzen.

{{< figure src="./images/maori.jpg" alt="Maori" class="align-left size-small" >}}

Mit dieser Kameradschaft im Herzen machte ich mich auf den Weg, um Auckland's reiches Tuch aus Geschichte und Kultur zu erkunden. Mein Ziel war das Auckland War Memorial Museum. Hier wurde Zeit zu einem flüssigen Konzept, als ich die Tiefen der Maori-Geschichte erforschte und die feinen Fäden traditioneller und identitätsstiftender Erzählungen durch das Gewebe der neuseeländischen Erzählung zurückverfolgte.

{{< figure src="./images/kiwi.jpg" alt="Kiwi" class="align-right size-small" >}}

Die Ausstellungen des Museums boten einen kaleidoskopischen Einblick in die Vergangenheit von Auckland, von ihren bescheidenen Anfängen bis hin zu ihrer Entwicklung zu einer pulsierenden Metropole. Ich staunte über die Geschichten von Widerstand und Triumph, gewebt in den Hintergrund von vulkanischen Landschaften und den Echoen von Kriegen.

## Sonntag: Kaffee, Rugby und Neujahrsfeier
<!--
Erstmal in ein Café gehen

All Blacks Erfahrung

Neujahr
Bier trinken, Countdown auf dem Sky Tower schauen
-->
Mit meinem Morgenkaffee ausgestattet, machte ich mich auf eine Reise durch die unverzichtbaren Erlebnisse Neuseelands.

{{< figure src="./images/haka.jpg" alt="Haka" class="align-right size-small" >}}

Als nächstes auf dem Plan stand ein intensives All Blacks-Erlebnis—ein tiefes Eintauchen in das Herz der neuseeländischen Rugbykultur. Vom donnernden Haka bis hin zur strategischen Brillanz auf dem Spielfeld gab die Begegnung mit der Legende der All Blacks Einblicke in den sportlichen Stolz des Landes.

{{< figure src="./images/new_year.jpg" alt="Neujahr" class="align-left size-small" >}}

Als die Sonne hinter dem Aucklander Skyline verschwand, rückte Silvester in den Mittelpunkt. Der Abend war von Kameradschaft und Feierlichkeiten geprägt. Mit einem Bier in der Hand verfolgte ich den Countdown zum Mitternacht gegen die ikonische Silhouette des Sky Towers. Der Nachthimmel funkelte mit Feuerwerken und der Begeisterung derer, die versammelt waren, um das vergangene Jahr zu verabschieden und die Versprechen des neuen zu begrüssen.
