+++  
draft = false  
date = 2023-12-09T22:00:00+02:00  
title = "Koh Rong Sanloem"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Samstag: Inselabenteuer – Holprige Fahrten und Strandidylle  
<!--  
Busfahrt (holprig)  
Fähre  
Ankunft auf der Insel  
Zum Strand gehen  
Den Abend geniessen  
-->  

Der Tag begann mit einem Wechsel der Kulisse, als wir aufbrachen zu einer Insel-Oase. Das Abenteuer begann mit einer holprigen Busfahrt, ein lebhafter Tanz der Räder, die das unebene Terrain durchquerten. Die rhythmischen Stösse waren ein Vorbote für den kommenden Charme der Insel.

Die Reise nahm eine maritim-wendige Richtung mit einer Fähre, deren sanftes Schaukeln das Herannahen des Inselrefugiums ankündigte. Als die Küsten näherkamen, mischte sich die Vorfreude mit dem Meereswind—ein Versprechen der Inselidylle.

Bei der Ankunft offenbarte die Insel ihren Charme, eine malerische Landschaft aus Sandstränden und azurblauen Gewässern. Die erste Agenda war ein Besuch am Strand—eine Gelegenheit, sich mit Sonne, Sand und Meer zu verbinden.

{{< figure src="./images/beach_me.jpg" alt="Strand" class="aligne-center size-medium" >}}

## Sonntag: Tauchtiefe – Kämpfen mit der Stimmung und Finden von Trost  
<!--  
(schlechte Laune)  
Frühstück (mit Noë)  
Anmeldung für den SSI Open-Water-Tauchkurs (allein)  
In einem Strandrestaurant sitzen und etwas Tauchtheorie lesen  
Zum Strand gehen  
Sonnenuntergang (mit Noë)  
Abendessen  
-->  

Der Tag begann mit einem Schatten, der von einer Sturmfront der Gefühle geworfen wurde—a schlechter Launentag, der wie eine störrische Wolke über allem schwebte. Das Frühstück, geteilt mit Noë, wurde zu einem ruhigen Zufluchtsort inmitten innerer Stürme.

In einem Versuch, die Erzählung zu wenden, begann ich eine Solo-Reise, um mich für den SSI Open-Water-Tauchkurs anzumelden. Die Entscheidung markierte einen Sprung ins Ungewisse, sowohl metaphorisch als auch tatsächlich.

{{< figure src="./images/beach_house.jpg" alt="Strandhaus" class="aligne-left size-medium" >}}

Sitzend in einem Strandrestaurant, tauchte ich ein in die Seiten der Tauchtheorie—a solitaire Unternehmung, die die Schönheit des Lernens mit der beruhigenden Präsenz des Meeres im Hintergrund vereinte.

Der Strand, mit seinem ewigen Hin und Her, lockte als Trost. Der Sand wurde zur Leinwand für Kontemplation, ein stiller Begleiter beim Navigieren durch die Komplexität des Tages.

Als die Sonne Richtung Horizont sank, verwandelte sich der Strand in eine Bühne für einen fesselnden Sonnenuntergang—a geteilter Moment mit Noë, der eine flüchtige, aber tiefgründige Auszeit von den Kämpfen des Tages bot.

{{< figure src="./images/sunset.jpg" alt="Sonnenuntergang" class="aligne-center size-medium" >}}

Das Abendessen, ein gemeinsames Tun, wurde zur symbolischen Schliessung eines Tages der inneren Erkundung und zum Beginn einer Reise in die Tiefen—sowohl im Inneren als auch unter der Oberfläche.

## Montag: Unter der Oberfläche – Ein Tag des Tauchens und gemeinsamer Momente  
<!--  
Frühstück (mit Noë)  
Beginnen des Tauchkurses  
Ausrüstung kennenlernen  
In der Bucht das erste Mal unter Wasser atmen  
Erlernen der Kontrolle der Tarierung, Ausrüstung, Notfallverhalten und mehr  
später ein spätes Mittagessen (mit Noë)  
Den Nachmittag zusammen geniessen  
Abendessen  
-->  

{{< figure src="./images/breakfast.jpg" alt="Frühstück" class="aligne-left size-medium" >}}

Der Tag begann mit einem vertrauten Ritual—einem gemeinsamen Frühstück mit Noë. Die morgendliche Sonne warf ein sanftes Licht, eine Andeutung auf die Unterwasserabenteuer, die auf uns warteten.

Die Reise in die Unterwasserwelt begann mit der Initiation eines Tauchkurses. Während ich in die Feinheiten der Ausrüstung eintauchte, lockte das Meer seine Geheimnisse. In der geschützten Bucht entfaltete sich das erste Zusammentreffen mit dem Atmen unter Wasser—a moment marked by a blend of excitement and the serenity that the underwater world offers.

Unter der Anleitung von Anne, einer erfahrenen Instruktorin, navigierten wir durch die wesentlichen Fähigkeiten—Erlernen der Kontrolle der Tarierung, der Ausrüstungshandhabung und des Verstehens von Notfallverfahren. Jede Lektion wurde zum Pinselstrich auf der Leinwand des neu gewonnenen Wissens, ebnete den Weg für ein transformierendes Taucherlebnis.

Aus den Tiefen aufsteigend, lockte ein spätes Mittagessen—a shared reprieve from the underwater realm. Die Inselaromen mischten sich mit der Salzluft des Meeres, wurden ein Festmahl der Nahrung nach einem Tag voller Aquabenteuer.

Der Nachmittag, in das Licht gemeinsamer Erlebnisse getaucht, wurde zur Leinwand für Entspannung. Gemeinsam genossen wir die einfache Freude der Insel—a ruhige Unterbrechung in der sich entfaltenden Reise.

Als die Nacht hereinbrach, markierte das Abendessen den Abschluss eines Tages, der die Oberfläche überstieg—a eine Eintauchen in die Geheimnisse des Tiefen, sowohl unter den Wellen als auch in den geteilten Momenten des Tages.

{{< figure src="./images/lok_lak.jpg" alt="Lok Lak" class="aligne-center size-medium" >}}

## Dienstag: In die Tiefe – Ein Tag voller Tauchabenteuer  
<!--  
Frühstück (mit Noë)  
Tauchen  
erste Fahrt in die Bucht, Übungen und Lernen, kurzer Schwimmweg zum M'Pai Bay Hausriff, Begegnung mit einem Stachelrochen  
zweites Tauchen vom Boot aus an der Corner Bar  
Abendessen (mit Noë)  
-->  

Der Tag begann mit dem vertrauten Rhythmus gemeinsamer Momente—a gemächliches Frühstück mit Noë. Die Vorfreude lag in der Luft, spiegelte die Unterwasserabenteuer wider, die auf uns warteten.

{{< figure src="./images/diving_equipment.jpg" alt="Tauchen" class="aligne-left size-medium" >}}

Mein Taucherlebnis begann mit dem ersten Tauchgang des Tages in der Bucht—a Tanz aus Übungen und kontinuierlichem Lernen. Nachher ein kurzer Schwimmweg zum M'Pai Bay Hausriff, offenbarte die Unterwasserwelt ihre Wunder. Der Sandboden führte uns zu einer grazilen Begegnung mit einem Stachelrochen—a moment etched in memory amidst the vibrant marine life.

Der zweite Tauchgang des Tages, eine Unternehmung vom Boot aus bei der Corner Bar, fügte eine weitere Bewegung zu unserer Unterwasser-Sinfonie hinzu. Der rhythmische Abstieg in die Tiefen offenbarte die ethereale Schönheit von Korallenriffen und dem lebhaften Meeresleben, das die untergetauchten Landschaften zierte. Jeder Tauchgang, eine Progression von Fähigkeiten und Erkenntnissen, malte ein Porträt des komplexen Ökosystems unter der Oberfläche.

Aus den Tiefen auftauchend, wurde das Abendessen—a moment of reflection and celebration. Die Aromen der Insel und des Meeres vermischten sich auf unseren Tellern—a kulinarischer Toast auf einen Tag voller Unterwasserentdeckungen.

## Mittwoch: Eintauchen ins Zertifikat – Unter den kambodschanischen Wellen  
<!--  
früh aufwachen (Bloggen machen)  
Frühstück (mit Noë)  
Tauchen  
zwei Tauchgänge vom Boot aus  
erstes am Korallengarten  
zweites an der Corner Bar  
Am Anfang sahen wir eine Qualle  
Zurück an der Oberfläche, treiben und warten auf das Boot, gestochen von einer Qualle  
die Eröffnung als zertifizierter Taucher (SSI Open Water Diver)  
Abendessen  
-->  

Der Tag öffnete sich mit der ruhigen Gelassenheit eines frühen Aufwachsens, einer ruhigen Unterbrechung, um die Essenz der Unterwasserabenteuer durch die Kunst des Bloggens einzufangen. Ein Vorgeschmack auf die Erzählung des Tages—the words began to weave the tales of submerged exploration.

Am Frühstückstisch gemeinsam mit Noë stärkte ich mich für einen Tag, der in die Annalen meiner Taucherreise eingraviert werden sollte. Die Unterwasserwelt wartete, und mit gespannter Vorfreude begann ich zwei Tauchgänge vom Boot aus—a einer aquatischen Odyssee, die mit Momenten des Triumphes enden sollte.

{{< figure src="./images/diving_me.jpg" alt="Tauchen" class="aligne-left size-medium" >}}

Der erste Tauchgang fand am Korallengarten statt—a eine untergetauchte Leinwand verziert mit bunten Korallenformationen und dem rhythmischen Tanz des Meereslebens. Die Tiefen offenbarte die stille Poesie des Ozeans—a tableau of colors and creatures that enchanted the senses.

Der zweite Tauchgang, bei der Corner Bar, begann mit einer faszinierenden Begegnung—a eine zarte Qualle, die graziös durch die Strömungen trieb. Aber die wahren Wunder des zweiten Tauchgangs erstreckten sich weit über die Qualenbegegnung hinaus. Schulen von farbenfrohen Fischen huschten spielerisch zwischen den Korallenriffen herum—a living kaleidoscope beneath the waves. Zarte Seegraswedel schwangen sich im Rhythmus der Strömungen, und verborgen innerhalb der verzweigten Korallenformationen, lugte ein neugieriger Krebs hervor—a momentarily capturing my attention.

Als wir zur Oberfläche auftauchten, getragen von der Umarmung des Ozeans, hinterliess ein kurzer Kontakt mit einer Qualle—a eine bleibende Erinnerung—a indelible memory etched in the tapestry of underwater exploration.

Die Krönung des Tages brachte forth eine Moment des Triumphes als ich ein zertifizierter Taucher wurde—a ein SSI Open Water Diver.  

Am Ende des Tageserlebnisses sammelten sich Noë und ich erneut für ein Abendessen.

## Donnerstag: Abschied mit einem bitteren Nachgeschmack und nächtliche Reisen  
<!--  
letzter Tag auf der Insel  
Frühstück holen  
entspannen  
Mittagessen holen  
Fähre nach Sihanoukville  
Nachtbus nach Siem Reap  
-->  

As the sun painted its final strokes across the canvas of our island sojourn, today marked the bittersweet finale of our stay.

Der Tag begann mit einem gemächlichen Frühstück—a Moment der stillen Reflektion, als wir die Aromen der Insel genossen und wussten, dass es unser letzter war.

{{< figure src="./images/beach.jpg" alt="Strand" class="aligne-right size-medium" >}}

Die Stunden entfalten sich in Entspannung—a bewusste Pause, um die Inselidylle vor ihrem Abschied aufzusaugen. Das sanfte Schaukeln von Palmen, das rhythmische Dröhnen der Wellen und das entfernte Lachen anderer Reisender wurden zum Hintergrund für eine reflektierende Unterbrechung.

Das Mittagessen, ein letztes gastronomisches Rendezvous mit den Inselangeboten—a encapsulated the diverse flavors that had accompanied me throughout my stay—a each dish, a parting taste of the island's essence, stirred a symphony of sensations.

Als der Tag sich neigte, bestiegen Noë und ich eine Fähre nach Sihanoukville—a ein Fahrzeug der Übergänge, das uns von der Umarmung der Insel zur Realität des Festlandes trug. Die Sonne sank unter den Horizont, warf ihren goldenen Schein auf die zurückweichenden Küsten—a ein poetisches Adieu an eine Insel, die zu einem vorübergehenden Zuhause geworden war.

Die nächtliche Reise lockte, und an Bord eines Busses, der nach Siem Reap fuhr—a begann die nächtliche Odyssee.  
