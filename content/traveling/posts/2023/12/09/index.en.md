+++ 
draft = false
date = 2023-12-09T22:00:00+02:00
title = "Koh Rong Sanloem"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Saturday: Island Escapade – Bumpy Rides and Beach Bliss
<!-- 
bus ride (bumpy)
ferry

arrive on island

go to the beach

enjoy the evening
 -->

The day ushered in a change of scenery as we embarked on a journey to an island oasis. The adventure commenced with a bumpy bus ride, a spirited dance of wheels navigating uneven terrain. The rhythmic bumps were a prelude to the forthcoming island charm.

The journey took a maritime turn with a ferry ride, where the gentle sway of the boat signaled the approach to the island sanctuary. As the shores neared, anticipation mingled with the sea breeze—a promise of island bliss.

Upon arrival, the island revealed its charm, a picturesque landscape of sandy shores and azure waters. The initial agenda involved a visit to the beach—an opportunity to connect with the sun, sand, and sea. 

{{< figure src="./images/beach_me.jpg" alt="beach" class="aligne-center size-medium" >}}

## Sunday: A Dive into the Depths – Battling Moods and Finding Solace
<!-- 
(bad mood day)

breakfast (with noe)

sign up for the SSI open water diving course (alone)

sit in a restaurant at the beach and read some diving theory

go to the beach

sunset (with noe)

dinner
 -->

The day dawned with a shadow cast by a tempest of emotions—a bad mood day that lingered in the air like a stubborn cloud. Breakfast, shared with Noë, became a quiet refuge in the midst of internal storms.

In an attempt to shift the narrative, I embarked on a solo journey to sign up for the SSI open water diving course. The decision marked a plunge into the unknown, both metaphorically and quite literally.

{{< figure src="./images/beach_house.jpg" alt="beach" class="aligne-left size-medium" >}}

Seated in a beachside restaurant, I delved into the pages of diving theory—a solitary endeavor that merged the beauty of learning with the calming presence of the sea as a backdrop.

The beach, with its timeless ebb and flow, beckoned as a solace. The sand became a canvas for contemplation, a silent companion in navigating the complexities of the day.

As the sun dipped toward the horizon, the beach transformed into a theater for a captivating sunset—a shared moment with Noë that offered a fleeting yet profound respite from the struggles of the day.

{{< figure src="./images/sunset.jpg" alt="sunset" class="aligne-center size-medium" >}}

Dinner, a communal act, became a symbolic closure to a day of internal exploration and the beginning of a journey into the depths—both within and beneath the surface.

## Monday: Beneath the Surface – A Day of Diving and Shared Moments
<!-- 
breakfast (with noe)

start diving course
learn the equipment
in bay, first time breathing underwater
learn to controll the buoyancy, equipment, emergency behaviour and more

late late lunch (with noe)

enjoy the afternoon together

dinner
 -->

{{< figure src="./images/breakfast.jpg" alt="breakfast" class="aligne-left size-medium" >}}

The day began with a familiar ritual—a shared breakfast with Noë. The morning sun cast a gentle glow, a prelude to the underwater adventures that awaited.

The journey into the underwater realm commenced with the initiation of a diving course. As I delved into the intricacies of the equipment, the mysteries of the sea beckoned. In the sheltered bay, the initial encounter with breathing underwater unfolded—a moment marked by a blend of excitement and the serenity that the underwater world offers.

Guided by Anne a seasoned instructor, we navigated through the essential skills—learning to control buoyancy, manage equipment, and understand emergency procedures. Each lesson became a brushstroke on the canvas of newfound knowledge, paving the way for a transformative diving experience.

Emerging from the depths, a late, late lunch beckoned—a shared reprieve from the underwater realm. The flavors of the island fused with the saltiness of the sea, becoming a celebration of sustenance after a day of aquatic adventures.

The afternoon, bathed in the glow of shared experiences, became a canvas for relaxation. Together, we reveled in the simple joy of the island—a tranquil interlude in the unfolding journey.

As the night descended, dinner marked the culmination of a day that transcended the surface—an immersion into the mysteries of the deep, both below the waves and within the shared moments that defined the day.

{{< figure src="./images/lok_lak.jpg" alt="Lok Lak" class="aligne-center size-medium" >}}

## Tuesday: Into the Abyss – A Day of Diving Adventures
<!--
breakfast (with noe)

diving
first dive in bay, doing more exercises and learning, short swim to dive in the M'Pai Bay house reef, seeing a sting ray
second dive from boat at Corner bar

dinner (with noe)
 -->

The day dawned with the familiar rhythm of shared moments—a leisurely breakfast with Noë. The anticipation in the air mirrored the underwater escapades that awaited.

{{< figure src="./images/diving_equipment.jpg" alt="diving" class="aligne-left size-medium" >}}

My diving odyssey commenced with the first dive of the day in the bay—a dance of exercises and continuous learning. After that a brief swim to the M'Pai Bay house reef, the underwater world unveiled its wonders. Floating around the ground led us to encounter a graceful stingray, a moment etched in memory amidst the vibrant marine life.

The second dive of the day, a venture from the boat near Corner Bar, added another movement to our underwater symphony. The rhythmic descent into the depths revealed the ethereal beauty of coral gardens and the teeming marine life that graced the submerged landscapes. Each dive, a progression of skills and revelations, painted a portrait of the intricate ecosystem beneath the surface.

Emerging from the depths, dinner became a moment of reflection and celebration. The flavors of the island and the sea mingled on our plates, a culinary toast to a day filled with underwater discoveries.

## Wednesday: A Dive into Certification—Beneath the Cambodian Waves
<!-- 
early wake up (do some blogging)

breakfast (with noe)

diving 
two dives from boat

first at coral garden

second at corner bar
at the beginning we saw a jelly fish
back at the surface floating, waiting for the boat to pick us up, getting stung by a jelly fish

being a certified diver (SSI open water diver)

dinner
 -->

The day unfolded with the quiet serenity of an early wake-up, a tranquil interlude to capture the essence of underwater adventures through the art of blogging. A prelude to the day's narrative, the words began to weave the tales of submerged exploration.

Over breakfast together with Noë, I fortified myself for a day that promised to be etched in the annals of my diving journey. The underwater realm awaited, and with eager anticipation, I embarked on two dives from the boat—an aquatic odyssey that would culminate in moments of triumph.

{{< figure src="./images/diving_me.jpg" alt="diving" class="aligne-left size-medium" >}}

The first dive unfolded at Coral Garden—a submerged tapestry adorned with vibrant coral formations and the rhythmic dance of marine life. The depths revealed the silent poetry of the ocean, a tableau of colors and creatures that enchanted the senses.

The second dive, at Corner Bar, commenced with a mesmerizing encounter—a delicate jellyfish gracefully drifting through the currents. But the true marvels of the second dive extended far beyond the jellyfish encounter. Schools of brilliantly colored fish darted playfully amidst the coral gardens, creating a living kaleidoscope beneath the waves. Delicate sea fans swayed with the rhythm of the currents, and hidden within the intricate coral formations, a curious crab peeked out, momentarily capturing my attention.

As we ascended to the surface, suspended in the embrace of the ocean, a brief encounter with a jellyfish left an unexpected mark—an indelible memory etched in the tapestry of underwater exploration.

The culmination of the day brought forth a moment of triumph as I emerged a certified diver—an SSI Open Water Diver. 

As the day's adventures concluded, Noë and I once again gathered for a dinner. 

## Thursday: Bittersweet Farewells and Nocturnal Journeys
<!-- 
last day on the island

get breakfast
relaxe
get lunch

ferry to sihanoukville
night bus to siem reap
-->

As the sun painted its final strokes across the canvas of our island sojourn, today marked the bittersweet finale of our stay.

The day began with a leisurely breakfast—a moment of quiet reflection as we savored the flavors of the island, knowing it was our last. 

{{< figure src="./images/beach.jpg" alt="beach" class="aligne-right size-medium" >}}

The hours unfolded in relaxation—a deliberate pause to absorb the island's tranquility before bidding it farewell. The gentle sway of palm trees, the rhythmic lull of the waves, and the distant laughter of fellow travelers became the backdrop for a reflective interlude.

Lunch, a final gastronomic rendezvous with the island's culinary offerings, encapsulated the diverse flavors that had accompanied me throughout my stay. Each dish, a parting taste of the island's essence, stirred a symphony of sensations.

As the day waned, Noë and I boarded a ferry bound for Sihanoukville—a vessel of transitions, carrying us from the island's embrace to the mainland's reality. The sun dipped below the horizon, casting its golden glow on the receding shores—a poetic adieu to an island that had become a temporary home.

The night journey beckoned, and aboard a bus destined for Siem Reap, the nocturnal odyssey began.
