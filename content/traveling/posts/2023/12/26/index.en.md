+++ 
draft = false
date = 2023-12-26T22:00:00+02:00
title = "Traveling New Zealand"
description = "Overview and ideas"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

# Overview
<iframe 
    src="https://www.google.com/maps/d/embed?mid=1HnAKFO6rQEiIeTesyDIJejymfb2lkIQ&ehbc=2E312F&noprof=1"
    width="100%"
    height="500">
</iframe>

# What to do in New Zealand

### North Island
- Auckland
  - Sky Tower
  - diving (Goat Island Marine Reserve)
  - War Memorial Museum 
- Waiheke
  - beaches
  - wine
- Taupo
  - Skydive
  - hiking (Tongariro Alpine Crossing)
  - relaxe (Lake Taupo)
  - Orakei Korako Geothermal Park
- Tongariro
  - ?
- Paekakariki
  - ?
- Wellington
  - Miramar
- Castlepoint
  - ?
- Rotorua
  - Maori culture
  - diving (Lake Rotoiti)
  - Polynesian Spa
  - hiking (Redwoods Forest)
- Waihi Beach
  - beach
- Paeroa
  - ?  

- Coromandel Peninsula
  - diving (Bay of Islands)
  - relaxe (Cathedral Cove) 
  - Hot Water Beach
  - hiking (Coromandel Coastal Walkway)
  - kayaking (Whitianga)
- Gisborne
  - East Cape Lighthouse
  - hiking (Motu Trails)
- Te Whanganui-A-Hei Coromandel
- Waitomo
  - glowworm cave tour

### South Island
- Nelson / Abel Tasman National Park
  - diving
  - kayaking
  - hiking
  - relaxe (Kaiteriteri Beach)
  - Beach (Wharariki)
- Wanaka
  - cruise (Lake Wanaka)
  - hiking (Roy's Peak)
  - diving (Lake Wanaka)
  - hiking (Rob Roy Glacier)
- Te Anau
  - glowworm cave tour
  - Fiordland National Park
    - hiking (Dusky Track or Hollyford) 
    - cruise (Doubtful Sound)
    - wildlife-watching
  - Manapouri
    - relaxe (Lake Manapouri)
- Milford Sound
  - diving
- Queenstown
  - Adventure activities (bungee jumping, skiing)
  - relaxe (Lake Wakatipu)
  - diving (Lake Wakatipu)
  - driving (Crown Range Road)
  - hiking (Moke Lake)
- Mount Cook National Park
  - hiking (Hooker Valley)
  - hiking (Mueller Hut)
  - Tasman Valley
  - Stargazing at Aoraki/Mount Cook Observatory
- Christchurch
  - cultural attractions
  - relaxe (Christchurch Botanic Gardens)
  - diving (Akaroa)
  - hiking (Godley Head Coastal)

<!-- 
https://roadtripplaner.com/trip/48/92/5

https://www.google.com/maps/dir/Auckland/Taup%C5%8D,+New+Zealand/Tongariro,+3989/Paek%C4%81k%C4%81riki/Miramar,+Wellington/Wellington/@-41.3230998,174.8030029,12.97z/data=!4m38!4m37!1m5!1m1!1s0x6d0d47fb5a9ce6fb:0x500ef6143a29917!2m2!1d174.7644881!2d-36.8508827!1m5!1m1!1s0x6d6bef77144b165b:0x500ef6143a309e0!2m2!1d176.0704465!2d-38.6842862!1m5!1m1!1s0x6d6ba743b4003f75:0x500ef6143a30fd0!2m2!1d175.6155011!2d-39.0413812!1m5!1m1!1s0x6d40a6a82f3b215b:0x500ef6143a2f300!2m2!1d174.9570875!2d-40.9810914!1m5!1m1!1s0x6d38af783cdaffcb:0x500ef6143a2e4e0!2m2!1d174.8165796!2d-41.3164578!1m5!1m1!1s0x6d38b1fc49e974cb:0xa00ef63a213b470!2m2!1d174.7787463!2d-41.2923814!3e0?entry=ttu



https://www.google.com/maps/dir/Wellington/Nelson/W%C4%81naka/Te+Anau/Milford+Sound,+Southland/Queenstown/Aoraki%2FMount+Cook+National+Park,+Canterbury+7999/Christchurch/Wellington/@-43.3803002,168.645953,7z/data=!3m1!4b1!4m56!4m55!1m5!1m1!1s0x6d38b1fc49e974cb:0xa00ef63a213b470!2m2!1d174.7787463!2d-41.2923814!1m5!1m1!1s0x6d3becc491b72e7d:0xa00ef88e796a480!2m2!1d173.2443635!2d-41.2985321!1m5!1m1!1s0xa9d5461db9ec2d6f:0x500ef868479c1e0!2m2!1d169.1417356!2d-44.6942992!1m5!1m1!1s0xa9d46d2095665e6b:0x500ef868479b450!2m2!1d167.7180426!2d-45.4144293!1m5!1m1!1s0xa9d5e04dba4b49e1:0x2a00ef86ab64de00!2m2!1d167.8973801!2d-44.6414024!1m5!1m1!1s0xa9d51df1d7a8de5f:0x500ef868479a600!2m2!1d168.6615141!2d-45.0301511!1m5!1m1!1s0x6d2bc5e1ab9cb2ed:0xf00ef87655c5860!2m2!1d170.2623985!2d-43.5946537!1m5!1m1!1s0x6d322f4863c5ed01:0x500ef8684799945!2m2!1d172.6305589!2d-43.5320214!1m5!1m1!1s0x6d38b1fc49e974cb:0xa00ef63a213b470!2m2!1d174.7787463!2d-41.2923814!3e0?entry=ttu
-->

# Comments
You have more ideas, add them as a comment

{{< chat cactus-jonas-burkhalter.gitlab.io-2023-12-26 >}}
