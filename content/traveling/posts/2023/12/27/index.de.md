+++ 
draft = false
date = 2023-12-27T22:00:00+02:00
title = "Vietnam to New Zealand"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Südostasien", "Reisen Neuseeland"]
+++

## Mittwoch: Saigon nach Melbourne
<!--
endlich fliegen
Saigon nach Melbourne 
Probleme mit der Bezahlung
Start um 23:55
-->
Die Vorfreude erreichte ihren Höhepunkt, als ich die Reise von Saigon nach Melbourne antrat, mit Auckland als dem ultimativen Ziel. Doch der Weg zum Abflug war nicht ohne Hürden—ein Verwirrspiel mit dem Zahlungssystem brachte eine Prise Ungewissheit in die Reiseerzählung. Unter dem Hintergrund eines sternenklaren Himmels hob das Flugzeug ab und bereitete die Bühne für eine Nacht voller Luftübertritte.

## Donnerstag: MC Coffee Chronicles
<!--
ankommen in Melbourne um 11:30

8 Stunden chillen in MC Coffee (Melbourne)

Flug von Melbourne nach Auckland
wieder Probleme mit der Bezahlung
Start um 23:55
-->
Um 11:30 landete ich in Melbourne, wo die Stadt unter mir lag und mir für einen kurzen Moment Einblicke in ihren Charakter schenkte. Doch der Reiseplan erforderte eine längere Wartezeit, und was wäre besser, als diese Zeit in der einladenden Atmosphäre von MC Coffee zu verbringen?

Als die Uhr gegen Mitternacht des 28. zeigte, rief ein weiterer Flug—diesmal von Melbourne nach Auckland. Doch das bekannte Problem mit dem Zahlungssystem kehrte zurück und fügte der Nacht eine zusätzliche Unsicherheit hinzu. Unbeeindruckt von diesen Herausforderungen hob das Flugzeug erneut in den Nachthimmel ab und setzte seinen Kurs auf die Küsten von Auckland fort.

{{< figure src="./images/airplan.jpg" alt="airplane" class="align-center size-medium" >}}

## Freitag: Frühmorgens und frische Starts
<!--
Ankunft in Auckland um 05:30

Taxi zum Hostel
Warten in einem Café, bis das Hostel öffnet
-->
Um 05:30 landete das Flugzeug in der City of Sails, wo das Versprechen eines neuen Tages und neuer Erlebnisse auf mich wartete. Ein Taxi brachte mich durch die aufwachende Stadt zum Hostel und bereitete den Rahmen für das nächste Kapitel vor.

Jedoch erzwang die frühe Ankunft eine Pause—eine stille Unterbrechung in einem lokalen Café. Mit dem Duft frisch gebrühten Kaffees in der Luft wartete ich auf die Eröffnung des Hostels und genoss die Vorfreude auf die bevorstehenden Erfahrungen in der lebhaften Stadt Auckland.

