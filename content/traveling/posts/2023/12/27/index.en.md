+++ 
draft = false
date = 2023-12-27T22:00:00+02:00
title = "Vietnam to New Zealand"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia", "traveling New Zealand"]
+++

## Wednesday: Saigon to Melbourne
<!--
finally flying
saigon to melbourne 
trouble with the payment
take off at 23:55
-->
The anticipation reached its zenith as I embarked on the journey from Saigon to Melbourne, with Auckland waiting as the ultimate destination. However, the path to departure wasn't without its share of twists—a tangle with the payment system added a touch of unpredictability to the travel narrative. Against the backdrop of a starlit sky, the aircraft took flight, setting the stage for a night of airborne transitions.

## Thursday: MC Coffee Chronicles
<!--

arriving in melbourne 11:30

chilling in mc coffee (melbourne) for 8h

taking the flight from melbourn to auckland
trouble with the payment again
take off at 23:55
-->
Touching down in Melbourne at the sunlit hour of 11:30, the city sprawled beneath, offering a transient glimpse of its character. However, the itinerary dictated an extended layover, and what better way to pass the time than within the welcoming embrace of MC Coffee?

As the clock ticked towards midnight on the 28th, another flight beckoned—a journey from Melbourne to Auckland. However, the familiar specter of trouble with the payment system made an encore, injecting an element of uncertainty into the night's endeavors. Undeterred, the aircraft soared into the night sky, bound for the shores of Auckland.

{{< figure src="./images/airplan.jpg" alt="airplan" class="aligne-center size-medium" >}}

## Friday: Early Mornings and Fresh Starts
<!--
arriving in auckland at 05:30

taking a taxi to the hostel
waiting in a coffee until the hostel opens 
-->
The plane touched down in the City of Sails at the early hour of 05:30, where the promise of a new day and new discoveries awaited. A taxi whisked me through the awakening city to the hostel, setting the stage for the next chapter.

However, the early arrival necessitated a pause—a quiet interlude spent in a local coffee shop. With the aroma of freshly brewed coffee permeating the air, I awaited the hostel's opening, savoring the anticipation of unfolding experiences in the vibrant city of Auckland.
