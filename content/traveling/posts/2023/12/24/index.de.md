+++ 
draft = false
date = 2023-12-24T22:00:00+02:00
title = "Saigon"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Sonntag: Sanfte Übergänge und die fröhliche Reise geht weiter
<!--
another travel day, this time smooth 

von Koh Rong mit der Fähre zum Festland
es gab grosse Wellen, aber die Fahrt fühlte sich wie eine Achterbahnfahrt an

dann ein Minivan nach Kampot und dann zur Grenze

nach der Grenze 2 Stunden warten auf den Bus

danach der Bus nach Saigon 
Ankunft dort gegen 3 Uhr nachts
und von dort ein Grab zum Hostel
-->

Die Abreise von Koh Rong brachte mit der Fähre auf das Festland eine einzigartige Abenteuerfahrt mit sich. Das Schiff, das durch lebhafte Wellen navigierte, bot eine unerwartete Aufregung, die eine Routinefahrt in eine berauschende Eskapade verwandelte.

Von den Ufern des Festlands aus folgten eine Serie sanfter Übergänge. Ein Minivan brachte mich in die charmante Stadt Kampot, die den nächsten Abschnitt der Reise einleitete. Die Überquerung der Grenze bedeutete eine zeitliche Pause, mit einer zweistündigen Wartezeit, die den nächsten Abschnitt ankündigte.

{{< figure src="./images/party_street.jpg" alt="Partystrasse" class="aligne-right size-smaller" >}}

Die anschliessende Busfahrt nach Saigon bot Momente der Besinnung unter dem Sternenhimmel. Um 3 Uhr nachts in der lebhaften Stadt angekommen, manifestierte sich die Vertrautheit des Reisens im Geräusch des urbanen Lebens. Die Strassenlabyrinthe der Stadt navigierend, wurde ein Grab-Taxi zum letzten Abschnitt der nächtlichen Odyssee, das mich in die tröstende Umarmung des Hostels brachte.

## Montag: Von Kaffees, Rückschlägen und Frustrationen
<!-- 
in ein Café gehen, ein Wiedersehen mit amerikanischem Football

Entspannen im Bett

Mitteilung bekommen, dass mein Flug verschoben wurde
versuche, es zu lösen, werde den Anschlussflug verpassen
sie können nichts tun

verärgert
-->

Ein Besuch in einem lokalen Café wurde zur Bühne für eine lockere Session des Schauens von wiederholten amerikanischen Football-Spielen – eine ruhige Zuflucht mitten in der Feiertagsruhe.

Der Rest des Tages verlief in der gemütlichen Umarmung des Hostels, wo die Verlockung der Entspannung vorherrschte. Geborgen im Komfort des Bettes, schien die Zeit auf ihre eigene Weise dahinzugleiten, was einen willkommenen Ausbruch aus dem üblichen hektischen Reisealltag bot.

Doch die Ruhe wich einer unerwarteten Wendung, als eine Mitteilung die Tranquillität störte. Ein plötzlicher Wechsel der Flugpläne liess Wellen der Frustration entstehen. Versuche, die Situation zu korrigieren, erwiesen sich als erfolglos, und die Erkenntnis, dass ich den Anschlussflug verpassen würde, drohte bedrohlich.

## Dienstag: Nachwirkende Frustrationen und die Gemütlichkeit von Kaffee
<!--
immer noch etwas frustriert

einen Kaffee holen

Blog schreiben

warten, dass der Tag endet

noch ein Kaffee
-->
{{< figure src="./images/coffee.jpg" alt="Kaffee" class="aligne-left size-medium" >}}

Die Schatten der Frustrationen von gestern hallten in den neuen Tag hinein und legten einen Schatten auf die Routine der Erkundung. Auf der Suche nach Trost begann der Morgen mit der vertrauten Umarmung einer Tasse Kaffee – ein Elixier, das oft als Begleiter in Momenten der Besinnung dient.

Der Tag verlief gemächlich, geprägt von einer wartenden Haltung – wartend darauf, dass die Rückstände der Frustrationen sich verflüchtigen, wartend auf eine erneute Richtung. Das Bloggen wurde zu einer kathartischen Übung, eine Möglichkeit, die Wirrungen der Gedanken zu durchstreifen, die die unerwarteten Herausforderungen der Reise begleiteten.

Während die Stunden dahinplätscherten, wurde die tröstende Aroma des Kaffees erneut gerufen, die für einen Moment zur Flucht vor den Frustrationen wurde, die hartnäckig haften blieben. Jeder Schluck diente als Zuflucht, ein kleiner Genuss mitten in einem Tag, der von einer gedämpften Atmosphäre geprägt war.

## Mittwoch: Die Kunst des Wartens und das Entdecken der dunklen Seite des Reisens
<!--
noch ein Tag des Wartens

Schreiben der Serie "Dark Side of Traveling"
-->

Mittwoch entwickelte sich zu einem weiteren Tag im zarten Tanz des Wartens – Wartens darauf, dass sich Umstände ändern, Wartens auf eine Lösung. Das Zeitgefühl des Tages spiegelte das Auf und Ab der Unsicherheit wider, eine komplexe Choreografie, die oft die Reisegeschichte eines Reisenden begleitet.

Mittendrin im Warten kam das kreative Schaffen ins Zentrum. Die Finger tanzten über die Tastatur und webten eine Erzählung, die die oft übersehenen Facetten des Reisens erkundete – dunkle Gassen, unerwartete Hindernisse und die emotionale Achterbahn, die die weniger glamouröse Seite der Erkundung ausmacht.

Während die Worte flossen, entstand eine Serie – die "Dunkle Seite des Reisens". Sie befasste sich mit den Feinheiten des Navigierens von Rückschlägen, der Frustration durch geänderte Pläne und der emotionalen Last, die mit der Suche nach neuen Horizonten einhergeht. Das Schreiben wurde zu einer kathartischen Entladung, indem es die Schatten der Unsicherheit in eine greifbare Erkundung des vielschichtigen Wesens der Reise des Reisenden verwandelte.
