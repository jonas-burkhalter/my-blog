+++ 
draft = false
date = 2023-12-24T22:00:00+02:00
title = "Saigon"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Sunday: Smooth Transitions and the Merry Journey Continues
<!--
another travel day, this time smooth 

from koh rong with the ferry to the main land
there were big waves but the ride felt like a rollercoster

then a mini van to kampot and then to the border

after the border 2h waiting for the bus

then the bus to saigon 
arriving there around 3am
and there a grab to the hostel
-->

Departing from Koh Rong, the ferry ride to the mainland brought with it a unique adventure. The vessel, navigating through lively waves, offered an unexpected thrill reminiscent of a rollercoaster, turning a routine commute into an exhilarating escapade.

From the shores of the mainland, a series of smooth transitions ensued. A mini-van transported me to the quaint town of Kampot, setting the stage for the next chapter of the journey. Crossing borders marked a temporal pause, with a two-hour wait heralding the next leg.

{{< figure src="./images/party_street.jpg" alt="party street" class="aligne-right size-smaller" >}}

The subsequent bus journey to Saigon provided moments of contemplation under the night sky. Arriving in the bustling city at the stroke of 3 am, the familiarity of travel manifested in the hum of urban life. Navigating the city's labyrinthine streets, a Grab ride became the final stretch of the night's odyssey, delivering me to the comforting embrace of the hostel.

## Monday: Of Coffees, Setbacks, and Frustrations
<!-- 
going to a coffee watching some american football replay

relaxing in bed

getting notified my flight got changed
try to fix it, will miss the connecting flight
they can't do anything

frustrated

 -->

A visit to a local coffee shop became the stage for a laid-back session of watching replayed American football—a serene respite amid the holiday stillness.

The remainder of the day unfolded in the cozy cocoon of the hostel, where the allure of relaxation held sway. Nestled in the comfort of the bed, time seemed to meander at its own pace, allowing for a welcome escape from the usual frenetic rhythm of travel.

However, serenity gave way to an unexpected twist when a notification disrupted the tranquility. A sudden change in flight plans sent shockwaves of frustration. Attempts to rectify the situation proved futile, and the realization of missing a connecting flight loomed ominously.

## Tuesday: Lingering Frustrations and the Comfort of Coffee
<!--
still a bit frustrated

get some coffee

blog

wait for the day to end 

another coffee 
-->
{{< figure src="./images/coffee.jpg" alt="coffee" class="aligne-left size-medium" >}}

The specter of yesterday's frustrations lingered into the new day, casting a shadow on the routine of exploration. Seeking solace, the morning began with the familiar embrace of a cup of coffee—an elixir that often doubles as a companion in moments of contemplation.

The day unfolded at a measured pace, marked by a sense of waiting—waiting for the residue of frustrations to dissipate, waiting for a renewed sense of direction. The act of blogging became a cathartic exercise, a way to navigate the tangle of thoughts that accompanied the unforeseen challenges of travel.

As the hours ambled on, the comforting aroma of coffee beckoned once again, providing a momentary escape from the frustrations that clung persistently. Each sip served as a respite, a small indulgence in the midst of a day marked by a subdued atmosphere.

## Wednesday: The Art of Waiting and Exploring the Dark Side of Traveling
<!--
another day of waiting

writting the dark side of traveling series
-->
Wednesday unfolded as another day in the delicate dance of waiting—waiting for circumstances to shift, waiting for resolutions to manifest. The cadence of the day mirrored the ebb and flow of uncertainty, an intricate choreography that often accompanies the traveler's narrative.

In the midst of the waiting game, creative endeavors took center stage. The fingers danced across the keyboard, weaving a narrative that explored the often-overlooked facets of travel—the dark alleys, the unexpected hurdles, and the emotional rollercoaster that defines the less glamorous side of exploration.

As the words flowed, a series emerged—the "Dark Side of Traveling." It delved into the intricacies of navigating setbacks, the frustration of altered plans, and the emotional toll that accompanies the pursuit of new horizons. The act of writing became a cathartic release, transforming the shadows of uncertainty into a tangible exploration of the multifaceted nature of the traveler's journey.
