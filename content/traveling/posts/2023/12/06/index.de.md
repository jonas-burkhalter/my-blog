+++  
draft = false  
date = 2023-12-06T22:00:00+02:00  
title = "Kampot"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Südostasien"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Südostasien"]  
+++

## Mittwoch: Die Reize und kulinarischen Genüsse von Kampot entdecken  
<!--  
Frühstück holen  
Mit dem Mini-Van von Kep nach Kampot fahren  
herumspazieren  
Abendessen (Amok)  
-->  

{{< figure src="./images/mini_van.jpg" alt="Mini-Van" class="aligne-left size-smaller" >}}  

Der Tag in Kampot begann mit einem herzhaften Frühstück, das den Tag für die Erkundung dieser charmanten Stadt bereitete. Die morgendlichen Aromen begleiteten uns auf der ersten Etappe unserer Reise—mit dem Mini-Van von Kep nach Kampot.  

Als wir die Strassen von Kampot betraten, wurde die Stadt durch ihren einzigartigen Charme willkommen. Ein gemächlicher Spaziergang offenbarte ein Tuch aus kolonialer Architektur, ruhiger Flusslandschaft und dem lebhaften Puls des lokalen Lebens.  

Die kulinarische Reise des Tages führte uns zu einem Abendessen, das das ikonische kambodschanische Gericht—Amok—zelebrierte. Die duftende und würzige Mischung aus Gewürzen und Kokosmilch verkörperte die Essenz der kambodschanischen Küche und bildete den würdigen Abschluss eines Tages voller Erkundung.  

## Donnerstag: Ein ruhiger Morgen und eine gemütliche Rückzugszeit  
<!--  
früher Morgen (allein auf Frühstücksuche unterwegs)  
leckere Torte zum Frühstück  
Noë krank/schlecht drauf  
Essen und Trinken holen  
ein fauler Tag im Bett  
Serien schauen und entspannen  
-->  

{{< figure src="./images/pie.jpg" alt="Torte" class="aligne-right size-smaller" >}}  

Der Tag begann mit der ruhigen Einsamkeit eines frühen Morgens in Kampot. Allein unterwegs auf der Suche nach Frühstück, stiess ich auf eine angenehme Überraschung—eine leckere Torte, die den Ton des Tages angab.  

Jedoch nahm der Tag eine Wendung, als Noë sich nicht wohl fühlte. Die Priorität verschob sich auf Pflege und Trost. Mit der Besorgung von nahrhaftem Essen und wohltuenden Getränken verwandelte sich der Tag in einen Rückzugsort—eine Auszeit vom hektischen Erleben der vergangenen Tage.  

Mit den Stunden, die dahin schmolzen, entschieden wir uns für einen faulen Tag in der wohltuenden Umarmung des Bettes. Serien wurden zu unseren Begleitern, die sanfte Flucht und die Gelegenheit zur Entspannung boten. Das Tempo des Tages verlangsamte sich zu einem ruhigen Rhythmus—eine notwendige Unterbrechung der Reise, die daran erinnerte, dass Reisen nicht nur Bewegung, sondern auch das Finden von Trost und Erholung sein kann.  

Die Nacht setzte das stille Ende eines gut verbrachten Tages in der Umarmung der Entspannung und Fürsorge fort.  

{{< figure src="./images/sunset.jpg" alt="Sonnenuntergang" class="aligne-center size-medium" >}}  

## Freitag: Kulinarische Genüsse und abenteuerliche Strassen  
<!--  
leckere Torte zum Frühstück (gemeinsam mit Noë)  
mit dem Roller fahren (verrückte Strassen)  
zur Pfefferplantage (allein) (La Plantage)  
kostenfreie Tour und Pfefferschmeckerei machen  
Abendessen (gemeinsam mit Noë)  
-->  

Der Tag in Kampot begann mit einem angenehmen Start—ein gemeinsames Frühstück mit einer weiteren leckeren Torte, das einen positiven Ton für den bevorstehenden Tag setzte.  

{{< figure src="./images/pepper_farm_pepper.jpg" alt="Pfefferplantage" class="aligne-left size-smaller" >}}  

Eifrig, die lokale Landschaft zu erleben, unternahm ich eine Rollerfahrt. Die verrückten Strassen boten ein adrenalingeladenes Abenteuer und enthüllten den einzigartigen Charakter der Umgebung von Kampot.  

Meine Solo-Reise führte mich zur La Plantage, einer Pfefferplantage, die als Zeugnis für den landwirtschaftlichen Reichtum Kambodschas stand. Bei der kostenfreien Tour tauchte ich ein in den komplexen Prozess der Pfefferanbau—vom Samen bis zur Würze. Die Luft war durchzogen vom erdigen Aroma von Pfefferkörnern, während ich an der Pfefferschmeckerei teilnahm und die Nuancen jeder Sorte genoss.  

{{< figure src="./images/pepper_farm.jpg" alt="Pfefferplantage" class="aligne-center size-medium" >}}  

Als der Tag zu den Abendfarben überging, teilten Noë und ich ein weiteres köstliches Essen—eine Krönung unserer gemeinsamen Erlebnisse und der lebendigen Aromen Kambodschas.  

{{< figure src="./images/burger.jpg" alt="Burger" class="aligne-center size-medium" >}}  