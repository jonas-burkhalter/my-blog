+++ 
draft = false
date = 2023-12-06T22:00:00+02:00
title = "Kampot"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "south east asia"]
categories = ["traveling"]
externalLink = ""
series = ["traveling south east asia"]
+++

## Wednesday: Exploring Kampot's Charms and Culinary Delights
<!-- 
get breakfast

take the mini van from kep to kampot

walk around

get dinner (amok)
 -->

{{< figure src="./images/mini_van.jpg" alt="mini van" class="aligne-left size-smaller" >}}

The day in Kampot began with a hearty breakfast, setting the stage for a day of exploration in this charming town. The morning flavors lingered as we embarked on the first leg of our journey—taking a mini-van from Kep to Kampot.

As we stepped onto Kampot's streets, the town welcomed us with its distinct charm. A leisurely walk around unfolded a tapestry of colonial architecture, riverfront tranquility, and the vibrant pulse of local life.

The day's culinary journey led us to a dinner featuring the iconic Cambodian dish—Amok. The fragrant and flavorful blend of spices and coconut milk encapsulated the essence of Cambodian cuisine, a fitting culmination to a day filled with exploration.

## Thursday: A Quiet Morning and a Cozy Retreat
<!--  
early morning (wandering around for breakfast alone)
nice pie for breakfast 

noe sick/not feeling well
get some food and drink for

lazy day in bed
watch some series and relaxe
 -->

{{< figure src="./images/pie.jpg" alt="pie" class="aligne-right size-smaller" >}}

The day unfolded with the quiet solitude of an early morning in Kampot. Wandering alone in search of breakfast, I stumbled upon a delightful surprise—a nice pie that set the tone for the day.

However, the day took a turn as Noë wasn't feeling well. The priority shifted to providing comfort and care. Gathering some nourishing food and soothing drinks, the day transformed into a retreat, a respite from the bustling exploration of the past days.

As the hours melted away, we opted for a lazy day in the comforting embrace of bed. Series became our companions, offering a gentle escape and a chance to unwind. The day's pace slowed to a tranquil rhythm, a necessary interlude in the journey—a reminder that travel is not just about constant movement but also about finding solace and restoration when needed.

The nightfall marked the quiet conclusion of a day well spent in the cocoon of relaxation and care.

{{< figure src="./images/sunset.jpg" alt="sunset" class="aligne-center size-medium" >}}

## Friday: Culinary Delights and Adventurous Roads
<!-- 
nice pie for breakfast (with noe)

ride a scooter (crazy roads)
to pepper plantage (alone) (La Plantage)
doing the free tour and the pepper tasting

get dinner (with noe)
 --> 

The day in Kampot commenced with a delightful start—a shared breakfast featuring another nice pie, setting a positive tone for the day ahead.

{{< figure src="./images/pepper_farm_pepper.jpg" alt="Pepper farm" class="aligne-left size-smaller" >}}

Eager to embrace the local landscape, I embarked on a scooter ride. The crazy roads offered an adrenaline-fueled adventure, unveiling the unique character of Kampot's terrain.

My solo journey led me to La Plantage, a pepper plantation that stood as a testament to Cambodia's agricultural richness. Engaging in the free tour, I delved into the intricate process of pepper cultivation, from seed to spice. The air was infused with the earthy aroma of peppercorns as I participated in pepper tastings, savoring the nuances of each variety.

{{< figure src="./images/pepper_farm.jpg" alt="Pepper farm" class="aligne-center size-medium" >}}

As the day transitioned to evening hues, Noë and I shared another delightful meal—a culmination of our shared experiences and the vibrant flavors of Cambodia.

{{< figure src="./images/burger.jpg" alt="burger" class="aligne-center size-medium" >}}
