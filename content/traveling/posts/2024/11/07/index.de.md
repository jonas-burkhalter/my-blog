+++ 
draft = false
date = 2024-11-07T22:00:00+02:00
title = "Belfast"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Irland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Irland"]
+++

## Donnerstag: Von den Küstenskippen zu Belfast
{{< figure src="./images/PXL_20241107_110724820.jpg" alt="Giant's Causeway" class="aligne-right size-small" >}}
Der Donnerstag begann zurück am Salmon Rock Beach, um einen letzten ruhigen Moment vor der heutigen Reise zu geniessen. Von dort aus machte ich einen malerischen Spaziergang zum Giant's Causeway. Die seltsamen, ineinandergreifenden Steine waren beeindruckend, aber die Menschenmengen nahmen etwas von der Magie. Auf dem Rückweg wählte ich den Weg entlang der Klippen, der weitreichende Ausblicke bot und deutlich weniger Menschen hatte.

{{< figure src="./images/PXL_20241107_124934117.jpg" alt="Aussicht" class="aligne-left size-small" >}}

Danach fuhr ich entlang der Küste und nahm die Landschaft mit jeder Kurve in mich auf. Mein ursprünglicher Plan beinhaltete noch einen weiteren Stopp, doch es stellte sich heraus, dass dieser auf Privatgrund war. Also fuhr ich weiter und landete schliesslich in Glenarm, wo ich zu Mittag ass und zum ersten Mal Hummer probierte. Der Geschmack war grossartig, obwohl es ein ziemlicher Kampf mit der Schale war!

{{< figure src="./images/PXL_20241107_143010800.jpg" alt="Hummer" class="aligne-center size-medium" >}}

Als ich in Belfast ankam, machte ich eine kurze Pause, bevor ich mich auf den Weg zum Common Market machte, um zu Abend zu essen. Die lebendige Atmosphäre, die leckeren Essensstände und ein paar Pints waren der perfekte Abschluss des Tages und liessen mich die Stimmung von Belfast aufnehmen.

## Freitag: Abtauchen in Belfasts Titanic-Erbe
{{< figure src="./images/PXL_20241108_121456388.MP.jpg" alt="Titanic" class="aligne-left size-small" >}}

Der Freitag begann mit einem Besuch der Titanic Experience, wo ich einen genaueren Blick auf den Bau dieses riesigen Schiffs und die Arbeiter dahinter werfen konnte. Ein Teil blieb mir besonders im Gedächtnis: Ein Zitat eines Werftarbeiters, der sagte: „Die bauen sie einfach und schieben sie rein!“ Es war eine so rohe, bodenständige Art, den Einsatz der Arbeiter von Belfast zu zeigen. Die Menschen hier waren Meister im Schiffsbau, und mit der Titanic setzten sie sich das Ziel, etwas wirklich Monumentales zu schaffen.

{{< figure src="./images/PXL_20241108_122410163.jpg" alt="Titanic" class="aligne-right size-medium" >}}

Die Ausstellung erklärte den gesamten Prozess, von der Entwurfsphase bis hin zur Montage des Schiffes. Harland und Wolff, die Schiffbauer, verwendeten über 3 Millionen Nieten, um die Titanic zusammenzusetzen – eine erstaunliche Leistung, wenn man sich vorstellt, dass jede einzelne von Hand eingeschlagen werden musste. In der Galerie stand ich und konnte sehen, wie der doppelt verstärkte Rumpf des Schiffes entworfen wurde, um sie „unsinkbar“ zu machen – eine Designinnovation für ihre Zeit. Sie hatte vier riesige Schornsteine, obwohl nur drei funktional waren; der vierte war rein ästhetisch hinzugefügt worden.

Nach der Ausstellung gönnte ich mir ein Mittagessen und besuchte dann die SS Nomadic, das letzte verbleibende Schiff der White Star Line, das einst Passagiere zur Titanic selbst beförderte. Es fühlte sich surreal an, an Bord eines Stücks Titanic-Geschichte zu gehen, das heute noch existiert.

{{< figure src="./images/PXL_20241108_140601669.jpg" alt="Stadthalle" class="aligne-left size-smaller" >}}

Auf dem Rückweg schlenderte ich durch den St. George's Market, stöberte in lokalen Handwerken und frischen Produkten und machte eine Runde um das Belfast City Hall. Am Abend kehrte ich wieder zum Common Market zurück, um etwas zu essen und ein Pint zu geniessen – die lebendige Energie von Belfast machte den Tag, der so stark von Geschichte geprägt war, perfekt.

{{< figure src="./images/PXL_20241108_175537639.jpg" alt="Abend" class="aligne-center size-medium" >}}