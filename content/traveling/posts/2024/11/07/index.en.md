+++ 
draft = false
date = 2024-11-07T22:00:00+02:00
title = "Belfast"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Thursday: From Coastal Cliffs to Belfast Bites
{{< figure src="./images/PXL_20241107_110724820.jpg" alt="Giant's Causeway" class="aligne-right size-small" >}}
Thursday began back at Salmon Rock Beach for one last quiet moment before the day's journey. From there, I took a scenic walk to the Giant's Causeway. The strange, interlocking stones were impressive, but the crowds took away from the magic. Heading back, I chose the cliff-top path, which offered sweeping views and far fewer people.

{{< figure src="./images/PXL_20241107_124934117.jpg" alt="view" class="aligne-left size-small" >}}

Afterward, I drove along the coast, soaking in the landscape with each turn. My original plan included one more stop, but it turned out to be on private property. So, I pressed on and found myself stopping in Glenarm for lunch, where I tried lobster for the first time. The flavor was great, though it turned into quite the battle with the shell!

{{< figure src="./images/PXL_20241107_143010800.jpg" alt="lobster" class="aligne-center size-medium" >}}

Once in Belfast, I took a quick break before venturing out to the Common Market for dinner. The lively atmosphere, tasty food stalls, and a couple of pints were the perfect way to wrap up the day and welcome Belfast’s vibe.



## Friday: Diving into Belfast's Titanic Legacy
{{< figure src="./images/PXL_20241108_121456388.MP.jpg" alt="titanic" class="aligne-left size-small" >}}

Friday started with a visit to the Titanic Experience, where I got a close-up look at the construction of this massive ship and the workers behind it. One part stuck with me: a quote from a shipyard worker that said, “They just builds ’em and shoves ’em in!” It was such a raw, down-to-earth way of showing the grit of Belfast’s workforce. The people here were masters of shipbuilding, and with Titanic, they set out to create something truly monumental.

{{< figure src="./images/PXL_20241108_122410163.jpg" alt="titanic" class="aligne-right size-medium" >}}

The exhibit detailed the entire process, from the blueprint phase to her assembly. Harland and Wolff, the shipbuilders, used over 3 million rivets to put Titanic together, a mind-boggling feat when you imagine each one had to be hammered in by hand. Standing in the gallery, I saw how the ship’s double-bottomed hull was reinforced to make her “unsinkable,” a design innovation for her time. She had four towering smokestacks—though only three were functional, with the fourth added purely for aesthetic grandeur.

After the exhibit, I grabbed some lunch and then visited the SS Nomadic, the last remaining White Star Line vessel, which once ferried passengers to the Titanic herself. It felt surreal stepping on board a piece of Titanic history that still exists today.

{{< figure src="./images/PXL_20241108_140601669.jpg" alt="city hall" class="aligne-left size-smaller" >}}

On my way back, I wandered through St. George’s Market, browsing local crafts and fresh produce, and took a loop around Belfast City Hall. That evening, I returned to the Common Market for some food and a pint—Belfast's lively energy making for a great end to a day steeped in history.


{{< figure src="./images/PXL_20241108_175537639.jpg" alt="evening" class="aligne-center size-medium" >}}
