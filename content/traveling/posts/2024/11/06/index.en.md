+++ 
draft = false
date = 2024-11-06T22:00:00+02:00
title = "Bushmills"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Wednesday: Bushmills Culinary Wonders
{{< figure src="./images/PXL_20241106_125322805.MP.jpg" alt="beach" class="aligne-right size-small" >}}

Wednesday saw me cruising over from Derry to Bushmills, where I kicked off the day with a delicious lunch. I had some time to kill before my next stop, so I ventured over to Salmon Rock Beach. Standing there, I soaked in the sight of the ocean stretching infinitely, a calm moment of quiet that set the mood for the day.

{{< figure src="./images/PXL_20241106_160446383.jpg" alt="distillery" class="aligne-left size-small" >}}

The Bushmills Distillery tour soon came, and it was packed with whisky-making trivia—just my kind of thing. Here’s a peek at the process: from 48,000 liters of mash, you get 5,000 liters of potent 89% alcohol after triple distillation. Water is then added to bring it down to 63% for casking, where it’s aged and gains most of its flavor—85% of that classic whiskey taste comes from the cask alone! The tour wound through the steps: malting, mashing, fermentation, distillation, and finally, casking and bottling. And, of course, there was a glass of whiskey waiting for us at the end. 

{{< figure src="./images/PXL_20241106_163416306.jpg" alt="airbnb" class="aligne-right size-small" >}}

With the tour done, I checked into my [Airbnb](
https://www.airbnb.com/rooms/26151821?viralityEntryPoint=1&s=76)—a charming loft overlooking the River Bush, with views of the old mill and the horizon that felt straight out of a postcard. It was an oasis, and I settled in happily.

Dinner at Tartine at the Distillers Arms was the perfect way to cap off the day. Smoked salmon with pickled vegetables, horseradish mayo, and wheaten bread kicked things off, followed by pan-fried seabass on chorizo and pea pasta with a white wine cream sauce. To finish, apple and rhubarb crumble with custard and vanilla ice cream hit the sweet spot. Everything was so beautifully presented that it felt like eating art.

{{< figure src="./images/PXL_20241106_181120681.jpg" alt="food" class="aligne-left size-third" >}}
{{< figure src="./images/PXL_20241106_184556057.jpg" alt="food" class="aligne-left size-third" >}}
{{< figure src="./images/PXL_20241106_190337514.jpg" alt="food" class="aligne-left size-third" >}}

When I finally headed back, stomach full and heart happy, I realized today was one of my favorite solo travel days yet.

