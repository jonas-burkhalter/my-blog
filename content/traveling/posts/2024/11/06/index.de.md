+++ 
draft = false
date = 2024-11-06T22:00:00+02:00
title = "Bushmills"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Irland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Irland"]
+++

## Mittwoch: Kulinarische Entdeckungen in Bushmills
{{< figure src="./images/PXL_20241106_125322805.MP.jpg" alt="Strand" class="aligne-right size-small" >}}

Am Mittwoch fuhr ich von Derry nach Bushmills, wo ich den Tag mit einem köstlichen Mittagessen begann. Da ich noch etwas Zeit hatte, machte ich mich auf den Weg zum Salmon Rock Beach. Dort stand ich und genoss den Blick auf den endlosen Ozean – ein ruhiger Moment, der die Stimmung des Tages setzte.

{{< figure src="./images/PXL_20241106_160446383.jpg" alt="Destillerie" class="aligne-left size-small" >}}

Die Führung durch die Bushmills-Destillerie folgte bald und war voll von Whisky-Wissen – genau mein Ding. Ein kleiner Einblick in den Prozess: Aus 48.000 Litern Maische erhält man 5.000 Liter kräftigen 89%igen Alkohol nach der dreifachen Destillation. Wasser wird dann hinzugefügt, um den Alkoholgehalt auf 63% zu reduzieren, bevor er in Fässern reift und seinen charakteristischen Geschmack entwickelt. 85% des typischen Whisky-Geschmacks kommen allein aus dem Fass! Die Führung führte uns durch die verschiedenen Schritte: Malzen, Maischen, Fermentation, Destillation und schliesslich Reifung und Abfüllung. Am Ende gab es natürlich ein Glas Whisky.

{{< figure src="./images/PXL_20241106_163416306.jpg" alt="Airbnb" class="aligne-right size-small" >}}

Nach der Tour checkte ich in meine [Airbnb](https://www.airbnb.com/rooms/26151821?viralityEntryPoint=1&s=76) ein – ein charmantes Loft mit Blick auf den River Bush und die alte Mühle, das wie aus einer Postkarte entsprungen schien. Es war eine Oase, und ich fühlte mich sofort zu Hause.

Das Abendessen im Tartine at the Distillers Arms war der perfekte Abschluss des Tages. Zu Beginn gab es Räucherlachs mit eingelegtem Gemüse, Meerrettich-Mayo und Weizenkuchenbrot, gefolgt von gebratenem Wolfsbarsch auf Chorizo- und Erbsen-Pasta mit einer Weisswein-Sahnesosse. Zum Dessert gab es Apfel-Rhabarber-Crumble mit Vanillesauce und Vanilleeis – ein wahrer Genuss! Alles war so schön angerichtet, dass es fast wie Kunst auf dem Teller war.

{{< figure src="./images/PXL_20241106_181120681.jpg" alt="Essen" class="aligne-left size-third" >}}
{{< figure src="./images/PXL_20241106_184556057.jpg" alt="Essen" class="aligne-left size-third" >}}
{{< figure src="./images/PXL_20241106_190337514.jpg" alt="Essen" class="aligne-left size-third" >}}

Als ich schliesslich zurückging, mit einem vollen Bauch und einem glücklichen Herzen, wurde mir klar, dass dieser Tag einer meiner liebsten Tage auf meiner Solo-Reise war.