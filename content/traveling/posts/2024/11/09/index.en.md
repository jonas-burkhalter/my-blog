+++ 
draft = false
date = 2024-11-09T22:00:00+02:00
title = "Newry"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Saturday: Let's go into the sea
{{< figure src="./images/PXL_20241109_105646868.jpg" alt="otters" class="aligne-left size-small" >}}
{{< figure src="./images/PXL_20241109_120542084.jpg" alt="ray" class="aligne-right size-small" >}}

Today started earlier than usual so I could catch the ferry from Strangford to Portaferry and get to Exploris Aquarium. On the way, I stopped for breakfast to fuel up. The aquarium was incredibly peaceful as one of the first visitors there, letting me wander through the exhibits in total quiet. I had time to really soak up each display, especially the ocean tunnel with fish gliding overhead. I lingered by the huge ocean tank and watched the feedings of the seals, penguins, and otters, where I learned more about their habits and personalities. The whole visit felt calm and unhurried, and it was a real treat.

{{< figure src="./images/PXL_20241109_125613396.jpg" alt="lunch" class="aligne-center size-small" >}}

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.08(1).jpg" alt="walk" class="aligne-right size-small" >}}
Afterward, I had a hearty lunch—chicken with mashed potatoes and onions—before catching the ferry back. Then, it was off to Newcastle to meet Claire again. We grabbed a couple of pints and took a stroll along the shops and beach before settling into a spot to watch a football game. 

In the evening, we capped things off with a round of pool at a local pub—and, for the record, I finally beat Claire once and for all! It was the perfect victory to end the day, with some lighthearted competition and laughter, making the night one to remember.

{{< figure src="./images/PXL_20241109_211114053.jpg" alt="pool" class="aligne-center size-medium" >}}

## Sunday: Best tour guide ever
Today was special—not only my last day of exploring, but I had the best tour guide for this region: Claire. I drove from Newcastle to Newry in the morning to meet her, and we set out for a day packed with local gems and insights. We kicked things off in Stranfield with a hearty farmhouse breakfast, fueling up for the day ahead. 


{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.06.jpg" alt="Carlingford" class="aligne-right size-medium" >}}

Our first stop was Gyles Quay Beach, one of Claire's favorite spots, where we took in the sea views before heading to Carlingford, a charming medieval town. We strolled through the old streets, taking in the history, and continued on to Omeath, Slieve Gullion, and Claire’s childhood home, surrounded by beautiful countryside.

{{< figure src="./images/PXL_20241110_132900213.jpg" alt="forest" class="aligne-left size-medium" >}}

We also stopped at the Camlough Lake viewpoint and Slieve Gullion, where the views were breathtaking and stretched for miles. After that, we made our way to Dundalk for a unique "Chinese food" experience at the Jade Garden. True to Irish style, the meal even came with a side of chips! 

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.01.jpg" alt="countryside" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

The day wrapped up with a final stop at a cozy pub, where we enjoyed our last pint together and exchanged goodbyes. It was a memorable day full of laughter, stories, and local secrets, and I’m incredibly grateful to Claire for her companionship and guidance. It was the perfect end to an amazing journey. Thank you, Claire, for making this trip unforgettable!

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.00(1).jpg" alt="view" class="aligne-center size-medium" >}}

## Monday: Let's go home
{{< figure src="./images/PXL_20241111_162920656.jpg" alt="airport" class="aligne-right size-medium" >}}

Today marked the final leg of my journey. I took one last scenic drive along the coast to Dundalk, where I stopped for breakfast and had a quick stroll around town to savor the Irish atmosphere one last time. From there, it was time to head to Dublin to return the car and make my way to the airport.

The airport routine—bag drop, security, and finding the gate—went smoother than I expected, leaving me time for a farewell pint of Guinness and a toast to an unforgettable trip. Then came the boarding call, and I found myself on the flight back to Zurich, followed by a quiet train ride back home.
