+++ 
draft = false
date = 2024-11-09T22:00:00+02:00
title = "Newry"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Irland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Irland"]
+++

## Samstag: Ab ins Meer
{{< figure src="./images/PXL_20241109_105646868.jpg" alt="Otter" class="aligne-left size-small" >}}
{{< figure src="./images/PXL_20241109_120542084.jpg" alt="Rochen" class="aligne-right size-small" >}}

Der heutige Tag begann früher als üblich, damit ich die Fähre von Strangford nach Portaferry erwischen und das Exploris Aquarium besuchen konnte. Auf dem Weg dorthin hielt ich an, um zu frühstücken und mich für den Tag zu stärken. Das Aquarium war unglaublich ruhig, da ich einer der ersten Besucher war, was es mir ermöglichte, ganz in Ruhe durch die Ausstellungen zu schlendern. Ich hatte genug Zeit, um jede Ausstellung in Ruhe zu geniessen, besonders den Ozeantunnel, in dem Fische über mir hinwegglitten. Ich verweilte an dem riesigen Ozeanbecken und beobachtete die Fütterung der Robben, Pinguine und Otter, wobei ich viel über ihre Gewohnheiten und Persönlichkeiten erfuhr. Der gesamte Besuch war ruhig und entspannt, und es war ein echtes Vergnügen.

{{< figure src="./images/PXL_20241109_125613396.jpg" alt="Mittagessen" class="aligne-center size-small" >}}

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.08(1).jpg" alt="Spaziergang" class="aligne-right size-small" >}}
Danach gab es ein herzhaftes Mittagessen – Hühnchen mit Kartoffelpüree und Zwiebeln – bevor ich die Fähre zurücknahm. Danach ging es weiter nach Newcastle, um Claire wieder zu treffen. Wir gönnten uns ein paar Pints und schlenderten durch die Geschäfte und entlang des Strandes, bevor wir uns in ein Lokal setzten, um ein Fussballspiel zu schauen. 

Am Abend rundeten wir den Tag mit einer Runde Billard in einem lokalen Pub ab – und zu meiner Freude besiegte ich Claire endlich einmal! Es war der perfekte Sieg, um den Tag zu beenden, mit einer leichten Konkurrenz und viel Gelächter, was die Nacht unvergesslich machte.

{{< figure src="./images/PXL_20241109_211114053.jpg" alt="Billard" class="aligne-center size-medium" >}}

## Sonntag: Beste Stadtführerin aller Zeiten
Heute war ein besonderer Tag – nicht nur mein letzter Tag des Erkundens, sondern auch der Tag, an dem ich die beste Stadtführerin der Region hatte: Claire. Ich fuhr am Morgen von Newcastle nach Newry, um sie zu treffen, und wir machten uns auf zu einem Tag voller lokaler Schätze und Einsichten. Wir starteten in Stranfield mit einem herzhaften Frühstück auf dem Bauernhof, um uns für den bevorstehenden Tag zu stärken.

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.06.jpg" alt="Carlingford" class="aligne-right size-medium" >}}

Unser erster Stopp war der Gyles Quay Beach, einer von Claires Lieblingsplätzen, wo wir die Aussicht auf das Meer genossen, bevor wir nach Carlingford weiterfuhren, einer charmanten mittelalterlichen Stadt. Wir schlenderten durch die alten Strassen und nahmen die Geschichte auf uns, bevor es weiter nach Omeath, Slieve Gullion und zu Claires Kindheitsheim ging, umgeben von schöner Natur.

{{< figure src="./images/PXL_20241110_132900213.jpg" alt="Wald" class="aligne-left size-medium" >}}

Wir hielten auch am Aussichtspunkt des Camlough Lake und Slieve Gullion an, wo die Ausblicke atemberaubend waren und sich weit in die Ferne erstreckten. Danach machten wir uns auf den Weg nach Dundalk für ein einzigartiges "Chinesisches Essen"-Erlebnis im Jade Garden. Ganz im irischen Stil gab es sogar eine Beilage mit Chips!

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.01.jpg" alt="Landlandschaft" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Der Tag endete mit einem letzten Stopp in einem gemütlichen Pub, wo wir unser letztes Pint zusammen genossen und uns verabschiedeten. Es war ein unvergesslicher Tag voller Lachen, Geschichten und lokaler Geheimnisse, und ich bin Claire unendlich dankbar für ihre Gesellschaft und Führung. Es war der perfekte Abschluss einer grossartigen Reise. Danke, Claire, dass du diese Reise unvergesslich gemacht hast!

{{< figure src="./images/WhatsApp Image 2024-11-11 at 09.39.00(1).jpg" alt="Aussicht" class="aligne-center size-medium" >}}

## Montag: Ab nach Hause
{{< figure src="./images/PXL_20241111_162920656.jpg" alt="Flughafen" class="aligne-right size-medium" >}}

Heute war der letzte Abschnitt meiner Reise. Ich machte noch eine letzte landschaftlich reizvolle Fahrt entlang der Küste nach Dundalk, wo ich für ein Frühstück anhielt und einen kurzen Spaziergang durch die Stadt machte, um die irische Atmosphäre ein letztes Mal zu geniessen. Von dort aus ging es weiter nach Dublin, um das Auto abzugeben und mich auf den Weg zum Flughafen zu machen.

Die Flughafenroutine – Gepäckabgabe, Sicherheit und das Finden des Gates – verlief reibungsloser als erwartet, sodass ich noch Zeit für ein Abschiedspint Guinness hatte und auf eine unvergessliche Reise anstiess. Dann kam der Boarding-Aufruf, und ich fand mich im Flugzeug auf dem Weg zurück nach Zürich, gefolgt von einer ruhigen Zugfahrt nach Hause.