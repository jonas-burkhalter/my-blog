+++ 
draft = false
date = 2024-11-04T22:00:00+02:00
title = "Derry"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Irland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Irland"]
+++

## Montag: Eine *Derry Girls*-Nacht
{{< figure src="./images/PXL_20241104_103553012.jpg" alt="Gleniff Horseshoe" class="aligne-right size-small" >}}

Am Montagmorgen wachte ich mit einem leichten Unwohlsein auf, aber das Reisen wartet nun mal nicht auf eine Erkältung! Nach einem langsamen Start mit einer Tasse Tee machte ich mich auf den Weg zum Gleniff Horseshoe, in der Hoffnung auf spektakuläre Landschaften. Doch wie es der Reisepreis wollte, hing dichter Nebel über den Bergen und verbarg alles jenseits der unteren Hänge. So viel zu den weiten Aussichten, die ich online gesehen hatte—manchmal hat die Natur eben andere Pläne.

{{< figure src="./images/PXL_20241104_104015516.jpg" alt="Gleniff Horseshoe" class="aligne-center size-large" >}}

Weiter ging es zum Wild Ireland, wo ich an einer geführten Tour teilnahm, die einen Einblick in die wilde Vergangenheit Irlands gab. Während ich den Geschichten von Luchsen, Wölfen und Bären lauschte, die einst dieses Land durchstreiften, fragte ich mich, wie die Wälder der Insel damals ausgesehen haben mussten. Nach der Tour streifte ich durch das Schutzgebiet, besuchte Füchse, Wildschweine und sogar Affen, von denen die meisten gerettet worden waren. Es war zwar schön zu sehen, wie Tieren aus schlimmen Situationen geholfen wird, aber ich konnte nicht umhin, zu denken, dass Gehege einfach kein wahres Zuhause für diese Tiere sind.

{{< figure src="./images/PXL_20241104_132404286.jpg" alt="Bär" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241104_130300615.jpg" alt="Wölfe" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Weiter ging es nach Derry! Plötzlich wechselten die Strassenschilder von Kilometern auf Meilen pro Stunde, und mein GPS gab die Meter in Fuss an—willkommen in Nordirland. Nachdem ich am Airbnb geparkt hatte, machte ich einen kurzen Spaziergang durch Derry, besichtigte das Guildhall, die Peace Bridge und natürlich das ikonische *Derry Girls*-Mural.

{{< figure src="./images/PXL_20241104_153450365.jpg" alt="Derry Girls" class="aligne-center size-medium" >}}

Da ich mich immer noch etwas unwohl fühlte, liess ich den Abend ruhig ausklingen und schaute mir *Derry Girls* aus dem Komfort meines Airbnbs in Derry an—ein gemütliches, passendes Ende für einen nebligen und nachdenklichen Tag.

## Dienstag: Lektionen in Geschichte
{{< figure src="./images/PXL_20241105_110027369.jpg" alt="Frühstück" class="aligne-left size-small" >}}

Der Dienstag begann langsam, mit etwas mehr Schlaf, um die anhaltende Erkältung loszuwerden. Schliesslich machte ich mich auf zum Craft Village in Derry, wo ein herzhaftes Frühstück half, mich wieder etwas mehr wie ein Mensch zu fühlen. Danach ging es zu den Stadtmauern, wo ich den Wegen folgte, die einen Blick über Derry boten und mir die Jahrhunderte voller Geschichte in den Steinen vorstellte.

{{< figure src="./images/PXL_20241105_112701595.jpg" alt="Derry Walls" class="aligne-right size-small" >}}

Nach etwas Herumstreifen kam ich zum Museum of Free Derry. Dieses kleine, aber kraftvolle Museum vermittelt einen wichtigen Teil der irischen Geschichte, besonders die Ereignisse rund um die Bürgerrechtsbewegung in den späten 1960er Jahren und den berüchtigten Bloody Sunday von 1972.

{{< figure src="./images/PXL_20241105_115150770.MP.jpg" alt="Museum of Free Derry" class="aligne-left size-small" >}}

In dieser Zeit marschierten die Bürger von Derry, inspiriert von globalen Bürgerrechtsbewegungen, gegen Diskriminierung in den Bereichen Wohnraum, Beschäftigung und Wahlrecht. Diese Proteste wurden mit gewaltsamen Niederschlagungen beantwortet, darunter der Bloody Sunday, bei dem britische Soldaten das Feuer auf unbewaffnete Demonstranten eröffneten und tragischerweise 14 Menschen töteten. Das Museum ist ein Denkmal für die Opfer und ein Mahnmal für die Widerstandskraft der Menschen in Derry und Nordirland.

Als ich ging, war ich emotional und körperlich erschöpft. Ich zog mich ins Airbnb zurück, gab mich einer kurzen Siesta hin und genoss einen ruhigen Abend, um mich zu erholen, in der Hoffnung, für morgen wieder fitter zu sein.

{{< figure src="./images/PXL_20241105_123026345.jpg" alt="Freiheit" class="aligne-center size-medium" >}}