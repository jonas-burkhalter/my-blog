+++ 
draft = false
date = 2024-11-04T22:00:00+02:00
title = "Derry"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Monday: A Derry Girls Night
{{< figure src="./images/PXL_20241104_103553012.jpg" alt="Gleniff Horseshoe" class="aligne-right size-small" >}}

Monday morning, I woke up feeling a bit under the weather, but travel waits for no cold! After a slow start with tea in hand, I set out toward Gleniff Horseshoe, hoping for some epic scenery. But, as travel luck would have it, a thick fog clung to the mountains, hiding everything beyond the lower slopes. So much for the sweeping views I’d seen online—sometimes nature has other ideas.

{{< figure src="./images/PXL_20241104_104015516.jpg" alt="Gleniff Horseshoe" class="aligne-center size-large" >}}

Moving along, I made my way to Wild Ireland, where I joined a guided tour that gave a glimpse into Ireland’s wild past. Listening to the stories of native lynx, wolves, and bears that once roamed these lands, I wondered how the island’s forests must have looked back then. After the tour, I wandered through the sanctuary, visiting foxes, wild boars, and even monkeys, most of whom were rescues. Though it’s heartening to see animals saved from terrible situations, I couldn’t shake the thought that enclosures just aren’t a true home for them.

{{< figure src="./images/PXL_20241104_132404286.jpg" alt="bear" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241104_130300615.jpg" alt="wolfs" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Onward to Derry! Suddenly, the road signs switched from kilometers to miles per hour, and my GPS ditched meters for feet—welcome to Northern Ireland. Once I parked at the Airbnb, I took a quick stroll through Derry, catching the Guildhall, Peace Bridge, and, of course, the iconic *Derry Girls* mural.

{{< figure src="./images/PXL_20241104_153450365.jpg" alt="derry girls" class="aligne-center size-medium" >}}

Still feeling a bit off, I settled in for a quiet evening, watching *Derry Girls* from the comfort of my Airbnb in Derry—a cozy, fitting end to a foggy, thoughtful day.

## Tuesday: Lessons in History
{{< figure src="./images/PXL_20241105_110027369.jpg" alt="breakfast" class="aligne-left size-small" >}}

Tuesday began slowly, with a bit more sleep to try to shake off this lingering cold. Eventually, I ventured out to Derry’s Craft Village, where a hearty breakfast helped me feel a bit more human. From there, I made my way to the city walls, following the paths that overlook Derry, imagining the centuries of stories embedded in the stone.

{{< figure src="./images/PXL_20241105_112701595.jpg" alt="derry walls" class="aligne-right size-small" >}}

After a bit of wandering, I arrived at the Museum of Free Derry. This small yet powerful museum captures a crucial part of Irish history, especially the events surrounding the civil rights movement in the late 1960s and the infamous Bloody Sunday of 1972.

{{< figure src="./images/PXL_20241105_115150770.MP.jpg" alt="museum of free derry" class="aligne-left size-small" >}}

During this period, citizens of Derry, inspired by global civil rights movements, marched against discrimination in housing, employment, and voting rights. These protests were met with violent crackdowns, including Bloody Sunday, where British soldiers opened fire on unarmed protesters, tragically killing 14 people. The museum stands as a memorial to those who lost their lives and serves as a reminder of the resilience of the people in Derry and Northern Ireland.

By the time I left, I was emotionally and physically drained. I retreated to the Airbnb, surrendering to a nap and a quiet evening to rest up, hoping to feel more like myself for whatever tomorrow might bring.

{{< figure src="./images/PXL_20241105_123026345.jpg" alt="freedom" class="aligne-center size-medium" >}}
