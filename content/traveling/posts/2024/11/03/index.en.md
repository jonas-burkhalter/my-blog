+++ 
draft = false
date = 2024-11-03T22:00:00+02:00
title = "Mayo and Sligo"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Sunday: A Not-Quite Summit, A Hidden Castle, and Farewell to Claire
{{< figure src="./images/PXL_20241103_105608030.jpg" alt="Croagh Patrick" class="aligne-right size-small" >}}

Sunday started off with a couple of tired faces and cups of tea after a restless night. We took our time packing up, then ventured out for breakfast, rallying ourselves for a climb up Croagh Patrick.

It didn’t take long to realize the peak wasn’t in our cards today—Claire’s leg was still protesting after last night’s “wall encounter,” and the clouds above looked like trouble. We hiked up partway, took in the views of Clew Bay, and then made our way back down, content with a different kind of mountain experience.

{{< figure src="./images/WhatsApp Image 2024-11-04 at 21.44.41.jpg" alt="Croagh Patrick" class="aligne-center size-medium" >}}

From there, we decided to break up the drive with a detour to McDermott Castle. Though the castle, perched on its island, was only partially visible, the autumn-kissed forest around it was like something out of a storybook. We wandered through the woods, savoring the quiet and the vivid colors of fall.

{{< figure src="./images/PXL_20241103_134345214.jpg" alt="McDermott Castle" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241103_140622161.jpg" alt="forest" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Then it was time for goodbyes—Claire headed home, and I continued on to Strandhill, where I wrapped up the day with a well-deserved lazy evening.
