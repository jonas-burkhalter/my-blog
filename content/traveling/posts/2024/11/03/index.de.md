+++  
draft = false  
date = 2024-11-03T22:00:00+02:00  
title = "Mayo und Sligo"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Sonntag: Kein Gipfel, ein verstecktes Schloss und Abschied von Claire  
{{< figure src="./images/PXL_20241103_105608030.jpg" alt="Croagh Patrick" class="aligne-right size-small" >}}  

Der Sonntag begann mit ein paar müden Gesichtern und Tassen Tee nach einer unruhigen Nacht. Wir liessen uns Zeit beim Packen, bevor wir zum Frühstück aufbrachen und uns aufrafften, um den Croagh Patrick zu besteigen.  

Es dauerte nicht lange, bis klar wurde, dass der Gipfel heute ausser Reichweite war – Claires Bein protestierte immer noch nach ihrem gestrigen "Wandvorfall", und die dunklen Wolken über uns verhiessen nichts Gutes. Wir wanderten ein Stück den Berg hinauf, genossen den Ausblick auf die Clew Bay und machten uns dann zufrieden wieder auf den Weg nach unten – eine etwas andere Art von Bergerlebnis.  

{{< figure src="./images/WhatsApp Image 2024-11-04 at 21.44.41.jpg" alt="Croagh Patrick" class="aligne-center size-medium" >}}  

Von dort aus beschlossen wir, die Fahrt mit einem Abstecher zum McDermott Castle zu unterbrechen. Das Schloss, das auf seiner Insel thront, war zwar nur teilweise sichtbar, aber der herbstliche Wald drumherum wirkte wie aus einem Märchenbuch. Wir spazierten durch den Wald, genossen die Ruhe und die leuchtenden Farben des Herbstes.  

{{< figure src="./images/PXL_20241103_134345214.jpg" alt="McDermott Castle" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241103_140622161.jpg" alt="forest" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

Dann war es Zeit für den Abschied – Claire machte sich auf den Heimweg, und ich setzte meine Reise nach Strandhill fort, wo ich den Tag mit einem wohlverdienten, entspannten Abend ausklingen liess.