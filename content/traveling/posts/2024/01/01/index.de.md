+++
draft = false
date = 2024-01-01T22:00:00+02:00
title = "Waiheke Island"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Neuseeland"]
+++

## Montag: Ein ruhiger Start auf Waiheke Island
<!--
Kaffee in Auckland

Fähre nach Waiheke Island

entspannen
shoppen gehen

herumspazieren

in einer Hängematte entspannen

Abendessen kochen

entspannen
-->
Der erste Tag des neuen Jahres begann in einem ruhigen Rhythmus, beginnend mit dem beruhigenden Genuss eines morgendlichen Kaffees in Auckland.

Die Hauptreise des Tages führte uns nach Waiheke Island, wo das sanfte Schaukeln der Fähre den Übergang vom städtischen Trubel zur Inselruhe markierte. Die Insel bot eine Leinwand der Entspannung, durchzogen von Momenten der Entdeckung.

{{< figure src="./images/ocean.jpg" alt="Ozean" class="aligne-right size-medium" >}}

Durch die charmanten Strassen wandernd, fügte eine gemütliche Shoppingtour einen Hauch lokalen Flairs zum Tag hinzu. Die natürliche Schönheit der Insel wurde zum Begleiter eines Spaziergangs, bei dem jeder Schritt neue Ausblicke bot.

Mitten in der ruhigen Atmosphäre lockte eine Hängematte – eine Einladung, sich zu entspannen und das gemächliche Tempo des Insellebens zu geniessen. Als die Sonne unter den Horizont sank, wurde das einfache Vergnügen, das Abendessen zuzubereiten, zu einer meditativen Handlung, die eine heimelige Atmosphäre in dieser Inselidylle schuf.

## Dienstag: Den unruhigen Geist beruhigen
<!--
absolut nichts tun
in der Hängematte liegen
-->

{{< figure src="./images/hammock_view.jpg" alt="Hängemattenblick" class="aligne-left size-medium" >}}
Dienstag entpuppte sich als Herausforderung – ein Versuch, die verlorene Kunst des Nichtstuns zu meistern. Trotz des Kampfes, loszulassen und sich zu entspannen, schien auf der Agenda nichts Wichtigeres zu stehen. Mit viel freier Zeit entschloss ich mich, die Unruhe anzugehen und zu versuchen, meinen Geist zu befreien.

In der stillen Gesellschaft der Natur wurde die Hängematte sowohl mein Zufluchtsort als auch mein Gegner. Das rhythmische Knarren hallte den Kampf zwischen einem unruhigen Geist und dem sanften Schwingen der Hängematte wider.

## Mittwoch: Strandvergnügen und Hängemattenharmonie
<!--
in der Hängematte entspannen

zum Strand gehen

noch mehr entspannen
-->
An den idyllischen Ufern von Waiheke Island gestaltete sich der heutige Tag als ein Tag der reinen Entspannung.

Der Morgen begann mit der Rückkehr zur vertrauten Zuflucht in der Hängematte – ein Oase der Ruhe, in der die Welt ausserhalb zu verblassen schien und sich dem sanften Schwingen der Inselruhe hingab.

Im Laufe des Tages lockte der Strand. Der von der Sonne durchflutete Sand wurde zu einer Leinwand für barfüssige Wanderungen. Die erfrischende Umarmung des Ozeans bildete einen angenehmen Kontrast zur Wärme der Sonne.

Zurück in der Hängematte wurde das Ritual fortgesetzt, eine Fortsetzung des Tagesmottos der gemächlichen Erholung. Mit dem Hintergrund der natürlichen Symphonie der Insel – dem Rascheln der Palmenblätter, dem fernen Murmeln des Meeres – wurde jeder Moment zu einer Ode an die Musse.

{{< figure src="./images/evening.jpg" alt="Abendstimmung" class="aligne-center size-medium" >}}

## Donnerstag: Ein Tag der ruhigen Übergänge
<!--
entspannen

Hostel wechseln

entspannen

"Familienabendessen" mit allen aus dem Hostel
-->
Der Morgen begann mit einer Fortsetzung des beruhigenden Inselrhythmus – einem Tag der Entspannung. Ob in der gemütlichen Umarmung der Hängematte oder beim Bummeln entlang der Inselpfade, jeder Moment trug das Wesen unaufgeregter Ruhe in sich.

Im Zeichen der Veränderung erlebte der Tag einen subtilen Wandel, als die Unterkunft gewechselt wurde. Der Wechsel des Hostels fügte dem Inselerlebnis einen Hauch Neuheit hinzu und öffnete die Tür zu neuen Umgebungen und frischen Perspektiven.

Als die Sonne den Himmel durchschritt, blühte der gemeinschaftliche Geist während eines köstlichen Abendessens auf. Zusammen mit anderen Reisenden aus dem Hostel wurde der Abend zu einem Mosaik aus Lachen, Geschichten und neuen Verbindungen – eine Feier der einzigartigen Bande, die das transient Leben auf einer Insel schmieden kann.

## Freitag: Shopping, Strandruhe und gemeinsames Cheers
<!--
shoppen gehen

zum Strand gehen

entspannen

"Familien" Abendessen mit allen aus dem Hostel
-->
Der Morgen begann mit dem Reiz des Einkaufsbummels, durch die charmanten Läden der Insel zu streifen.

Die Reise führte uns zu den Ufern – ein ruhiger Ort, wo der rhythmische Tanz der Wellen das gemächliche Tempo des Insellebens widerspiegelte.

Als der Tag sich dem Ende neigte, stand der gemeinschaftliche Geist erneut im Mittelpunkt eines geselligen Abendessens. Der Gemeinschaftstisch des Hostels verwandelte sich in einen Treffpunkt für Lachen, Geschichten und den Austausch unterschiedlicher Perspektiven.

## Samstag: Paddleboarding
<!--
Paddleboarding

BBQ am Strand

entspannen
-->
Das Abenteuer des Morgens entfaltete sich auf den ruhigen Gewässern rund um die Insel, als Paddleboarding zur Leinwand wurde, um die Küstenschönheit zu erkunden. Mit jedem Schlag schnitt das Paddle durch das kristallklare Wasser und bot Momente der glücklichen Einsamkeit und der Verbindung zum rhythmischen Puls des Ozeans.

{{< figure src="./images/paddleboarding.jpg" alt="Paddleboarding" class="aligne-right size-medium" >}}

Ein BBQ am Strand fügte dem Inselerlebnis eine geschmackvolle Note hinzu. Der Duft gegrillter Köstlichkeiten mischte sich mit der Meeresbrise und schuf eine sensorische Symphonie, die das entspannte Wesen des Insellebens widerspiegelte.

Mit der Sonne, die ihre goldenen Strahlen auf den Horizont warf, wurde der Abend zur Zeit purer Entspannung.

## Sonntag: Ein Tag der Entspannung und des Sports
<!--
entspannen
amerikanischen Fussball schauen
-->
Der Sonntag flüsterte Versprechen von Ruhe, und ich antwortete mit offenen Armen. Auf der Couch liegend liess ich die Welt an mir vorbeiziehen, zufrieden in der sanften Umarmung der Entspannung.

Am Nachmittag tauschte ich die Ruhe gegen den Nervenkitzel des amerikanischen Fussballs. Mit jedem Spielzug spürte ich den Puls der Aufregung – eine Erinnerung daran, dass selbst in Momenten der Stille immer Platz für ein wenig Spannung ist.

## Montag: Strandtag
<!--
entspannen

zum Strand gehen mit Nick und Finn
-->
{{< figure src="./images/beach.jpg" alt="Strand" class="aligne-left size-medium" >}}

Der Montag verflog gemütlich und lud mich ein, die Ruhe zu geniessen. Ohne Plan und Ziel, genoss ich die einfache Freude der Entspannung.

Später, begleitet von Nick, Lily und Finn, machten wir uns auf den Weg zum Strand. Zwischen Wellen und Lachen genossen wir unbeschwerte Momente.

## Dienstag: Unterwasserwelt
<!--
wieder zum Strand

ein wenig wandern

ein wenig schnorcheln, kleine Krabben und ein paar Fische sehen
-->
Mit der Sonne als meinem Kompass kehrte ich wieder zum Strand zurück. Doch heute hielt der Tag ein Abenteuer bereit. Jenseits des Ufers begab ich mich auf eine kurze Wanderung, die mich zu malerischen Ausblicken und versteckten Buchten führte.

{{< figure src="./images/beach_view.jpg" alt="Strandblick" class="aligne-right size-medium" >}}

Zurück an den Ufern tauschte ich meine Wanderschuhe gegen Schnorchelausrüstung und war gespannt, die Unterwasserwelt zu erkunden. Zwischen den felsigen Spalten erwartete mich eine Welt voller Wunder. Ich spähte in die Tiefe und erhaschte einen Blick auf kleine Krabben, die flink zwischen den Felsen huschten.