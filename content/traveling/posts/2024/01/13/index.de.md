+++
draft = false
date = 2024-01-13T22:00:00+02:00
title = "Road Trip (1/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Neuseeland"]
+++

## Samstag: Aufbruch zum Road Trip
<!--
Start in Auckland
Wohnmobil abholen
Links fahren #brainfuck
Fahrt nach Taupo zum Camping
Spaziergang machen
Abendessen kochen
Sonnenuntergang im Fluss schwimmen und viele Krabben sehen
-->
Mit der aufgehenden Sonne über Auckland bereiteten wir uns voller Vorfreude auf unseren lang ersehnten Road Trip vor. Nachdem wir das Wohnmobil abgeholt und uns an die Herausforderung des Fahrens auf der linken Strassenseite gewöhnt hatten (ein echtes Gehirntraining!), machten wir uns auf den Weg zu unserem ersten Ziel: Taupo.

Während die Kilometer hinter uns lagen, staunten wir über die atemberaubende Landschaft, die sich vor unseren Fenstern entfaltete, ein Flickenteppich aus sanften Hügeln und weiten, offenen Himmeln. Mit jedem Kilometer wuchs die Vorfreude, uns in das bevorstehende Abenteuer zu stürzen.

{{< figure src="./images/IMG_20240113_174900.jpg" alt="camping" class="aligne-left size-medium" >}}

In Taupo angekommen, machten wir uns auf den Weg zu unserem Campingplatz, um unser Zelt aufzubauen und nach der langen Fahrt die Beine zu vertreten. Bei einem Spaziergang entlang des Flusses genossen wir die Schönheit der Umgebung und atmeten die frische, klare Luft ein.

Nach einem dringend benötigten Abendessen beschloss ich, den Tag mit einem Sonnenuntergangsschwimmen im nahegelegenen Fluss abzurunden. Als ich in das kühle Wasser eintauchte, wurde ich von einer wunderbaren Überraschung begrüsst—kleine Krabben, die am Flussbett entlang huschten, ihre Bewegungen ein Tanz aus Licht und Schatten im schwindenen Tageslicht.

## Sonntag: Entdeckungen in der Natur
<!--
Zum Krater des Mondes (enttäuschend), aber schöner Spaziergang
Nach Hot Water Bay fahren
Südstaaten-Hähnchen auf Waffeln essen
Kurzes Schwimmen im Fluss
Abendessen
-->
{{< figure src="./images/IMG_20240114_113356.jpg" alt="crater of the moon" class="aligne-right size-medium" >}}

Unsere Reise am Sonntag begann mit einem Besuch des Kraters des Mondes—ein geologisches Wunder, das Aufregung versprach, uns jedoch etwas enttäuschte. Nichtsdestotrotz nutzten wir die Gelegenheit für einen Spaziergang und bewunderten die fremdartigen Landschaften und einzigartigen Felsformationen, die uns umgaben.

Um die noch verbliebene Enttäuschung abzuschütteln, fuhren wir weiter nach Hot Water Beach, wo natürliche heisse Quellen auf unsere erschöpften Körper warteten. Als wir uns in das warme, erfrischende Wasser begaben, spürten wir, wie unsere Sorgen dahin schmolzen, ersetzt durch ein Gefühl der Ruhe und Entspannung.

{{< figure src="./images/IMG_20240114_160122.jpg" alt="southern style chicken on waffels" class="aligne-left size-medium" >}}

Mit dem Appetit geweckt, gönnte ich mir ein köstliches Gericht—Südstaaten-Hähnchen auf Waffeln—ein kulinarisches Highlight, das sowohl meinen Hunger als auch meine Seele sättigte. Erfrischt und bereit für das nächste Abenteuer, wagten wir einen schnellen Sprung in den nahegelegenen Fluss und genossen das kühle Wasser, bevor wir zum Campingplatz zurückkehrten.

## Montag: Ein Tag der Entspannung und Erneuerung
<!--
Fauler Morgen, Frühstück
Spa-Tag in den Wairakei Terraces & Thermal Health Spa
Das heisse Wasser geniessen und einfach entspannen
Fahrt zum Campingplatz in Turangi
-->
Der Montag begrüsste uns mit dem Versprechen von Freizeit und Ruhe. Nach einem faulen Morgen, den wir mit Frühstück auf unserem Campingplatz genossen, machten wir uns auf zu einem Tag der Verwöhnung und Entspannung in den Wairakei Terraces & Thermal Health Spa.

{{< figure src="./images/wairakei-terraces-spa.jpg" alt="Wairakei Terraces & Thermal Health Spa" class="aligne-right size-medium" >}}

Als wir das Spa betraten, wurden wir von der beruhigenden Umarmung des heissen Wassers und den regenerierenden Thermalbecken empfangen. Mit jedem Eintauchen fühlten wir, wie sich die Spannung aus unseren Muskeln löste, ersetzt durch ein Gefühl tiefen Friedens und Erneuerung.

Verschwunden im Rhythmus der Entspannung, liessen wir uns von den heilenden Eigenschaften des Thermalwassers tragen, die uns in eine Hülle der Ruhe hüllten. Die Zeit schien stillzustehen, während wir das einfache Vergnügen genossen, einfach im Moment zu sein.

Als der Tag sich dem Ende neigte und unsere Stimmung aufblühte, verabschiedeten wir uns schweren Herzens vom Spa, erfrischt und erneuert von der Zeit in den heilenden Gewässern. Mit neuer Energie in unseren Adern fuhren wir weiter zum nächsten Ziel, dem Campingplatz in Turangi.

{{< figure src="./images/IMG_20240115_205402.jpg" alt="camping" class="aligne-center size-medium" >}}

Angekommen an unserem Ziel, bauten wir unser Zelt unter dem Sternenhimmel auf und waren dankbar für den Tag der Entspannung, der sowohl Körper als auch Seele erneuert hatte. Mit dem sanften Summen der Natur als unser Wiegenlied, schliefen wir ein, gespannt auf die Abenteuer, die uns am nächsten Tag erwarten würden.

## Dienstag: Prüfungen und Übergänge
<!--
Tagestour machen
( müssen dorthin, noch kein Essen, Chaos, Aufgabe aufgegeben, kein Wandern)
Frustriert im Van liegen
Fahrt zu einem anderen Campingplatz in Mangaweka
-->
Unsere Pläne für eine Tagestour stiessen auf unerwartetes Chaos, als wir vor der Herausforderung standen, den Trailhead ohne Frühstück, Mittagessen oder jegliches Essen für den Tag zu erreichen. Frustriert und entmutigt entschied ich mich, die Wandertour aufzugeben. Niedergeschlagen zogen wir uns in unseren Van zurück, wo wir schweigend lagen und mit Enttäuschung und Frustration kämpften.

{{< figure src="./images/IMG_8208.JPG" alt="camping" class="aligne-right size-small" >}}

Wir entschieden uns, die Richtung zu ändern und fuhren zu einem anderen Campingplatz in Mangaweka. Während wir fuhren, bot uns die Landschaft vor unseren Fenstern eine beruhigende Linderung für unsere geplagten Gemüter und erinnerte uns an die Schönheit, die uns immer noch umgab.

Am neuen Ziel angekommen, bauten wir unser Zelt mit ruhiger Entschlossenheit auf, fest entschlossen, das Beste aus dem Tag zu machen, trotz seines holprigen Starts. Mit jedem Moment fanden wir Trost in der einfachen Tatsache, zusammen zu sein und dankbar für die Möglichkeit, trotz der Widrigkeiten weiterzumachen.

{{< figure src="./images/IMG_8222.JPG" alt="camping" class="aligne-center size-medium" >}}

## Mittwoch: Umarmung der Natur
<!--
Wach auf neben dem Fluss
Frühstück
Fahrt nach Paekakariki
Unterwegs in einem Café halten
Und im Nga Manu Nature Reserve
Die Aussicht während der Fahrt geniessen
Camping, kochen, essen, schlafen
-->
Als wir neben dem sanften Rauschen des Flusses aufwachten, fanden wir uns in der Umarmung der Natur wieder, begrüsst von der Ruhe des Morgens. Nach einem nahrhaften Frühstück machten wir uns auf den nächsten Abschnitt unserer Reise, mit dem Ziel Paekakariki.

Unterwegs machten wir einen Stopp in einem gemütlichen Café, genossen das reiche Aroma von frisch gebrühtem Kaffee und gönnten uns einen Moment des koffeinhaltigen Glücks. Erfrischt und bereit für die Erkundung, setzten wir unsere Reise zum Nga Manu Nature Reserve fort, wo wir von einer Vielzahl faszinierender Tiere empfangen wurden.

{{< figure src="./images/IMG_20240117_161712.jpg" alt="camping" class="aligne-right size-small" >}}

Als wir durch das Reservat wanderten, wurden unsere Sinne mit einer Symphonie aus Anblicken und Geräuschen verwöhnt. Innerhalb der Gehege des Reservats hatten wir das Privileg, eine Vielzahl von faszinierenden Tieren aus nächster Nähe zu beobachten. Unter ihnen hatten wir das Glück, einen Kiwi zu erblicken, der selten und scheu in seinem sorgfältig gestalteten Lebensraum nach Nahrung suchte. In der Nähe baskten Tuataras, alte und mysteriöse Wesen, im Sonnenlicht, während Enten spielerisch quakten und Aale sich graziös durch das klare Wasser ihres Geheges glitten.

{{< figure src="./images/IMG_20240117_145551.jpg" alt="camping" class="aligne-left size-medium" >}}

Als wir weiter in das Reservat vordrangen, freuten wir uns über den Anblick eines Keas, der neugierig und verschmitzt auf seiner Stange sass und seine Umgebung mit scharfem Blick musterte. Jede Begegnung gab uns einen Einblick in das reiche Mosaik der vielfältigen Tierwelt Neuseelands, das uns in Ehrfurcht vor der Schönheit und Wunder der natürlichen Welt zurückliess.

Als wir unsere Fahrt fortsetzten, wurden wir mit atemberaubenden Ausblicken verwöhnt, die so weit reichten, wie das Auge sehen konnte. Die offene Strasse lockte uns, Abenteuer und Entdeckungen um jede Ecke zu versprechen.

{{< figure src="./images/IMG_20240117_191607.jpg" alt="camping" class="aligne-right size-small" >}}

Am Campingplatz angekommen, nahmen wir uns keine Zeit und bauten schnell unser Zelt auf, um uns nach einem Tag voller Entdeckungen zu entspannen. Der Duft des Abendessens lag in der Luft.

## Donnerstag: Küstenwanderlust
<!--
Fahrt nach Wellington
Mit der Fähre nach Picton
Fahrt zum Aussie Bay Campingplatz und das Meer und den Strand geniessen
-->
{{< figure src="./images/IMG_20240118_121547.jpg" alt="camping" class="aligne-left size-medium" >}}

Früh aufgebrochen machten wir uns auf den Weg nach Wellington. Bei der Ankunft bestiegen wir sofort die Fähre nach Picton, bereit, die majestätischen Gewässer der Cook Strait zu durchqueren.

Während die Fähre durch die Wellen schnitt, bestaunten wir die atemberaubende Aussicht, die sich vor uns entfaltete. Die zerklüftete Küste gab den Blick auf sanfte Hügel und azurblaue Gewässer frei. Mit jedem Moment wuchs die Vorfreude auf die Abenteuer, die uns auf der anderen Seite erwarteten.

{{< figure src="./images/IMG_20240118_201333.jpg" alt="camping" class="aligne-right size-medium" >}}

In Picton angekommen, setzten wir die Reise fort, unser Ziel war der Aussie Bay Campingplatz—ein verstecktes Juwel an der Küste. Mit dem Meereswind in unseren Haaren und dem salzigen Duft des Ozeans in der Luft genossen wir das einfache Vergnügen des Strandparadieses.

An unserem Campingplatz angekommen, verschwendeten wir keine Zeit und tauchten sofort in die Ruhe unserer Umgebung ein. Mit den Füssen im Sand und den Wellen, die sanft ans Ufer schlugen, gaben wir uns dem Rhythmus des Meeres hin und liessen alle Sorgen mit der Flut davontreiben.

{{< figure src="./images/IMG_8292.JPG" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_8316.JPG" alt="camping" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>