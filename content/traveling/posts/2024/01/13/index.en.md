+++ 
draft = false
date = 2024-01-13T22:00:00+02:00
title = "Road Trip (1/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

## Saturday: Embarking on the Road Trip
<!--
Start in Auckland
Get camper 
Drive on the left side #brainfuck
Drive to camping in taupo
Go for a walk
Cook Dinner
Sunset swim in river and see lots of crabs
-->
With the sun rising over Auckland, we eagerly prepared to embark on our much-anticipated road trip. After collecting the van and adjusting to the challenge of driving on the left side of the road (a true brain workout!), we set our sights on our first destination: Taupo.

As the miles stretched out before us, we marveled at the stunning scenery unfolding outside our windows, a patchwork of rolling hills and vast open skies. With each passing kilometer, anticipation bubbled within us, eager to immerse ourselves in the adventure ahead.

{{< figure src="./images/IMG_20240113_174900.jpg" alt="camping" class="aligne-left size-medium" >}}

Arriving in Taupo, we made our way to our campsite, eager to set up camp and stretch our legs after the long drive. Setting out for a walk, we meandered along the river, taking in the beauty of the surrounding landscape and breathing in the crisp, fresh air.

After a much needed dinner, I decided to cap off the day with a sunset swim in the nearby river. Entering the cool embrace of the water, I was greeted by a delightful surprise—clusters of tiny crabs scuttling along the riverbed, their movements a dance of light and shadow in the fading daylight. 

## Sunday: Exploring Nature's Wonders
<!-- 
Go to crater of the moon (disappointing) but nice walk
Go tho hot water bay
Eat southern style chicken on waffels
Quick swim in river
Dinner 
-->
{{< figure src="./images/IMG_20240114_113356.jpg" alt="crater of the moon" class="aligne-right size-medium" >}}

Our Sunday journey began with a visit to the Craters of the Moon—a geological wonder that promised excitement but left us somewhat disappointed. Nevertheless, we embraced the opportunity to stretch our legs on the scenic walk, marveling at the otherworldly landscapes and unique rock formations that surrounded us.

Eager to wash away any lingering disappointment, we made our way to Hot Water Beach, where natural hot springs awaited our weary bodies. Immersing ourselves in the warm, rejuvenating waters, we felt our cares melt away, replaced by a sense of serenity and relaxation.

{{< figure src="./images/IMG_20240114_160122.jpg" alt="southern style chicken on waffels" class="aligne-left size-medium" >}}

With appetites whetted, I indulged in a delicious meal of Southern-style chicken on waffles—a culinary delight that satisfied both my cravings and my souls. Energized and ready for adventure, we took a quick dip in the nearby river, reveling in the cool embrace of its waters before heading back to camp.

## Monday: A Day of Relaxation and Renewal
<!--
Lazy morning, breakfast
Spa day at Wairakei Terraces & Thermal Health Spa
Enjoy the hot water and just relax
Drive to a camping in turangi
-->

Monday greeted us with the promise of leisure and tranquility. After a lazy morning spent savoring breakfast at our campsite, we set our sights on a day of indulgence and relaxation at Wairakei Terraces & Thermal Health Spa.

{{< figure src="./images/wairakei-terraces-spa.jpg" alt="Wairakei Terraces & Thermal Health Spa" class="aligne-right size-medium" >}}

Entering the spa's serene oasis, we were greeted by the soothing embrace of hot water and rejuvenating thermal pools. With each dip, we felt tension melt away from our muscles, replaced by a sense of deep calm and renewal.

Lost in the rhythm of relaxation, we surrendered to the healing properties of the thermal waters, allowing their warmth to envelop us in a cocoon of serenity. Time seemed to stand still as we reveled in the simple pleasure of being present in the moment.

As the day waned and our spirits soared, we reluctantly bid farewell to the spa, feeling refreshed and revitalized from our time amidst the healing waters. With renewed energy coursing through our veins, we embarked on the next leg of our journey, driving to a camping site in Turangi.

{{< figure src="./images/IMG_20240115_205402.jpg" alt="camping" class="aligne-center size-medium" >}}

Arriving at our destination, we set up camp under the starry sky, grateful for the day of indulgence and relaxation that had rejuvenated both body and soul. With the gentle hum of nature as our lullaby, we drifted off to sleep, eager for the adventures that awaited us on the morrow.

## Tuesday: Trials and Transitions
<!--
Go for a day trek
( need to get there, no food yet, chaos, give up, don't go trekking)
Lay in van frustrated
Drive to another camping in mangaweka
-->
Our plans for a day trek were met with unexpected chaos as we faced challenges getting to the trailhead without having breakfast, lunch or any other food for the day. Frustrated and feeling defeated, I made the difficult decision to abandon the trekking idea. Disheartened, we retreated to the comfort of our van, where we lay in silence, grappling with disappointment and frustration.

{{< figure src="./images/IMG_8208.JPG" alt="camping" class="aligne-right size-small" >}}

We decided to turn our wheels towards another camping spot in Mangaweka. As we drove, the scenery outside our windows provided a soothing balm for our troubled spirits, reminding us of the beauty that still surrounded us.

Arriving at our new destination, we set up camp with a sense of quiet resolve, determined to make the most of the day despite its rocky start. With each passing moment, we found solace in the simple act of being together, grateful for the opportunity to persevere in the face of adversity.

{{< figure src="./images/IMG_8222.JPG" alt="camping" class="aligne-center size-medium" >}}

## Wednesday: Nature's Embrace
<!--
Wake up next to the river
Breakfast
Drive to paekakariki
On the way stop in a coffee
And the Nga Manu Nature Reserve
Enjoy the view while driving
On camping cook, eat, sleep
-->
Waking up to the gentle murmur of the river, we found ourselves nestled in nature's embrace, greeted by the tranquility of the morning. After a nourishing breakfast, we embarked on the next leg of our journey, setting our sights on Paekakariki.

En route, we made a pit stop at a cozy coffee shop, savoring the rich aroma of freshly brewed java and indulging in a moment of caffeinated bliss. Energized and ready to explore, we continued our journey to Nga Manu Nature Reserve, where we were greeted by a myriad of fascinating creatures.

{{< figure src="./images/IMG_20240117_161712.jpg" alt="camping" class="aligne-right size-small" >}}

As we wandered through the reserve, our senses were treated to a symphony of sights and sounds. Within the confines of the reserve's enclosures, we had the privilege of observing a variety of captivating animals up close. Among them, we were fortunate to catch a glimpse of a kiwi, elusive and rare, as it foraged for food in its carefully crafted habitat. Nearby, tuataras, ancient and enigmatic, basked in the sunlight, while ducks quacked playfully and eels glided gracefully through the clear waters of their enclosure.

{{< figure src="./images/IMG_20240117_145551.jpg" alt="camping" class="aligne-left size-medium" >}}

Venturing further into the reserve, we were delighted by the sight of a kea, mischievous and curious, as it perched atop its perch, surveying its surroundings with keen interest. Each encounter offered us a glimpse into the rich tapestry of New Zealand's diverse wildlife, leaving us in awe of the beauty and wonder of the natural world.

As we resumed our drive, we were treated to breathtaking views that stretched as far as the eye could see. The open road beckoned, promising adventure and discovery around every bend.

{{< figure src="./images/IMG_20240117_191607.jpg" alt="camping" class="aligne-right size-small" >}}

Arriving at our camping spot, we wasted no time in setting up camp, eager to unwind after a day of exploration. With the aroma of dinner wafting through the air.

## Thursday: Coastal Wanderlust
<!--
Drive to wellington
Take the ferry to picton
Drive to the aussie bey campsite and enjoy the sea and beach
-->
{{< figure src="./images/IMG_20240118_121547.jpg" alt="camping" class="aligne-left size-medium" >}}

Setting out early, we embarked on the drive to Wellington. Upon arrival, we wasted no time in boarding the ferry bound for Picton, ready to traverse the majestic waters of the Cook Strait.

As the ferry sliced through the waves, we marveled at the breathtaking views that unfolded before us, the rugged coastline giving way to rolling hills and azure waters. With each passing moment, anticipation mounted for the adventures that awaited us on the other side.

{{< figure src="./images/IMG_20240118_201333.jpg" alt="camping" class="aligne-right size-medium" >}}

Arriving in Picton, we hit the road once more, our destination set for the Aussie Bay campsite—a hidden gem nestled along the coastline. With the sea breeze in our hair and the salty tang of the ocean in the air, we reveled in the simple pleasure of beachside bliss.

Upon reaching our campsite, we wasted no time in immersing ourselves in the serenity of our surroundings. With toes sinking into the sand and waves lapping gently at the shore, we surrendered to the rhythm of the sea, letting all worries drift away with the tide.

{{< figure src="./images/IMG_8292.JPG" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_8316.JPG" alt="camping" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>
