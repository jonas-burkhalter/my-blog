+++ 
draft = false
date = 2024-01-19T22:00:00+02:00
title = "Road Trip (2/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

## Friday: Coastal Retreat and Culinary Delights
<!--
Morning swim in sea
Drive to campsite near nelson
Swim again
Pizza
-->
The day began with a refreshing morning swim in the sea, the cool waters invigorating our senses and setting the tone for the adventures that awaited. With saltwater still clinging to our skin, we bid farewell to the Aussie Bay campsite and set our sights on our next destination: a campsite near Nelson.

As we traversed the winding coastal roads, the beauty of the landscape unfolding before us served as a constant reminder of nature's awe-inspiring wonders. Arriving at our new campsite, we wasted no time in immersing ourselves in the tranquil surroundings.

Before long, the lure of the sea beckoned once more, and we found ourselves diving into the inviting waters for another refreshing swim. With each stroke, we felt a sense of freedom and joy, reveling in the simple pleasure of being one with the ocean.

<div style="clear:both;"></div>
{{< figure src="./images/IMG_8333.JPG" alt="beach" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_8335.JPG" alt="beach" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

As evening descended, we traded our swimsuits for a different kind of indulgence—pizza. We savored every bite of piping hot slices, the tantalizing flavors a delicious reward for a day well spent.

## Saturday: Coastal Adventures in Abel Tasman National Park
<!--
Kayak and hiking (rr kayak) tasman abel nationalpark
start with seeing a Stingray
paddle along the coast
relax on the beach
see a stingray again
paddle to the island to see some Fur seal
lunch breack at the beach
start hike back
see some Tasmanienwachtel
hike along the coast
go for a quick swim before getting back to the van
-->

{{< figure src="./images/IMG_8371.JPG" alt="camping" class="aligne-left size-small" >}}

Embarking on a day of coastal exploration, we ventured into the pristine wilderness of Abel Tasman National Park. Setting out on a kayak tour, my journey began with a breathtaking encounter—a majestic stingray gracefully gliding beneath the surface, a reminder of the wonders that awaited us.

{{< figure src="./images/IMG_20240120_105919.jpg" alt="beach" class="aligne-right size-medium" >}}

Paddling along the rugged coastline, we were treated to panoramic views of sheer cliffs and golden beaches, the rhythmic sound of waves accompanying our every stroke. As we reached the shore, we paused to bask in the tranquility of the pristine sands, soaking in the serenity of the moment.

{{< figure src="./images/IMG_8358.JPG" alt="camping" class="aligne-left size-small" >}}

Continuing our coastal odyssey, we were delighted to encounter another stingray, its sleek form gliding effortlessly through the crystal-clear waters. Further along, we paddled to an island sanctuary, where furry seals basked lazily in the sun, unperturbed by our presence.

{{< figure src="./images/IMG_8387.JPG" alt="camping" class="aligne-right size-small" >}}

Pausing for a well-deserved lunch break on the beach, we replenished our energy amidst the beauty of nature's bounty. Refueled and rejuvenated, we embarked on a hike along the coastal trail, encountering Tasmanian quail and soaking in the breathtaking vistas that stretched out before us.

As the day drew to a close, we indulged in a quick swim, the cool waters washing away the weariness of the day's adventures. With hearts full and spirits soaring, we returned to our van, grateful for the memories made and the wonders experienced in Abel Tasman National Park.

## Sunday: Scenic Delights and Coastal Wonders
<!--
Upper Buller Gorge Viewpoint

Kilkenny lookout (old man showing pictures of the river while flooded)

Seal Colony tauranga bay

Drive to a campsite near Punakaiki
-->
{{< figure src="./images/IMG_8405.JPG" alt="road" class="aligne-right size-small" >}}

Our Sunday journey unfolded with a scenic drive to the Upper Buller Gorge Viewpoint, where we were greeted by sweeping vistas of rugged terrain and meandering rivers below. Captivated by the raw beauty of the landscape, we lingered awhile, allowing the majesty of nature to envelop us.

Continuing our adventure, we made our way to Kilkenny lookout, where we were met by a wise old man eager to share tales of the river during its turbulent floods. His stories painted vivid pictures of nature's raw power, leaving us in awe of the forces that shape our world.

{{< figure src="./images/IMG_20240121_164142.jpg" alt="seals" class="aligne-left size-small" >}}

Eager to witness more of nature's wonders, we journeyed to Tauranga Bay, home to a thriving seal colony. With anticipation building, we approached the rocky shores, where we were greeted by the playful antics of seals basking in the sun. 

<div style="clear:both;"></div>
{{< figure src="./images/IMG_8415.JPG" alt="beach" class="aligne-center size-medium" >}}

As the day drew to a close, we bid farewell to the seals and set our sights on a campsite near Punakaiki. Nestled amidst the tranquil beauty of the coastal landscape, we settled in for the night.

## Monday: Nature's Marvels and Rustic Charm
<!--
Pancake Rocks and Blowholes Track
Then eat pancakes in the coffee next to it

Nice walk in Hokitika Gorge 

Drive to ross to camp behind a old school pub.
-->
{{< figure src="./images/IMG_8450.JPG" alt="pancake rocks" class="aligne-right size-small" >}}

Today began with a visit to the Pancake Rocks and Blowholes Track, where we marveled at the geological wonders sculpted by the relentless forces of nature. The rugged coastline and dramatic rock formations provided a stunning backdrop as we wandered along the trails.

Eager to satisfy our appetites, we indulged in a delicious stack of pancakes at the nearby café, savoring each bite amidst the coastal breeze. With satisfied stomachs and spirits lifted, we continued our journey, setting our sights on the tranquil beauty of Hokitika Gorge.

Navigating winding paths through lush forests, we found ourselves at the edge of the gorge, where the vibrant turquoise waters awaited. Taking in the breathtaking scenery, we embarked on a leisurely walk along the riverbank. 

<div style="clear:both;"></div>
{{< figure src="./images/IMG_8461.JPG" alt="winding paths through lush forests" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_8468.JPG" alt="riverbank" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

As the day waned, we made our way to Ross, where we found a charming camping spot behind an old-school pub. Surrounded by rustic charm and the warm glow of the setting sun, we settled in for the night.

## Tuesday: A Night with the Kiwis
<!--
Drive to franz josef (new fridge) and then to okarito

8pm start the kiwi tour
Meet our 'team' for the evening lan the guide with us 6
Learn the calls of female and male okarito kiwi
Drive to the first kiwi revier/territory (about 2*2 km)
Listen for anything but a lot of nothing... (Ian with radio sender/receiver)
And then a few meter in front of us in the bush the calls of ???   And ??? 
But they walked away of us
Go to the next territory
And there he is on the way a kiwi!!! 
Then a lot of kiwi calls. We try to follow the male in the direction of the female call
And then more calls from another territory. Amazing to hear this calls
After this successful night drive back
-->

Our Tuesday began with a journey to Franz Josef, where we made a pit stop to acquire a new fridge before continuing on to Okarito, a hidden gem nestled amidst the West Coast wilderness.

{{< figure src="./images/IMG_20240123_181159.jpg" alt="camping" class="aligne-left size-large" >}}
{{< figure src="./images/IMG_8512.JPG" alt="camping" class="aligne-center size-medium" >}}

As evening descended, excitement mounted as we prepared for the highlight of our day—the kiwi tour. Joined by our guide Ian and fellow adventurers, we set out into the darkness, eager to catch a glimpse of these elusive creatures.

Under Ian's expert guidance, we learned the distinctive calls of both male and female Okarito kiwis. With Ian equipped with radio sender/receiver, we ventured into the wilderness, our senses heightened in anticipation of the encounter to come.

Amidst the silence of the bush, we listened intently, hoping for the telltale rustle of feathers or the soft padding of tiny feet. Suddenly, our patience was rewarded as the calls of kiwis broke the stillness, filling us with awe and wonder.

With bated breath, we followed the sounds, our hearts racing with excitement. Then, a magical moment—a kiwi appeared before us, its curious gaze meeting ours before disappearing into the shadows.

Buoyed by our success, we continued our nocturnal adventure, tracing the calls of kiwis as they echoed through the darkness. Each encounter was a testament to the resilience of these remarkable birds and the beauty of the natural world.

As the night drew to a close, we returned to our campsite, hearts full and spirits soaring from the unforgettable experience of sharing the wilderness with Okarito kiwis. With memories to cherish and stories to share, we drifted off to sleep, grateful for the magic of this special night.

## Wednesday: Coastal Explorations and Wilderness Wonders
<!--
Short swim in the cold sea
Look out
Marine reserve (sadly no clear dolphin seeing)
More lookouts
Camping near Makarora
-->
My Wednesday began with a bracing dip in the chilly embrace of the sea. After a hot shower and a breakfast, we set out to further explore the coastal wonders that awaited us.

{{< figure src="./images/IMG_8547.JPG" alt="camping" class="aligne-left size-medium" >}}

At lookout points along the rugged coastline, we paused to drink in the panoramic views, the vast expanse of the ocean stretching out before us in all its majestic glory. Though the clear skies offered glimpses of promise, our hopes of spotting dolphins in the marine reserve were dashed by the elusive creatures' absence.

{{< figure src="./images/IMG_8542.JPG" alt="lookout" class="aligne-right size-smaller" >}}

Undeterred, we continued our exploration, seeking solace in the beauty of nature's bounty. More lookout points revealed breathtaking vistas of rolling waves and dramatic cliffs.

As the day drew to a close, we made our way to our campsite near Makarora, nestled amidst the pristine wilderness of the South Island. Surrounded by towering mountains and lush forests, we settled in for the night, grateful for the beauty of the natural world.

{{< figure src="./images/IMG_20240124_185917.jpg" alt="camping" class="aligne-center size-medium" >}}

## Thursday: A Day of Discovery and Delight
<!--
Blue pools
Bridge closed, need to cross the cold river to get there

Drive to Wanaka

Lavendel farm

Puzzle world (fascinating illusions, multi level maze)

Camping in wanaka
-->

<div style="clear:both;"></div>
{{< figure src="./images/IMG_8575.JPG" alt="blue pools river crossing" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_20240125_105617.jpg" alt="blue pools" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

Our Thursday began with a journey to the enchanting Blue Pools, a natural wonder hidden deep within the wilderness. However, our path was obstructed by a closed bridge, forcing us to forge across the icy waters of the river to reach our destination. Despite the chill, the allure of the Blue Pools beckoned us forward.

Arriving at our destination, we were greeted by the mesmerizing hues of the crystal-clear water, its azure depths a testament to nature's artistry. 

Leaving the Blue Pools behind, we continued our journey to Wanaka, where a world of delights awaited us. Our first stop was a lavender farm, where the air was filled with the sweet fragrance of blooming flowers. Lost in a sea of purple blooms, we reveled in the tranquility of the countryside.

<div style="clear:both;"></div>
{{< figure src="./images/IMG_20240125_130649.jpg" alt="lavender farm" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240125_130236.jpg" alt="lavender farm" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Next, we ventured into the fascinating world of Puzzle World, where illusions danced before our eyes and a multi-level maze challenged our wits. Lost in a labyrinth of mind-bending puzzles and optical illusions, we laughed and marveled at the wonders of perception.

<div style="clear:both;"></div>
{{< figure src="./images/IMG_8614.JPG" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_8616.JPG" alt="camping" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

As the day drew to a close, we made our way to our campsite in Wanaka, and settled in for the night.

## Friday: From Breakfast to Arrowtown
<!--
Breakfast (steak sandwich)

Road to queenstown

Stroll around

Camping arrowtown
-->

{{< figure src="./images/IMG_20240126_141040.jpg" alt="camping" class="aligne-left size-medium" >}}

Our Friday kicked off with a hearty breakfast featuring indulgent steak sandwiches, setting the tone for a day of exploration and delight. With satisfied appetites, we hit the road bound for Queenstown, eager to immerse ourselves in the vibrant energy of this iconic destination.

Upon arrival, we embarked on a leisurely stroll through the bustling streets, soaking in the lively atmosphere and admiring the breathtaking scenery that surrounded us. From the shores of Lake Wakatipu to the rugged peaks of the Remarkables, Queenstown captivated us at every turn.

As the day unfolded, we found ourselves drawn to a campsite neark the charming allure of Arrowtown, a historic gold mining town nestled amidst the rolling hills. 
