+++  
draft = false  
date = 2024-01-19T22:00:00+02:00  
title = "Road Trip (2/4)"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Neuseeland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Neuseeland"]  
+++  

## Freitag: Küstliche Ruhe und kulinarische Genüsse

Der Tag begann mit einem erfrischenden Morgenschwimmen im Meer, das kühle Wasser belebte unsere Sinne und setzte den Ton für die Abenteuer, die uns erwarteten. Mit Salzwasser, das noch an unserer Haut klebte, verabschiedeten wir uns vom Aussie Bay Campingplatz und machten uns auf den Weg zu unserem nächsten Ziel: einem Campingplatz in der Nähe von Nelson.

Während wir die kurvigen Küstenstrassen entlangfuhren, erinnerte uns die Schönheit der Landschaft ständig an die überwältigenden Wunder der Natur. Als wir auf unserem neuen Campingplatz ankamen, tauchten wir sofort in die ruhige Umgebung ein.

Nicht lange danach zog uns das Meer wieder in seinen Bann und wir fanden uns erneut in den einladenden Gewässern für ein weiteres erfrischendes Schwimmen wieder. Mit jedem Schwimmzug fühlten wir uns frei und froh, einfach in den Ozean einzutauchen.

Als der Abend dämmerte, tauschten wir unsere Badebekleidung gegen eine andere Art der Freude – Pizza. Wir genossen jeden Bissen heisser Stücke, der verlockende Geschmack war eine köstliche Belohnung für einen gut verbrachten Tag.

## Samstag: Küstliche Abenteuer im Abel Tasman Nationalpark

Wir machten uns auf den Weg in den unberührten Abel Tasman Nationalpark. Mit dem Kajak begannen wir eine Tour, die mit einer atemberaubenden Begegnung begann – einem majestätischen Rochen, der elegant unter der Oberfläche gleitete und uns an die Wunder erinnerte, die uns noch bevorstanden.

Während wir entlang der rauen Küste paddelten, wurden wir mit Panoramablicken auf steile Klippen und goldene Strände verwöhnt. Das rhythmische Geräusch der Wellen begleitete jeden unserer Schwimmzüge. Als wir das Ufer erreichten, legten wir eine Pause ein, um die Ruhe des unberührten Strandes zu geniessen.

Unsere Küstentour setzte sich fort und wir begegneten einem weiteren Rochen, der mühelos durch das kristallklare Wasser glitt. Weiter draussen paddelten wir zu einer Insel, auf der Seelöwen faul in der Sonne lagen und sich von unserer Anwesenheit nicht stören liessen.

Nach einer wohlverdienten Mittagspause am Strand setzten wir unsere Wanderung entlang des Küstenpfades fort, begegneten Tasmanischen Wachteln und genossen die atemberaubenden Ausblicke, die sich uns boten.

Am Ende des Tages gönnten wir uns ein schnelles Schwimmen, das kühle Wasser wusch die Erschöpfung der Erlebnisse des Tages hinweg. Mit vollen Herzen und hoch erhobenen Gemütern kehrten wir in unseren Van zurück und waren dankbar für die Erinnerungen und Wunder, die wir im Abel Tasman Nationalpark erlebt hatten.

## Sonntag: Malerische Höhepunkte und Küstliche Wunder

Unsere Sonntagsreise begann mit einer landschaftlich reizvollen Fahrt zum Upper Buller Gorge Viewpoint, wo wir mit weiten Ausblicken auf das raue Gelände und die sich schlängelnden Flüsse begrüsst wurden. Fasziniert von der rohen Schönheit der Landschaft verweilten wir eine Weile, um die Majestät der Natur zu erleben.

Weiter ging es zum Kilkenny Lookout, wo uns ein weiser älterer Mann begrüsste und bereitwillig Geschichten vom Fluss während seiner turbulenten Überschwemmungen erzählte. Seine Erzählungen malten lebendige Bilder von der rohen Kraft der Natur, die uns in Staunen versetzten.

Erfüllt von Naturwundern reisten wir weiter zur Tauranga Bay, wo wir eine florierende Seelöwenkolonie entdeckten. Mit wachsender Vorfreude näherten wir uns den felsigen Ufern, wo uns die verspielten Seelöwen begrüssten, die in der Sonne lagen.

Als der Tag zu Ende ging, verabschiedeten wir uns von den Seelöwen und machten uns auf den Weg zu einem Campingplatz in der Nähe von Punakaiki. Eingebettet in die ruhige Schönheit der Küstenlandschaft, richteten wir uns für die Nacht ein.

## Montag: Wunder der Natur und rustikaler Charme

Der Tag begann mit einem Besuch der Pancake Rocks und Blowholes Track, wo wir die geologischen Wunder bewunderten, die von den unaufhörlichen Kräften der Natur geformt wurden. Die raue Küste und dramatischen Felsformationen boten einen atemberaubenden Hintergrund, während wir die Wanderwege entlang schlenderten.

Hungrig von der Wanderung gönnten wir uns einen leckeren Stapel Pfannkuchen im Café in der Nähe, der Duft des Küstenwinds und der Bissen liessen unsere Herzen höher schlagen. Mit vollendetem Bauch machten wir uns auf den Weg in die friedliche Schönheit der Hokitika Gorge.

Durch windinge Pfade im üppigen Wald fanden wir uns am Rand der Schlucht wieder, wo das lebendige türkisfarbene Wasser auf uns wartete. Die atemberaubende Szenerie geniessend, machten wir einen gemächlichen Spaziergang entlang des Flusses.

Am Ende des Tages machten wir uns auf den Weg nach Ross, wo wir einen charmanten Campingplatz hinter einer alten Kneipe fanden. Umgeben von rustikalem Charme und dem warmen Schein der untergehenden Sonne richteten wir uns für die Nacht ein.

## Dienstag: Eine Nacht mit den Kiwis

Unser Dienstag begann mit einer Reise nach Franz Josef, wo wir einen Zwischenstopp machten, um einen neuen Kühlschrank zu besorgen, bevor wir nach Okarito weiterfuhren, einem versteckten Juwel mitten im Westküsten-Wildnis.

Als der Abend einbrach, stieg die Aufregung, als wir uns auf das Highlight des Tages vorbereiteten – die Kiwi-Tour. Zusammen mit unserem Führer Ian und anderen Abenteurern machten wir uns auf in die Dunkelheit, gespannt darauf, einen Blick auf diese scheuen Kreaturen zu erhaschen.

Unter Ians fachkundiger Leitung lernten wir die markanten Rufe von männlichen und weiblichen Okarito-Kiwis. Mit Ian, ausgestattet mit einem Radio-Sender-Empfänger, machten wir uns in die Wildnis auf, voller Vorfreude auf die bevorstehende Begegnung.

Plötzlich wurde unser Warten belohnt, als die Rufe der Kiwis die Stille durchbrachen. Wir folgten gespannt den Geräuschen und erlebten einen magischen Moment, als ein Kiwi vor uns erschien und neugierig zu uns hinüberschaute, bevor er in den Schatten verschwand.

Dank unserer erfolgreichen Entdeckung setzten wir unser nächtliches Abenteuer fort und folgten den Rufen der Kiwis durch die Dunkelheit. Jeder Aufeinandertreffen war ein Beweis für die Widerstandskraft dieser bemerkenswerten Vögel und die Schönheit der natürlichen Welt.

Am Ende der Nacht kehrten wir zu unserem Campingplatz zurück, mit Herzen voller Freude und dem Gefühl, die Wildnis mit den Okarito-Kiwis geteilt zu haben. Dankbar für die Magie dieser besonderen Nacht, schliefen wir zufrieden ein.

## Mittwoch: Küstliche Erkundungen und Wunder der Wildnis

Mein Mittwoch begann mit einem erfrischenden Sprung in das kalte Meer. Nach einer heissen Dusche und einem Frühstück setzten wir unsere Entdeckung der Küstenwunder fort.

An Aussichtsplattformen entlang der rauen Küste machten wir Halt, um die Panoramablicke zu geniessen. Auch wenn der klare Himmel uns hoffnungsvoll auf Delfine in der Meeresreserve hoffen liess, wurden wir enttäuscht, da die scheuen Tiere nicht zu finden waren.

Nicht entmutigt, setzten wir unsere Erkundung fort und fanden Trost in der Schönheit der Natur. Weitere Aussichtsplattformen boten atemberaubende Ausblicke auf rollende Wellen und dramatische Klippen.

Am Ende des Tages machten wir uns auf den Weg zu unserem Campingplatz in der Nähe von Makarora, eingebettet in die unberührte Wildnis der Südinsel. Umgeben von mächtigen Bergen und üppigen Wäldern richteten wir uns für die Nacht ein.

## Donnerstag: Ein Tag der Entdeckung und Freude

Unser Donnerstag begann mit einer Reise zu den bezaubernden Blue Pools, einem Naturwunder, das tief im wilden Gelände verborgen lag. Leider war der Brücke, die uns dorthin führen sollte, gesperrt, sodass wir durch das kalte Flusswasser waten mussten, um unser Ziel zu erreichen. Trotz der Kälte zog uns die Schönheit der Blue Pools weiter.

Nach der Ankunft wurden wir von den faszinierenden Blau-Tönen des klaren Wassers empfangen, das in seiner Tiefe die Kunstfertigkeit der Natur widerspiegelte.

Verlassen der Blue Pools setzten wir unsere Reise nach Wanaka fort, wo uns eine Welt voller Wunder erwartete. Unser erster Halt war ein Lavendelfeld, wo die Luft mit dem süssen Duft der blühenden Blumen gefüllt war. Wir liessen uns von der Ruhe der ländlichen Gegend verzaubern.

Unser nächstes Ziel war Puzzle World, ein Ort, der uns mit optischen Täuschungen und einem mehrstufigen Labyrinth fesselte. Inmitten von Denksportaufgaben und faszinierenden Illusionen verloren wir uns in der Welt der Wahrnehmung.

Am Ende des Tages machten wir uns auf den Weg zu unserem Campingplatz in Wanaka und richteten uns für die Nacht ein.

## Freitag: Vom Frühstück bis nach Arrowtown

Unser Freitag begann mit einem herzhaften Frühstück, das aus Steak-Sandwiches bestand und den Ton für einen Tag voller Erkundungen und Freude setzte. Mit gestilltem Hunger machten wir uns auf den Weg nach Queenstown, gespannt darauf, in die lebendige Energie dieses ikonischen Ortes einzutauchen.

Nach unserer Ankunft begaben wir uns auf einen gemächlichen Spaziergang durch die geschäftigen Strassen, genossen die lebhafte Atmosphäre und bewunderten die atemberaubende Umgebung. Von den Ufern des Lake Wakatipu bis zu den zerklüfteten Gipfeln der Remarkables wurde Queenstown bei jedem Schritt noch faszinierender.

Am Ende des Tages machten wir uns auf den Weg zu einem Campingplatz in der Nähe des charmanten Arrowtown, einer historischen Goldgräberstadt, die in den sanften Hügeln eingebettet liegt.