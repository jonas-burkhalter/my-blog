+++ 
draft = false
date = 2024-01-27T22:00:00+02:00
title = "Road Trip (3/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Neuseeland"]
+++

## Samstag: Friedliche Wildnis und himmlische Wunder
<!--
Lachs farm

Lupinenfelder (verwelkt)

Camping mitten im Nirgendwo (Fairlie)

Sterne beobachten
-->

{{< figure src="./images/IMG_20240127_153702.jpg" alt="camping" class="aligne-left size-medium" >}}

Unsere Samstagsreise führte uns zu den ruhigen Gewässern einer Lachszucht, wo wir die fleissigen Kreaturen bewunderten, die unter der Oberfläche schwammen.

{{< figure src="./images/IMG_20240127_162119.jpg" alt="camping" class="aligne-right size-small" >}}

Unsere Erkundung setzte sich fort, als wir nach Lupinenfeldern suchten. Obwohl die meisten verwelkt waren, erinnerte uns ihre vergängliche Schönheit an die flüchtige Natur des Lebens und die Bedeutung, jeden Moment zu schätzen.

Als der Tag sich dem Ende zuneigte, machten wir uns auf den Weg zu einem abgelegenen Campingplatz mitten im Nirgendwo, eingebettet in die Wildnis von Fairlie. Umgeben von der Symphonie der Natur, bauten wir unser Zelt unter einem Himmel voller Sterne auf, der sich vor uns wie eine riesige Leinwand ausbreitete, die darauf wartete, bemalt zu werden.

## Sonntag: Auf dem Weg nach Christchurch
<!--
Fahrt nach Christchurch

Possumwolle

Foodhall

Puzzle
-->
Wir begannen unseren Sonntag mit einer malerischen Fahrt nach Christchurch, wobei der Weg uns immer wieder beeindruckende Ausblicke auf Neuseelands vielfältige Landschaften bot. Unser erster Halt in der Stadt war ein einzigartiger Fund – ein Laden für Possumwolle.

Anschliessend gingen wir in die geschäftige Foodhall, wo die reichen Aromen verschiedenster Küchen die Luft erfüllten. Wir begaben uns auf ein kulinarisches Abenteuer, probierten Gerichte aus verschiedenen Ständen und genossen jeden Bissen.

Der Tag setzte sich mit der Suche nach einem Puzzle fort, was uns in mehrere Geschäfte führte. Wir durchstöberten Regale voller Denksportaufgaben und komplexer Herausforderungen.

Nach einem Tag voller Erkundungen und Spass reflektierten wir über unsere Abenteuer, als wir uns für die Nacht niederliessen und dankbar für die Entdeckungen des Tages und die Erinnerungen waren, die wir in dem lebendigen Christchurch gemacht hatten.

## Montag: Vom Wildtier zu den Wellen
<!--
Fauler Morgen beim Fussball schauen

Willowbank Wildlife Reserve

Fahrt nach Omihi
Camping und den Strand geniessen
-->
Wir begannen unseren Montag mit einem faulen Morgen, entspannt und beim Fussballschauen, und genossen das langsame Tempo nach den abenteuerlichen Tagen.

{{< figure src="./images/IMG_20240129_131304.jpg" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240129_133332.jpg" alt="camping" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Nach einer kurzen Erholung besuchten wir das Willowbank Wildlife Reserve, wo wir uns in das reiche Naturerbe Neuseelands vertieften. Beim Spaziergang durch das Reservat trafen wir auf eine Vielzahl von Tieren in gut gepflegten Lebensräumen und lernten mehr über ihr Verhalten und die Bemühungen zum Artenschutz.

{{< figure src="./images/IMG_20240129_140042.jpg" alt="camping" class="aligne-center size-medium" >}}

Am Nachmittag machten wir uns wieder auf den Weg nach Omihi. Bei unserer Ankunft fanden wir einen perfekten Campingplatz in der Nähe des Strandes. Den Rest des Tages verbrachten wir damit, die friedliche Küstenlandschaft zu geniessen, wobei der Klang der Wellen eine beruhigende Kulisse bildete.

## Dienstag: Entspannung und Küstenausflüge
<!--
Lange schlafen
Schwimmen gehen
Fahrt nach Kaikoura
Nochmal schwimmen gehen
-->
{{< figure src="./images/IMG_8728.JPG" alt="camping" class="aligne-left size-small" >}}

Wir gönnten uns einen langen, erholsamen Schlaf, um uns vollständig zu erholen. Nachdem wir aufwachten, machten wir uns sofort auf den Weg zu einem erfrischenden Bad, das uns mit frischem Elan erfüllte.

Nach dem Abtrocknen und dem Geniessen der morgendlichen Ruhe fuhren wir weiter nach Kaikoura. Die landschaftlich reizvolle Fahrt bot atemberaubende Ausblicke auf die Küste, sodass die Reise genauso angenehm war wie das Ziel.

Bei unserer Ankunft in Kaikoura konnten wir einem weiteren Bad nicht widerstehen. Das klare, einladende Wasser lockte uns, und wir verbrachten den Nachmittag damit, die Küstenschönheit zu geniessen, die Sonne zu tanken und uns an den einfachen Freuden des Seins am Meer zu erfreuen.

## Mittwoch: Unterwasserwunder und Küstenausflüge
<!--
Tauchen
Fahrt nach Rarangi

Bändermakrelen, blauer Moki, roter Moki, Marmorflundern, Seesterne, Seeigel (gegessen),
Mana das ausgemusterte Küstenwächterboot
-->
Mein Mittwoch begann mit einem aufregenden Tauchausflug auf dem "Mana", dem ausgemusterten Küstenwächterboot. Beim Eintauchen in die Unterwasserwelt entdeckten wir das lebendige Meeresleben, sichteten Bändermakrelen, blauen Moki, roten Moki und Marmorflundern, die jeweils einen Farbtupfer in die Tiefe des Ozeans brachten. Seesterne und Seeigel zierten den Meeresboden, wobei letzterer sogar eine einzigartige kulinarische Erfahrung bot, da wir ihn unter Wasser probierten.

{{< figure src="./images/diving-1.jpeg" alt="diving" class="aligne-left size-medium" >}}
{{< figure src="./images/G0111235.JPG" alt="diving" class="aligne-right size-medium" >}}
{{< figure src="./images/G0121256.JPG" alt="diving" class="aligne-left size-medium" >}}
{{< figure src="./images/G0151347.JPG" alt="diving" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

{{< figure src="./images/IMG_20240131_175904.jpg" alt="camping" class="aligne-left size-smaller" >}}

Nach unserem Tauchgang fuhren wir nach Rarangi, eine Fahrt, die uns noch mehr atemberaubende Küstenblicke bot. Die Reise war eine perfekte Ergänzung zu den Unterwasserwundern des Tages und brachte uns noch mehr malerische Schönheit.

Als wir in Rarangi ankamen, hinterliessen die Erinnerungen an die Abenteuer des Tages in uns eine tiefgehende Wertschätzung für die Schönheit und Vielfalt der Küste Neuseelands.

<div style="clear:both;"></div>

{{< figure src="./images/IMG_8755.JPG" alt="camping" class="aligne-center size-medium" >}}
