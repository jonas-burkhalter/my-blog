+++ 
draft = false
date = 2024-01-27T22:00:00+02:00
title = "Road Trip (3/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

## Saturday: Serene Wilderness and Celestial Wonders
<!--
Salmon farm

Lupine fields (verwelkt) 

Camping in middle of nowhere (Fairlie)  

Watch stars
-->

{{< figure src="./images/IMG_20240127_153702.jpg" alt="camping" class="aligne-left size-medium" >}}

Our Saturday journey took us to the tranquil waters of a salmon farm, where we marveled at the industrious creatures swimming beneath the surface.

{{< figure src="./images/IMG_20240127_162119.jpg" alt="camping" class="aligne-right size-small" >}}

Continuing our exploration, we sought out fields of lupines. Though most were wilted, their fleeting beauty served as a reminder of the ephemeral nature of life and the importance of cherishing each moment.

As the day waned, we made our way to a secluded campsite in the middle of nowhere, nestled amidst the wilderness of Fairlie. Surrounded by the symphony of nature, we set up camp beneath a canopy of stars, the night sky stretching out before us like a vast canvas waiting to be painted.

## Sunday: Christchurch Bound
<!--
Drive to Christchurch

Possum wool

Foodhall

Puzzle
-->
We began our Sunday with a scenic drive to Christchurch, the road offering glimpses of New Zealand's diverse landscapes. Our first stop in the city was a unique find—a possum wool shop. 

Next, we headed to the bustling food hall, where the rich aromas of diverse cuisines filled the air. We indulged in a culinary adventure, sampling dishes from various stalls and savoring every bite.

The day continued with a quest to find a puzzle, leading us to explore multiple shops. We searched through shelves filled with brain teasers and intricate challenges.

After a day full of exploration and fun, we reflected on our adventures as we settled in for the night, grateful for the day's discoveries and the memories made in vibrant Christchurch.

## Monday: From Wildlife to Waves
<!--
Lazy morning watching football

Willowbank wildlife reserve

Drive to omihi
Camping and enjoying the beach
-->
We started our Monday with a lazy morning, relaxing and watching football, savoring the slow pace after days of adventure.

{{< figure src="./images/IMG_20240129_131304.jpg" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240129_133332.jpg" alt="camping" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Once energized, we visited the Willowbank Wildlife Reserve, where we immersed ourselves in the rich tapestry of New Zealand’s fauna. Strolling through the reserve, we encountered a variety of animals in well-maintained habitats, learning about their behaviors and conservation efforts.

{{< figure src="./images/IMG_20240129_140042.jpg" alt="camping" class="aligne-center size-medium" >}}

In the afternoon, we hit the road again, driving to Omihi. Upon arrival, we found a perfect camping spot near the beach. The rest of the day was spent enjoying the serene coastline, the sound of the waves providing a soothing backdrop.

## Tuesday: Relaxation and Coastal Escapes
<!--
Sleep long
Go for a swim 
Drive to kaikoura
Go for another swim
-->
{{< figure src="./images/IMG_8728.JPG" alt="camping" class="aligne-left size-small" >}}

We indulged in a long, restful sleep, allowing ourselves to fully recharge. Once awake, we headed straight for a refreshing swim, letting the cool waters invigorate us.

After drying off and enjoying the morning's tranquility, we set off for Kaikoura. The scenic drive offered stunning coastal views, making the journey as enjoyable as the destination.

Upon arriving in Kaikoura, we couldn't resist another swim. The clear, inviting waters beckoned, and we spent the afternoon enjoying the coastal beauty, soaking up the sun, and reveling in the simple pleasure of being by the sea.

## Wednesday: Underwater Wonders and Coastal Drives
<!--
Diving
Drive to rarangi

Banded wrasse, blue moki, red moki, marblefish, seesterne, igel (gegessen),
Mana the retired coast guard Boot
-->
My Wednesday started with an exhilarating diving adventure on Mana, the retired coast guard boat. Plunging into the underwater world, we explored the vibrant marine life, spotting banded wrasse, blue moki, red moki, and marblefish, each adding a splash of color to the ocean’s depths. Starfish and sea urchins adorned the seabed, with the latter even providing a unique culinary experience as we sampled them underwater.

{{< figure src="./images/diving-1.jpeg" alt="diving" class="aligne-left size-medium" >}}
{{< figure src="./images/G0111235.JPG" alt="diving" class="aligne-right size-medium" >}}
{{< figure src="./images/G0121256.JPG" alt="diving" class="aligne-left size-medium" >}}
{{< figure src="./images/G0151347.JPG" alt="diving" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

{{< figure src="./images/IMG_20240131_175904.jpg" alt="camping" class="aligne-left size-smaller" >}}

After our dive, we drove to Rarangi, the journey offering more stunning coastal views. The drive was a perfect complement to the day's underwater wonders, adding a touch of scenic beauty to our trip.

As we settled into Rarangi, the memories of the day’s adventures left us with a profound appreciation for the beauty and diversity of New Zealand’s coast.
<div style="clear:both;"></div>

{{< figure src="./images/IMG_8755.JPG" alt="camping" class="aligne-center size-medium" >}}
