+++
draft = false
date = 2024-01-10T22:00:00+02:00
title = "Auckland"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Neuseeland"]
+++

## Mittwoch: Rückkehr nach Auckland
<!--
Mit der Fähre zurück nach Auckland

Kaffee im "The Shelf" wieder

Einkaufen

Noë am Flughafen abholen
-->
Mit dem Charme der Insel noch in meinen Gedanken, verabschiedete ich mich von Waiheke und stieg in die Fähre zurück nach Auckland, dessen Skyline sich wie ein alter Freund am Horizont abzeichnete.

In der geschäftigen Metropole angekommen, suchte ich Trost in der vertrauten Umarmung von "The Shelf", wo der Duft von frisch gebrühtem Kaffee mich wie eine warme Umarmung willkommen hiess. Ich genoss den reichen Geschmack und fand Komfort in der Routine eines morgendlichen Rituals.

Nach ein paar Besorgungen war der Moment endlich gekommen. Mit einem Lächeln auf den Lippen und Aufregung im Herzen machte ich mich auf den Weg zurück zum Flughafen, um Noë zu überraschen und abzuholen ☺️. Glücklich wiedervereint, stiegen unsere Laune und wir tauschten Umarmungen und Geschichten über unsere Erlebnisse während der Trennung aus.

## Donnerstag: Ein Tag der Entspannung und Vorbereitung auf die Reise
<!-- 
Fauler Tag, hauptsächlich nichts tun
Frühstück holen
Die ersten Tage des Roadtrips planen
-->
Heute genossen wir die Schönheit des Nichtstuns—ein wahrer Faulenzertag im besten Sinne. Ohne Verpflichtungen und ohne einen strikten Zeitplan, genossen wir den Luxus, einfach zu sein.

Der Tag begann mit einem gemütlichen Frühstück, bei dem wir jeden Bissen genossen und die Aromen auf unserem Gaumen tanzten, während wir über einer dampfenden Tasse Kaffee verweilten. Der Morgen zog sich vor uns wie eine Leinwand, die darauf wartete, bemalt zu werden, und wir nahmen die Ruhe des Moments an.

Als der Tag weiter verstrich, richteten wir unsere Aufmerksamkeit auf das aufregende Abenteuer, das vor uns lag: der Roadtrip. Ausgerüstet mit Notizbüchern und Apps, begannen wir, die ersten Tage unserer Reise zu skizzieren und eine Route zu planen, die Abenteuer und Entdeckungen an jeder Ecke versprach.

Mit jedem Ziel, das wir auf der Karte verzeichneten, und jeder Sehenswürdigkeit, die wir notierten, wuchs die Vorfreude in uns—eine spürbare Aufregung über die Abenteuer, die uns auf der offenen Strasse erwarteten.

## Freitag: Vorbereitung für die offene Strasse
<!--
Frühstück
Entspannen
Einkaufen für den Roadtrip, warme Kleidung und Essen
-->
Mit dem Freitag kam die Vorfreude auf den bevorstehenden Roadtrip. Begierig darauf, unser Abenteuer zu beginnen, starteten wir den Tag mit einem kräftigen Frühstück, um uns für die bevorstehende Reise zu stärken.

Am Nachmittag richteten wir unsere Aufmerksamkeit auf praktische Angelegenheiten und begaben uns auf eine Einkaufstour, um Vorräte für den Roadtrip zusammenzustellen. Ausgerüstet mit einer Liste und einer klaren Absicht durchstöberten wir die Geschäfte nach warmer Kleidung, um uns vor der Kälte der offenen Strasse zu schützen, und deckten uns mit Proviant ein, um unsere Reise zu befeuern.