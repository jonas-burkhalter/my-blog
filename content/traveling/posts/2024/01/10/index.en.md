+++ 
draft = false
date = 2024-01-10T22:00:00+02:00
title = "Auckland"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

## Wednesday: Return to Auckland
<!--
taking the ferry back to auckland

coffee in the shelf again

some shopping 

Pick up Noë at the airport
-->
With the island's charm lingering in my thoughts, I bid farewell to Waiheke and boarded the ferry back to Auckland, the city's skyline looming on the horizon like an old friend.

Arriving in the bustling metropolis, I sought solace in the familiar embrace of "The Shelf," where the aroma of freshly brewed coffee welcomed me like a warm hug. Savoring the rich flavors, I found comfort in the routine of a morning ritual.

After some basic grocery shopping, the moment had finally arrived. With smiles on my face and excitement in my hearts, I made my way back to the airport to surprise and pick up Noë ☺️. Happily reunited, our spirits soared as we embraced in warm hugs and exchanged stories of our adventures apart. 

## Thursday: A Day of Relaxation and Road Trip Preparations
<!-- 
lazy day, mostly do nothing
get some breakfast
plan first days of the roadtrip
-->

Today, we embraced the beauty of doing nothing—a true lazy day in every sense. With no obligations to fulfill and no itinerary to adhere to, we reveled in the luxury of simply being.

Starting the day with a leisurely breakfast, we savored each bite, allowing the flavors to dance upon our palate as we lingered over a steaming cup of coffee. The morning stretched before us like a canvas waiting to be painted, and we embraced the serenity of the moment.

As the day unfolded, we turned our attention to the exciting adventure looming on the horizon: the road trip ahead. Armed with notebooks and apps, we began to sketch out the first days of our journey, plotting a course that promised adventure and discovery at every turn.

With each destination mapped out and each roadside attraction noted, we felt a sense of anticipation building within us—a palpable excitement for the adventures that awaited us on the open road.

## Friday: Preparing for the Open Road
<!--
breakfast
relaxe
go shopping for the road trip, warm clothing and food 
-->
With the dawn of Friday came the anticipation of the road trip ahead. Eager to set off on our adventure, we began the day with a hearty breakfast, fueling up for the journey that lay ahead.

In the afternoon, we turned our attention to practical matters, embarking on a shopping expedition to gather supplies for the road trip. Armed with a list and a sense of purpose, we scoured the shops for warm clothing to ward off the chill of the open road and stocked up on provisions to fuel our journey.
