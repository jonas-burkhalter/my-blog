+++ 
draft = false  
date = 2024-10-31T22:00:00+02:00  
title = "Galway"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Donnerstag: Wiedersehen und Halloween-Spass  
{{< figure src="./images/PXL_20241031_112444542.jpg" alt="Dunguaire Castle" class="aligne-left size-smaller" >}}

Donnerstag markierte das Ende meiner Zeit in Doolin und den Beginn der Halloween-Festlichkeiten in Galway mit Claire. Auf dem Weg dorthin machte ich einen kurzen Umweg, um das Dunguaire Castle zu besichtigen – ein letzter malerischer Halt, bevor wir in die lebhafte Stadt kamen.

{{< figure src="./images/PXL_20241031_121839326.LONG_EXPOSURE-01.COVER.jpg" alt="latin quarter" class="aligne-right size-smaller" >}}

In Galway angekommen, begannen wir mit einem Pint und schlenderten durch das lebendige Latin Quarter und den Eyre Square. Nachdem wir etwas gegessen und eine weitere Runde gedreht hatten, machten wir uns auf den Weg zum Airbnb. Wir richteten uns ein und stürzten uns im wahrsten Halloween-Geist in einige festliche Traditionen: Apfeldunkeln, Karamelläpfel und etwas Gesichtsbemalung, um den Look abzurunden.

Mit Make-up versehen, gesellten wir uns zu den Menschenmengen auf den lebhaften Strassen von Galway, umgeben von einer unglaublichen Auswahl an Kostümen und Charakteren. Der Abend war erfüllt von Drinks, Livemusik und endlosen Lachern – eine Halloween-Feier mit der perfekten Mischung aus irischem Charme und gruseliger Unterhaltung.

{{< figure src="./images/Snapchat-509599545.jpg" alt="halloween" class="aligne-center size-small" >}}

## Freitag: Ein entspannter Tag in Salthill

{{< figure src="./images/WhatsApp Image 2024-11-04 at 21.44.51(2).jpg" alt="salthill" class="aligne-left size-medium" >}}

Freitag begann gemächlich mit einem späten Start und einem Spaziergang nach Salthill zum Frühstück. Nachdem wir gegessen hatten, gingen wir am Meer entlang und genossen die salzige Brise und die Aussicht auf die Galway Bay. Am Ende der Promenade beteiligten wir uns an der lokalen Tradition, die Wand für Glück zu treten, bevor wir umkehrten.

Zurück im Airbnb nahmen wir es ruhig – ein Nickerchen, Duschen und allgemein ein bisschen Energie tanken für den Abend. Meine einzige Hose, frisch gewaschen, aber noch unangenehm feucht, musste schnell getrocknet werden. Also improvisierte ich und bügelte sie ordentlich, bis sie fast wieder tragbar war.

Wir gingen zum Abendessen und anschliessend in eine entspannte Kneipe für ein paar Pints. Noch etwas erschöpft von Halloween, beendeten wir den Abend früh und liessen uns von *Derry Girls* zu ein paar letzten Lachern hinreissen, bevor wir ins Bett gingen. Manchmal ist ein einfacher Tag wie dieser der beste Weg, um sich zu erholen, selbst auf einer Reise.

## Samstag: Ein Tag auf Inisheer

{{< figure src="./images/PXL_20241102_131823446.jpg" alt="shipwreck" class="aligne-right size-medium" >}}

Samstag führte uns von Galway zur Fähre in Rossaveal, die uns zur kleinsten der Aran Islands, Inisheer, brachte. Dort angekommen, holten wir uns ein Mittagessen und machten uns dann zu einem Spaziergang auf, um die charmant unberührte Insel zu erkunden.

{{< figure src="./images/PXL_20241102_143113763.jpg" alt="stone wall" class="aligne-left size-small" >}}

Unser erster Halt war ein Schiffswrack – das Wrack der MV Plassey – rostig und dramatisch gegen die felsige Küste, als wäre es direkt aus einer alten irischen Legende gegriffen. Von dort aus schlenderten wir zum Leuchtturm, genossen die ruhige Atmosphäre der Insel und machten uns dann auf den Weg zu den nahegelegenen Ruinen. Unser Inisheer-Abenteuer beendeten wir mit einem Stück Apfelkuchen, bevor wir wieder auf die Fähre stiegen.

{{< figure src="./images/PXL_20241102_151423242.jpg" alt="coast" class="aligne-center size-medium" >}}

{{< figure src="./images/PXL_20241102_184502998.jpg" alt="sheep" class="aligne-right size-small" >}}

Zurück auf dem Festland begannen wir die Fahrt nach Louisburgh, als die Dämmerung einsetzte. Aus den Schatten tauchte eines von Irlands inoffiziellen Strassenhindernissen auf: eine Herde Schafe, die sich quer über die Strasse ausbreitete und sie wie ihr eigenes Revier beanspruchte. Die Fahrt war gleichermassen amüsant und anstrengend!

Endlich im Airbnb angekommen, parkten wir, und Claire ging voraus, um alles zu inspizieren. Plötzlich ein Schrei! Ich eilte herbei und fand sie lachend am Boden, verheddert, nachdem sie im Dunkeln über eine kleine Mauer gestolpert war. Es war unmöglich, nicht zu lachen.
