+++ 
draft = false
date = 2024-10-31T22:00:00+02:00
title = "Galway"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Thursday: Reunion and Halloween Shenanigans
{{< figure src="./images/PXL_20241031_112444542.jpg" alt="Dunguaire Castle" class="aligne-left size-smaller" >}}

Thursday marked the end of my time in Doolin and the start of Halloween festivities in Galway with Claire. On the way, I took a quick detour to check out Dunguaire Castle—one last scenic pit stop before arriving in the lively city.

{{< figure src="./images/PXL_20241031_121839326.LONG_EXPOSURE-01.COVER.jpg" alt="latin quarter" class="aligne-right size-smaller" >}}

Once in Galway, we kicked things off with a pint and wandered through the vibrant Latin Quarter and Eyre Square. After grabbing a bite and another round, we finally made our way to the Airbnb. We settled in and, in true Halloween spirit, dived into some festive traditions: an apple dunk, toffee apples, and a bit of face paint to complete the look. 

With makeup in place, we joined the crowds on Galway’s lively streets, surrounded by an incredible array of costumes and characters. The night was filled with drinks, live music, and endless laughs—a Halloween celebration with just the right blend of Irish charm and spooky fun.

{{< figure src="./images/Snapchat-509599545.jpg" alt="halloween" class="aligne-center size-small" >}}

## Friday: A Laid-Back Day in Salthill

{{< figure src="./images/WhatsApp Image 2024-11-04 at 21.44.51(2).jpg" alt="salthill" class="aligne-left size-medium" >}}

Friday began at a leisurely pace with a late start and a stroll down to Salthill for breakfast. After eating, we walked along the seaside, taking in the salty breeze and views of Galway Bay. At the end of the promenade, we joined in on the local tradition of kicking the wall for good luck before turning back.

Back at the Airbnb, we took it easy—napping, showering, and generally recharging for the night ahead. My one pair of pants, fresh from the wash but still annoyingly damp, needed some quick drying. So, improvising, I gave them a solid ironing session until they were just about wearable again.

We headed out for dinner, followed by a relaxed pub night with a couple of pints. Still a bit worn out from Halloween, we called it an early night, ending with a few laughs over *Derry Girls* before turning in. Sometimes, even on a trip, a simple day like this is the best way to recharge.

## Saturday: A Day on Inisheer

{{< figure src="./images/PXL_20241102_131823446.jpg" alt="shipwreck" class="aligne-right size-medium" >}}

Saturday took us from Galway to the ferry at Rossaveal, bound for the smallest of the Aran Islands, Inisheer. Once there, we grabbed some lunch and then set off on a walk to explore this charmingly rugged island.

{{< figure src="./images/PXL_20241102_143113763.jpg" alt="stone wall" class="aligne-left size-small" >}}

Our first stop was a shipwreck—the wreck of the MV Plassey—rusted and dramatic against the rocky shore, as if it had been plucked right out of an old Irish legend. From there, we wandered over to the lighthouse, basking in the island’s tranquil vibe, before making our way to the ruins nearby. We wrapped up our Inisheer adventure with a slice of apple pie before hopping back on the ferry.

{{< figure src="./images/PXL_20241102_151423242.jpg" alt="coast" class="aligne-center size-medium" >}}

{{< figure src="./images/PXL_20241102_184502998.jpg" alt="sheep" class="aligne-right size-small" >}}

Back on the mainland, we started our drive to Louisburgh as dusk set in. Out of the shadows came one of Ireland’s unofficial road hazards: a herd of sheep sprawled across the road, treating it as their own. The drive was equal parts amusing and exhausting!

Finally arriving at the Airbnb, we parked and Claire went ahead to scope things out. Suddenly, a shriek! I hurried over and found her on the ground, laughing and tangled up after tripping over a small wall in the dark. It was impossible not to laugh.
