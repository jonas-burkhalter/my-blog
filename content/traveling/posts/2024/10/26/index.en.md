+++ 
draft = false
date = 2024-10-26T22:00:00+02:00
title = "Dingle"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Saturday: by the Sea
Saturday began on a cozy note with a bit of Irish history reading and a hearty breakfast. My main mission for the day was to check out the local aquarium and—hopefully—book another boat tour, since my tour planned for the next day had been canceled.

{{< figure src="./images/PXL_20241026_105343761.jpg" alt="boats" class="aligne-left size-small" >}}

On my way to the aquarium, I noticed a sign for an open [boat tour](https://www.blasketislandsealifetours.com/) spot leaving in just half an hour. Perfect timing! I snapped up the spot, with just enough time to wander down to the harbor and admire the boats before gearing up and boarding the M/V Freedom.

{{< figure src="./images/PXL_20241026_115544702.MP.jpg" alt="dolphins" class="aligne-right size-small" >}}

Not five minutes in, we were already greeted by dolphins swimming near the shore, playfully following alongside the boat. We pressed on along the rugged coastline, where the cliffs loomed in dramatic colors, carved and jagged like something out of a movie. The rocks were staggeringly high, with shades that changed with every turn of the boat.

{{< figure src="./images/PXL_20241026_120614184.jpg" alt="cliff" class="aligne-center size-medium" >}}

We made our way to Inishvickillane, one of the Blasket Islands, where an impressive seal colony spends its winters. They were sprawled out like they owned the place, clearly unfazed by the occasional boat passing by. After admiring them, we continued to the Cathedral Rocks on Inishnabro, a breathtakingly steep and weathered formation that looked like a natural fortress. Then, we circled back along the coast, soaking in more views of Ireland’s wild shore.

{{< figure src="./images/PXL_20241026_121150029.jpg" alt="cliff" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241026_131147130.jpg" alt="cliff" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Post-boat tour, I was ready for a feast, so I found a spot for a meal of tender lamb chops and potatoes, washed down with a few pints of Guinness. It was the perfect end to a day that reminded me that sometimes the best plans are the unexpected ones.

{{< figure src="./images/PXL_20241026_145234558.MP.jpg" alt="food" class="aligne-center size-medium" >}}

## Sunday: A Rainy Day Done Right
{{< figure src="./images/PXL_20241027_102921711.jpg" alt="axolotl" class="aligne-left size-small" >}}

Sunday kicked off with a classic Irish drizzle, but I wasn’t about to let a little rain slow me down. First stop after packing up: the aquarium. 

{{< figure src="./images/PXL_20241027_105408902.MP.jpg" alt="african claw frog" class="aligne-right size-small" >}}

Surrounded by tanks of colorful fish, I was instantly in my happy place. Clownfish darted around in mesmerizing colors, and a turtle hovered gracefully, looking as if it was flying underwater. Beyond the fish, there were otters, penguins, butterflies... it was a whole world of wonder. In the underwater tunnel, fish swam right over my head, and at the touch basin, I even got to pet a ray—an unexpectedly smooth and fascinating encounter.

{{< figure src="./images/PXL_20241027_110001427.jpg" alt="clownfish" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241027_113743601.MP.jpg" alt="ray" class="aligne-right size-medium" >}}
{{< figure src="./images/PXL_20241027_114411717.jpg" alt="tunnel" class="aligne-center size-medium" >}}

After that aquatic adventure, I drove to Adare and found a cozy spot for lunch. Pizza was the meal of choice, and it was the perfect warm-up on a rainy day. I wandered around the charming town for a while, admiring Adare's colorful streets and quaint shops before deciding to head to my next base in Doolin, where I’d be staying for a few days.

Between the aquarium, pizza, and small-town wandering, it was a perfect blend of laid-back exploration and simple pleasures.

{{< figure src="./images/PXL_20241027_171744933.jpg" alt="Doolin" class="aligne-center size-medium" >}}
