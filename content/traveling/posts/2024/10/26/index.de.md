+++  
draft = false  
date = 2024-10-26T22:00:00+02:00  
title = "Dingle"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Samstag: Am Meer  
Der Samstag begann gemütlich mit etwas irischer Geschichtslektüre und einem ausgiebigen Frühstück. Meine Hauptmission für den Tag war es, das lokale Aquarium zu besuchen und – hoffentlich – eine weitere Bootstour zu buchen, da meine geplante Tour für den nächsten Tag abgesagt worden war.  

{{< figure src="./images/PXL_20241026_105343761.jpg" alt="Boote" class="aligne-left size-small" >}}  

Auf dem Weg zum Aquarium entdeckte ich ein Schild für eine offene [Bootstour](https://www.blasketislandsealifetours.com/), die in nur einer halben Stunde starten sollte. Perfektes Timing! Ich schnappte mir den Platz, mit gerade genug Zeit, um zum Hafen hinunterzuschlendern und die Boote zu bewundern, bevor ich mich ausrüstete und an Bord der M/V Freedom ging.  

{{< figure src="./images/PXL_20241026_115544702.MP.jpg" alt="Delfine" class="aligne-right size-small" >}}  

Keine fünf Minuten nach dem Start wurden wir bereits von Delfinen begrüsst, die nahe der Küste schwammen und verspielt neben dem Boot herzogen. Wir fuhren entlang der rauen Küste, wo die Klippen in dramatischen Farben aufragten – zerklüftet und wild wie aus einem Film. Die Felsen waren atemberaubend hoch und wechselten mit jeder Wendung des Bootes ihre Farbtöne.  

{{< figure src="./images/PXL_20241026_120614184.jpg" alt="Klippe" class="aligne-center size-medium" >}}  

Wir steuerten Inishvickillane an, eine der Blasket Islands, wo eine beeindruckende Seehundkolonie überwintert. Sie lagen ausgestreckt, als ob sie den Ort für sich allein beanspruchen würden, unbeeindruckt von den gelegentlichen Booten. Nach einer Weile fuhren wir weiter zu den Cathedral Rocks auf Inishnabro, einer atemberaubend steilen und verwitterten Felsformation, die wie eine natürliche Festung wirkte. Dann kehrten wir entlang der Küste zurück und genossen weitere Ausblicke auf Irlands wilde Ufer.  

{{< figure src="./images/PXL_20241026_121150029.jpg" alt="Klippe" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241026_131147130.jpg" alt="Klippe" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

Nach der Bootstour war ich bereit für ein Festmahl, also fand ich ein Restaurant, in dem zarte Lammkoteletts mit Kartoffeln serviert wurden – dazu ein paar Pints Guinness. Es war der perfekte Abschluss eines Tages, der mich daran erinnerte, dass die besten Pläne oft die unerwarteten sind.  

{{< figure src="./images/PXL_20241026_145234558.MP.jpg" alt="Essen" class="aligne-center size-medium" >}}  

## Sonntag: Ein regnerischer Tag, richtig gemacht  
{{< figure src="./images/PXL_20241027_102921711.jpg" alt="Axolotl" class="aligne-left size-small" >}}  

Der Sonntag begann mit typisch irischem Nieselregen, aber ich liess mich davon nicht aufhalten. Der erste Halt nach dem Packen: das Aquarium.  

{{< figure src="./images/PXL_20241027_105408902.MP.jpg" alt="Afrikanischer Krallenfrosch" class="aligne-right size-small" >}}  

Umgeben von Tanks mit bunten Fischen war ich sofort in meinem Element. Clownfische huschten in faszinierenden Farben umher, und eine Schildkröte schwebte elegant, als würde sie durch das Wasser fliegen. Neben den Fischen gab es Otter, Pinguine und Schmetterlinge... es war eine ganze Welt voller Wunder. Im Unterwassertunnel schwammen die Fische direkt über meinen Kopf, und am Berührungsbecken konnte ich sogar einen Rochen streicheln – eine unerwartet glatte und faszinierende Erfahrung.  

{{< figure src="./images/PXL_20241027_110001427.jpg" alt="Clownfisch" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241027_113743601.MP.jpg" alt="Rochen" class="aligne-right size-medium" >}}  
{{< figure src="./images/PXL_20241027_114411717.jpg" alt="Tunnel" class="aligne-center size-medium" >}}  

Nach diesem aquatischen Abenteuer fuhr ich nach Adare und fand ein gemütliches Plätzchen zum Mittagessen. Pizza war die Mahlzeit meiner Wahl – perfekt, um mich an einem regnerischen Tag aufzuwärmen. Ich schlenderte durch die charmante Stadt, bewunderte die bunten Strassen und hübschen Läden von Adare, bevor ich beschloss, zu meiner nächsten Unterkunft in Doolin weiterzufahren, wo ich die nächsten Tage verbringen würde.  

Zwischen Aquarium, Pizza und kleinen Stadtabenteuern war es eine perfekte Mischung aus entspannter Erkundung und einfachen Freuden.  

{{< figure src="./images/PXL_20241027_171744933.jpg" alt="Doolin" class="aligne-center size-medium" >}}  