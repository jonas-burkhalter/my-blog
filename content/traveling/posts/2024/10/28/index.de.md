+++  
draft = false  
date = 2024-10-28T22:00:00+02:00  
title = "Doolin"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Montag: Ein perfekter Tag im Burren  
{{< figure src="./images/PXL_20241028_125435048.PANO.jpg" alt="Panorama" class="aligne-center size-fullwidth" >}}  

{{< figure src="./images/PXL_20241028_130858651.jpg" alt="Lough Avalla Farm Loop Walk" class="aligne-left size-medium" >}}  
Das heutige Abenteuer führte mich in den Burren National Park, wo ich den Lough Avalla Farm Loop Walk unternahm. Der Weg führte durch eine sich ständig verändernde Landschaft, die geradezu mystisch wirkte. An jeder Ecke gab es etwas Besonderes: eine heilige Quelle, ein Grabmal, einen erratischen Steinaltar und Pflanzenfelder, die sich über uralte Burren-Felsformationen erstreckten. Tiefe Risse durchzogen die Steine, und Moos bedeckte Bäume und Felsen, was eine märchenhafte Atmosphäre schuf, die ich stundenlang hätte erkunden können.  

{{< figure src="./images/PXL_20241028_132838694.jpg" alt="Lough Avalla Farm Loop Walk" class="aligne-right size-small" >}}  

Nach etwa anderthalb Stunden endete der Spaziergang mit einem Schild, das auf Tee und Kuchen hinwies. Ich konnte nicht widerstehen und folgte ihm zu einer gemütlichen Hütte, in der Harry, der Besitzer und Erschaffer des Weges, alle mit Apfelkuchen, Brownies und heissem Tee begrüsste. Es war eine herzerwärmende Pause und vermittelte einen echten Eindruck von irischer Gastfreundschaft.  

Erfrischt machte ich mich an den Mullaghmore Loop. Dieser 7,5 km lange Rundweg schlängelt sich um den Gipfel von Mullaghmore, vorbei an Lough Gealáin und durch Kalksteinflächen, Grasland und Haselwälder. Die Aussichten waren spektakulär, und als die Sonne tief stand, schien die gesamte Landschaft zu leuchten.  

{{< figure src="./images/PXL_20241028_152814641.jpg" alt="Mullaghmore Loop" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241028_154700942.MP.jpg" alt="Mullaghmore Loop" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

Zurück im Airbnb wirkte eine heisse Dusche Wunder bei müden Muskeln, und um den Tag abzurunden, machte ich mich auf den Weg zu Fitz’s Pub. Mit ein paar Pints in der Hand genoss ich traditionelle irische Musik bei ihrer offenen Session – jedes Stück lebendig und unvergesslich. Ein paar Pints und viele Lacher später ging ich nach Hause und hatte das Gefühl, jeden Hauch von Magie des Burren aufgesogen zu haben.  

{{< figure src="./images/PXL_20241028_220553987.MP.jpg" alt="Clownfisch" class="aligne-center size-medium" >}}  

## Dienstag: Die mächtigen Cliffs of Moher  
{{< figure src="./images/PXL_20241029_122410436.jpg" alt="Turm" class="aligne-right size-small" >}}  

Der Dienstag in Doolin begann langsam und entspannt. Frühstück und Kaffee mit Gabi und Chrisi, den Airbnb-Gastgebern, sowie ein paar nette Gespräche mit Mitreisenden setzten den Ton für einen ruhigen Tag.  

{{< figure src="./images/PXL_20241029_130927378.MP.jpg" alt="Küste" class="aligne-left size-medium" >}}  

Nach dem Frühstück machte ich mich auf den Weg zum Pier, hielt kurz an, um ein Foto von Doonagore Castle zu machen – ein klassischer Anblick, der wie aus einem Märchenbuch aussieht. Am Pier angekommen, spazierte ich entlang der Meeresküste, wo felsige Klippen die Uferlinie säumten. Die Wellen krachten mit Wucht gegen die Steine, und bei jedem Aufprall entstand ein nebliger Sprühregen. Nach einiger Zeit an der rauen Küste ging ich zurück, erfrischt von der salzigen Luft.  

{{< figure src="./images/PXL_20241029_155133087.jpg" alt="Spaziergang" class="aligne-right size-smaller" >}}  

Ein spätes Mittagessen gab mir die nötige Energie für den nächsten Halt: die Cliffs of Moher. Der Weg dorthin war etwa 5 km lang, und mit jedem Schritt wurden die Klippen beeindruckender. Am Rand stehend beobachtete ich die Wellen, die gegen die Felswand unter mir schlugen – jede Brandung ein Beweis für die schiere Kraft der Natur.  

{{< figure src="./images/PXL_20241029_165836782.jpg" alt="Klippen" class="aligne-center size-large" >}}  

Als der Abend nahte, machte ich mich auf den Rückweg für eine ruhige, entspannende Nacht in Doolin. Ein weiterer schöner Tag entlang Irlands wilder Küste, perfekt ausbalanciert zwischen Abenteuer und Ruhe.  

## Mittwoch: Eine Verschnaufpause zur Mitte der Reise  
Heute ging es vor allem darum, es ruhig angehen zu lassen. Ich entspannte mich mit ein paar Serien, vertiefte mich in ein gutes Buch und plante ohne Eile die kommenden Tage.  

Am Nachmittag ging ich zum Abendessen aus, bevor ich zurückkehrte, um die Entspannung fortzusetzen. Manchmal ist ein entspannter Tag genau das, was man braucht, um für die bevorstehenden Abenteuer neue Energie zu tanken.  

{{< figure src="./images/PXL_20241030_163824876.jpg" alt="Bangers and Mash" class="aligne-center size-medium" >}}  