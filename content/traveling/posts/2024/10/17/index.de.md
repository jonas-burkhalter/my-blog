+++ 
draft = false
date = 2024-10-11T22:00:00+02:00
title = "Irland und Nordirland Reisen"
description = "Überblick und Ideen"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Irland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Irland"]  
+++

# Überblick
<iframe 
    src="https://www.google.com/maps/d/embed?mid=1noUTvAUhu35x7N3zTNsneJZ-cw3uQRg&ehbc=2E312F&noprof=1" 
    width="100%"
    height="500">
</iframe>

# Was man in Irland tun kann

## Irland
- Achill Island
    - Entspannen
- Aran Islands
    - Entspannen
- Cliffs of Moher
    - O'Brien's Tower
    - Aussichtspunkte
- Cork
    - Blarney Castle & Gardens
    - Butter Museum
    - Cork City Gaol
    - Cobh
    - English Market
    - Fitzgerald Park
    - Midleton Distillery
    - Saint Fin Barre's Cathedral
- Donegal
    - Donegal Castle
    - Glencolmcille
    - Glenveagh Castle
    - Railway Heritage Centre
    - Slieve League Cliffs
- Dublin
    - Ein Pint Guinness trinken
    - Book of Kells Experience
    - Croke Park (Ein Spiel Fussall erleben)
    - Dublin Castle
    - Guinness Storehouse
    - Howth
    - Jameson Distillery Bow St.
    - Kilmainham Gaol
    - National Gallery of Ireland
    - National Museum of Ireland-Decorative Arts & History
    - St. Patrick's Cathedral
    - St. Stephen's Green
    - Temple Bar
- Galway
    - Ard Bia at Nimmos
    - Crane Bar
    - Eyre Square Galway
    - Galway Cathedral
    - Kai Restaurant
    - Latin Quarter
    - McDonagh's
    - Monroe's Tavern
    - Tigh Neachtain
- Kerry
    - Killarney Nationalpark
    - Puffin Island
    - Ring of Kerry
- Kilkenny
    - Smithwick's Experience
- Letterfrack
    - Connemara Nationalpark
    - Kylemore Abbey & Victorian Walled Garden
- Limerick
    - Adare

## Nordirland
- Belfast
    - Black Taxi Tours
    - City Hall
    - Crumlin Road Gaol
    - Peace Wall Belfast
    - St. George’s Market
    - Titanic Belfast
    - Ulster Museum
- Bushmills
    - Giant’s Causeway
    - Verstecktes Dorf Galboly
    - Old Bushmills Distillery
- Derry
    - Halloween feiern
    - Craft Village
    - Grianan von Aileach
    - Guildhall
    - Museum of Free Derry
    - Peace Bridge
    - St. Columb’s Cathedral
    - Tower Museum
    - Stadtmauern
