+++ 
draft = false
date = 2024-10-11T22:00:00+02:00
title = "Traveling Ireland and Northern Ireland"
description = "Overview and ideas"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

# Overview
<iframe 
    src="https://www.google.com/maps/d/embed?mid=1noUTvAUhu35x7N3zTNsneJZ-cw3uQRg&ehbc=2E312F&noprof=1" 
    width="100%"
    height="500">
</iframe>

# What to do in Ireland

## Ireland
- Achill Island
    - Relaxe
- Aran Islands
    - Relaxe
- Cliffs of Moher
    - O'Brien's Tower
    - Viewpoints
- Cork
    - Blarney Castle & Gardens
    - Butter Museum
    - Cork City Gaol
    - Cobh
    - English Market
    - Fitzgerald Park
    - Midleton Distillery
    - Saint Fin Barre's Cathedral
- Donegal
    - Donegal Castle
    - Glencolmcille
    - Glenveagh Castle
    - Railway Heritage Centre
    - Slieve League Cliffs
- Dublin
    - Get a pint of Guinness
    - Book of Kells Experience
    - Croke Park (Get a game of footy)
    - Dublin Castle
    - Guinness Storehouse
    - Howth
    - Jameson Distillery Bow St.
    - Kilmainham Gaol
    - National Gallery of Ireland
    - National Museum of Ireland-Decorative Arts & History
    - St Patrick's Cathedral
    - St Stephen's Green
    - Temple Bar
- Galway
    - Ard Bia at Nimmos
    - Crane Bar
    - Eyre Square Galway
    - Galway Cathedral
    - Kai Restaurant
    - Latin Quarter
    - McDonagh's
    - Monroe's Tavern
    - Tigh Neachtain
- Kerry
    - Killarney National Park
    - Puffin Island
    - Ring of Kerry
- Kilkenny
    - Smithwick's Experience
- Letterfrack
    - Connemara National Park
    - Kylemore Abbey & Victorian Walled Garden
- Limerick
    - Adare

## Northern Ireland
- Belfast
    - Black Taxi Tours
    - City Hall
    - Crumlin Road Gaol
    - Peace Wall Belfast
    - St. George’s Market
    - Titanic Belfast
    - Ulster Museum
- Bushmills
    - Giants Causeway
    - Hidden Village of Galboly
    - Old Bushmills Distillery
- Derry
    - Celebrate Halloween
    - Craft Village
    - Grianan of Aileach
    - Guildhall
    - Museum of Free Derry
    - Peace Bridge
    - St. Columb’s Cathedral
    - Tower Museum
    - Walls
