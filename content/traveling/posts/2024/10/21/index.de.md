+++  
draft = false  
date = 2024-10-21T22:00:00+02:00  
title = "Kilkenny"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Montag: Kilkenny in Kilkenny  
Der Montag begann mit der Abfahrt aus Dublin, in Richtung Süden, und – natürlich – sofortigem Fahrstress. Doch je schmaler die Strassen und je weniger der Verkehr wurde, desto mehr legte sich die Anspannung. Nach nur 30 Minuten hielt ich für eine Pause an (denn wer muss schon hetzen, oder?). Zum Glück hatte ich ein paar landschaftlich reizvolle Punkte in meine Route eingebaut, sodass der Zwischenstopp nicht nur meiner Nerven, sondern auch der Aussicht diente.  

{{< figure src="./images/PXL_20241021_092930446.MP.jpg" alt="Fluss" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241021_095403514.MP.jpg" alt="Bäume" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

Ein bisschen entspannter ging es weiter nach Glendalough, wo die Sonne schien, die Ausblicke atemberaubend waren, und der Spaziergang am Lower Lake genau das Richtige war, um wieder runterzukommen. Hungrig vom entspannten Schlendern schnappte ich mir ein paar Curry-Chips und setzte mich an einen sonnigen Platz neben einem kleinen Bach. Es gibt etwas Seltsam-Befriedigendes daran, Pommes neben einem plätschernden Bach zu essen – pure Glückseligkeit.  

{{< figure src="./images/PXL_20241021_104744747.jpg" alt="Chips" class="aligne-right size-small" >}}  

Offenbar reichten die Pommes nicht ganz, also machte ich mich auf die Suche nach einem richtigen Mittagessen und fuhr anschliessend weiter. Diesmal führte mich die Route durch das, was ich ziemlich sicher als Niemandsland bezeichnen würde – vorbei an Aghavannagh und hinein in die Wildnis, bevor ich endlich Kilkenny erreichte. Zum Glück waren die Strassen inzwischen breiter, aber immer noch weit entfernt vom Wahnsinn in Dublin.  

Nach einem schnellen Check-in im Airbnb machte ich mich auf, Kilkenny und sein berühmtes Schloss zu erkunden. Die mittelalterlichen Vibes waren stark, und nachdem ich über das Gelände spaziert war, ging es weiter zur Smithwick's Experience – denn was wäre ein Tag voller Sightseeing ohne ein bisschen Bierkunde? Die Tour führte mich durch die Geschichte der Brauerei, von Mönchen über James und Edward Smithwick bis hin zu den modernen Brauverfahren. Fun Fact: Sie verwenden nur 2 % geröstetes Malz, im Vergleich zu den 10 % bei Guinness, was Smithwick's seine charakteristische Farbe verleiht.  

Nach einer "Riech"-Session (eine Bierverkostung ohne das Trinken – das Leben ist grausam) durfte ich endlich ein Kilkenny-Bier in Kilkenny geniessen. Erfolg freigeschaltet! 🎉  

{{< figure src="./images/PXL_20241021_161601275.jpg" alt="Smithwick's" class="aligne-center size-medium" >}}  

Damit war der Tag fast perfekt. Ich holte mir noch etwas zu essen und machte mich zurück ins Airbnb, um zu entspannen, ein bisschen zu bloggen und die Abenteuer der nächsten Tage zu planen. Irland begann langsam, seine Magie auf mich wirken zu lassen – ein Pint und ein malerischer Ausblick nach dem anderen.  