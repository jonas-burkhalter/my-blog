+++ 
draft = false
date = 2024-10-21T22:00:00+02:00
title = "Kilkenny"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Monday: Kilkenny in Kilkenny
Monday kicked off with me leaving Dublin behind, heading south, and—of course—being immediately stressed out by the driving again. But as the roads got narrower and the traffic thinned, the stress melted away. After just 30 minutes, I pulled over for a break (because who needs to rush, right?). Thankfully, I had added some scenic spots to my route, so the pit stop wasn’t just for my sanity but for some good views, too.

{{< figure src="./images/PXL_20241021_092930446.MP.jpg" alt="river" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241021_095403514.MP.jpg" alt="trees" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

A bit more relaxed, I made my way to Glendalough, where the sun was shining, the views were stunning, and the lower lake walk was just what I needed to reset. Hungry from all that leisurely walking, I grabbed some curry chips and settled at a sunny table by a small stream. There’s something oddly satisfying about eating fries next to a babbling brook. Pure bliss.

{{< figure src="./images/PXL_20241021_104744747.jpg" alt="chips" class="aligne-right size-small" >}}

Apparently, fries weren't quite enough, so I went on a quest for a real lunch, then hit the road again. This time, I drove through what I’m pretty sure was no-man’s land, passing through Aghavannagh and into the wilderness before finally reaching Kilkenny. Thankfully, the roads were wider by this point but nowhere near the madness of Dublin.

After a quick Airbnb check-in, I set off to explore Kilkenny and its famous castle. The medieval vibes were strong, and after wandering around the grounds, I made my way to the Smithwick’s Experience—because what’s a day of sightseeing without some beer education? The tour took me through the brewery’s history, from monks to James and Edward Smithwick, and how they make that beautiful red ale. Fun fact: they use only 2% roasted malt, compared to Guinness’s 10%, which gives Smithwick’s its signature color.

After a “smelling” session (a beer tasting without the drinking, because life is cruel), I finally got to enjoy a Kilkenny beer in Kilkenny. Achievement unlocked! 🎉

{{< figure src="./images/PXL_20241021_161601275.jpg" alt="smithwick's" class="aligne-center size-medium" >}}

With that done, I grabbed some food, then made my way back to the Airbnb to relax, blog a bit, and plan the adventures for the next few days. Ireland was slowly working its magic on me, one pint and one scenic view at a time.