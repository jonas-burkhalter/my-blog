+++ 
draft = false
date = 2024-10-24T22:00:00+02:00
title = "Kenny"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Thursday: Cliffs, Rain, and Car Naps
{{< figure src="./images/PXL_20241024_073531603.jpg" alt="breakfast" class="aligne-right size-smaller" >}}

Thursday started with yet another amazing breakfast—this time I opted for the full Irish breakfast. With a belly full of sausages, bacon, and eggs, I hit the road toward Killarney, ready for another day of exploring. 

{{< figure src="./images/PXL_20241024_101317489.jpg" alt="ladies view" class="aligne-left size-smaller" >}}

First stop: Ladies View, a breathtaking spot with a sweeping view of the landscape. I soaked it in for a few minutes before heading onward. Next up was Blackwater Port and The Cove, both picturesque in their own right, but I could already sense the infamous Irish weather creeping in. As the rain started, my sightseeing stops became shorter and shorter. 

{{< figure src="./images/PXL_20241024_104825787.PANO.jpg" alt="panorama view" class="aligne-center" >}}

{{< figure src="./images/PXL_20241024_130449458.jpg" alt="cliff" class="aligne-right size-medium" >}}

By the time I reached the Kerry Cliffs, I got lucky—the clouds held back just enough to give me about 20-30 minutes of minimal rain. The cliffs were stunning, towering over the Atlantic with waves crashing below. Standing there, I couldn’t help but marvel at how dramatic and wild the Irish coastline is.

With my scenic fix in, I drove to Fisherman's Bar for lunch, where I filled up on hearty food before the rain really started coming down. With a full stomach and the weather getting worse, I needed a break—so I turned the back of the car into a cozy little hideout, watching some series and relaxing for a bit while the rain pounded on the roof.

Eventually, I accepted that the rain wasn’t going anywhere, and rather than battle the elements, I decided to skip the rest of my planned stops and head straight to my Airbnb for the night. Not every day can be perfect weather in Ireland, but with views like the Kerry Cliffs, even the rain can’t dampen the magic.

## Friday: Carrauntoohil...Almost
Today was supposed to be hike day. But after yesterday's downpour and a forecast that promised even more rain, I woke up thinking, "Maybe hiking isn't in the cards." While eating breakfast, I eyed the sky skeptically—it looked “okay,” not great, but okay. So, feeling cautiously optimistic, I grabbed my gear and drove to Cronin's Yard, the starting point for the Carrauntoohil hike, Ireland’s tallest mountain.

{{< figure src="./images/PXL_20241025_100113634.PANO.jpg" alt="hike" class="aligne-center" >}}

Once there, it didn’t take long to realize that reaching the peak wasn’t happening today. The summit was somewhere in the clouds, and I had no plans of hiking into the foggy unknown. But the first part of the trail looked doable and had some gorgeous views, so off I went. The weather held up just enough for me to reach a good stopping point, where I glanced up to the peak and saw... nothing but clouds. Yep, that was my cue.

{{< figure src="./images/PXL_20241025_103239516.jpg" alt="hike" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241025_112829660.jpg" alt="hike" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

On the way back down, I took my time, snapping photos of the mist-covered mountains and soaking in the scenery before heading back to the car. 

After that mini-adventure, I checked into my B&B early, indulging in some well-earned relaxation. Dinner was a treat: fish and chips paired with a pint of Guinness, and for dessert, a fried Snickers. Not exactly a mountaintop view, but for today, it was the perfect end to a laid-back, rainy Irish hike day.

{{< figure src="./images/PXL_20241025_163533655.jpg" alt="sea" class="aligne-center size-medium" >}}
