+++  
draft = false  
date = 2024-10-24T22:00:00+02:00  
title = "Kenny"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Donnerstag: Klippen, Regen und Autoschläfchen  
{{< figure src="./images/PXL_20241024_073531603.jpg" alt="Frühstück" class="aligne-right size-smaller" >}}  

Der Donnerstag begann mit einem weiteren fantastischen Frühstück – dieses Mal entschied ich mich für das vollständige irische Frühstück. Mit einem Bauch voller Würstchen, Speck und Eiern machte ich mich auf den Weg nach Killarney, bereit für einen weiteren Tag voller Erkundungen.  

{{< figure src="./images/PXL_20241024_101317489.jpg" alt="Ladies View" class="aligne-left size-smaller" >}}  

Erster Stopp: Ladies View, ein atemberaubender Aussichtspunkt mit einem weitreichenden Blick über die Landschaft. Nach ein paar Minuten Staunen ging es weiter. Nächste Stationen waren Blackwater Port und The Cove, beide auf ihre Weise malerisch, doch ich spürte bereits, wie sich das berüchtigte irische Wetter ankündigte. Als der Regen einsetzte, wurden meine Stopps immer kürzer.  

{{< figure src="./images/PXL_20241024_104825787.PANO.jpg" alt="Panorama" class="aligne-center" >}}  

{{< figure src="./images/PXL_20241024_130449458.jpg" alt="Klippe" class="aligne-right size-medium" >}}  

Als ich die Kerry Cliffs erreichte, hatte ich Glück – die Wolken hielten gerade so lange zurück, dass ich 20-30 Minuten fast ohne Regen hatte. Die Klippen waren atemberaubend und ragten hoch über den Atlantik, während die Wellen darunter auf die Felsen krachten. Dort zu stehen, liess mich einfach nur staunen, wie dramatisch und wild die irische Küste ist.  

Nach meinem landschaftlichen Highlight fuhr ich zum Fisherman's Bar, wo ich ein herzhaftes Mittagessen genoss, bevor der Regen so richtig einsetzte. Mit vollem Magen und dem Wetter, das sich immer weiter verschlechterte, brauchte ich eine Pause – also verwandelte ich den Kofferraum meines Autos in eine gemütliche kleine Höhle, schaute ein paar Serien und entspannte, während der Regen aufs Dach prasselte.  

Irgendwann akzeptierte ich, dass der Regen nicht nachlassen würde. Statt mich mit den Elementen herumzuschlagen, liess ich die restlichen geplanten Stopps aus und fuhr direkt zu meinem Airbnb für die Nacht. Nicht jeder Tag in Irland kann perfektes Wetter bieten, aber mit Aussichten wie den Kerry Cliffs kann selbst der Regen die Magie nicht trüben.  

## Freitag: Carrauntoohil... Fast  
Heute sollte eigentlich Wandertag sein. Aber nach dem gestrigen Wolkenbruch und einer Wettervorhersage, die noch mehr Regen versprach, wachte ich mit dem Gedanken auf: „Vielleicht ist Wandern heute keine gute Idee.“ Beim Frühstück beäugte ich skeptisch den Himmel – es sah „okay“ aus, nicht grossartig, aber okay. Also schnappte ich mir vorsichtig optimistisch meine Ausrüstung und fuhr zum Cronin's Yard, dem Ausgangspunkt für die Wanderung auf den Carrauntoohil, Irlands höchsten Berg.  

{{< figure src="./images/PXL_20241025_100113634.PANO.jpg" alt="Wanderung" class="aligne-center" >}}  

Dort angekommen, wurde mir schnell klar, dass ich den Gipfel heute nicht erreichen würde. Der Gipfel verschwand irgendwo in den Wolken, und ich hatte keine Lust, in das neblige Ungewisse zu wandern. Aber der erste Teil des Weges sah machbar aus und bot wunderschöne Aussichten – also los. Das Wetter hielt gerade so lange, dass ich einen guten Zwischenstopp erreichte, von dem aus ich nach oben blickte und... nichts ausser Wolken sah. Das war mein Zeichen.  

{{< figure src="./images/PXL_20241025_103239516.jpg" alt="Wanderung" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241025_112829660.jpg" alt="Wanderung" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

Auf dem Rückweg liess ich mir Zeit, machte Fotos von den nebelverhangenen Bergen und genoss die Landschaft, bevor ich zurück zum Auto ging.  

Nach diesem Mini-Abenteuer checkte ich früh in mein B&B ein und gönnte mir eine wohlverdiente Auszeit. Das Abendessen war ein Genuss: Fish and Chips mit einem Pint Guinness, und zum Nachtisch ein frittierter Snickers. Kein Gipfelblick, aber für heute war es der perfekte Abschluss eines entspannten, regnerischen irischen Wandertages.  

{{< figure src="./images/PXL_20241025_163533655.jpg" alt="Meer" class="aligne-center size-medium" >}}  