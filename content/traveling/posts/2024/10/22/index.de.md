+++  
draft = false  
date = 2024-10-22T22:00:00+02:00  
title = "Cork"  
description = ""  
slug = ""  
authors = ["Jonas Burkhalter"]  
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Dienstag: Von Burgen zur Titanic  
{{< figure src="./images/PXL_20241022_094226856.jpg" alt="Burg" class="aligne-right size-medium" >}}  

Am Dienstag begann ein weiterer Abschnitt des Roadtrips, mit Kilkenny im Rückspiegel und dem Rock of Cashel vor mir. Ich legte dort eine kurze Pause ein, um die alten Ruinen zu erkunden und die beeindruckende mittelalterliche Atmosphäre aufzusaugen, bevor ich mich wieder auf den Weg machte. Nächster Stopp: Cobh – ein Ort voller Geschichte und... Fisch & Chips.  

{{< figure src="./images/PXL_20241022_135358010.jpg" alt="Cobh" class="aligne-left size-small" >}}  

Mein Weg führte mich zur Titanic Bar, direkt am Hafen, von dem aus die Titanic ihre letzte Reise antrat. Es war ein surreales Gefühl, Fisch & Chips an einem so geschichtsträchtigen Ort zu essen. Mit der Titanic in meinen Gedanken (und meinem Magen) konnte ich dem Titanic Experience-Tour nicht widerstehen.  

{{< figure src="./images/PXL_20241022_131343438.jpg" alt="Baupläne" class="aligne-right size-small" >}}  

Mein Boarding-Pass nannte mich Thomas McCornack, einen Drittklassenpassagier. Die Tour führte mich durch das Schiff und begann in der dritten Klasse, wo es – sagen wir mal – weniger glamourös zuging. Natürlich musste ich einen Blick in die erste Klasse werfen, um zu sehen, wie die andere Hälfte lebte, bevor ich miterlebte, was unausweichlich war: den Untergang. Nach all dem Drama wollte ich wissen, wie mein Schicksal ausging – überlebte "ich"? Es stellte sich heraus, dass Thomas einer der Glücklichen war! Nach einem kalten Bad im Atlantik wurde er von einem Rettungsboot aufgelesen. Ich fühlte tatsächlich einen kleinen Triumph, dass ich "überlebte" – selbst wenn es nur eine Nachstellung war.  

Nach dieser "Überlebens"-Erfahrung feierte ich mit einem Blondie und heisser Schokolade. Denn was könnte besser sein als Kuchen nach einer Titanic-Überfahrt?  

{{< figure src="./images/PXL_20241022_132950381.jpg" alt="Dessert" class="aligne-center size-medium" >}}  

Von dort aus machte ich mich auf den letzten Streckenabschnitt nach Kilbrittain, einem friedlichen kleinen Fleckchen auf dem Land, wo mein B&B auf mich wartete. Ich wurde mit der klassischen irischen Gastfreundschaft empfangen und verbrachte den Abend mit einer einfachen Mahlzeit aus Schinken und Käse, begleitet von einer Tasse Tee. Nach einem Tag voller Geschichte und Autofahren war die Ruhe auf dem Land genau das Richtige, um abzuschalten.  

## Mittwoch: Ein ruhiger Tag auf dem irischen Land  
{{< figure src="./images/PXL_20241023_065355223.jpg" alt="Morgen" class="aligne-left size-small" >}}  

Nach Tagen voller Sightseeing, Autofahrten und Titanic-Nacherzählungen war der Mittwoch ganz dem Entspannen gewidmet. Ich brauchte eine Pause von dem ständigen Strom neuer Eindrücke, und zum Glück war mein B&B in Kilbrittain der perfekte Ort, um innezuhalten.  

Der Tag begann mit einem fantastischen Frühstück – fluffige Scones, Joghurt, Obstsalat und natürlich Bohnen auf Toast (denn wenn man schon in Irland ist...). Nach diesem Festmahl nahm ich mir eine langsame Morgenroutine vor. Kein Stress, keine Pläne, nur dringend benötigte Entspannung.  

Später ging ich spazieren, um die Ruhe des ländlichen Umfelds zu geniessen. Die stillen Strassen und grünen Hügel waren genau das Richtige, um neue Energie zu tanken. Als ich zurückkam, verbrachte ich den Rest des Tages herrlich faul – las über Irlands Geschichte, schaute ein paar Serien und liess die Stunden vorbeiziehen.  

{{< figure src="./images/PXL_20241023_094815452.MP.jpg" alt="" class="aligne-left size-medium" >}}  
{{< figure src="./images/PXL_20241023_104842283.jpg" alt="Boot" class="aligne-right size-medium" >}}  
<div style="clear:both;"></div>  

Es war kein Tag voller Abenteuer, aber manchmal braucht man genau so einen Tag, um die Batterien wieder aufzuladen. Das war genau dieser Tag – und er war perfekt.  
