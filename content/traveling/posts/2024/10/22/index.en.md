+++ 
draft = false
date = 2024-10-22T22:00:00+02:00
title = "Cork"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Tuesday: From Castles to the Titanic
{{< figure src="./images/PXL_20241022_094226856.jpg" alt="castle" class="aligne-right size-medium" >}}

Tuesday marked another leg of the road trip, with Kilkenny in the rearview mirror and the Rock of Cashel ahead. I stopped there for a quick break and a stroll around the ancient ruins, soaking in the towering medieval vibes before hitting the road again. Next stop: Cobh, where the history—and the fish & chips—get serious.

{{< figure src="./images/PXL_20241022_135358010.jpg" alt="cobh" class="aligne-left size-small" >}}

I made my way to the Titanic Bar for lunch, right next to the port where the Titanic made its final departure. It felt surreal eating fish and chips with that kind of history lingering in the air. With the Titanic on my mind (and in my belly), I couldn’t resist doing the Titanic Experience tour.

{{< figure src="./images/PXL_20241022_131343438.jpg" alt="blueprints" class="aligne-right size-small" >}}

My boarding pass named me as Thomas McCornack, a third-class passenger. The tour took me through the ship, starting in third class, where things were—let's say—less glamorous. Naturally, I had to sneak a peek into first class to see how the other half lived before watching the inevitable happen: the sinking. After all the drama, it was time to find out my fate—did “I” survive? Turns out, Thomas was one of the lucky ones! After a chilly dip in the Atlantic, he got picked up by a lifeboat. Honestly, I felt a small sense of victory for making it through... even if it was just a reenactment.

Post-survival, I celebrated with a blondie and hot chocolate. Because what’s better than cake after surviving the Titanic?

{{< figure src="./images/PXL_20241022_132950381.jpg" alt="dessert" class="aligne-center size-medium" >}}

From there, I set off on the final stretch to Kilbrittain, a peaceful little spot in the countryside where my B&B awaited. I was welcomed with the classic Irish hospitality and spent the evening enjoying some simple ham and cheese, paired with a relaxing cup of tea. After a day filled with history and driving, the quiet countryside was the perfect place to unwind.

## Wednesday: A Slow Day in the Irish Countryside


{{< figure src="./images/PXL_20241023_065355223.jpg" alt="morning" class="aligne-left size-small" >}}

After days of sightseeing, driving, and Titanic reenactments, Wednesday was all about taking it easy. I needed a break from the constant stream of new impressions, and luckily, my B&B in Kilbrittain was the perfect place to hit pause.

The day started with an amazing breakfast—fluffy scones, yogurt, fruit salad, and, of course, beans on toast (because when in Ireland...). After that feast, I took relaxation to the next level with a slow morning. No rush, no plans, just some much-needed chill time.

Later, I went for a walk around the peaceful countryside, soaking in the calm after days of action. The quiet roads and green hills were exactly what I needed to reset. When I got back, the rest of the day was blissfully lazy—reading up on Ireland’s history, watching some series, and letting the hours drift by.

{{< figure src="./images/PXL_20241023_094815452.MP.jpg" alt="" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241023_104842283.jpg" alt="boat" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

It wasn’t a day packed with adventure, but sometimes you just need a day to do nothing but recharge. This was that day, and it was perfect.