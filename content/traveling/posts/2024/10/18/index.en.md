+++ 
draft = false
date = 2024-10-18T22:00:00+02:00
title = "Dublin"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "ireland"]
categories = ["traveling"]
externalLink = ""
series = ["traveling ireland"]
+++

## Friday: Lost in Left Lanes and Guinness Gains
{{< figure src="./images/PXL_20241018_111920087.jpg" alt="airplane" class="aligne-right size-medium" >}}

After a smooth train ride to Zurich airport and a flight to Dublin, I arrived ready to take on Ireland! Or so I thought. Picking up the rental car was easy enough… until I remembered the whole “driving on the left” thing. Add in a manual transmission, and navigating around Dublin suddenly felt like some sort of advanced brain teaser. Fun? Sure. Stressful? Absolutely. I arrived at the Airbnb in need of something stronger than tea.

After mentally rebooting, I decided a walk into the city would help. My first mission: get a bus ticket. Easy, right? Well, not when the Irish skies opened up and decided to drop what felt like all the rain in the world at once. On top of that, after a solid hour of wandering, I still had no bus ticket in hand. Excellent start.

Now, it was time for food and beer. But of course, I forgot about my usual travel struggle: decision paralysis. There were just too many options. Pub after pub, restaurant after restaurant. Menus blending together in a haze of Irish stew and craft beer names I couldn’t pronounce. I wandered in circles, overwhelmed and second-guessing every choice. It's like my brain short-circuits every time I travel—I start out starved but end up too anxious to actually sit down and eat anywhere.

I almost gave up and walked away from the buzzing bar scene when, finally, the travel gods took pity. I stumbled upon a cozy little bar. And let me tell you, nothing fixes a rough day quite like live music, Guinness stew, and a pint (or three) of the good stuff. By the time I left, life had righted itself again.

{{< figure src="./images/PXL_20241018_171327852.jpg" alt="stew" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241018_171029030.jpg" alt="guinness" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

The night ended on a win, with a freshly bought rechargeable bus card, and me feeling ready to conquer Ireland... just maybe not the left-side driving.

## Saturday: Irish Breakfasts, Bullet Holes, and Gaelic Football
Saturday started with a bus ride into Dublin’s city center and a determined hunt for breakfast. Turns out, every café in the city was packed, so I spent a good 20-30 minutes wandering before finally finding a perfect little spot just out of the main buzz. There, I treated myself to a full Irish breakfast—black pudding included—and the day was off to a solid start.

Then, Claire arrived! After nearly a year since we last saw each other, it felt like a reunion. I’d forgotten how much I enjoyed her company, and catching up felt just as good as the breakfast. With Claire leading the way, we spent the rest of the day exploring the city, and her historical insights made everything come alive.

{{< figure src="./images/PXL_20241019_095741284.jpg" alt="college" class="aligne-left size-medium" >}}

First stop: the GPO, where she casually pointed out bullet holes from the Easter Rising (as one does). From there, we hit all the highlights: Trinity College with its beautiful campus, a quick stroll through the buzzing Temple Bar area, and then Christ Church Cathedral and St. Patrick's Cathedral—though we admired those from the outside because, well, the list of things to see was long, and there were pubs waiting.

{{< figure src="./images/PXL_20241019_114805025.jpg" alt="park" class="aligne-right size-medium" >}}

On a whim, we stumbled into Marsh’s Library, this little hidden gem that felt like stepping into a different century. After absorbing some history, we made our way to St Stephen's Green and enjoyed the rare Irish sunshine for a while.

With the sightseeing box thoroughly checked, it was time for lunch and, naturally, a few pints at a cozy pub. We got so caught up in conversation and the great atmosphere that we almost forgot the sun was still shining outside.

The day didn’t end there, though. Claire had planned a trip to Croke Park for some Gaelic football, and I was all in. We watched Connacht take on Leinster, followed by Munster vs. Ulster, which since it is Claire's Team we both cheer for. The match was insane—especially in the last few minutes when both offense went into overdrive. The game ended in a nail-biting penalty finish, and Ulster clinched the win. I was officially hooked on Gaelic football.

{{< figure src="./images/PXL_20241019_183123876.MP.jpg" alt="football" class="aligne-left size-medium" >}}
{{< figure src="./images/Snapchat-513873387.jpg" alt="pub" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

After the excitement, we headed back to the pub for—yes—more beer. Laughing, reminiscing, and soaking up the last bits of the day, it was the perfect end to a day that combined history, sports, and reuniting with an old friend.

## Sunday: Whiskey, Guinness, and a Prison Tour
{{< figure src="./images/PXL_20241020_101221258.MP.jpg" alt="walk" class="aligne-right size-medium" >}}

Sunday started with a long, glorious sleep-in—exactly what I needed after Saturday’s adventures. Once I finally rolled out of bed, I decided to take a walk (about 50 minutes) to Kilmainham Gaol, Dublin's famous prison. But first, breakfast... or lunch, since by the time I got there, it was hard to tell which I needed more. After refueling, I was ready to dive into some Irish history—only to find that the next available prison tour wasn’t until 16:30. Well, so much for that plan.

No problem, I thought. Guinness Storehouse was nearby, so I headed there next, eager to start the tour. Except… they were fully booked too. At this point, it was becoming a theme. Fortunately, the Jameson Distillery was on the way, and they had space—whiskey saves the day!

{{< figure src="./images/PXL_20241020_115359487.jpg" alt="walk" class="aligne-left size-small" >}}

The Jameson tour started off strong, literally. With a glass of whiskey in hand, learning about the history and craftsmanship of Jameson became significantly more fun. By the time we reached the third part of the tour—a tasting session—I was officially an expert (or at least, I felt like one after a few sips).

{{< figure src="./images/PXL_20241020_141317556.jpg" alt="walk" class="aligne-right size-small" >}}

Whiskey confidence in tow, I headed back to the Guinness Storehouse to finally get that tour. It was a deep dive into the 4 essential ingredients, the fermentation process, and the magic that turns those simple elements into the black gold that is Guinness. After a small tasting, I made my way up to the Gravity Bar for the grand finale: a pint of Guinness with a stunning view over Dublin. Few things beat that combo.

By this point, I was finally ready for Kilmainham Gaol, so I headed back for the 16:30 tour. The history of the place was gripping, and our guide walked us through it with the perfect mix of humor and respect. We started in the chapel, where the song "Grace" came to life—this was where Joseph Plunkett married Grace Gifford just hours before his execution for his role in the 1916 Easter Rising. From there, we saw the cold, tiny cells where prisoners were held, and the stark courtyard where the leaders of the rebellion were executed by firing squad. The tour took us to the main gate, too, where public hangings once took place. It was both sobering and fascinating to hear how this place was entwined with Ireland's fight for independence.

{{< figure src="./images/PXL_20241020_160036302.jpg" alt="walk" class="aligne-center size-medium" >}}

After an intense day of whiskey, beer, and a healthy dose of history, I returned to the Airbnb for a much-needed relaxing evening. I packed up my things, ready to head off on the next leg of the adventure in the morning.
