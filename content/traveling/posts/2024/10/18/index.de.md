+++ 
draft = false
date = 2024-10-18T22:00:00+02:00
title = "Dublin"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Irland"]  
categories = ["Reisen"]  
externalLink = ""  
series = ["Reisen Irland"]  
+++

## Freitag: Verloren auf der linken Spur und Guinness als Belohnung
{{< figure src="./images/PXL_20241018_111920087.jpg" alt="Flughafen" class="aligne-right size-medium" >}}

Nach einer angenehmen Zugfahrt zum Flughafen Zürich und einem Flug nach Dublin kam ich bereit an, Irland zu erobern! Oder zumindest dachte ich das. Das Abholen des Mietwagens war einfach genug... bis ich an das „Fahren auf der linken Seite“ erinnerte. Und dann noch ein Schaltgetriebe dazu – plötzlich fühlte sich das Navigieren durch Dublin wie ein fortgeschrittenes Gehirntraining an. Spassig? Sicher. Stressig? Absolut. Ich kam im Airbnb an und brauchte dringend etwas Stärkeres als Tee.

Nach einem mentalen Neustart beschloss ich, dass ein Spaziergang in die Stadt helfen würde. Meine erste Mission: Ein Busticket besorgen. Einfach, oder? Nun, nicht wenn der irische Himmel sich öffnete und alles Regen auf einmal fallen liess. Nach einer soliden Stunde des Umherirrens hatte ich immer noch kein Busticket in der Hand. Ein ausgezeichneter Start.

Jetzt war es Zeit für Essen und Bier. Aber natürlich vergass ich meinen üblichen Reisekampf: Entscheidungsparalyse. Es gab einfach zu viele Optionen. Pub nach Pub, Restaurant nach Restaurant. Menüs, die sich in einem Nebel aus Irish Stew und Craft-Bier-Namen vermischten, die ich nicht aussprechen konnte. Ich lief im Kreis, überwältigt und zweifelnd an jeder Wahl. Es ist, als ob mein Gehirn jedes Mal einen Kurzschluss erleidet, wenn ich reise – ich starte hungrig, aber bin dann zu ängstlich, um mich wirklich irgendwo hinzusetzen und zu essen.

Ich wollte schon aufgeben und mich von der pulsierenden Barszene entfernen, als die Reisegötter endlich Mitleid hatten. Ich stolperte in eine gemütliche kleine Bar. Und lass mich dir sagen, nichts heilt einen anstrengenden Tag besser als Livemusik, Guinness-Stew und ein Pint (oder drei) vom guten Zeug. Als ich ging, hatte sich das Leben wieder gerichtet.

{{< figure src="./images/PXL_20241018_171327852.jpg" alt="Eintopf" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20241018_171029030.jpg" alt="Guinness" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Der Abend endete mit einem Gewinn – eine frisch gekaufte wiederaufladbare Buskarte und ich fühlte mich bereit, Irland zu erobern... nur vielleicht nicht das Fahren auf der linken Seite.

## Samstag: Irisches Frühstück, Kugellöcher und Gaelic Football
Der Samstag begann mit einer Busfahrt ins Stadtzentrum von Dublin und einer entschlossenen Suche nach Frühstück. Es stellte sich heraus, dass jedes Café in der Stadt überfüllt war, also verbrachte ich 20-30 Minuten mit Herumirren, bevor ich endlich einen perfekten kleinen Ort fand, etwas abseits vom Trubel. Dort gönnte ich mir ein vollständiges irisches Frühstück – mit schwarzem Pudding – und der Tag nahm einen soliden Anfang.

Dann kam Claire! Nach fast einem Jahr seit unserem letzten Treffen fühlte es sich wie ein Wiedersehen an. Ich hatte vergessen, wie sehr ich ihre Gesellschaft genoss, und das Wiedersehen war genauso gut wie das Frühstück. Mit Claire an meiner Seite verbrachten wir den Rest des Tages damit, die Stadt zu erkunden, und ihre historischen Einblicke liessen alles lebendig werden.

{{< figure src="./images/PXL_20241019_095741284.jpg" alt="College" class="aligne-left size-medium" >}}

Erster Halt: das GPO, wo sie beiläufig auf die Kugellöcher aus dem Osteraufstand hinwies (wie man das eben so macht). Danach besuchten wir die Höhepunkte: Trinity College mit seinem wunderschönen Campus, einen kurzen Spaziergang durch das lebendige Temple Bar-Viertel, und dann Christ Church Cathedral und St. Patrick's Cathedral – obwohl wir diese nur von aussen bewunderten, weil die Liste der Sehenswürdigkeiten lang war und Pubs warteten.

{{< figure src="./images/PXL_20241019_114805025.jpg" alt="Park" class="aligne-right size-medium" >}}

Auf gut Glück stolperten wir in Marsh’s Library, dieses kleine versteckte Juwel, das sich anfühlte, als würde man in ein anderes Jahrhundert eintauchen. Nachdem wir etwas Geschichte aufgesogen hatten, machten wir uns auf den Weg zum St Stephen's Green und genossen ein wenig das seltene irische Sonnenwetter.

Nachdem wir das Sightseeing-Programm gründlich abgehakt hatten, war es Zeit für ein Mittagessen und natürlich ein paar Pints in einem gemütlichen Pub. Wir waren so in Gespräche und die grossartige Atmosphäre vertieft, dass wir fast vergassen, dass draussen immer noch die Sonne schien.

Der Tag war aber noch nicht zu Ende. Claire hatte einen Ausflug nach Croke Park geplant, um Gaelic Football zu sehen, und ich war voll dabei. Wir sahen Connacht gegen Leinster, gefolgt von Munster gegen Ulster, für die Claire und ich beide die Daumen drückten. Das Spiel war wahnsinnig – besonders in den letzten Minuten, als beide Offensiven aufs Gas traten. Das Spiel endete in einem spannenden Elfmeterschiessen, und Ulster sicherte sich den Sieg. Ich war offiziell süchtig nach Gaelic Football.

{{< figure src="./images/PXL_20241019_183123876.MP.jpg" alt="Fussball" class="aligne-left size-medium" >}}
{{< figure src="./images/Snapchat-513873387.jpg" alt="Pub" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Nach der Aufregung gingen wir zurück in den Pub für – ja – noch mehr Bier. Lachen, Erinnerungen austauschen und die letzten Momente des Tages geniessen – es war der perfekte Abschluss eines Tages, der Geschichte, Sport und das Wiedersehen mit einer alten Freundin vereinte.

## Sonntag: Whiskey, Guinness und eine Gefängnistour
{{< figure src="./images/PXL_20241020_101221258.MP.jpg" alt="Spaziergang" class="aligne-right size-medium" >}}

Der Sonntag begann mit einem langen, herrlichen Ausschlafen – genau das, was ich nach den Abenteuern vom Samstag brauchte. Als ich endlich aus dem Bett rollte, beschloss ich, einen Spaziergang (ca. 50 Minuten) zum Kilmainham Gaol, dem berühmten Gefängnis von Dublin, zu machen. Aber zuerst Frühstück... oder Mittagessen, da es schwer zu sagen war, was ich jetzt mehr brauchte. Nach einer Stärkung war ich bereit, in die irische Geschichte einzutauchen – nur um festzustellen, dass die nächste verfügbare Gefängnistour erst um 16:30 Uhr begann. Naja, dann eben nicht.

Kein Problem, dachte ich mir. Das Guinness Storehouse war in der Nähe, also ging ich dorthin, um die Tour zu starten. Aber... sie waren auch ausgebucht. Zu diesem Zeitpunkt wurde es zu einer wiederkehrenden Theme. Glücklicherweise war die Jameson Distillery auf dem Weg, und sie hatten noch Platz – Whiskey rettet den Tag!

{{< figure src="./images/PXL_20241020_115359487.jpg" alt="Spaziergang" class="aligne-left size-small" >}}

Die Jameson-Tour begann stark, im wahrsten Sinne des Wortes. Mit einem Glas Whiskey in der Hand machte es deutlich mehr Spass, mehr über die Geschichte und Handwerkskunst von Jameson zu erfahren. Als wir den dritten Teil der Tour – eine Verkostung – erreichten, fühlte ich mich offiziell als Experte (oder zumindest, als ob ich es nach ein paar Schlücken war).

{{< figure src="./images/PXL_20241020_141317556.jpg" alt="Spaziergang" class="aligne-right size-small" >}}

Mit Whiskey-Vertrauen im Gepäck ging ich zurück zum Guinness Storehouse, um nun endlich diese Tour zu machen. Es war eine tiefgehende Entdeckung der 4 wesentlichen Zutaten, des Fermentationsprozesses und der Magie, die diese einfachen Elemente in das schwarze Gold, das Guinness ist, verwandelt. Nach einer kleinen Verkostung ging ich hinauf zur Gravity Bar für das grosse Finale: ein Pint Guinness mit einer atemberaubenden Aussicht auf Dublin. Wenige Dinge schlagen diese Kombination.

Zu diesem Zeitpunkt war ich endlich bereit für die Kilmainham Gaol-Tour, also machte ich mich auf den Weg zurück zur 16:30-Uhr-Tour. Die Geschichte des Ortes war packend, und unser Guide führte uns mit der perfekten Mischung aus Humor und Respekt durch. Wir begannen in der Kapelle, wo das Lied "Grace" lebendig wurde – hier heirateten Joseph Plunkett und Grace Gifford nur Stunden vor seiner Hinrichtung aufgrund seiner Rolle im Osteraufstand von 1916. Danach sahen wir die kalten, winzigen Zellen, in denen Gefangene gehalten wurden, und den düsteren Innenhof, in dem die Anführer des Aufstands von einem Erschiessungskommando exekutiert wurden. Die Tour führte uns auch zum Haupttor, wo früher öffentliche Hängungen stattfanden. Es war sowohl bewegend als auch faszinierend zu hören, wie dieser Ort mit dem Unabhängigkeitskampf Irlands verbunden war.

{{< figure src="./images/PXL_20241020_160036302.jpg" alt="Spaziergang" class="aligne-center size-medium" >}}

Nach einem intensiven Tag mit Whiskey, Bier und einer ordentlichen Portion Geschichte kehrte ich ins Airbnb zurück für einen dringend benötigten entspannten Abend. Ich packte meine Sachen und war bereit, am nächsten Morgen zum nächsten Teil des Abenteuers aufzubrechen.
