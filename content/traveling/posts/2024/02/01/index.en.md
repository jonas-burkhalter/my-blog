+++ 
draft = false
date = 2024-02-01T22:00:00+02:00
title = "Road Trip (4/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["traveling", "New Zealand"]
categories = ["traveling"]
externalLink = ""
series = ["traveling New Zealand"]
+++

## Thursday: From Sea to Land
<!--
Ferry from picton to Wellington
Drive to mangaweka
-->
Our Thursday began with a ferry ride back from Picton to Wellington, the journey across the Cook Strait offering stunning views of the surrounding seascape. The gentle sway of the ferry and the crisp ocean breeze made for a serene start to our day.

Once we arrived in Wellington, we set off on a drive to Mangaweka. The road trip took us through diverse landscapes, from bustling city streets to tranquil countryside. The changing scenery kept us captivated throughout the journey.

As we reached Mangaweka, we found a quaint spot to camp, nestled in the natural beauty of the area. 

## Friday: Alpine Adversity and Riverside Respite
<!--
Drive to Tongariro Alpine Crossing
Alpine crossing really bad weather only ...
Drive to 
Hipapatua Recreational Reserve Camping
Swim in the river
-->

{{< figure src="./images/IMG_20240202_110255.jpg" alt="camping" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_20240202_105214.jpg" alt="camping" class="aligne-right size-small" >}}

Our Friday began with high hopes as we drove to the Tongariro Alpine Crossing, excited to tackle one of New Zealand's most famous treks. However, upon arrival, we were met with truly bad weather—thick clouds, heavy rain, and strong winds—making the crossing impossible.

Undeterred, we adjusted our plans and continued our journey to Hipapatua Recreational Reserve Camping. The drive provided a welcome change of scenery, and our spirits lifted as we arrived at the picturesque campsite.

Eager to shake off the morning's disappointment, we headed straight for the river. A refreshing swim in the cool waters was just what we needed to rejuvenate ourselves. 

{{< figure src="./images/IMG_20240202_105525.jpg" alt="camping" class="aligne-center size-medium" >}}

## Saturday: Soothing Waters and Camping Comfort
<!--
Drive to rotorua

Relax at Polynesian Spa 

Go to camping
Rotorua Family Holiday Park
-->
We began our Saturday with a drive to Rotorua, the geothermal heart of New Zealand. The journey was filled with anticipation for the relaxation that awaited us.

Upon arriving in Rotorua, we headed straight to the Polynesian Spa. The soothing, mineral-rich waters provided the perfect remedy for our travel-weary bodies. We luxuriated in the warm pools, letting the therapeutic waters melt away our stress and rejuvenate our spirits.

After a blissful afternoon at the spa, we made our way to the Rotorua Family Holiday Park. Setting up camp, we enjoyed the comfortable amenities and friendly atmosphere of the park, appreciating the blend of relaxation and adventure that defined our day. 

## Sunday: From Papamoa to Pahoia
<!--
Drive to papamoa 
All campsites full/closed

Go to the beach 

Drive to Pahoia Beachstay - Back to Basics
Lucky amazing campsite

Enjoy sunset
-->
Our Sunday started with a drive to Papamoa, hoping to find a campsite near the beautiful beach. Unfortunately, all campsites were either full or closed, leaving us to rethink our plans.

Undeterred, we decided to make the most of our time and headed to the beach for a relaxing break. The soft sand and gentle waves provided a perfect backdrop for a few hours of unwinding and soaking up the sun.



{{< figure src="./images/IMG_20240204_203000.jpg" alt="camping" class="aligne-left size-medium" >}}
<!-- {{< figure src="./images/IMG_8796.JPG" alt="camping" class="aligne-left size-medium" >}} -->

Determined to find a place to stay, we drove to Pahoia Beachstay - Back to Basics. Luck was on our side as we discovered an amazing, tranquil campsite nestled in nature. As the day drew to a close, we were treated to a breathtaking sunset, the perfect end to an unexpected adventure. With the sky painted in hues of orange and pink, we felt grateful for the day's twists and turns that led us to this serene spot.

{{< figure src="./images/IMG_20240204_200705.jpg" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240204_201832.jpg" alt="camping" class="aligne-right size-medium" >}}


{{< figure src="./images/IMG_20240204_204634.jpg" alt="camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240204_225903.jpg" alt="camping" class="aligne-right size-medium" >}}


## Monday: Final Drive and Farewell
<!--
Drive to Waharau Blackberry Flats Campground

Pack everything up
-->
{{< figure src="./images/IMG_20240205_060910.jpg" alt="camping" class="aligne-center size-medium" >}}

Our Monday began with a drive to Waharau Blackberry Flats Campground, the road taking us through scenic landscapes and serene countryside.

{{< figure src="./images/IMG_20240205_175138.jpg" alt="camping" class="aligne-left size-smaller" >}}

Upon arriving, we set about packing everything up, reflecting on the incredible journey we've had. As we finished packing, we took a moment to appreciate the peaceful surroundings of Blackberry Flats Campground. 

## Tuesday: Wrapping Up in Auckland
<!--
Drive to auckland
Bring back the van

Eat avocado toast

Barber

Enjoy last evening
-->
Our Tuesday began with a drive back to Auckland, a bittersweet journey as we prepared to wrap up our adventure. Upon arrival, we returned the van that had been our trusty companion throughout the trip, marking the end of our road journey.

Feeling a bit nostalgic, we treated ourselves to a delicious avocado toast. Next, I visited a barber for a fresh haircut.

We spent our last evening in Auckland enjoying the city one final time, soaking in the vibrant atmosphere and reflecting on our unforgettable journey. As night fell, we felt a mix of contentment and anticipation for the next chapter, grateful for the memories and the adventures that had made this trip so special.

## Wednesday & Thursday: Homeward Bound
<!--
Get breakfast 
Hang around in a coffee
Go to the airport 
Flight to doha (17h)
Flight to zürich (6h)
Back home😢
-->
Our Wednesday morning began with a leisurely breakfast, savoring our final meal in Auckland. Afterward, we lingered at a cozy coffee shop, enjoying one last dose of caffeine and soaking up the city’s ambiance.

As the time to leave approached, we made our way to the airport, ready for the long journey home. Our flight to Doha was a lengthy 17 hours, followed by a 6-hour flight to Zürich.
