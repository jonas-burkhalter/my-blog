+++ 
draft = false
date = 2024-02-01T22:00:00+02:00
title = "Road Trip (4/4)"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Reisen", "Neuseeland"]
categories = ["Reisen"]
externalLink = ""
series = ["Reisen Neuseeland"]
+++

## Donnerstag: Von Meer zu Land
<!--
Fähre von Picton nach Wellington
Fahrt nach Mangaweka
-->
Unser Donnerstag begann mit einer Fährfahrt von Picton nach Wellington. Die Überfahrt über die Cook Strait bot atemberaubende Ausblicke auf die umliegende Meereslandschaft. Das sanfte Schaukeln der Fähre und die frische Meeresbrise sorgten für einen ruhigen Start in den Tag.

In Wellington angekommen, machten wir uns auf den Weg nach Mangaweka. Die Fahrt führte uns durch abwechslungsreiche Landschaften, von lebhaften Stadtstrassen bis hin zu ruhigen ländlichen Gegenden. Die sich ständig ändernde Szenerie hielt uns während der gesamten Reise in Bann.

In Mangaweka angekommen, fanden wir einen idyllischen Campingplatz, eingebettet in die natürliche Schönheit der Umgebung.

## Freitag: Alpenwidrigkeiten und Flussidylle
<!--
Fahrt zum Tongariro Alpine Crossing
Alpine Crossing wirklich schlechtes Wetter ...
Fahrt zum Hipapatua Recreational Reserve Camping
Schwimmen im Fluss
-->

{{< figure src="./images/IMG_20240202_110255.jpg" alt="Camping" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_20240202_105214.jpg" alt="Camping" class="aligne-right size-small" >}}

Am Freitag starteten wir voller Vorfreude und fuhren zum Tongariro Alpine Crossing, um eine der berühmtesten Wanderungen Neuseelands zu unternehmen. Doch das Wetter machte uns einen Strich durch die Rechnung – dichte Wolken, starker Regen und heftige Winde machten die Überquerung unmöglich.

Unbeirrt änderten wir unsere Pläne und fuhren weiter zum Hipapatua Recreational Reserve Camping. Die Fahrt bot eine willkommene Abwechslung, und unsere Stimmung besserte sich, als wir an dem malerischen Campingplatz ankamen.

Um den Frust des Morgens abzuschütteln, sprangen wir sofort in den Fluss. Ein erfrischendes Bad in den kühlen Gewässern war genau das Richtige, um unsere Energien wieder aufzuladen.

{{< figure src="./images/IMG_20240202_105525.jpg" alt="Camping" class="aligne-center size-medium" >}}

## Samstag: Wohltuende Gewässer und Campingkomfort
<!--
Fahrt nach Rotorua
Entspannen im Polynesian Spa
Camping im Rotorua Family Holiday Park
-->
Unser Samstag begann mit einer Fahrt nach Rotorua, dem geothermischen Herzen Neuseelands. Die Fahrt war von Vorfreude auf die bevorstehende Entspannung geprägt.

In Rotorua angekommen, steuerten wir direkt den Polynesian Spa an. Die wohltuenden, mineralreichen Thermalbecken waren das perfekte Heilmittel für unsere reisegeplagten Körper. Wir liessen uns in den warmen Becken treiben und spürten, wie die heilenden Wasser uns neue Energie schenkten.

Nach einem erholsamen Nachmittag im Spa fuhren wir weiter zum Rotorua Family Holiday Park. Dort richteten wir unser Lager ein und genossen die komfortablen Annehmlichkeiten und die freundliche Atmosphäre des Parks – eine gelungene Mischung aus Entspannung und Abenteuer.

## Sonntag: Von Papamoa nach Pahoia
<!--
Fahrt nach Papamoa
Alle Campingplätze voll/geschlossen
Strandbesuch
Fahrt zum Pahoia Beachstay - Back to Basics
Glück, toller Campingplatz
Sonnenuntergang geniessen
-->
Unser Sonntag begann mit einer Fahrt nach Papamoa, in der Hoffnung, einen Campingplatz in Strandnähe zu finden. Leider waren alle Plätze entweder voll oder geschlossen, sodass wir unsere Pläne umstellen mussten.

Unbeirrt nutzten wir die Gelegenheit, eine Pause am Strand einzulegen. Der weiche Sand und die sanften Wellen boten die perfekte Kulisse, um ein paar entspannte Stunden zu verbringen und die Sonne zu geniessen.

{{< figure src="./images/IMG_20240204_203000.jpg" alt="Camping" class="aligne-left size-medium" >}}
<!-- {{< figure src="./images/IMG_8796.JPG" alt="Camping" class="aligne-left size-medium" >}} -->

Entschlossen, einen Platz zum Übernachten zu finden, fuhren wir weiter zum Pahoia Beachstay - Back to Basics. Mit etwas Glück entdeckten wir einen fantastischen, ruhigen Campingplatz, der mitten in der Natur lag. Als der Tag zu Ende ging, wurden wir mit einem atemberaubenden Sonnenuntergang belohnt – ein perfekter Abschluss für ein unerwartetes Abenteuer. Mit dem Himmel in Orangetönen und Rosa gefärbt, waren wir dankbar für die Wendungen des Tages, die uns an diesen friedlichen Ort geführt hatten.

{{< figure src="./images/IMG_20240204_200705.jpg" alt="Camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240204_201832.jpg" alt="Camping" class="aligne-right size-medium" >}}

{{< figure src="./images/IMG_20240204_204634.jpg" alt="Camping" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240204_225903.jpg" alt="Camping" class="aligne-right size-medium" >}}

## Montag: Abschiedsfahrt und Rückblick
<!--
Fahrt zum Waharau Blackberry Flats Campground
Alles einpacken
-->
{{< figure src="./images/IMG_20240205_060910.jpg" alt="Camping" class="aligne-center size-medium" >}}

Unser Montag begann mit einer Fahrt zum Waharau Blackberry Flats Campground, eine Route, die uns durch malerische Landschaften und ruhige Gegenden führte.

{{< figure src="./images/IMG_20240205_175138.jpg" alt="Camping" class="aligne-left size-smaller" >}}

Dort angekommen, begannen wir, alles zusammenzupacken und liessen dabei die unglaublichen Erlebnisse unserer Reise Revue passieren. Nachdem wir fertig waren, genossen wir einen Moment der Ruhe und nahmen die friedliche Umgebung des Blackberry Flats Campgrounds in uns auf.

## Dienstag: Abschied in Auckland
<!--
Fahrt nach Auckland
Wohnmobil zurückgeben
Avocado-Toast essen
Friseur
Letzten Abend geniessen
-->
Am Dienstag fuhren wir zurück nach Auckland – eine bittersüsse Fahrt, da sich unser Abenteuer langsam dem Ende zuneigte. In Auckland angekommen, gaben wir unser treues Wohnmobil zurück, das uns auf der ganzen Reise begleitet hatte.

Um den Abschied zu versüssen, gönnten wir uns ein köstliches Avocado-Toast. Danach ging ich zum Friseur, um mir einen frischen Haarschnitt zu holen.

Den letzten Abend in Auckland verbrachten wir damit, die Stadt ein letztes Mal zu geniessen, die lebendige Atmosphäre auf uns wirken zu lassen und über die unvergessliche Reise nachzudenken. Als die Nacht hereinbrach, waren wir erfüllt von Zufriedenheit und Vorfreude auf das nächste Kapitel, dankbar für die Erinnerungen und Abenteuer dieser Reise.

## Mittwoch & Donnerstag: Heimwärts
<!--
Frühstück
In einem Café entspannen
Zum Flughafen fahren
Flug nach Doha (17h)
Flug nach Zürich (6h)
Zurück zu Hause😢
-->
Unser Mittwoch begann mit einem gemütlichen Frühstück – das letzte in Auckland. Anschliessend verbrachten wir die Zeit in einem gemütlichen Café, genossen eine letzte Tasse Kaffee und liessen die Atmosphäre der Stadt auf uns wirken.

Als es Zeit wurde, machten wir uns auf den Weg zum Flughafen, bereit für die lange Heimreise. Der Flug nach Doha dauerte 17 Stunden, gefolgt von einem weiteren 6-stündigen Flug nach Zürich. 