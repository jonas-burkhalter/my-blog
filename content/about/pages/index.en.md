+++ 
draft = false
date = 1997-07-31T00:00:00+02:00
title = "About me"
slug = "" 
+++

## Hobbies

### Floorball

{{< figure src="./images/floorball.jpg" alt="floorball team" class="aligne-right size-larger">}}

Floorball fuels my competitive spirit and brings me immense joy. The fast-paced nature of the game, the teamwork, and the exhilaration of scoring goals ignite a fire within me. Whether it's a friendly scrimmage or a high-stakes league match, I thrive on the energy and camaraderie that floorball cultivates.

https://biberistaktiv.ch/unihockey/

### Climbing

{{< figure src="./images/climbing.jpg" alt="climbing in the snow" class="aligne-left size-small" >}}

When I'm not on the floorball field, you can often find me scaling rock walls and embracing the challenges of climbing and bouldering. There's an indescribable thrill in conquering difficult routes, relying on strength, agility, and mental focus. Climbing has taught me invaluable life lessons about resilience, perseverance, and pushing past my limits. It's a transformative journey that constantly shapes my character and ignites a sense of adventure within me.

## Profession

{{< figure src="./images/software_architectur.png" alt="draw io diagram" class="aligne-right size-larger" >}}

As a software developer, I thrive on the creative process of transforming ideas into functional applications. With a keen eye for detail and a deep understanding of software architecture principles, I excel at designing scalable and maintainable systems. I believe that the architecture of a software solution is the backbone that ensures its stability, performance, and long-term success.

In my fullstack development journey, I relish the opportunity to work on both the frontend and backend aspects of an application. Whether it's crafting an intuitive user interface or developing powerful logic, I enjoy the versatility and challenges that come with being a fullstack developer. This allows me to have a holistic view of the entire software development process and deliver end-to-end solutions.

[More details about my career](/en/about/work)