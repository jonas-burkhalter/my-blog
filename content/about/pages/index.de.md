+++ 
draft = false
date = 1997-07-31T00:00:00+02:00
title = "Über mich"
slug = "" 
+++

## Hobbys

### Unihockey

{{< figure src="./images/floorball.jpg" alt="Unihockey-Team" class="aligne-right size-larger">}}

Unihockey weckt meinen Wettkampfgeist und bereitet mir immense Freude. Die schnelle Spielweise, das Teamwork und der Nervenkitzel, Tore zu schiessen, entfachen ein Feuer in mir. Ob bei einem freundschaftlichen Spiel oder einem spannenden Ligamatch, ich blühe auf durch die Energie und die Kameradschaft, die Unihockey fördert.

https://biberistaktiv.ch/unihockey/

### Klettern

{{< figure src="./images/climbing.jpg" alt="Klettern im Schnee" class="aligne-left size-small" >}}

Wenn ich nicht auf dem Unihockeyfeld bin, findet man mich oft dabei, Felsen zu erklimmen und die Herausforderungen des Kletterns und Boulderns zu meistern. Es gibt ein unbeschreibliches Gefühl, schwierige Routen zu überwinden, sich auf Stärke, Beweglichkeit und mentale Fokussierung zu verlassen. Klettern hat mir wertvolle Lebenslektionen über Resilienz, Ausdauer und das Überwinden meiner eigenen Grenzen beigebracht. Es ist eine transformative Reise, die ständig meinen Charakter prägt und ein Gefühl von Abenteuer in mir weckt.

## Beruf

{{< figure src="./images/software_architectur.png" alt="Diagramm von Draw.io" class="aligne-right size-larger" >}}

Als Softwareentwickler blühe ich im kreativen Prozess auf, Ideen in funktionale Anwendungen zu verwandeln. Mit einem scharfen Blick für Details und einem tiefen Verständnis der Prinzipien der Softwarearchitektur bin ich darauf spezialisiert, skalierbare und wartbare Systeme zu entwerfen. Ich glaube, dass die Architektur einer Softwarelösung das Rückgrat ist, das ihre Stabilität, Leistung und langfristigen Erfolg gewährleistet.

In meiner Arbeit als Fullstack-Entwickler geniesse ich die Möglichkeit, sowohl an den Frontend- als auch an den Backend-Aspekten einer Anwendung zu arbeiten. Ob beim Erstellen einer intuitiven Benutzeroberfläche oder beim Entwickeln leistungsstarker Logik, ich schätze die Vielseitigkeit und die Herausforderungen, die mit der Arbeit als Fullstack-Entwickler verbunden sind. Dies ermöglicht mir, einen ganzheitlichen Blick auf den gesamten Softwareentwicklungsprozess zu haben und End-to-End-Lösungen zu liefern.

[Weiter Details zu meiner Laufbahn](/de/about/work)