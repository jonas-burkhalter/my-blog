+++  
draft = false  
date = 2013-08-01T00:00:00+02:00  
title = "Berufserfahrung"
slug = ""  
+++  

## Senn/Hirte auf der Alp Hannig
{{< figure src="./images/logo_hannig_alp.png" alt="Logo" class="aligne-right size-smaller" >}}
**Zeitraum**: Juni 2025 - September 2025</br>
**Ort**: Saas-Fee, Schweiz</br>
[Einige Posts aus dieser Zeit](/de/series/alp-hannig/)

## Softwareingenieur bei Soobr - smart cleaning
{{< figure src="./images/logo_soobr.png" alt="Logo" class="aligne-right size-smaller" >}}
**Zeitraum**: August 2021 - März 2025</br>
**Ort**: Ittigen, Schweiz</br>
[Einige Posts aus dieser Zeit](/de/series/soobr/)

Als Softwareingenieur bei Soobr habe ich an innovativen Lösungen für die Smart-Cleaning-Branche gearbeitet. Mein Aufgabenbereich umfasst die Entwicklung und Wartung von Software-Systemen, die Reinigungsprozesse optimieren. Zu den Schlüsseldiensttechnologien, die ich eingesetzt habe, gehören:

- **Java**: Entwicklung robuster und skalierbarer Softwarelösungen mit Fokus auf Performance und Wartbarkeit.  
- **Spring Boot**: Erstellung von Microservices und Backend-Lösungen mit dem Spring Boot Framework.  
- **React.js**: Entwicklung dynamischer und reaktiver Benutzeroberflächen für Webanwendungen.  
- **Softwarearchitektur**: Gestaltung von Software-Systemen mit skalierbarer und wartbarer Architektur.  
- **Containerisierung**: Nutzung von Container-Technologien wie Docker zur Bereitstellung und Verwaltung von Anwendungen.  
- **Google Cloud Platform (GCP)**: Nutzung von Cloud-Diensten für skalierbare und zuverlässige Anwendungsbereitstellung.  
- **SQL**: Verwaltung und Abfrage relationaler Datenbanken für effizientes Datenhandling.  
- **GitLab**: Verwaltung von CI/CD-Pipelines und Versionskontrolle mit GitLab.  
- **Git**: Zusammenarbeit am Code mithilfe von Versionskontrolle zur Sicherstellung effizienter Teamarbeitsprozesse.

Diese Rolle hat mir eine Plattform geboten, technische Fähigkeiten mit branchenspezifischem Wissen zu kombinieren, um effiziente und skalierbare Lösungen voranzutreiben.

## Zivildienst Gorneren Alp
{{< figure src="./images/logo_gorneren_alp.png" alt="Logo" class="aligne-right size-smaller" >}}
**Zeitraum**: Juli 2019 - August 2019</br>
**Ort**: Kiental, Schweiz</br>
[Einige Posts aus dieser Zeit](/de/series/zivildienst-gorneren-alp/)

## Anwendungsentwickler bei Nowhow Solutions AG  
{{< figure src="./images/logo_nowhow.png" alt="Logo" class="aligne-right size-smaller" >}}
**Zeitraum**: Juni 2018 - Juli 2021</br>
**Ort**: Bern, Schweiz

Während meiner Zeit bei Nowhow Solutions AG habe ich als Anwendungsentwickler mitgewirkt, der sich auf Folgendes konzentriert hat:

- **Vue.js**: Nutzung dieses JavaScript-Frameworks zur Erstellung dynamischer und benutzerfreundlicher Webanwendungen.  
- **SQL**: Vertiefung meiner Datenbankkenntnisse.  
- **C#**: Entwicklung von Anwendungen mit soliden objektorientierten Prinzipien und effizientem Code.  
- **Entity Framework**: Nutzung dieses ORM-Werkzeugs für effiziente und skalierbare Datenbankinteraktionen.  
- **.NET Framework**: Arbeit an Anwendungen mit der .NET-Plattform zur Erstellung sicherer und wartbarer Lösungen.  
- **Git**: Verwaltung der Versionskontrolle für kollaborative Projekte mit Fokus auf Branching- und Merging-Strategien.

Diese Rolle hat mir ermöglicht, Lösungen zu entwickeln, die auf die Bedürfnisse der Kunden abgestimmt sind, und mein Verständnis für Front-End-Frameworks und Full-Stack-Entwicklung zu vertiefen.

## Ausbildung Anwendungsentwickler bei Mitel Schweiz  
{{< figure src="./images/logo_mitel.png" alt="Logo" class="aligne-right size-smaller" >}}
**Zeitraum**: August 2013 - Juli 2017</br>
**Ort**: Solothurn, Schweiz

Meine Lehrjahre als IT-Spezialist bei Mitel waren entscheidend für meine Karriereentwicklung. Während dieser Zeit:

- **HTML & CSS**: Erstellung von Web-Seiten und Benutzeroberflächen mit responsivem Design, um Usability und Zugänglichkeit sicherzustellen.  
- **C#**: Entwicklung von Anwendungen mit Fokus auf objektorientierte Programmierprinzipien und robuster Anwendungsentwicklung.  
- **SQL**: Erwerb praktischer Erfahrungen mit Datenbankoperationen, einschliesslich Abfrage und Verwaltung relationaler Datenbanken, um Datenintegrität und Performance sicherzustellen.  
- **Git**: Erlernen von Versionskontrolltechniken für kollaborative und effiziente Projektmanagement, einschliesslich Branching- und Merging-Strategien.

Diese Erfahrung hat ein umfassendes Verständnis von IT-Systemen und Programmierung vermittelt und die Grundlage für meine zukünftigen Rollen gelegt.

# Ausbildung

## Berner Fachhochschule
**Zeitraum**: September 2018 - Februar 2023</br>
**Abschluss**: Bachelor of Science in Informatik</br>
**Schwerpunkt**: Internet of Things (IoT)

- Vertiefung in vernetzten Systemen, Cloud Computing und IoT-Entwicklung.
- Projektarbeit im Bereich IoT zur Verbindung von Geräten und datengetriebenen Lösungen.
- Entwicklung von Softwaresystemen und Anwendung moderner Technologien.

## Gewerblich-Industrielle Berufsschule Bern
**Zeitraum**: August 2013 - Juli 2017</br>
**Abschluss**: Informatiker EFZ, Fachrichtung Applikationsentwicklung

- Ausbildung in objektorientierter Programmierung, Webtechnologien (HTML, CSS, JavaScript).
- Praxisnähe mit relationalen Datenbanken und SQL.
- Grundlagen in Git für Versionskontrolle und Zusammenarbeit im Team.
