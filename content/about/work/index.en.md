+++ 
draft = false
date = 2013-08-01T00:00:00+02:00
title = "Professional Experience"
slug = "" 
+++

## Senn/Shepherd on the Alp Hannig
{{< figure src="./images/logo_hannig_alp.png" alt="Logo" class="aligne-right size-smaller" >}}
**Dates**: June 2025 - September 2025</br>
**Location**: Saas-Fee, Switzerland</br>
[Some posts from this time](/en/series/alp-hannig/)

## Softwareingenieur at Soobr - smart cleaning
{{< figure src="./images/logo_soobr.png" alt="Logo" class="aligne-right size-smaller" >}}
**Dates**: August 2021 - March 2025</br>
**Location:** Ittigen, Switzerland</br>
[Some posts from this time](/en/series/soobr/)

As a Softwareingenieur at Soobr, I have been working on innovative solutions for the smart cleaning industry. My role involves developing and maintaining software systems that streamline cleaning processes. Key technologies I have utilized include:

- **Java**: Building robust and scalable software solutions with a focus on performance and maintainability.
- **Spring Boot**: Creating microservices and backend solutions using the Spring Boot framework.
- **React.js**: Developing dynamic and responsive user interfaces for web applications.
- **Softwarearchitectur**: Designing software systems with scalable and maintainable architecture.
- **Containerization**: Utilizing container technologies like Docker to deploy and manage applications.
- **Google Cloud Platform (GCP)**: Leveraging cloud services for scalable and reliable application deployment.
- **SQL**: Managing and querying relational databases for efficient data handling.
- **GitLab**: Managing CI/CD pipelines and version control with GitLab.
- **Git**: Collaborating on code using version control to ensure efficient team workflows.

This role has provided a platform to combine technical skills with industry-specific knowledge to drive an efficient and scalable solution.

## Community service on the Gorneren Alp
{{< figure src="./images/logo_gorneren_alp.png" alt="Logo" class="aligne-right size-smaller" >}}
**Zeitraum**: Juli 2019 - August 2019</br>
**Ort**: Kiental, Switzerland</br>
[Some posts from this time](/en/series/community-service-gorneren-alp/)

## Application developer at Nowhow Solutions AG
**Dates:**: June 2018 - July 2021</br>
**Location:** Bern, Switzerland

During my time at Nowhow Solutions AG, I contributed as an application developer, focusing on:

- **Vue.js**: Leveraging this JavaScript framework for creating dynamic and user-friendly web applications.
- **SQL**: Continuing to strengthen my database expertise.
- **C#**: Developing applications with solid object-oriented principles and efficient code.
- **Entity Framework**: Utilizing this ORM tool for efficient and scalable database interactions.
- **.NET Framework**: Working on applications using the .NET platform to build secure and maintainable solutions.
- **Git**: Managing version control for collaborative projects, with a focus on branching and merging strategies.

This role allowed me to deliver solutions tailored to client needs while deepening my understanding of front-end frameworks and full-stack development.

## Apprenticeship application developer at Mitel Switzerland
**Dates:** August 2013 - July 2017</br>
**Location:** Solothurn, Switzerland

My foundational years as a apprentice IT specialist at Mitel were instrumental in shaping my career. During this period:

- **HTML & CSS**: I built web pages and user interfaces with responsive design principles, ensuring usability and accessibility.
- **C#**: I developed applications with a focus on object-oriented programming principles and robust application development.
- **SQL**: I gained hands-on experience with database operations, including querying and managing relational databases, ensuring data integrity and performance.
- **Git**: I learned version control techniques for collaborative and efficient project management, including branching and merging strategies.

This experience provided a comprehensive understanding of IT systems and programming, forming the groundwork for my future roles.


# Education

## Berner Fachhochschule
**Dates**: September 2018 - February 2023</br>
**Degree**: Bachelor of Science in Computer Science</br>
**Focus**: Internet of Things (IoT)

- Specialization in connected systems, cloud computing, and IoT development.  
- Project work in IoT for connecting devices and data-driven solutions.  
- Development of software systems and application of modern technologies.

## Gewerblich-Industrielle Berufsschule Bern
**Dates**: August 2013 - July 2017</br>
**Degree**: IT Specialist EFZ, Focus on Application Development

- Training in object-oriented programming, web technologies (HTML, CSS, JavaScript).  
- Practical experience with relational databases and SQL.  
- Basics of Git for version control and teamwork collaboration.  