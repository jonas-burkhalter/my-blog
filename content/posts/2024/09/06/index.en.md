+++ 
draft = false
date = 2024-09-06T22:00:00+02:00
title = "Creux du Van"
description = "Getaway with Noë"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["hiking"]
categories = ["hiking"]
externalLink = ""
series = []
+++
We traded the usual routine for a quick escape, hopping on the train from Solothurn to Noiraigue. Our plan was simple: spend some time together, hike, and making the most of a cozy treehouse stay. With bags packed, we set off, energized and ready for Creux du Van.

{{< figure src="./images/PXL_20240906_101608030.jpg" alt="hiking" class="aligne-left size-small" >}}

From the start, the [trail](https://map.schweizmobil.ch/?lang=de&photos=yes&logo=yes&detours=yes&season=summer&bgLayer=pk&resolution=7.8&E=2546232&N=1199292&layers=Wanderland&trackId=1493161751) was exactly what we hoped for—beautiful, with just the right balance of quiet and curiosity. Most of the other hikers branched off onto different trails early on, so it felt like we had the whole path to ourselves. We hiked along, in that blissful zone where every step just feels easy. The incline crept up gradually, but it was more invigorating than challenging. We stopped here and there, taking in the sounds of nature, a few casual photos, and of course, appreciating some snacks.

{{< figure src="./images/WhatsApp Image 2024-11-24 at 20.28.36.jpg" alt="hiking" class="aligne-right size-smaller" >}}

Eventually, we reached the top and were rewarded with an unbelievable view. The cliffs of Creux du Van curve around you in this perfect amphitheater of rock, stretching down into the valleys and rolling hills below. We found a spot near the edge, sat down with our lunch, and let ourselves be awestruck for a while. It’s hard to feel anything but small (in the best way) when you’re surrounded by scenery like that.


{{< figure src="./images/PXL_20240906_112255588.jpg" alt="creux du van" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20240906_114204551.jpg" alt="creux du van" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

{{< figure src="./images/PXL_20240906_113150252.jpg" alt="creux du van" class="aligne-right size-smaller" >}}
After lunch, we followed the cliffside trail, feeling pretty lucky to have clear skies and a peaceful afternoon. Making our way down the other side, we headed into Val-de-Travers, where our [Airbnb](https://www.airbnb.co.uk/rooms/16942821?check_out=2024-09-07&viralityEntryPoint=1&unique_share_id=17765155-4A46-4B19-8B6F-45699FD1B9F9&slcid=17c3cb75a89343b8975d6ad0a40956cb&s=76&feature=share&adults=2&check_in=2024-09-06&channel=native&slug=Y0uyIWht&source_impression_id=p3_1731502495_P3PopQb_zr0QHuYg) awaited—a cozy treehouse. The treehouse was even better than we’d imagined. After dropping off our bags and wandering around the surroundings a bit, we ventured out for dinner. I went for a comforting cordon bleu, while Noë ordered a pizza.

{{< figure src="./images/PXL_20240906_154527348.jpg" alt="tree house" class="aligne-left size-smaller" >}}

We wrapped up the night with the perfect ending: a little fireplace inside the treehouse. We stretched out, let the fire warm us up, and fully embraced that ‘mini-vacation’ feeling that only sets in once you’re truly unwinding. 

{{< figure src="./images/PXL_20240906_180421661.jpg" alt="evening" class="aligne-center size-medium" >}}

The next morning, we woke up to a lovely breakfast, which felt like the final little gift from the treehouse before we headed back to reality. After a day of hiking, eating, and relaxing, we boarded the train home, fully refreshed and already dreaming of our next getaway.
