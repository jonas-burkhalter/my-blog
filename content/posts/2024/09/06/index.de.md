+++ 
draft = false
date = 2024-09-06T22:00:00+02:00
title = "Creux du Van"
description = "Kurztrip mit Noë"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Wandern"]
categories = ["Wandern"]
externalLink = ""
series = []
+++

Wir tauschten die alltägliche Routine gegen einen spontanen Ausflug und nahmen den Zug von Solothurn nach Noiraigue. Unser Plan war simpel: Zeit zusammen verbringen, wandern und das Beste aus einer gemütlichen Übernachtung im Baumhaus machen. Mit gepackten Rucksäcken machten wir uns voller Energie auf den Weg zum Creux du Van.

{{< figure src="./images/PXL_20240906_101608030.jpg" alt="Wandern" class="aligne-left size-small" >}}

Von Anfang an war der [Wanderweg](https://map.schweizmobil.ch/?lang=de&photos=yes&logo=yes&detours=yes&season=summer&bgLayer=pk&resolution=7.8&E=2546232&N=1199292&layers=Wanderland&trackId=1493161751) genau das, was wir uns erhofft hatten – wunderschön, mit der perfekten Mischung aus Ruhe und Neugierde. Die meisten anderen Wanderer wählten früh andere Routen, sodass es sich anfühlte, als hätten wir den Weg für uns allein. Wir wanderten entspannt dahin, in diesem herrlichen Zustand, in dem sich jeder Schritt leicht anfühlt. Der Anstieg war sanft, eher belebend als herausfordernd. Wir hielten ab und zu an, genossen die Geräusche der Natur, machten ein paar Fotos und naschten natürlich von unseren mitgebrachten Snacks.

{{< figure src="./images/WhatsApp Image 2024-11-24 at 20.28.36.jpg" alt="Wandern" class="aligne-right size-smaller" >}}

Schliesslich erreichten wir den Gipfel und wurden mit einer unglaublichen Aussicht belohnt. Die Klippen des Creux du Van bilden ein perfektes Felsenamphitheater, das in die Täler und sanften Hügel unter uns abfällt. Wir suchten uns einen Platz nahe am Rand, setzten uns mit unserem Lunch hin und liessen uns eine Weile von der Kulisse beeindrucken. Es ist schwer, sich bei so einer Landschaft nicht angenehm klein zu fühlen.

{{< figure src="./images/PXL_20240906_112255588.jpg" alt="Creux du Van" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20240906_114204551.jpg" alt="Creux du Van" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

{{< figure src="./images/PXL_20240906_113150252.jpg" alt="Creux du Van" class="aligne-right size-smaller" >}}

Nach dem Mittagessen folgten wir dem Pfad entlang der Klippen und genossen das Glück, klaren Himmel und einen friedlichen Nachmittag zu haben. Auf dem Abstieg zur anderen Seite des Gebirges erreichten wir schliesslich Val-de-Travers, wo uns unser [Airbnb](https://www.airbnb.co.uk/rooms/16942821?check_out=2024-09-07&viralityEntryPoint=1&unique_share_id=17765155-4A46-4B19-8B6F-45699FD1B9F9&slcid=17c3cb75a89343b8975d6ad0a40956cb&s=76&feature=share&adults=2&check_in=2024-09-06&channel=native&slug=Y0uyIWht&source_impression_id=p3_1731502495_P3PopQb_zr0QHuYg) erwartete – ein gemütliches Baumhaus. Das Baumhaus übertraf unsere Vorstellungen. Nachdem wir unsere Taschen abgestellt und die Umgebung erkundet hatten, gingen wir zum Abendessen. Ich entschied mich für ein herzhaftes Cordon bleu, während Noë eine Pizza wählte.

{{< figure src="./images/PXL_20240906_154527348.jpg" alt="Baumhaus" class="aligne-left size-smaller" >}}

Den Abend liessen wir perfekt ausklingen: mit einem kleinen Kaminfeuer im Baumhaus. Wir streckten uns aus, liessen uns von der Wärme durchdringen und genossen dieses „Mini-Urlaubsgefühl“, das sich erst einstellt, wenn man wirklich abschaltet.

{{< figure src="./images/PXL_20240906_180421661.jpg" alt="Abend" class="aligne-center size-medium" >}}

Am nächsten Morgen wachten wir zu einem liebevoll zubereiteten Frühstück auf – die letzte kleine Überraschung des Baumhauses, bevor wir uns wieder in den Alltag verabschiedeten. Nach einem Tag voller Wandern, gutem Essen und Entspannung stiegen wir in den Zug nach Hause – erfrischt und schon voller Vorfreude auf unseren nächsten Ausflug.