+++ 
draft = false
date = 2024-08-09T22:00:00+02:00
title = "Oberaletschgletscher"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["hiking"]
categories = ["hiking"]
externalLink = ""
series = []
+++

## [Friday: Early Start and a Grueling Hike to Oberaletschgletscherhütte](https://map.schweizmobil.ch/?lang=en&photos=yes&logo=yes&detours=yes&season=summer&resolution=8.39&E=2641899&N=1141755&bgLayer=pk&trackId=1629360754)
<!--
get up early to catch the 06:16 train, almost misted it and had to run. Had to catch my breath once i catched the train

riding 2h till Brig
from there i started walking up
throug Naters, Blatten, Belalp
there i had some lunch knowing i had about half the way done
further to the Oberaletschgletscher and on the side of it to the Oberaletschgletscherhütte
this part of the hike felt forever, the energy i started with slowly leafing 

arriving absolutly destroyed, hungry and tired

then dinner was ready, i was on a table with 4 norwegin and 1 german.
we had a tomatosoup, salat, muchroom risotto and a dessert

going to bed really early 
-->

{{< figure src="./images/PXL_20240809_084133283.LONG_EXPOSURE-01.COVER.jpg" alt="hiking" class="aligne-left size-smaller" >}}

The day kicked off with a rush to catch the 06:16 train—I nearly missed it and had to sprint to make it, leaving me breathless as I finally settled in for the 2-hour ride to Brig. From Brig, the real adventure began as I set off on foot, trekking through Naters, Blatten, and up to Belalp.

<div style="clear:both;"></div>
{{< figure src="./images/PXL_20240809_100531911.PANO.jpg" alt="hiking" class="aligne-center size-max" >}}

{{< figure src="./images/PXL_20240809_162427259.PANO.jpg" alt="hiking" class="aligne-right size-medium" >}}
After reaching Belalp and having some lunch, I knew I was only halfway done. The journey continued towards the Oberaletschgletscher, hiking alongside it until finally reaching the Oberaletschgletscherhütte. That last stretch felt endless as my initial energy drained away.

Completely exhausted, hungry, and worn out, I arrived just in time for dinner. I joined a table with four Norwegians and a German, and we enjoyed a well-deserved meal of tomato soup, salad, mushroom risotto, and dessert. 

After such a long day, I went to bed early, absolutely wiped out.


{{< figure src="./images/PXL_20240809_103707596.jpg" alt="hiking" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20240809_182008751.jpg" alt="hiking" class="aligne-right size-medium" >}}

## [Saturday: A Change of Plans and a Satisfying End](https://map.schweizmobil.ch/?lang=en&photos=yes&logo=yes&detours=yes&season=summer&resolution=8.39&E=2644483&N=1138131&bgLayer=pk&trackId=1763166759)
<!-- 
some nice and rich breakfrast in the hut

starting the walk back along the Oberaletschgletscher
i knew i couldn't walk all the way to the Burgerhütte as planned so i decided to replan and walk to Riedalp and from there take a Gondeli back 

Doing so i walked all the way down to Grünsee which felt nice and easy. But then the ascend began once again, and the tired legs and I felt the struggle from the day before 

arriving on top i went to the restaurant ... and had a icetea and an amazing flammkuchen.
after this break and a look at the great view i went down the last view meters to the gondeli in riederalp. with withch i went down and back to the train to solothurn.
-->
{{< figure src="./images/PXL_20240810_081140238.MP.jpg" alt="hiking" class="aligne-right size-small" >}}

After a hearty breakfast at the hut, I started the trek back along the Oberaletschgletscher. Realizing I wouldn’t make it all the way to the Burgerhütte as originally planned, I opted for a new route, deciding to head towards Riederalp instead.

{{< figure src="./images/PXL_20240810_093953596.jpg" alt="hiking" class="aligne-left size-small" >}}
{{< figure src="./images/PXL_20240810_122435404.jpg" alt="hiking" class="aligne-right size-small" >}}

The descent to Grünsee was smooth and pleasant, but the climb afterward was a challenge. My legs, still sore from the day before, made the uphill struggle real. But finally, I reached the top and rewarded myself with an iced tea and a fantastic Flammkuchen at a nearby restaurant.

<div style="clear:both;"></div>
{{< figure src="./images/PXL_20240810_124457588.PANO.jpg" alt="hiking" class="aligne-left size-max" >}}

With a moment to enjoy the stunning view, I made my way down the last few meters to the gondola in Riederalp. From there, I took the easy ride down and caught the train back to Solothurn, wrapping up the adventure on a satisfying note.
