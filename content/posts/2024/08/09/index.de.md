+++ 
draft = false
date = 2024-08-09T22:00:00+02:00
title = "Oberaletschgletscher"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Wandern"]
categories = ["Wandern"]
externalLink = ""
series = []
+++

## [Freitag: Früher Start und anstrengender Aufstieg zur Oberaletschgletscherhütte](https://map.schweizmobil.ch/?lang=en&photos=yes&logo=yes&detours=yes&season=summer&resolution=8.39&E=2641899&N=1141755&bgLayer=pk&trackId=1629360754)

{{< figure src="./images/PXL_20240809_084133283.LONG_EXPOSURE-01.COVER.jpg" alt="wandern" class="aligne-left size-smaller" >}}

Der Tag begann hektisch: Ich musste zum Zug um 06:16 rennen, fast hätte ich ihn verpasst. Ausser Atem stieg ich ein und machte es mir für die zweistündige Fahrt nach Brig bequem. Von dort begann das Abenteuer zu Fuss – durch Naters, Blatten und hinauf nach Belalp.

<div style="clear:both;"></div>
{{< figure src="./images/PXL_20240809_100531911.PANO.jpg" alt="wandern" class="aligne-center size-max" >}}

{{< figure src="./images/PXL_20240809_162427259.PANO.jpg" alt="wandern" class="aligne-right size-medium" >}}

In Belalp angekommen, gönnte ich mir ein Mittagessen und wusste, dass die Hälfte geschafft war. Weiter ging es in Richtung Oberaletschgletscher und entlang seiner Flanke zur Oberaletschgletscherhütte. Dieser letzte Abschnitt zog sich endlos, und die anfängliche Energie verliess mich zusehends.

Komplett erschöpft, hungrig und müde erreichte ich schliesslich die Hütte, gerade rechtzeitig fürs Abendessen. Ich sass an einem Tisch mit vier Norwegern und einem Deutschen. Gemeinsam genossen wir ein wohlverdientes Menü aus Tomatensuppe, Salat, Pilzrisotto und einem Dessert. 

Nach diesem langen Tag ging ich früh ins Bett – vollkommen erledigt.

{{< figure src="./images/PXL_20240809_103707596.jpg" alt="wandern" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20240809_182008751.jpg" alt="wandern" class="aligne-right size-medium" >}}

## [Samstag: Planänderung und ein gelungener Abschluss](https://map.schweizmobil.ch/?lang=en&photos=yes&logo=yes&detours=yes&season=summer&resolution=8.39&E=2644483&N=1138131&bgLayer=pk&trackId=1763166759)

{{< figure src="./images/PXL_20240810_081140238.MP.jpg" alt="wandern" class="aligne-right size-small" >}}

Nach einem reichhaltigen Frühstück in der Hütte machte ich mich auf den Rückweg entlang des Oberaletschgletschers. Schnell wurde mir klar, dass ich die Burgerhütte nicht wie geplant erreichen würde, also entschied ich mich für eine neue Route Richtung Riederalp.

{{< figure src="./images/PXL_20240810_093953596.jpg" alt="wandern" class="aligne-left size-small" >}}
{{< figure src="./images/PXL_20240810_122435404.jpg" alt="wandern" class="aligne-right size-small" >}}

Der Abstieg zum Grünsee war angenehm und leicht, aber der folgende Aufstieg brachte mich an meine Grenzen. Meine Beine waren vom Vortag noch schwer, und jeder Schritt war eine Herausforderung. Doch schliesslich erreichte ich den Gipfel und belohnte mich mit einem Eistee und einem fantastischen Flammkuchen in einem nahegelegenen Restaurant.

<div style="clear:both;"></div>
{{< figure src="./images/PXL_20240810_124457588.PANO.jpg" alt="wandern" class="aligne-left size-max" >}}

Nach einer kurzen Pause und dem Geniessen der atemberaubenden Aussicht machte ich mich auf den letzten Abschnitt hinunter zur Gondelstation in Riederalp. Von dort ging es gemütlich mit der Gondel ins Tal und mit dem Zug zurück nach Solothurn – ein perfekter Abschluss für dieses Abenteuer.