+++ 
draft = false
date = 2024-03-06T22:00:00+02:00
title = "Frutigen, Bonderchami"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Wandern", "Schneeschuhwandern"]
categories = ["Wandern", "Schneeschuhwandern"]
externalLink = ""
series = []
+++

## Mittwoch: Ein ruhiger Start in die Bergabenteuer
<!-- 
Ich hatte geplant, ein paar Tage wandern und Schneeschuhwandern zu gehen. Also fragte ich Jere, ob ich in dieser Zeit in seiner Wohnung in Frutigen bleiben könnte.

Ankunft in Frutigen  
Kurze Wanderung (1,5 Stunden) am Fluss entlang  

Einkaufen & Kochen  
Gemütlicher Abend  
-->
{{< figure src="./images/IMG_20240306_170653.jpg" alt="wandern" class="aligne-right size-large" >}}

Nach meiner Ankunft in Frutigen hatte ich etwas Zeit, bis Jere da war, also machte ich eine kurze, 1,5-stündige Wanderung entlang des Flusses. Der ruhige Spaziergang war eine grossartige Möglichkeit, die Beine zu vertreten und die entspannte Umgebung zu geniessen.

Als Jere ankam, gingen wir gemeinsam einkaufen und kochten ein einfaches Abendessen. Der Abend war gemütlich und entspannt – ein perfekter Start in meine Zeit in den Bergen.

## [Donnerstag in den Alpen: Entdeckungstour in Adelboden und ein entspannter Burgerabend](https://map.schweizmobil.ch/?lang=de&photos=yes&logo=yes&detours=yes&season=summer&resolution=9.97&E=2611013&N=1149513&bgLayer=pk&trackId=702064029)
<!-- 
Bus nach Adelboden Oey  
Von dort mit Schneeschuhen den Aufstieg zur Bonderchami  
Nach einer kurzen Pause und etwas Lunch zurück nach Adelboden  

Entspannter Abend mit Burger  
-->
Ich nahm den Bus nach Adelboden Oey und machte mich mit den Schneeschuhen auf den Weg zur Bonderchami. Der Aufstieg war anstrengend, aber die beeindruckende Alpenlandschaft machte alles wett. Nach einer kleinen Pause und etwas Lunch ging es wieder zurück nach Adelboden.

{{< figure src="./images/IMG_20240307_093349.jpg" alt="wandern" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240307_093533.jpg" alt="wandern" class="aligne-right size-medium" >}}
{{< figure src="./images/IMG_20240307_112001.jpg" alt="wandern" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240307_112004.jpg" alt="wandern" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Den Tag liessen wir entspannt ausklingen – mit einem leckeren Burger, der den perfekten Abschluss für einen abenteuerlichen Tag bildete.

## Freitag: Brunch, Wandern und ein gemütlicher Abend
<!-- 
Paco kommt an  
Zuerst Brunch – Rösti, Speck, Spiegelei  

Danach nach Kandersteg für eine 2-stündige Rundwanderung  

Waffeln & Bier im Schweizerhof  

Gemütlicher Abend  
-->
{{< figure src="./images/IMG_20240308_123754.jpg" alt="wandern" class="aligne-left size-small" >}}

Am Freitag kam Paco an, und wir starteten den Tag mit einem herzhaften Brunch: Rösti, Speck und Spiegelei – ein absoluter Genuss. Danach fuhren wir nach Kandersteg, um eine 2-stündige Rundwanderung zu unternehmen.

{{< figure src="./images/IMG_20240308_133357.jpg" alt="Waffeln" class="aligne-right size-small" >}}

Nach der Wanderung gönnten wir uns Waffeln und Bier im Schweizerhof – die perfekte Belohnung für unsere Anstrengungen. Der Abend war erneut entspannt und gemütlich, ein schöner Abschluss für einen weiteren Tag in den Bergen.

## Samstag: Ein letzter Brunch und die Heimreise
<!-- 
Noch einmal Brunch  

Dann die Rückreise nach Hause  
-->
Der Samstag begann mit einem weiteren köstlichen Brunch, bei dem wir die letzten Momente alpiner Gemütlichkeit genossen. Nach einem entspannten Morgen packten wir unsere Sachen und traten die Heimreise an, um ein paar erholsame Tage in den Bergen abzuschliessen.