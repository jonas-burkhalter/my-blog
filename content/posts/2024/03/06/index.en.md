+++ 
draft = false
date = 2024-03-06T22:00:00+02:00
title = "Frutigen, Bonderchami"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["hiking", "snowshoeing"]
categories = ["hiking", "snowshoeing"]
externalLink = ""
series = []
+++

## Wednesday: A Tranquil Start to Mountain Adventures
<!-- 
I planned to go hiking and snowshoe walking for a few days. so i asked jere to stay at his appartment in frutigen for this time.

arrive in frutigen
short hike (1.5h) around the river

einkaufen & kochen 
gemütlicher abend
-->
{{< figure src="./images/IMG_20240306_170653.jpg" alt="hiking" class="aligne-right size-large" >}}

Arriving in Frutigen, I had some time to kill while waiting for Jere, so I set off on a short 1.5-hour hike around the river. The peaceful walk was a great way to stretch my legs and take in the tranquil surroundings.

Once Jere arrived, we did some grocery shopping and cooked a simple meal together. The evening was cozy and relaxed, a perfect start to my time in the mountains.

## [Thursday in the Alps: Exploring Adelboden and a Cozy Burger Night](https://map.schweizmobil.ch/?lang=en&photos=yes&logo=yes&detours=yes&season=summer&resolution=9.97&E=2611013&N=1149513&bgLayer=pk&trackId=702064029)
<!--
taking the bus to adelboden oey
from there walking with the snowshoes all the way up to the Bonderchami
and after a short break and some luche heading back to adelboden

chill abend mit burger
-->
I took the bus to Adelboden Oey and made my way up to Bonderchami, enjoying the stunning alpine scenery along the way. After some exploring, I returned to Adelboden.

{{< figure src="./images/IMG_20240307_093349.jpg" alt="hiking" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240307_093533.jpg" alt="hiking" class="aligne-right size-medium" >}}
{{< figure src="./images/IMG_20240307_112001.jpg" alt="hiking" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240307_112004.jpg" alt="hiking" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

The day ended with a chill evening, topped off with a delicious burger, making it a perfect blend of adventure and relaxation.

## Friday: Brunch, Hiking, and a Cozy Evening
<!--
paco arrives
so first we had brunch, rösti, späck, spiegelei...

after that we went to kandersteg and hiked a rundweg for about 2h

then we enjoyed some waffel & bier in the schweizerhof

gemütlicher abend
-->
{{< figure src="./images/IMG_20240308_123754.jpg" alt="hiking" class="aligne-left size-small" >}}

Paco arrived, so we kicked off the day with a hearty brunch of rösti, späck, and fried eggs—an absolute treat. After fueling up, we headed to Kandersteg for a 2-hour hike on a scenic Rundweg.

{{< figure src="./images/IMG_20240308_133357.jpg" alt="waffels" class="aligne-right size-small" >}}

Post-hike, we treated ourselves to waffles and beer at the Schweizerhof, a perfect reward for the day’s efforts. The evening was relaxed and cozy, rounding off a great day in the mountains.

## Saturday: One Last Brunch and Heading Home
<!--
brunch again

back home
-->
We started the day with another satisfying brunch, enjoying the last bit of alpine comfort. After a relaxed morning, it was time to pack up and head back home, bringing an end to a refreshing few days in the mountains.