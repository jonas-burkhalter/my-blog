+++ 
draft = false
date = 2024-02-08T22:00:00+02:00
title = "Fasnacht"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Fasnacht"]
categories = ["Fasnacht"]
externalLink = ""
series = ["Fasnacht"]
+++

## Donnerstag: Zurück zur Fasnacht
<!--
chesslete verpasst

chinderumzug

gassenfasnacht

früh nach Hause, Jetlag
-->
Nachdem ich die Chesslete verpasst hatte, kam ich am Donnerstag endlich wieder nach Hause. Ohne Zeit zu verlieren, schnappte ich mir mein Kostüm und machte mich direkt auf den Weg zum Chinderumzug. Das Beste daran? Meine Freunde zu überraschen, die keine Ahnung hatten, dass ich zurück war – sie waren komplett überwältigt!

Der Tag ging weiter mit einer lebhaften Feier an der Gassenfasnacht, wo wir uns unter die bunten Menschenmengen mischten und ausgelassen feierten. Die Energie war ansteckend, aber schliesslich holte mich der Jetlag ein. Ich entschied mich, früh nach Hause zu gehen.

## Freitag: Entspannung und Vergnügen
<!--
blubbern

gassenfasnacht
-->
Nach einem anstrengenden ersten Tag beschlossen wir, es am Freitagmorgen ruhig angehen zu lassen und gingen „Blubbern“. Der Vormittag im Wellnessbad, mit warmem Wasser und sprudelnden Blasen, war die perfekte Möglichkeit, um zu entspannen und neue Energie zu tanken.

Am Abend waren wir bereit für mehr Spass und kehrten zur Gassenfasnacht zurück. Die Strassen waren erfüllt von Musik und Gelächter, und wir tauchten erneut vollständig in die Feierlichkeiten ein und genossen jeden Moment der lebhaften Atmosphäre.

{{< figure src="./images/IMG-20240209-WA0013.jpg" alt="Gassenfasnacht" class="aligne-center size-large" >}}

## Samstag: Spieltag und Regenabend
<!--
match

gassenfasnacht
regen
-->
Am Samstag war keine Zeit für Entspannung, denn ich hatte ein Unihockeyspiel. Das Spiel war intensiv, aber das Gefühl, wieder auf dem Feld zu stehen, war es absolut wert.

Am Abend kehrten wir zur Gassenfasnacht zurück, voller Vorfreude auf mehr Festlichkeiten. Leider begann es zu regnen, was die Stimmung im Freien etwas trübte. Wir suchten schnell Unterschlupf bei Andi, wo wir den Abend drinnen fortsetzten und trotz des Wetters eine gute Zeit mit Freunden hatten.

## Sonntag: Umzug und Familienzeit
<!--
umzug
-->
{{< figure src="./images/IMG-20240211-WA0012.jpg" alt="Umzug" class="aligne-left size-smaller" >}}

Heute stand der erste „Umzug“ auf dem Programm – ein Highlight der Fasnacht, mit all den wunderschönen Wagen und den „Guugge“, die Musik machten. Die Strassen waren erfüllt von lebhaften Farben, Musik und der kreativen Energie der Parade-Teilnehmer – ein wirklich spektakulärer Anblick.

Nach der Parade begann die Gassenfasnacht-Party, die den festlichen Geist weiter aufrechterhielt. Die lebhafte Atmosphäre zog sich durch den gesamten Tag.

<div style="clear:both;"></div>
{{< figure src="./images/IMG_20240211_000106.jpg" alt="Flämmli" class="aligne-center size-small" >}}

## Montag: Ein dringend benötigter Ruhetag
<!-- 
-
-->
Nach Tagen voller Action war der Montag endlich ein Tag zum Ausruhen und Schlaf nachholen. Ich nutzte die Pause voll aus, um mich zu entspannen und neue Energie zu tanken. Es war die perfekte Pause, bevor es wieder zurück in die Festlichkeiten ging.

## Dienstag: Umzug, Familie und Fasnachtsfieber
<!--
umzug
12i chlapf/hintermänner

monstergugge

gassenfasnacht
-->
Der Tag begann mit einem weiteren bunten „Umzug“, bei dem die Strassen mit lebhaften Wagen und festlicher Energie erfüllt waren. Nach der Parade traf ich meine Familie – meine Schwester, meine Nichte und meinen Neffen. Es war schön, sie zu sehen und etwas Zeit zusammen zu verbringen.

{{< figure src="./images/IMG_20240213_155009.jpg" alt="Gassenfasnacht" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240213_161237.jpg" alt="Gassenfasnacht" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

In der Zwischenzeit machten sich meine Freunde auf, um den „12i Chlapf“ zu springen – eine Fasnachtstradition, die sie sich nicht entgehen lassen konnten. Später trafen wir uns alle zur Monstergugge wieder, wo alle Guggen zusammenkamen, um in einer epischen, lauten und chaotischen Aufführung Musik zu machen.

Die Nacht endete mit einer Rückkehr zur Gassenfasnacht, wo wir feierten, bis ich nicht mehr konnte, und vollständig in die elektrische Atmosphäre der Fasnacht eintauchte.

{{< figure src="./images/IMG-20240213-WA0013.jpg" alt="Gassenfasnacht" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20240213_233113330.jpg" alt="Gassenfasnacht" class="aligne-right size-medium" >}}