+++ 
draft = false
date = 2024-02-08T22:00:00+02:00
title = "Fasnacht"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Fasnacht"]
categories = ["Fasnacht"]
externalLink = ""
series = ["Fasnacht"]
+++

## Thursday: Back to Fasnacht
<!--
chesslete verpasst

chinderumzug

gassenfasnacht

go home early, jetlag
-->
After missing the Chesslete, I finally arrived back home on Thursday. Without wasting any time, I grabbed my costume and headed straight to the Chinderumzug. The best part? Surprising my friends, who had no idea I was back—they were completely caught off guard!

The day continued with a lively celebration at the Gassenfasnacht, where we partied among the vibrant crowds and colorful costumes. The energy was infectious, but the jetlag eventually caught up with me. I decided to call it a night early and headed home.


## Friday: Relaxation and Revelry
<!--
blubbern

gassenfasnacht
-->
After a tiring first day back, we decided to take it easy on Friday morning by going "Blubbern." We spent the morning at the wellness bath, soaking in the warm waters and letting the bubbles work their magic. It was the perfect way to unwind and recharge.

By evening, we were ready for more fun and headed back to the Gassenfasnacht. The streets were alive with music and laughter, and we fully immersed ourselves in the festivities once again, enjoying every moment of the vibrant celebration.

{{< figure src="./images/IMG-20240209-WA0013.jpg" alt="Gassenfasnacht" class="aligne-center size-large" >}}

## Saturday: Game Day and Rainy Night
<!--
match

gassenfasnacht
regen
-->
There was no time to relax on Saturday, as I had a floorball match to play. The game was intense, but the thrill of being back on the court was worth it.

In the evening, we returned to the Gassenfasnacht, eager to soak in more of the festivities. Unfortunately, the rain began to pour, putting a damper on the outdoor fun. We quickly took refuge at Andi's place, where we continued the evening indoors, enjoying good company and laughter despite the weather.

## Sunday: Parade and Family Time
<!--
umzug
-->
{{< figure src="./images/IMG-20240211-WA0012.jpg" alt="Umzug" class="aligne-left size-smaller" >}}

Today marked the first "Umzug" (parade), a highlight of the Fasnacht with all the beautiful floats on display and the "Guugge" playing music. The streets were filled with vibrant colors, music, and the creative energy of the parade participants, making it a truly spectacular sight.

After the parade, the Gassenfasnacht party kicked off, keeping the festive spirit alive. The lively atmosphere carried through the day.

<div style="clear:both;"></div>
{{< figure src="./images/IMG_20240211_000106.jpg" alt="Flämmli" class="aligne-center size-small" >}}

## Monday: A Much-Needed Day of Rest
<!-- 
-
-->
After days of nonstop action, Monday was finally a day to relax and catch up on sleep. I took full advantage of the break, allowing myself to unwind and recharge. It was the perfect pause before diving back into the festivities.

## Tuesday: Parade, Family, and Fasnacht Frenzy
<!--
umzug
12i  chlapf/hintermänner

monstergugge

gassefasnacht
-->
The day kicked off with another colorful "Umzug" (parade), filling the streets with lively floats and festive energy. After the parade, I bumped into my family—my sister, niece, and nephew. It was great to catch up and enjoy some quality time together.

{{< figure src="./images/IMG_20240213_155009.jpg" alt="Gassenfasnacht" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_20240213_161237.jpg" alt="Gassenfasnacht" class="aligne-right size-medium" >}}
<div style="clear:both;"></div>

Meanwhile, my friends headed off to jump the "12i chlapf," a Fasnacht tradition they couldn’t resist. Later, we all reunited for the Monstergugge, where all the Gugge came together to play music in an epic, loud, and chaotic performance.

The night ended with a return to the Gassenfasnacht, partying until I could go no more, fully immersed in the electric atmosphere of Fasnacht.

{{< figure src="./images/IMG-20240213-WA0013.jpg" alt="Gassenfasnacht" class="aligne-left size-medium" >}}
{{< figure src="./images/PXL_20240213_233113330.jpg" alt="Gassenfasnacht" class="aligne-right size-medium" >}}

