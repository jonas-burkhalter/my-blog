+++ 
draft = false
date = 2024-06-26T22:00:00+02:00
title = "Spring Boot: Versioning entities (von Kai Müller)"
description = ""
slug = ""
authors = ["Kai Müller"]
tags = ["Arbeit", "Informatik", "Programmierung"]
categories = ["Arbeit", "Informatik", "Programmierung"]
externalLink = "https://medium.com/soobr/spring-boot-versioning-entities-253562ac24e2"
series = ["soobr"]
+++
