+++ 
draft = false
date = 2024-07-13T22:00:00+02:00
title = "Moleson"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Wandern"]
categories = ["Wandern"]
externalLink = ""
series = []
+++

## [Samstag: Wanderung nach Teysachaux mit Paco](https://www.myswitzerland.com/de-ch/erlebnisse/route/teysachaux/?_gl=1*pdqkjn*_up*MQ..*_ga*NjE3NjcwOTMwLjE3MjA4MTAyMTM.*_ga_F6N1LVHY7B*MTcyMDgxMDIxNC4xLjAuMTcyMDgxMDIxNC4wLjAuMjEzODY4MDUz) 

Gemeinsam mit Paco machten wir uns per Zug und Bus auf den Weg nach Les Rosalys in Les Paccots, bereit für die anspruchsvolle Wanderung nach Teysachaux. Der Weg begann entspannt und führte durch Wälder, bevor er sich zu weiten Wiesen öffnete, die einen fantastischen Blick auf die Freiburger Landschaft boten.

In Villard-Dessous wurde es steiler: 450 Meter steiniger Aufstieg lagen vor uns. Nach einer kurzen Pause bei Alp Tremetta setzten wir unseren Weg fort. Der schmale Grat erforderte volle Konzentration, während wir die steilen Passagen bis zum Gipfel meisterten.

{{< figure src="./images/PXL_20240713_110427795.MP.jpg" alt="wandern" class="aligne-left size-large" >}}
{{< figure src="./images/PXL_20240713_115752374.jpg" alt="wandern" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

Oben angekommen, wurden wir mit atemberaubenden Ausblicken auf den Genfersee und die umliegenden Berge belohnt. Anstelle des klassischen Fondues entschieden wir uns für Pommes und ein kühles Bier, bevor wir die Seilbahn für den Abstieg nahmen und zurück nach Solothurn fuhren.

{{< figure src="./images/PXL_20240713_135704557.MP.jpg" alt="wandern" class="aligne-center size-large" >}}