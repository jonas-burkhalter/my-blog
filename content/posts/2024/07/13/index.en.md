+++ 
draft = false
date = 2024-07-13T22:00:00+02:00
title = "Moleson"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["hiking"]
categories = ["hiking"]
externalLink = ""
series = []
+++

## [Saturday: Hike to Teysachaux with Paco](https://www.myswitzerland.com/de-ch/erlebnisse/route/teysachaux/?_gl=1*pdqkjn*_up*MQ..*_ga*NjE3NjcwOTMwLjE3MjA4MTAyMTM.*_ga_F6N1LVHY7B*MTcyMDgxMDIxNC4xLjAuMTcyMDgxMDIxNC4wLjAuMjEzODY4MDUz) 
<!-- 
-->

Together with Paco, we set off by train and bus to Les Rosalys in Les Paccots, ready for the challenging hike to Teysachaux. The path started off easy, winding through forests before opening up to wide meadows with views of the stunning Fribourg landscape.

As we reached Villard-Dessous, things got steeper, with 450 meters of rocky ascent ahead. After a short break at Alp Tremetta, we pushed on to the ridge, where focus was key—navigating the narrow, steep sections all the way to the summit.

{{< figure src="./images/PXL_20240713_110427795.MP.jpg" alt="hiking" class="aligne-left size-large" >}}
{{< figure src="./images/PXL_20240713_115752374.jpg" alt="hiking" class="aligne-right size-large" >}}
<div style="clear:both;"></div>

At the top, we were rewarded with amazing views of Lake Geneva and the surrounding mountains. Instead of the classic fondue, we went for fries and a cold beer before taking the cable car down and heading back to Solothurn.

{{< figure src="./images/PXL_20240713_135704557.MP.jpg" alt="hiking" class="aligne-center size-large" >}}
