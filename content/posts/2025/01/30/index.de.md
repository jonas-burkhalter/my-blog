+++ 
draft = false
date = 2025-01-30T22:00:00+02:00
title = "Käse Geschmack"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "Käse", "Arbeit"]
categories = ["Alp Hannig", "Käse", "Arbeit"]
externalLink = ""
series = ["Alp Hannig"]
+++

### Der Geschmack von Käse
{{< figure src="./images/PXL_20250210_194502639.jpg" alt="Geschmäcker" class="aligne-right size-small" >}}

Der einzigartige Geschmack von Käse entsteht durch das faszinierende Zusammenspiel verschiedener Mikroorganismen. Hier ein tieferer Einblick in die Welt der Bakterien, Pilze und Hefen, die für die Aromenvielfalt verantwortlich sind.

#### Bakterien 🦠
Bakterien sind die ersten kleinen Helfer auf dem Weg zum Käsegeschmack. Sie sorgen für die Gärung, beeinflussen Textur und Aroma und spielen eine Schlüsselrolle in der Reifung.
- **Milchsäurebakterien (Lactobazillen)**: Diese Bakterien wandeln Milchzucker in Milchsäure um. Das sorgt nicht nur für die Dicklegung der Milch, sondern verleiht Käsen wie Joghurt, Frischkäse und Quark ihren säuerlichen Geschmack. Sie sind entscheidend für die Textur und das milde Aroma vieler Frischkäse.
- **Propionsäurebakterien**: Diese Bakterien sind die geheimen Architekten des Emmentalers! Sie produzieren das nussige Aroma und sind verantwortlich für die berühmten Löcher ("Augen") im Käse. Sie verleihen auch anderen Hartkäsesorten wie Appenzeller eine besondere Tiefe.
- **Oberflächenbakterien**: Besonders aktiv bei Rotschmierkäsen wie Munster oder Limburger. Diese Bakterien sorgen für die würzige Note und den charakteristischen, oft intensiven Duft – ja, genau DER Duft, der Käseliebhaber begeistert und andere in die Flucht schlägt!

#### Schimmelpilze 🍄
Schimmel klingt erstmal unappetitlich, aber keine Sorge: Diese Pilze sind wahre Aromakünstler und tragen maßgeblich zur Geschmacksvielfalt bei.
- **Penicillium roqueforti**: Der Star in Blauschimmelkäsen wie Roquefort und Gorgonzola. Dieser Schimmelpilz sorgt für den kräftigen, leicht pfeffrigen Geschmack und die charakteristischen blauen Adern, die den Käse optisch und geschmacklich einzigartig machen.
- **Penicillium camemberti**: Dieser weiße Edelschimmel verleiht Käsen wie Camembert und Brie ihre cremige Konsistenz und das charakteristische, leicht pilzige Aroma. Die samtige weiße Rinde ist nicht nur schön anzusehen, sondern auch essbar und aromatisch.
- **Mucor**: Bekannt als "Katzenfell", bildet dieser Schimmel eine samtige, graue Rinde, die bei traditionellen Bergkäsen wie Tomme de Savoie zu finden ist. Er trägt zu einem erdigen, rustikalen Geschmack bei.
- **Sporendonema casei**: Dieser Schimmelpilz sorgt für die typische rötliche Rinde von Käsen wie dem Mimolette und verleiht ihnen eine leicht nussige bis fruchtige Note.

#### Hefen 🍞
Hefen sind die stillen Helden in der Käseherstellung. Sie tragen subtil, aber entscheidend zur Geschmacksentwicklung bei und spielen eine wichtige Rolle in der Rindenbildung und Reifung.
- **Debaryomyces**: Diese Hefe unterstützt die Reifung von Rotschmierkäsen und beeinflusst Aroma und Konsistenz. Sie hilft, den kräftigen Geschmack abzumildern und eine cremige Textur zu erzeugen.
- **Kluyveromyces**: Diese Hefe verstoffwechselt Milchzucker und trägt zur Geschmacksentwicklung bei, insbesondere bei Weichkäsen. Sie sorgt für leichte, fruchtige Aromen.
- **Candida**: Häufig in verschiedenen Käsesorten zu finden, beeinflusst diese Hefe die Reifung durch die Produktion von Enzymen, die das Aroma komplexer machen.
- **Geotrichum candidum**: Diese Hefe ist für die Bildung der natürlichen Rinde bei Käsen wie Saint-Marcellin verantwortlich und sorgt für einen milden, nussigen Geschmack. Sie verleiht dem Käse eine leicht samtige Textur an der Oberfläche.

#### Unterschiede und Aufgaben der Mikroorganismen 🧬
- **Bakterien** sind primär für die Umwandlung von Milchzucker in Milchsäure verantwortlich und sorgen für die Grundstruktur des Käses. Sie beeinflussen die Textur und erzeugen milde bis nussige Aromen.
- **Schimmelpilze** tragen vor allem zur Oberflächenreifung bei und sind verantwortlich für kräftige, oft pfeffrige oder pilzartige Aromen. Sie sorgen für die charakteristische Färbung und Textur, sei es der Blauschimmel im Inneren oder der weiße Edelschimmel außen.
- **Hefen** unterstützen die Reifung und beeinflussen die Rindenbildung. Sie tragen subtile, aber wichtige Aromen bei und helfen, das Gleichgewicht zwischen den anderen Mikroorganismen zu halten.

#### Gärungsprozesse 🧪
Die Gärung ist das Herzstück der Käseherstellung. Verschiedene Gärungsarten sorgen für unterschiedliche Geschmackserlebnisse.
- **Milchsäuregärung**: Wandelt Milchzucker in Milchsäure um und sorgt für die säuerliche Note in Frischkäsen und Joghurts.
- **Propionsäuregärung**: Erzeugt das nussige Aroma und die Löcher im Emmentaler und ähnlichen Hartkäsesorten.
- **Buttersäuregärung**: Diese Gärung kann unerwünschte, stechende Aromen erzeugen – meistens ein Zeichen von Fehlreifung. In manchen traditionellen Käsesorten wird sie jedoch bewusst eingesetzt, um kräftige Aromen zu erzeugen.

### Fazit
Ob mild oder würzig, cremig oder kräftig – der Geschmack von Käse ist das Ergebnis eines perfekten Zusammenspiels von Mikroorganismen. Bakterien, Schimmelpilze und Hefen arbeiten Hand in Hand, um die Vielfalt an Aromen und Texturen zu schaffen, die wir lieben. Also, beim nächsten Bissen einfach mal an die kleinen Helfer denken, die für dieses Geschmackserlebnis verantwortlich sind! 🧀
