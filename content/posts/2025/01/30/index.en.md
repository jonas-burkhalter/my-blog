+++ 
draft = false
date = 2025-01-30T22:00:00+02:00
title = "Cheese Taste"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "cheese", "work"]
categories = ["Alp Hannig", "cheese", "work"]
externalLink = ""
series = ["Alp Hannig"]
+++

### The Taste of Cheese
The unique taste of cheese is created through the fascinating interplay of various microorganisms. Here's a deeper look into the world of bacteria, fungi, and yeasts that are responsible for the diversity of flavors.

#### Bacteria 🦠
Bacteria are the first little helpers on the way to cheese flavor. They aid in fermentation, influence texture and aroma, and play a key role in aging.
- **Lactic Acid Bacteria (Lactobacilli)**: These bacteria convert milk sugar into lactic acid. This not only thickens the milk but also gives cheeses like yogurt, cream cheese, and curd their tangy taste. They are crucial for the texture and mild flavor of many fresh cheeses.
- **Propionic Acid Bacteria**: These bacteria are the secret architects of Emmental! They produce the nutty flavor and are responsible for the famous holes ("eyes") in the cheese. They also add a special depth to other hard cheeses like Appenzeller.
- **Surface Bacteria**: Particularly active in washed-rind cheeses like Munster or Limburger. These bacteria create the spicy note and characteristic, often intense smell – yes, that very scent that excites cheese lovers and drives others away!

#### Mold Fungi 🍄
Mold might sound unappetizing at first, but don't worry: these fungi are true flavor artists and contribute significantly to the flavor diversity.
- **Penicillium roqueforti**: The star in blue cheeses like Roquefort and Gorgonzola. This mold provides the strong, slightly peppery taste and the characteristic blue veins that make the cheese visually and flavor-wise unique.
- **Penicillium camemberti**: This white noble mold gives cheeses like Camembert and Brie their creamy consistency and the characteristic, slightly mushroomy aroma. The velvety white rind is not only pleasant to look at but also edible and flavorful.
- **Mucor**: Known as "cat fur," this mold forms a velvety, gray rind found on traditional mountain cheeses like Tomme de Savoie. It contributes an earthy, rustic flavor.
- **Sporendonema casei**: This mold is responsible for the typical reddish rind of cheeses like Mimolette and gives them a slightly nutty to fruity flavor.

#### Yeasts 🍞
Yeasts are the silent heroes in cheese production. They subtly but crucially contribute to flavor development and play an important role in rind formation and aging.
- **Debaryomyces**: This yeast supports the aging of washed-rind cheeses and influences flavor and texture. It helps soften the strong taste and creates a creamy texture.
- **Kluyveromyces**: This yeast metabolizes milk sugar and contributes to flavor development, especially in soft cheeses. It creates light, fruity flavors.
- **Candida**: Commonly found in various cheese types, this yeast affects aging by producing enzymes that make the flavor more complex.
- **Geotrichum candidum**: This yeast is responsible for the formation of the natural rind in cheeses like Saint-Marcellin and contributes a mild, nutty flavor. It gives the cheese a slightly velvety texture on the surface.

#### Differences and Roles of Microorganisms 🧬
- **Bacteria** are primarily responsible for converting milk sugar into lactic acid and provide the basic structure of the cheese. They influence the texture and generate mild to nutty flavors.
- **Mold Fungi** mainly contribute to surface aging and are responsible for strong, often peppery or mushroom-like flavors. They create the characteristic color and texture, whether it's the blue mold inside or the white noble mold on the outside.
- **Yeasts** support aging and influence rind formation. They contribute subtle but essential flavors and help maintain the balance between the other microorganisms.

#### Fermentation Processes 🧪
Fermentation is at the heart of cheese production. Different types of fermentation create different taste experiences.
- **Lactic Acid Fermentation**: Converts milk sugar into lactic acid and provides the tangy note in fresh cheeses and yogurts.
- **Propionic Acid Fermentation**: Creates the nutty flavor and the holes in Emmental and similar hard cheeses.
- **Butyric Acid Fermentation**: This fermentation can produce undesirable, pungent aromas – usually a sign of improper aging. However, it is sometimes intentionally used in traditional cheeses to create strong flavors.

### Conclusion
Whether mild or spicy, creamy or bold – the taste of cheese is the result of a perfect interplay of microorganisms. Bacteria, mold fungi, and yeasts work hand in hand to create the variety of flavors and textures we love. So, the next time you take a bite, think about the little helpers responsible for this taste experience! 🧀
