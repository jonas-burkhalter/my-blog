+++
draft = false
date = 2025-01-21T22:00:00+02:00
title = "A Passion for Food"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Family", "Cooking"]
categories = ["Family", "Cooking"]
externalLink = ""
series = []
+++

{{< figure src="./images/DSCF0003.jpg" alt="Apron" class="aligne-right size-medium" >}}

Sometimes I wonder how different my life would have been if my grandparents hadn’t owned that little restaurant. It was their pride and joy – until I turned two years old. That’s when they closed its doors for good, and we moved into the apartment above the former restaurant. But even after the closure, the kitchen never truly went silent. Every Tuesday, my grandparents cooked meals in the old restaurant kitchen for the local retirement home, and I – at the age of four – was always right there in the middle of it.

{{< figure src="./images/DSCF0075.jpg" alt="grandfather" class="aligne-left size-medium" >}}

After kindergarten, I’d go straight to the kitchen. “Helping” might not be the most accurate term for what I did, but I felt like the most important little sous-chef ever. Later, when we delivered the meals to the retirement home, it felt like we were on a special mission. And honestly, those experiences left a lasting impression on me.

That’s where my passion for cooking really began. As a kid, I often made dinner for myself. It wasn’t always fancy, but it was enough to keep me fed. At one point, I even considered becoming a professional chef or pastry chef. But when I learned about the working hours in the industry, that idea was quickly off the table—faster than you can say "mise en place."

What I loved the most, though, were the big celebrations. For every graduation or birthday, I always wished for one thing: my grandfather firing up his Oklahoma smoker for a barbecue. Watching him work his magic with the fire, the smoke, and the food was mesmerizing. And the flavors? Simply unmatched. His grilled chicken and ribs were legendary. Every time felt special, and I learned so much from him—without him ever formally teaching me.

At 14, I got my first big "solo gig." During a weekend youth camp, no one else wanted to cook, so I stepped up. Cooking for 20 people? Sure, why not. And it worked! From then on, I knew I had a knack for it. Between the ages of 16 and 20, I cooked every year at the spring camp, feeding between 40 and 55 people. We shared responsibilities like meal planning, shopping, and cooking, and we always ended up in the kitchen together, wielding our cooking spoons like pros.

{{< figure src="./images/IMG_20230724_095550.jpg" alt="Cooking GS" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_20230724_095602.jpg" alt="Kitchen GS" class="aligne-right size-smaller" >}}

From 2021 to 2024, I took on an even bigger role at a youth leader training camp, where I was fully responsible for the kitchen. It was all outdoors, cooking over an open fire. Dishes like Älplermagronen (Swiss macaroni and cheese), curry, mashed potatoes, lasagna, pizza, spareribs, brownies, couscous salad, chocolate croissants, homemade pasta, and crumbles were staples on the menu. It was challenging but deeply rewarding to organize everything and make people happy with good food.

{{< figure src="./images/IMG_20231221_104919.jpg" alt="Cooking in cambodia" class="aligne-right size-small" >}}
Looking back, I realize how much my grandfather shaped my passion for cooking. He talked a lot and explained everything as he went—whether it was how to light the fire in his Oklahoma smoker or how to prepare meals for others. Through his words and actions, he taught me something far more important: cooking isn’t just about making food—it’s about bringing people together. And for that, I’m endlessly grateful to him.