+++ 
draft = false
date = 2025-01-21T22:00:00+02:00
title = "Passion fürs Essen"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Familie", "Kochen"]
categories = ["Familie", "Kochen"]
externalLink = ""
series = []
+++

{{< figure src="./images/DSCF0003.jpg" alt="Kochschürze" class="aligne-right size-medium" >}}

Manchmal frage ich mich, wie viel anders mein Leben verlaufen wäre, hätten meine Grosseltern nicht dieses kleine Restaurant gehabt. Es war ihr ganzer Stolz – bis ich zwei Jahre alt war. Dann schlossen sie die Türen für immer, und wir zogen in die ehemalige Wohnung über dem Restaurant ein. Doch selbst nach der Schliessung war die Küche dort nie wirklich still. Jeden Dienstag kochten meine Grosseltern in der alten Restaurantküche Essen fürs Altersheim, und ich – mit meinen vier Jahren – war natürlich mittendrin.

{{< figure src="./images/DSCF0075.jpg" alt="Grossvater" class="aligne-left size-medium" >}}

Nach dem Kindergarten zog es mich immer direkt in die Küche. "Helfen" war vermutlich nicht das, was ich wirklich tat, aber ich fühlte mich wie der wichtigste kleine Sous-Chef überhaupt. Wenn wir später ins Altersheim fuhren, um das Essen auszugeben, hatte ich das Gefühl, als ob wir eine kleine Mission erfüllen würden. Und was soll ich sagen – diese Zeit hat mich geprägt.

Meine Begeisterung fürs Kochen war entfacht. Schon als Kind habe ich regelmässig das Abendessen für mich selbst übernommen. Es gab zwar nicht immer Haute Cuisine, aber immerhin genug, dass niemand verhungerte. Ich überlegte sogar eine Weile, ob ich eine Lehre als Koch oder Patissier machen sollte. Aber als ich hörte, wie die Arbeitszeiten in der Gastronomie so aussehen, war die Idee schneller wieder vom Tisch, als ich "Mise en Place" sagen konnte.

Was ich jedoch immer geliebt habe, waren die grossen Feiern. Bei jedem Schulabschluss oder Geburtstag wünschte ich mir, dass mein Grossvater sein Oklahoma-Barbecue anwirft. Es war einfach magisch, ihm zuzuschauen. Das Feuer, der Rauch, der Duft – und natürlich das Essen! Sein Grillhähnchen, die Rippchen – unübertroffen. Es war jedes Mal ein Highlight, und ich habe so viel von ihm gelernt, ohne dass er mir je eine Kochlektion gegeben hätte.

Mit 14 kam dann mein erster eigener „Grosseinsatz“: ein Wochenende mit der Jungschar. Keiner wollte kochen, also tat ich es. Für 20 Leute. Einfach so. Es hat funktioniert, und ab da war klar, dass ich ein Talent habe. Zwischen 16 und 20 kochte ich dann jedes Jahr im Frühlingslager für 40 bis 55 Personen. Wir teilten uns die Verantwortung für Menüplanung, Einkaufen und Kochen und schwangen zusammen den Kochlöffel.

{{< figure src="./images/IMG_20230724_095550.jpg" alt="Kochen GS" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_20230724_095602.jpg" alt="Küche GS" class="aligne-right size-smaller" >}}

Von 2021 bis 2024 war ich schliesslich in einem Jungschar-Ausbildungslager für die Küche zuständig – komplett draussen, über offenem Feuer. Älplermagronen, Curry, Kartoffelstock, aber auch Lasagne, Pizza, Spareribs, Brownies, Couscous-Salat, Schoggigipfeli, selbstgemachte Teigwaren und Crumble standen regelmässig auf dem Speiseplan. Es war eine Herausforderung, aber auch eine Freude, das Ganze zu organisieren und die Leute mit gutem Essen glücklich zu machen.

{{< figure src="./images/IMG_20231221_104919.jpg" alt="Kochen in Kambodia" class="aligne-right size-small" >}}
Am Ende, wenn ich zurückblicke, merke ich, wie sehr mein Grossvater meine Leidenschaft fürs Kochen geprägt hat. Er hat viel geredet und mir immer alles erklärt – ob es darum ging, wie man das Feuer in seinem Oklahoma entfacht, oder wie er in der Küche für andere gekocht hat. Durch seine Worte und Taten hat er mir etwas viel Wichtigeres beigebracht: Kochen ist mehr als nur Essen zubereiten – es ist eine Art, Menschen zusammenzubringen. Und dafür bin ich ihm unendlich dankbar.