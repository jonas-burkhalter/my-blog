+++ 
draft = false
date = 2025-01-07T22:00:00+02:00
title = "Lessons Learned at Soobr"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["work", "computer science", "startup"]
categories = ["work", "computer science", "startup"]
externalLink = ""
series = ["soobr"]
+++

For three and a half years, I was part of Soobr AG, a time filled with challenges, exciting projects, and intense learning experiences. But eventually, a feeling started to creep in that I couldn’t ignore: my motivation faded, and with it, my enthusiasm for the daily work. The pace that had once been electrifying began to feel burdensome. During my performance review, it became clear to me: if you’re not fully energized and moving at maximum speed, you don’t belong here. A harsh statement, but one that made me reflect deeply.

What began as an exciting startup experience gradually turned into a series of monotonous business tasks. Software development suffered from quick fixes and hacks, and the quality of the code deteriorated. Robert C. Martin once said, “The only way to go fast is to go well.” Yet here, the code was often far from that ideal—dominated by short-term fixes and pragmatic solutions that were rarely elegant or sustainable. The once vibrant startup culture shifted toward a strategy of stability, but the software thinking didn’t follow suit. We were not thinking ahead; we were just reacting to what was immediately needed. Long-term planning was nearly non-existent, and short-term solutions ruled the day. Innovation and the dynamism that had once driven me were barely perceptible.

At the same time, it was also a period in which I learned a great deal. I deepened my knowledge of Java with Spring Boot, worked extensively with Google Cloud Console, MySQL, and React with MUI. Software architecture, in particular, became one of my key focuses. I introduced Domain-Driven Design (DDD) thinking into the company, struggled through the entanglements of existing services, and tried to apply the Strangler Fig Pattern. The goal was Macro Services, but with waning support and a lack of urgency, little effort was put into this direction. Documentation played a role, so I delved into https://docs.structurizr.com/ to bring structure to the architecture.

In addition to software architecture, I gained valuable experience in feature development: from concept to database and backend, to frontend, feedback, and improvements. CI/CD pipelines also became a focus of mine. I optimized and refined many workflows, improved caching, and reduced unnecessary runtimes. Working with outsourcing teams was both interesting and challenging – a try with India, the close collaboration in a sprint team, and all the hurdles that come with remote work. Operational tasks like log monitoring, error handling, deployments, releases, and database changes became part of my daily work. Each of these challenges taught me something new.

Over time, I started to feel less and less connected. The team was fantastic, but I felt increasingly detached from it. The work as a developer still fascinated me, but it became more and more draining. The balancing act between pragmatism and quality, the constant tweaking instead of sustainable solutions – it all took its toll. I’ve learned a lot during this time, but I’ve also lost a lot of energy. Leaving isn’t easy, but it’s necessary. I’m curious to see where my next chapter will take me.
