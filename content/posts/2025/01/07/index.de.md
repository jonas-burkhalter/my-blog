+++ 
draft = false
date = 2025-01-07T22:00:00+02:00
title = "Lessons Learned bei Soobr"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Arbeit", "Informatik", "Startup"]
categories = ["Arbeit", "Informatik", "Startup"]
externalLink = ""
series = ["soobr"]
+++

Dreieinhalb Jahre lang war ich Teil der Soobr AG, einer Zeit voller Herausforderungen, spannender Projekte und intensiver Lernprozesse. Doch irgendwann begann sich ein Gefühl einzuschleichen, das ich nicht ignorieren konnte: Die Motivation schwand, und mit ihr meine Begeisterung für die tägliche Arbeit. Die Geschwindigkeit, die anfangs so elektrisierend war, wurde zur Last. Beim Mitarbeitergespräch wurde mir klar vermittelt: Wer nicht mit voller Energie und maximalem Tempo dabei ist, hat hier keinen Platz. Ein harter Satz, der mich zum Nachdenken brachte.

Was einst wie eine aufregende Startup-Erfahrung begann, wurde zunehmend zu einer Abfolge eintöniger Business-Tasks. Die Software-Entwicklung litt unter Quickfixes und Hacks, und der Code verlor an Qualität. Robert C. Martin sagte einmal: "The only way to go fast is to go well." Doch hier war der Code oft weit davon entfernt – geprägt von Quickfixes und pragmatischen Lösungen, die selten schön oder nachhaltig waren. Die einst pulsierende Startup-Kultur wich einer Stabilitätsstrategie, doch das Software-Denken folgte nicht dieser Entwicklung. Es wurde nicht  vorausschauend gedacht, sondern nur auf das reagiert, was akut nötig war. Langfristige Planung gab es kaum, während kurzfristige Lösungen dominierten. Innovation und Dynamik, die mich einst antrieben, waren kaum noch spürbar.

Gleichzeitig war es aber auch eine Zeit, in der ich enorm viel lernte. Ich vertiefte mein Wissen in Java mit Spring Boot, arbeitete intensiv mit der Google Cloud Console, MySQL und React mit MUI. Besonders Software-Architektur wurde zu einem meiner Schwerpunkte. Ich brachte DDD-Denken ins Unternehmen, kämpfte mich durch die Verflechtungen der bestehenden Services und versuchte, das Strangler Fig Pattern anzuwenden. Macro Services waren das Ziel, doch mit nachlassender Unterstützung und geringer Dringlichkeit wurde kaum noch Aufwand in diese Richtung betrieben. Dokumentation spielte eine  Rolle, weshalb ich mich mit https://docs.structurizr.com/ beschäftigte, um Struktur in die Architektur zu bringen.

Neben der Software-Architektur sammelte ich wertvolle Erfahrungen in der Feature-Entwicklung: Von der Idee über die Datenbank und das Backend bis hin zum Frontend, Feedback und Verbesserungen. Auch CI/CD-Pipelines wurden zu einem meiner Schwerpunkte. Ich optimierte und verfeinerte viele Abläufe, verbesserte das Caching und reduzierte unnötige Laufzeiten. Interessant und zugleich schwierig war die Arbeit mit Outsourcing-Teams – ein Versuch mit Indien, die enge Zusammenarbeit in einem Sprint-Team und all die Herausforderungen, die mit Remote-Arbeit einhergehen. Operations-Aufgaben wie Log-Überwachung, Fehlerhandling, Deployments, Releases und Datenbankänderungen wurden ebenfalls Teil meines Arbeitsalltags. Jede dieser Herausforderungen lehrte mich etwas Neues. 

Mit der Zeit merkte ich, wie mein Zugehörigkeitsgefühl schwand. Das Team war großartig, doch ich fühlte mich immer weniger als Teil davon. Die Arbeit als Informatiker faszinierte mich weiterhin, doch sie wurde zunehmend zermürbend. Der Spagat zwischen Pragmatismus und Qualität, das ständige Nachbessern statt nachhaltiger Lösungen – all das hinterließ Spuren. Ich habe in dieser Zeit viel gelernt, aber auch viel Energie verloren. Der Abschied fällt nicht leicht, doch er ist notwendig. Ich bin gespannt, wohin mich mein nächstes Kapitel führen wird.
