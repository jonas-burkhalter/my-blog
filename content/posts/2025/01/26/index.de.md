+++ 
draft = false
date = 2025-01-26T22:00:00+02:00
title = "Alp Hannig im Schnee"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "Arbeit"]
categories = ["Alp Hannig", "Arbeit"]
externalLink = ""
series = ["Alp Hannig"]
+++

## Motivation
Nach jahrelanger Arbeit im Büro spüre ich, wie meine Motivation für diese Tätigkeit nachlässt – eine Veränderung muss her. Während meines Zivildienstes sammelte ich bereits wertvolle Erfahrungen in der Landwirtschaft, die mein Interesse an der Arbeit auf der Alp weiter gestärkt haben. Die Verbindung von Tradition, Gemeinschaft und Natur fasziniert mich besonders und ich möchte diese einzigartige Atmosphäre wieder erleben.

## Bewerbung
Also machte ich mich auf die Suche nach passenden Möglichkeiten und stiess dabei auf zalp.ch, eine Plattform für Alpstellen und Informationen rund ums Älplerleben. Nach einigem Stöbern fand ich eine Alp, die mir gefallen könnte, und bewarb mich. 

{{< figure src="./images/zalp.png" alt="zalp" class="aligne-center size-larger" >}}

Schon kurz darauf erhielt ich eine Antwort, und es wurde direkt ein Termin für ein Bewerbungsgespräch per Videocall mit Liliane und Francesco festgelegt. Das Gespräch verlief sehr positiv, und nach vielen Infos über die Alp und uns waren alle weiterhin begeistert. Daraufhin wurde ein weiteres Gespräch mit dem Stiftungschef Kurt angesetzt, bei dem auch gleich ein Termin für die Besichtigung der Alp organisiert wurde.

## Hannig Alp
{{< figure src="./images/PXL_20250126_105601881.jpg" alt="wanderung" class="aligne-left size-small" >}}

Schon einige Tage später trafen wir uns in Saas Fee. Kurt, Liliane, Inga (eine weitere Interessierte) und ich hatten ein erstes kurzes Kennenlernen. Anschliessend machten wir uns auf den Weg zur Hannig Alp. Gemeinsam stapften wir durch den frischen Neuschnee der vergangenen Nacht, was die Wanderung besonders eindrucksvoll machte. Der Weg führte uns durch eine traumhafte Winterlandschaft, und die Aussicht auf die umliegenden Berge bei bestem Wetter war einfach überwältigend – eine wirklich schöne kurze Wanderung.

{{< figure src="./images/PXL_20250126_112151239.jpg" alt="Hannig Alp" class="aligne-right size-medium" >}}

Oben angekommen, nahm Liliane sich viel Zeit, uns die Alp zu zeigen. Sie führte uns durch die Unterkunft und die Käserei, was einen tollen Eindruck hinterliess. Zum Abschluss gingen wir ins Restaurant Alpenblick, wo wir eine leckere Suppe genossen, bevor wir uns wieder auf den Rückweg machten.

Die ganze Erfahrung war sehr angenehm, und meine Vorfreude auf die Arbeit auf der Alp ist nach wie vor sehr gross.

{{< figure src="./images/PXL_20250126_115451320.jpg" alt="Sass-Fee" class="aligne-center size-medium" >}}

## Vertrag
Nach dem Gespräch mit dem Alpchef wurden die Vertragsdetails besprochen, und die Informationen wurden mir kurz darauf zugeschickt. Nach der Klärung letzter Details war schliesslich alles geregelt. Der Sommer ist nun geplant, und ich freue mich schon sehr auf die kommende Zeit auf der Alp.

## Links
- [zalp.ch](https://zalp.ch/stellenboerse/alpstellen)
- [alphannig.ch](https://www.alphannig.ch/)
