+++ 
draft = false
date = 2025-01-26T22:00:00+02:00
title = "Alp Hannig in the snow"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "work"]
categories = ["Alp Hannig", "work"]
externalLink = ""
series = ["Alp Hannig"]
+++

## Motivation  
After years of working in an office, I can feel my motivation for this job waning – a change is needed. During my civil service, I already gained valuable experience in agriculture, which further fueled my interest in working on an alp. The combination of tradition, community, and nature fascinates me, and I’m eager to experience this unique atmosphere again.

## Application  
I began searching for suitable opportunities and came across zalp.ch, a platform for alp jobs and information about life as an alpine herder. After browsing through the site, I found an alp that seemed like a good fit, and I applied.

{{< figure src="./images/zalp.png" alt="zalp" class="aligne-center size-larger" >}}

Shortly after, I received a response, and a video interview with Liliane and Francesco was scheduled. The conversation went very well, and after learning more about the alp and each other, everyone remained enthusiastic. As a result, another meeting with the foundation’s director, Kurt, was arranged, and a visit to the alp was also scheduled.

## Hannig Alp
{{< figure src="./images/PXL_20250126_105601881.jpg" alt="hike" class="aligne-left size-small" >}}

A few days later, we met in Saas Fee. Kurt, Liliane, Inga (another interested candidate), and I had a brief introduction. We then set off for the Hannig Alp. Together, we trudged through the fresh snow from the previous night, which made the hike even more impressive. The path led us through a stunning winter landscape, and the view of the surrounding mountains in the perfect weather was simply breathtaking – a really beautiful short hike.

{{< figure src="./images/PXL_20250126_112151239.jpg" alt="Hannig Alp" class="aligne-right size-medium" >}}

Once we reached the top, Liliane took the time to show us around the alp. She led us through the accommodation and the cheese dairy, which left a great impression. To finish, we went to the Restaurant Alpenblick, where we enjoyed a delicious soup before heading back down.

The whole experience was very pleasant, and my excitement for working on the alp remains high.

{{< figure src="./images/PXL_20250126_115451320.jpg" alt="Sass-Fee" class="aligne-center size-medium" >}}

## Contract  
After the meeting with the alp’s director, we discussed the contract details, and the information was sent to me shortly after. Once all the final details were clarified, everything was settled. The summer is now planned, and I’m really looking forward to the upcoming time on the alp.

## Links  
- [zalp.ch](https://zalp.ch/stellenboerse/alpstellen)  
- [alphannig.ch](https://www.alphannig.ch/)