+++
draft = false
date = 2025-01-29T22:00:00+02:00
title = "Cheese Varieties"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "cheese", "work"]
categories = ["Alp Hannig", "cheese", "work"]
externalLink = ""
series = ["Alp Hannig"]
+++

### The World of Cheese Varieties
Cheese is not just cheese! From creamy-soft to crumbly-firm, from mild to intense – the variety of cheese types is impressive. Here's an overview of the main categories.

### Fresh Cheese
- **Color:** Ivory white
- **Texture:** Smooth and creamy
- **Examples:** Feta, Brousse du Rove

### Whey and Acidic Milk Cheeses
- **Color:** Snow white
- **Texture:** Grainy to delicately melting
- **Examples:** Brocciu, Ricotta Romana

### Soft Cheese with Natural Rind
- **Color:** Cream to alabaster white
- **Texture:** Soft, creamy, semi-dry
- **Examples:** Sainte-Maure de Touraine, Bijou

### Soft Cheese with Surface Mold
- **Color:** White to flaxen
- **Texture:** Soft and creamy
- **Mold Culture:** *Penicillium camemberti*
- **Examples:** Camembert, Brie, Coeur de Neufchâtel

### Soft Cheese with Washed Rind
- **Color:** Orange, bronze, reddish-brown
- **Texture:** Soft, creamy to runny
- **Bacteria Culture:** *Brevibacterium linens*
- **Examples:** Langres, Weisslacker

### Pressed-Curd Cheese
- **Color:** Light beige to ash gray
- **Texture:** Smooth, firmer, drier or creamier curd
- **Examples:** Raclette, Appenzeller, Pecorino

### Burnt and Pressed-Curd Cheese
- **Color:** Ochre to reddish-brown rind, light yellow to cream-colored curd
- **Texture:** Firm, melting, with or without holes
- **Specialty:** Some varieties weigh up to 110 kg
- **Examples:** Gruyère, Emmental, Parmigiano Reggiano

### Cheese with Internal Mold
- **Color:** White with blue, green or black veins
- **Texture:** Melting, grainy or runny
- **Mold Culture:** *Penicillium roqueforti*, *Penicillium glaucum*
- **Examples:** Roquefort, Gorgonzola

### Pasta-Filata Cheese
- **Color:** White to cream-colored
- **Texture:** Fibrous, elastic
- **Examples:** Mozzarella, Burrata

### Processed Cheese
- **Color:** Whitish, yellowish, or greenish
- **Texture:** Melting
- **Examples:** La Vache qui Rit, Cancoillotte

### Cheese Preparations
- **Color:** Varies
- **Texture:** Firm, crumbly or melting
- **Examples:** Gaperon d'Auvergne, Janu Siers

### Conclusion
Whether mild or spicy, firm or creamy – the world of cheese is incredibly diverse. Next time you're shopping, it’s worth trying a new variety and exploring the full range of flavors!
