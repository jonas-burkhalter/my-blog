+++ 
draft = false
date = 2025-01-29T22:00:00+02:00
title = "Käse Sorten"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "Käse", "Arbeit"]
categories = ["Alp Hannig", "Käse", "Arbeit"]
externalLink = ""
series = ["Alp Hannig"]
+++

### Die Welt der Käsesorten
Käse ist nicht gleich Käse! Von cremig-weich bis bröckelig-fest, von mild bis intensiv – die Vielfalt der Käsesorten ist beeindruckend. Hier ein Überblick über die wichtigsten Kategorien.

### Frischkäse
- **Farbe:** Elfenbeinweiss
- **Konsistenz:** Glatt und cremig
- **Beispiele:** Feta, Brousse du Rove

### Molke- und Sauermilchkäse
- **Farbe:** Schneeweiss
- **Konsistenz:** Körnig bis zartschmelzend
- **Beispiele:** Brocciu, Ricotta Romana

### Weichkäse mit Naturrinde
- **Farbe:** Creme bis Alabasterweiss
- **Konsistenz:** Weich, cremig, halbtrocken
- **Beispiele:** Sainte-Maure de Touraine, Bijou

### Weichkäse mit Aussenschimmel
- **Farbe:** Weiss bis Flachsfarben
- **Konsistenz:** Weich und cremig
- **Schimmelkultur:** *Penicillium camemberti*
- **Beispiele:** Camembert, Brie, Coeur de Neufchâtel

### Weichkäse mit gewaschener Rinde
- **Farbe:** Orange, Bronze, Rotbraun
- **Konsistenz:** Weich, cremig bis fliessend
- **Bakterienkultur:** *Brevibacterium linens*
- **Beispiele:** Langres, Weisslacker

### Käse mit gepresstem Teig
- **Farbe:** Hellbeige bis Aschgrau
- **Konsistenz:** Geschmeidig, fester, trockener oder cremiger Teig
- **Beispiele:** Raclette, Appenzeller, Pecorino

### Käse mit gebranntem und gepresstem Teig
- **Farbe:** Ocker- bis rotbraune Rinde, hellgelber bis cremefarbener Teig
- **Konsistenz:** Fest, zartschmelzend, mit oder ohne Lochbildung
- **Besonderheit:** Manche Sorten wiegen bis zu 110 kg
- **Beispiele:** Gruyère, Emmentaler, Parmigiano Reggiano

### Käse mit Innenschimmel
- **Farbe:** Weiss mit blauen, grünen oder schwarzen Adern
- **Konsistenz:** Schmelzend, körnig oder fliessend
- **Schimmelkultur:** *Penicillium roqueforti*, *Penicillium glaucum*
- **Beispiele:** Roquefort, Gorgonzola

### Pasta-Filata-Käse
- **Farbe:** Weiss bis cremefarben
- **Konsistenz:** Faserig, elastisch
- **Beispiele:** Mozzarella, Burrata

### Schmelzkäse
- **Farbe:** Weisslich, gelblich oder grünlich
- **Konsistenz:** Schmelzend
- **Beispiele:** La Vache qui Rit, Cancoillotte

### Käsezubereitungen
- **Farbe:** Variiert
- **Konsistenz:** Fest, brüchig oder schmelzend
- **Beispiele:** Gaperon d'Auvergne, Janu Siers

### Fazit
Ob mild oder würzig, fest oder cremig – die Welt des Käses ist unglaublich vielfältig. Beim nächsten Einkauf lohnt es sich, eine neue Sorte zu probieren und die geschmackliche Bandbreite zu entdecken!
