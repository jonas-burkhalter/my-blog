+++
draft = false
date = 2025-01-28T22:00:00+02:00
title = "Cheese Production"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "cheese", "work"]
categories = ["Alp Hannig", "cheese", "work"]
externalLink = ""
series = ["Alp Hannig"]
+++

## The Journey to Cheese
Cheese production is an art in itself. From fresh milk to the matured cheese wheel, it requires time, patience, and the right craftsmanship. But how exactly does simple milk turn into delicious cheese? Let’s take a closer look at the key steps.

### Milk and Milking
It all starts with the milk – and not all milk is the same. Cows, goats, and sheep provide different types of milk with varying fat and protein content, which affect the taste and texture of the cheese. While cow’s milk is often mild and abundant, goat’s milk brings a tangy note, and sheep’s milk, with its higher fat content, results in particularly creamy varieties.

Once the animals have been milked, the fresh milk is transported to the dairy. Depending on the type of cheese, it is treated differently, such as filtering or warming it, to create the ideal conditions for further processing.

### Curdling the Milk
Now begins the actual transformation. Lactic acid bacteria and rennet (or plant-based alternatives) are added to cause the milk to curdle. It thickens into a substance known as curd. The curd is then cut into small pieces – this is called the “bruch” (curd cut). The cutting process separates the whey from the solid part of the milk and influences the future texture of the cheese.

### Shaping the Cheese
The curd is placed into molds to give the cheese its characteristic shape. Different methods are used depending on the type of cheese: some cheeses are wrapped in cloth, while others are placed in special wooden molds or metal frames. In this phase, the structure of the cheese is solidified – some cheeses are regularly turned to ensure a consistent texture.

### Draining
Now, the cheese must lose excess liquid. For example, fresh cheese simply drains through gravity, while firmer cheeses are pressed under weight. Soft cheeses often develop their first rind in this phase, which later influences the flavor.

### Salting for Flavor and Preservation
Salt plays a crucial role – it not only adds flavor but also affects the texture and shelf life. The cheese can either be dry-salted or placed in a brine solution. Salt slows down the growth of unwanted bacteria and ensures that the desired cheese culture can develop optimally.

### Maturation – The Affination
Now begins the maturation process. In special cellars or storage rooms, the cheese develops its full flavor over weeks, months, or even years. Temperature and humidity are carefully controlled, and some cheeses are regularly turned, brushed, or treated with special cultures to develop their characteristic rind and flavor.

### Different Production Methods
Not all cheese is made the same way. Depending on the production method, we distinguish between:
- Fermier Cheese: Made directly on the farm, usually in small quantities with a lot of handcraft.
- Artisanal Cheese: Produced in small, handcraft dairies, often using traditional methods.
- Laitier Cheese: Made in larger dairies but still focused on quality.
- Industrial Cheese: Mass-produced with standardized procedures to maintain consistent taste.

### Conclusion
Cheese making is a mix of science, tradition, and craftsmanship. Whether handmade farm cheese or industrially produced hard cheese, every cheese has its own story and character. So, next time you take a bite, take a moment to consider how much work and knowledge went into that small piece!

### Links
- [schweizerkaese.ch](https://www.schweizerkaese.ch/storyroom/herstellung/so-entsteht-schweizer-kaese)
- [schoenegger.com](https://www.schoenegger.com/kaesewissen/kaeseherstellung/)
- [ich-liebe-kaese.de](https://ich-liebe-kaese.de/kaesewissen/kaeseherstellung/alles-ueber-die-kaeseherstellung/)
