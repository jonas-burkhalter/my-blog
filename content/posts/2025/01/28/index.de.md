+++ 
draft = false
date = 2025-01-28T22:00:00+02:00
title = "Käse Herstellung"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "Käse", "Arbeit"]
categories = ["Alp Hannig", "Käse", "Arbeit"]
externalLink = ""
series = ["Alp Hannig"]
+++

## Der Weg zum Käse
Käseherstellung ist eine Kunst für sich. Von der frischen Milch bis zum gereiften Käselaib braucht es Zeit, Geduld und das richtige Handwerk. Doch wie genau entsteht aus einfacher Milch ein köstlicher Käse? Schauen wir uns die wichtigsten Schritte einmal genauer an.

### Milch und Melken
Alles beginnt mit der Milch – und nicht jede ist gleich. Kühe, Ziegen und Schafe liefern unterschiedliche Milcharten mit variierenden Fett- und Eiweißgehalten, die den Geschmack und die Konsistenz des Käses beeinflussen. Während Kuhmilch oft mild und ergiebig ist, bringt Ziegenmilch eine würzige Note mit, und Schafsmilch sorgt mit ihrem hohen Fettgehalt für besonders cremige Sorten.

Nachdem die Tiere gemolken wurden, wird die frische Milch zur Käserei transportiert. Je nach Käsesorte wird sie unterschiedlich behandelt, etwa durch Filtern oder Erwärmen, um ideale Bedingungen für die weitere Verarbeitung zu schaffen.

### Dicklegen der Milch
Nun beginnt die eigentliche Verwandlung. Milchsäurebakterien und Lab (oder pflanzliche Alternativen) werden hinzugefügt, um die Milch zum Gerinnen zu bringen. Sie verdickt sich zur sogenannten Gallerte. Diese wird in kleine Stücke geschnitten – der sogenannte Bruch entsteht. Durch das Schneiden trennt sich die Molke vom festen Teil der Milch, und die spätere Konsistenz des Käses wird beeinflusst.

### Formen des Käses
Der Bruch wird in Formen gefüllt, um dem Käse seine typische Gestalt zu geben. Je nach Sorte kommen unterschiedliche Methoden zum Einsatz: Manche Käsesorten werden in Tüchern eingewickelt, andere kommen in spezielle Holzformen oder Metallrahmen. In dieser Phase wird die Struktur des Käses gefestigt – manche werden zudem regelmäßig gewendet, um eine gleichmäßige Konsistenz zu gewährleisten.

### Abtropfen lassen
Nun muss der Käse überschüssige Flüssigkeit verlieren. Frischkäse beispielsweise tropft einfach durch Schwerkraft ab, während festerer Käse unter Druck gepresst wird. Weichkäse entwickelt in dieser Phase oft seine erste Rinde, die später das Aroma beeinflusst.

### Salzen für Geschmack und Haltbarkeit
Salz spielt eine wichtige Rolle – es sorgt nicht nur für Geschmack, sondern beeinflusst auch die Textur und Haltbarkeit. Der Käse kann entweder trocken gesalzen oder in eine Salzlake gelegt werden. Salz verlangsamt das Wachstum unerwünschter Bakterien und sorgt dafür, dass sich die gewünschte Käsekultur optimal entwickeln kann.

### Reifung – Die Affination
Jetzt beginnt die Reifezeit. In speziellen Kellern oder Lagern entfaltet der Käse über Wochen, Monate oder gar Jahre hinweg sein volles Aroma. Temperatur und Luftfeuchtigkeit werden genau kontrolliert, und einige Käsesorten werden regelmäßig gewendet, gebürstet oder mit speziellen Kulturen behandelt, um ihre charakteristische Rinde und ihren Geschmack zu entwickeln.

### Verschiedene Produktionsweisen
Nicht jeder Käse entsteht auf dieselbe Weise. Je nach Produktionsmethode unterscheidet man:
- Fermier-Käse: Direkt auf dem Bauernhof hergestellt, meist in kleinen Mengen und mit viel Handarbeit.
- Artisanal-Käse: In handwerklichen Käsereien produziert, oft mit traditionellen Methoden.
- Laitier-Käse: In größeren Molkereien gefertigt, aber immer noch mit Fokus auf Qualität.
- Industrie-Käse: In Massenproduktion hergestellt, mit standardisierten Verfahren für gleichbleibenden Geschmack.

### Fazit
Die Herstellung von Käse ist eine Mischung aus Wissenschaft, Tradition und Handwerkskunst. Ob handgemachter Bauernkäse oder industriell gefertigter Hartkäse – jeder Käse hat seine eigene Geschichte und seinen eigenen Charakter. Also, beim nächsten Bissen vielleicht kurz innehalten und überlegen, wie viel Arbeit und Wissen in diesem kleinen Stück steckt!

### Links
- [schweizerkaese.ch](https://www.schweizerkaese.ch/storyroom/herstellung/so-entsteht-schweizer-kaese)
- [schoenegger.com](https://www.schoenegger.com/kaesewissen/kaeseherstellung/)
- [ich-liebe-kaese.de](https://ich-liebe-kaese.de/kaesewissen/kaeseherstellung/alles-ueber-die-kaeseherstellung/)