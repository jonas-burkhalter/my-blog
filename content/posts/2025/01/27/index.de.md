+++ 
draft = false
date = 2025-01-27T22:00:00+02:00
title = "Käse Geschichte"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "Käse", "Arbeit"]
categories = ["Alp Hannig", "Käse", "Arbeit"]
externalLink = ""
series = ["Alp Hannig"]
+++

Käse! Kaum ein anderes Lebensmittel ist so vielseitig, lecker und manchmal auch ein bisschen streng riechend. Aber wo kommt dieser göttliche Genuss eigentlich her? Wer kam auf die glorreiche Idee, Milch so lange stehen zu lassen, bis sie sich in etwas verwandelte, das heute als kulinarisches Gold gilt? Werfen wir einen Blick in die Geschichte des Käses, mit besonderem Fokus auf die Schweiz, denn wenn es um Käse geht, dann sind wir Schweizer ja quasi das Mutterland des geschmolzenen Glücks.

Die Anfänge des Käses reichen ungefähr 7000 Jahre zurück. Damals, also um 5000 v. Chr., gab es noch keine schicken Käsereien mit Edelstahlkesseln und Hygienestandards. Nein, die Menschen waren Nomaden, hielten Tiere und hatten irgendwann das Problem, dass Milch nicht ewig haltbar ist. Irgendjemand kam dann wohl auf die Idee, sie in Tiermägen aufzubewahren, was dank der natürlichen Enzyme dazu führte, dass die Milch gerann und eine feste Masse entstand. Tadaa, der Käse war geboren! Geschmacklich war das Ganze sicher noch nicht auf Emmentaler-Niveau, aber hey, aller Anfang ist schwer.

In der Antike wurde Käse weiterentwickelt, und die Römer waren es, die das Handwerk zur Perfektion brachten. Sie bauten erste Käsereien, experimentierten mit verschiedenen Reifegraden und verbreiteten ihre Käsekunst in ganz Europa. Auch in der Schweiz blieb das nicht unbemerkt. Dank der römischen Einflüsse begann man hier, gezielt Käse herzustellen. Das war auch dringend nötig, denn mit dem Bergklima und den vielen Kühen war es nur logisch, dass Milchprodukte eine tragende Rolle spielten.

Im Mittelalter wurde Käseproduktion in der Schweiz zur richtigen Kunstform. Klöster hatten große Käsekeller, und Alpkäse wurde zum Exportschlager. Vor allem im Berner Oberland und in der Innerschweiz entstanden erste spezialisierte Käsereien, die sich auf Hartkäse konzentrierten. Denn Hartkäse war lange haltbar und konnte gut transportiert werden – ein klarer Vorteil in einer Zeit ohne Kühlschränke.

Dann kam das 18. und 19. Jahrhundert, eine Zeit des Wandels und der Innovation in der Käseherstellung. Durch die Industrialisierung veränderte sich die Produktion grundlegend. Käsereien wurden größer, und neue Technologien wie verbesserte Press- und Reifetechniken sorgten für eine gleichmäßigere Qualität. Zudem begann der internationale Handel mit Käse an Bedeutung zu gewinnen. Schweizer Käse wurde in andere Länder exportiert, und neue Sorten entstanden, die auf den Geschmack der Konsumenten zugeschnitten waren. In dieser Zeit entwickelte sich auch der moderne Emmentaler, der mit seinen charakteristischen Löchern weltweit bekannt wurde.

Und heute? Käse ist aus der Schweiz nicht wegzudenken. Fondue und Raclette sind fester Bestandteil unserer Esskultur, und die Liebe zum Käse hat sich nicht verändert – höchstens verfeinert. 

Ob uralt oder modern, mit Löchern oder ohne – Käse bleibt eine der schönsten Erfindungen der Menschheit. Und solange es Brot, Wein und ein schönes Stück Käse gibt, ist die Welt doch eigentlich in Ordnung.

