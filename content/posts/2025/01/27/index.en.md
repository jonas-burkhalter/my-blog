+++
draft = false
date = 2025-01-27T22:00:00+02:00
title = "Cheese History"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Alp Hannig", "cheese", "work"]
categories = ["Alp Hannig", "cheese", "work"]
externalLink = ""
series = ["Alp Hannig"]
+++

Cheese! Few foods are as versatile, delicious, and sometimes a little bit stinky as cheese. But where does this divine delight actually come from? Who came up with the glorious idea of letting milk sit until it transformed into something that is now considered culinary gold? Let's take a look at the history of cheese, with a special focus on Switzerland, because when it comes to cheese, us Swiss are practically the motherland of melted happiness.

The origins of cheese date back about 7,000 years. Back then, around 5000 BC, there were no fancy dairies with stainless steel kettles and hygiene standards. No, people were nomads, kept animals, and eventually faced the problem that milk doesn't last forever. Someone probably had the idea to store it in animal stomachs, which, thanks to the natural enzymes, caused the milk to curdle and form a solid mass. Tadaa, cheese was born! It probably didn't taste like Emmental back then, but hey, every beginning is tough.

In antiquity, cheese was further developed, and it was the Romans who perfected the craft. They built the first dairies, experimented with different aging processes, and spread their cheese-making skills across Europe. This did not go unnoticed in Switzerland. Thanks to Roman influences, cheese-making began here too. This was also much needed, as the mountain climate and abundance of cows made dairy products a crucial part of life.

In the Middle Ages, cheese production in Switzerland became a true art form. Monasteries had large cheese cellars, and alpine cheese became a major export product. Particularly in the Bernese Oberland and Central Switzerland, the first specialized dairies were established, focusing on hard cheese. Hard cheeses were long-lasting and easy to transport – a clear advantage in a time without refrigerators.

Then came the 18th and 19th centuries, a time of change and innovation in cheese production. The Industrial Revolution fundamentally changed production. Dairies became larger, and new technologies like improved pressing and aging techniques ensured more consistent quality. Additionally, international cheese trade began to grow in importance. Swiss cheese was exported to other countries, and new varieties were created, tailored to the tastes of consumers. During this time, the modern Emmental was also developed, becoming famous worldwide for its characteristic holes.

And today? Cheese is indispensable in Switzerland. Fondue and Raclette are firm staples of our culinary culture, and our love for cheese has not changed – perhaps just refined.

Whether ancient or modern, with holes or without – cheese remains one of humanity’s greatest inventions. And as long as there’s bread, wine, and a nice piece of cheese, the world is really in a good place.
