+++ 
draft = false
date = 2025-02-05T22:00:00+02:00
title = "Langenbruck"
description = "Kurztrip mit Noë"
slug = ""
authors = ["Jonas Burkhalter"]
tags = []
categories = []
externalLink = ""
series = []
+++

Dieses Wochenende haben Noë und ich uns mal wieder eine Auszeit gegönnt. Raus aus dem Alltag, rein ins Abenteuer – na ja, oder zumindest ins gemütliche Entspannen in der Ferienwohnung ihrer Grosstante. 

## Mittwoch – Sterne, Suppe und verschwommene Galaxien
{{< figure src="./images/PXL_20250205_203358965.NIGHT.jpg" alt="space eye" class="aligne-left size-small" >}}

Wir starteten unser Wochenende mit einem Besuch im Space Eye, der Sternwarte nahe Bern. Unser Astronomie-Wissen war schon ziemlich gut, aber es ist immer wieder faszinierend, den Nachthimmel durch ein neues Teleskop zu betrachten. 

{{< figure src="./images/PXL_20250205_193714283.MP.jpg" alt="space eye shuttle" class="aligne-right size-small" >}}

In der beeindruckenden Video-Kuppel bekamen wir eine Erklärung des aktuellen Nachthimmels, was zwar vieles bekannt, aber trotzdem spannend war. Zur Stärkung gab es eine wärmende Suppe, bevor wir uns ans Teleskop mit 7000mm Brennweite wagten. Damit haben wir Jupiter unter die Lupe genommen, ein paar Sternhaufen bestaunt und sogar die Andromeda-Galaxie gesehen. Spoiler: Sie sieht immer noch aus wie ein verschwommener Fleck, aber ein schöner verschwommener Fleck.

{{< figure src="./images/PXL_20250205_201902048.NIGHT.jpg" alt="space eye teleskop" class="aligne-center size-medium" >}}

## Donnerstag – Nebelwelten und Vogelbeobachtung
{{< figure src="./images/IMG_5887.jpg" alt="Specht" class="aligne-left size-small" >}}

Erst mal ausgeschlafen – schließlich waren wir am Vorabend auf intergalaktischer Mission. Danach ging’s auf nach Langenbruck. Dort erwartete uns dichter Nebel, der die Landschaft in eine mystische Kulisse verwandelte. Vor unserem Fenster tummelten sich Vögel, die wir begeistert beobachteten, während die Heizung auf ihr Bestes gab, die Wohnung aufzutauen. Also kuschelten wir uns erst einmal in Decken, bis es endlich warm wurde. 

<div style="clear:both;"></div>
{{< figure src="./images/IMG_5632.jpg" alt="Nebel" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_5792.jpg" alt="Nebel" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_5741.jpg" alt="Nebel" class="aligne-left size-small" >}}
<div style="clear:both;"></div>

Nach dem Mittagessen wagten wir uns in den Nebel zu einem Spaziergang, bei dem wir mehr Nebel als Weg sahen. Dafür wurden die Fotos unterwegs geheimnisvoll und magisch. 

Abends ließen wir uns im Chiuchli Bärenwil verwöhnen: Noë wählte den Vegi-Herbstteller, ich den klassischen Metzgete-Teller. Beide Gerichte waren ein Genuss und wärmten uns nach dem kühlen Tag richtig durch.

{{< figure src="./images/PXL_20250206_182254828.jpg" alt="Chiuchli Bärenwil" class="aligne-center size-small" >}}

## Freitag – Wellness und die Kunst des Nichtstuns
Der Tag stand ganz im Zeichen der Entspannung: ab ins "Forty Seven" in Baden. Warm-Kalt-Bäder, Therme, Sauna – das volle Programm. Wir haben uns gefühlt wie frisch gekochte Spaghetti, so entspannt waren wir danach. Zurück in der Wohnung gab es ein entspanntes Abendessen, aber lange hielten wir uns nicht wach. So früh wie an diesem Abend sind wir selten ins Bett gefallen.

## Samstag – Sonne, Pläne und hausgemachte Ravioli
{{< figure src="./images/IMG_5833.jpg" alt="wandern" class="aligne-left size-smaller" >}}

Der Tag begann gemächlich – ausschlafen, gemütlich Kaffee trinken, ein bisschen in den Tag hineinleben. Dann ein Spaziergang aufs Wanneflüheli, endlich zeigte sich die Sonne, und wir genossen die wärmenden Strahlen in vollen Zügen. 
{{< figure src="./images/IMG_5845.jpg" alt="Wanneflüheli" class="aligne-right size-small" >}}

Zurück in der Wohnung schmiedeten wir Pläne für unsere nächsten Ferien – Vorfreude ist schließlich die schönste Freude. Am Abend wagten wir uns an selbstgemachte Ravioli. Der Teig wurde ausgerollt, die Füllung vorbereitet, und die Küche verwandelte sich in ein kleines Chaos. Aber das Ergebnis war jeden Aufwand wert.

{{< figure src="./images/IMG_5841.jpg" alt="Wanneflüheli" class="aligne-center size-medium" >}}

## Sonntag – Aufräumen und Abschied nehmen
Der letzte Morgen begann mit einem gemütlichen Frühstück, bevor wir uns ans Aufräumen machten. Die Zeit war wie im Flug vergangen, und es fiel uns schwer, Abschied zu nehmen. Doch leider war es Zeit wieder zum Alltag zurückzukehren.
