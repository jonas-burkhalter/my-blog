+++ 
draft = false
date = 2025-02-05T22:00:00+02:00
title = "Langenbruck"
description = "Getaway with Noë"
slug = ""
authors = ["Jonas Burkhalter"]
tags = []
categories = []
externalLink = ""
series = []
+++
Last weekend, Noë and I decided to take a little break. We needed to step away from the daily grind and dive into a mini adventure—or at least enjoy some cozy downtime at her great-aunt’s holiday apartment.

## Wednesday – Stars, Soup, and Blurry Galaxies
{{< figure src="./images/PXL_20250205_203358965.NIGHT.jpg" alt="space eye" class="aligne-left size-small" >}}

We kicked off our weekend with a visit to Space Eye, the observatory near Bern. We already knew a fair bit about astronomy, but there’s always something magical about seeing the night sky through a different telescope.

{{< figure src="./images/PXL_20250205_193714283.MP.jpg" alt="space eye shuttle" class="aligne-right size-small" >}}

Inside the impressive video dome, we got a rundown of what was visible in the night sky. A lot of it was familiar, but it was still fascinating. To warm up, we had some hearty soup before checking out the telescope with a 7000mm focal length. We zoomed in on Jupiter, admired a few star clusters, and even spotted the Andromeda Galaxy. Spoiler alert: it still looks like a fuzzy smudge—but a pretty cool fuzzy smudge.

{{< figure src="./images/PXL_20250205_201902048.NIGHT.jpg" alt="space eye telescope" class="aligne-center size-medium" >}}

## Thursday – Foggy Landscapes and Bird Watching
{{< figure src="./images/IMG_5887.jpg" alt="woodpecker" class="aligne-left size-small" >}}

We slept in—after all, our galactic adventures the night before were exhausting. Then we headed to Langenbruck, where thick fog wrapped the landscape in an eerie, mystical vibe. Birds flitted around outside our window, and we watched them while waiting for the heater to do its job. We bundled up in blankets until the apartment finally warmed up.

After lunch, we ventured out for a foggy walk, where we could barely see the path ahead. But the mist made for some seriously atmospheric photos.

<div style="clear:both;"></div>
{{< figure src="./images/IMG_5632.jpg" alt="fog" class="aligne-left size-small" >}}
{{< figure src="./images/IMG_5792.jpg" alt="fog" class="aligne-left size-medium" >}}
{{< figure src="./images/IMG_5741.jpg" alt="fog" class="aligne-left size-small" >}}
<div style="clear:both;"></div>

In the evening, we treated ourselves at Chiuchli Bärenwil: Noë had the veggie autumn plate, and I went for the traditional "Metzgete" platter. Both dishes were delicious and warmed us right up after the chilly day.

{{< figure src="./images/PXL_20250206_182254828.jpg" alt="Chiuchli Bärenwil" class="aligne-center size-small" >}}

## Friday – Spa Day and the Joy of Doing Nothing
Friday was all about relaxation: we headed to "Forty Seven" in Baden for some serious pampering. Hot and cold baths, thermal pools, saunas—the works. By the end, we felt like overcooked noodles, totally relaxed and floppy. Back at the apartment, we had a simple dinner and crashed early. We don’t usually go to bed that early, but we were completely wiped out.

## Saturday – Sunshine, Future Plans, and Homemade Ravioli
{{< figure src="./images/IMG_5833.jpg" alt="hiking" class="aligne-left size-smaller" >}}

The day started slow—sleeping in, sipping coffee, and just enjoying the moment. Later, we took a walk up to Wanneflüheli. The sun finally decided to show up, and we soaked in every warm ray.
{{< figure src="./images/IMG_5845.jpg" alt="Wanneflüheli" class="aligne-right size-small" >}}

Back at the apartment, we started brainstorming ideas for our next vacation—because planning trips is half the fun. In the evening, we attempted homemade ravioli. Rolling out the dough, preparing the filling, and turning the kitchen into a bit of a disaster zone. But the delicious results were totally worth the mess.

{{< figure src="./images/IMG_5841.jpg" alt="Wanneflüheli" class="aligne-center size-medium" >}}

## Sunday – Packing Up and Heading Home
Our last morning began with a leisurely breakfast before we got down to tidying up. The weekend had flown by, and it was tough to say goodbye. But, sadly, it was time to head back to reality.