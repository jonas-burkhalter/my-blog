+++ 
draft = false
date = 2025-02-13T22:00:00+02:00
title = "Home-Barista Latte Art"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = []
categories = []
externalLink = ""
series = []
+++

Anyone can enjoy coffee. But crafting the perfect cappuccino, with creamy milk foam and intricate latte art on top? That’s a whole different game. Up until now, my attempts at hearts looked more like random blobs. That’s exactly why I found myself at [Blaser Cafe](https://www.blasercafe.ch/kaffeekurse/home-barista-latte-art) today, in a cozy little coffee school—ready to take on the challenge. My goal? To become a home barista and master the magic of latte art!

The course started with 20 minutes of theory—boring? Not at all! Milk is not just milk. To achieve the perfect milk foam, you need about 3.5% fat and enough proteins to create a stable structure. While UHT milk works, fresh milk often produces better results. Then came the technique: Hold the steam wand just below the surface to introduce air into the milk to create big bubbles, then tilt the wand slightly to create a gentle vortex in the pitcher. To finish, tap the pitcher on the table a few times and swirl it gently. The result? Silky microfoam—creamy, glossy, and free of large bubbles. Perfection!

{{< figure src="./images/heart.png" alt="herz" class="aligne-center size-fullwidth" >}}
{{< figure src="./images/farn.png" alt="farn" class="aligne-center size-fullwidth" >}}

{{< figure src="./images/PXL_20250213_183456047.jpg" alt="herz und tulpe" class="aligne-left size-small" >}}

Then it got serious: Off to the espresso machine! For 2.5 hours, the steam hissed, the milk frothed, and I struggled to produce something more than an uninspired milk puddle. Josh showed us how to pour a heart—start by slowly pouring in the center and finish with a bold pull-through at the end. Sounds easy, but my first “heart” looked more like abstract expressionism. Next up: the fern. More control, smoother movements—and suddenly, I was improving!

{{< figure src="./images/PXL_20250213_191725989.jpg" alt="schwan" class="aligne-right size-small" >}}

But the ultimate highlight? The swan. Just once—really, only once—I nailed it. The lines flowed, the wings were defined—a moment of pure barista magic! Josh gave me an approving nod, and for a brief second, I felt like a true professional.

To wrap things up, we cleaned our stations. And then? Then, I was officially a home barista, with a newfound appreciation for milk foam and deep respect for those who make latte art look effortless. Conclusion: Lots of coffee, lots of laughter, and the realization that even an amorphous cloud is a good place to start.
