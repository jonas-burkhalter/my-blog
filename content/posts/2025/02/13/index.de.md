+++ 
draft = false
date = 2025-02-13T22:00:00+02:00
title = "Home-Barista Latte Art"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = []
categories = []
externalLink = ""
series = []
+++

Kaffee geniessen kann jeder. Aber einen perfekten Cappuccino zaubern, mit cremigem Milchschaum und kunstvollen Mustern obendrauf? Das ist eine Kunst für sich. Bis jetzt waren meine Herzen nur als Fleck zu erkennen. Genau deshalb stand ich heute bei [Blaser Cafe](https://www.blasercafe.ch/kaffeekurse/home-barista-latte-art) in einer kleinen, charmanten Kaffeeschule – bereit, mich der Herausforderung zu stellen. Mein Ziel? Zum Home-Barista aufsteigen und die Magie der Latte Art meistern!

Der Kurs begann mit 20 Minuten Theorie – trocken? Keineswegs! Milch ist nicht gleich Milch. Für den perfekten Milchschaum braucht es einen Fettgehalt von etwa 3,5 % und ausreichend Proteine, um eine stabile Struktur zu gewährleisten. H-Milch funktioniert zwar, aber frische Milch liefert oft die besseren Ergebnisse. Dann die Technik: Die Düse knapp unter der Oberfläche halten, um Luft in die Milch zu bringen und grosse Blasen zu erzeugen, danach die Düse leicht seitlich halten, sodass ein Wirbel im Kännchen entsteht. Zum Schluss ein paar Mal auf den Tisch klopfen um grosse Blasen zu entfernen und das Kännchen sanft durchwirbeln. Das Resultat? Samtiger Mikroschaum, cremig und glänzend, ohne grosse Luftblasen. Perfektion!

{{< figure src="./images/heart.png" alt="herz" class="aligne-center size-fullwidth" >}}
{{< figure src="./images/farn.png" alt="farn" class="aligne-center size-fullwidth" >}}

{{< figure src="./images/PXL_20250213_183456047.jpg" alt="herz und tulpe" class="aligne-left size-small" >}}

Dann wurde es ernst: Ab zur Maschine! 2,5 Stunden lang dampfte, zischte und schäumte es, während ich versuchte, mehr als eine unmotivierte Milchpfütze zu kreieren. Josh zeigte uns, wie man ein Herz giesst – Kännchen ansetzen, mittig langsam ausgiessen und dann am Ende einmal beherzt durchziehen. Klingt simpel, doch mein erstes „Herz“ sah eher nach abstrakter Kunst aus. Farn war der nächste Schritt: Mehr Kontrolle, fliessendere Bewegungen – und siehe da, es wurde besser!

{{< figure src="./images/PXL_20250213_191725989.jpg" alt="schwan" class="aligne-right size-small" >}}

Doch dann kam das grosse Highlight: der Schwan. Einmal, wirklich nur einmal, gelang er mir grandios. Die Linien waren geschwungen, die Flügel erkennbar – ein Moment purer Barista-Magie! Josh nickte anerkennend, und ich fühlte mich kurz wie ein echter Profi.

Zum Abschluss wurde natürlich aufgeräumt. Und dann? Dann war ich offiziell Home-Barista, mit einem neuen Blick auf Milchschaum und einem tiefen Respekt für all jene, die Latte Art so mühelos aussehen lassen. Fazit: Viel Kaffee, viel Lachen und die Erkenntnis, dass selbst eine amorphe Wolke ein guter Anfang sein kann.
