+++ 
draft = false
date = 2023-08-02T22:00:00+02:00
title = "Arbeiten für ein Startup"
description = "Ein Rückblick auf die ersten zwei Jahre"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["Arbeit", "Startup", "Informatik", "Programmierung"]
categories = ["Arbeit", "Startup", "Informatik", "Programmierung"]
externalLink = ""
series = ["soobr"]
+++

Nach meiner Ausbildung habe ich drei Jahre in einem kleinen Unternehmen gearbeitet. Nachdem ich meine Fähigkeiten dort mit erfahrenen Entwicklern verfeinert hatte, verspürte ich den Wunsch nach mehr technischen Verantwortlichkeiten. In einem Team, das meine Ideen schätzte, sie aber auch kräftig herausforderte. Ausserdem begann der ständige Wechsel der Projekte, obwohl lehrreich, zunehmend einschränkend zu wirken. Auf der Suche nach einer intensiveren Erfahrung, wandte ich mich einem produktbasierten Unternehmen zu. Die Aussicht, eine Lösung von ihrer Entstehung bis hin zur Weiterentwicklung zu betreuen, ohne ständig Projekte wechseln zu müssen, zog mich stark an.

# Erste Tage bei Soobr
Zum Glück fand ich eine interessante Position in einem Startup.

## Soobr
[Soobr](https://www.soobr.com/) ist ein Schweizer Technologieunternehmen, das die digitale Transformation der Reinigungsindustrie vorantreibt.

{{< figure src="./images/soobr_interfaces.png" alt="soobr interfaces" class="aligne-left size-large" >}}

Soobr bietet eine optimierte und bedarfsgerechte Planung und Ausführung von Reinigungsrouten auf Basis von Raumbelegungsdaten und künstlicher Intelligenz. Durch die Nutzung vorhandener Leistungsanforderungen, Gebäudedaten und Sensordaten werden die tatsächlichen Reinigungsbedarfe ermittelt.

Die kombinierten Daten führen zu einer dynamischen Tourenplanung, die auf dem Gebäudeplan visualisiert wird und den Reinigungskräften auf einem Tablet oder Smartphone angezeigt wird. Dies ermöglicht die Automatisierung der zeitaufwendigsten Schritte im Reinigungsprozess, was zu Kosteneinsparungen, Transparenz und höherer Qualität führt.

## Team
<!-- 
    - Kaspar
    - Stefan
    - Tobi
    - Tinu
    - Fabio
    - Nicolai
    - Linda
    - Kai
-->
{{< figure src="./images/kickstarter.jpg" alt="Team 2021" class="aligne-right size-medium" >}}

Zu Beginn bestand das Team aus den Köpfen, die das Startup gegründet haben. Mit einer dynamischen Struktur aus zwei Entwicklern, zwei Verkaufsprofis und zwei Projektmanagern. Die Teamkraft wurde durch die kürzliche Hinzunahme eines Projektmanagers direkt vor meiner Zeit und dem bevorstehenden Eintritt eines weiteren Entwicklers kurz nach meinem Start noch verstärkt.

Alles in allem war es eine Zeit, in der jeder wusste, was passierte. Mit so wenigen Menschen wuchs eine persönliche Verbindung zwischen allen.

## Technologischer Stack
In diesem aufstrebenden Unternehmen fand ich mich vollständig in einer Symphonie aus modernsten Technologien wieder, die unsere innovative Landschaft prägen. Im Kern unserer Backend-Operationen basiert alles auf Java in Verbindung mit Spring. Wenn wir den Fokus auf das Frontend legen, setzen wir auf React als unsere bevorzugte Plattform.
Während des Planungsprozesses nimmt der CP-SAT Solver eine zentrale Rolle ein, indem er die Reinigungsprozesse optimiert.
Was das Hosting der Anwendung betrifft, so umschliesst Google Cloud Platform (GCP) unsere gesamte Infrastruktur. Die inhärenten Cloud-Funktionen von GCP ermöglichen es uns, mit der Zeit zu skalieren, sodass unsere Operationen agil bleiben und die sich ständig weiterentwickelnden Anforderungen unserer Branche erfüllen können.
Wenn es um Analytics geht, glänzt Cube JS mit seinen bemerkenswerten Fähigkeiten. Seine aussergewöhnliche Fähigkeit, neue Informationen nahtlos zu integrieren und schnell Diagramme zu generieren, lässt uns staunen über sein Potenzial.

| Technologie    | Beschreibung           |
| :---:          | :---:                 |
| Java           | Backend               |
| React          | Frontend              |
| GCP            | Cloud                 |
| Cube.js        | Analytics             |
| CP-SAT Solver  | Planungsoptimierung   |

# Meine Arbeit

## Kennenlernen des Produkts
<!-- 
    - 2021 Q3 
        - Kennenlernen des Produkts
        - Notfallplanung
        - Zeitzonen
    - 2021 Q4
        - Sicherheit (IP-Range-Blocking, Zwei-Faktor-Authentifizierung, ...)
-->

Ab dem dritten Quartal 2021 lag der Fokus darauf, alle Aspekte des Produkts kennenzulernen und alle Beteiligten zu treffen.
Mein Ziel in diesem Quartal war es, eine Notfallplanungsfunktion zu erstellen. Diese Funktion würde es den Nutzern ermöglichen, eine PDF-Version des Raumplans, des Serviceplans und eine simulierte Planung herunterzuladen.
Ausserdem mussten wir uns mit Zeitzonen befassen, da wir Kunden in Japan hatten. Das Backend handhabte dies bereits korrekt, also war es meine Aufgabe sicherzustellen, dass alle Daten im Frontend korrekt angezeigt wurden.

Zum Jahresende konzentrierte sich mein Hauptfokus im letzten Quartal darauf, unser System mit neuen Sicherheitsmassnahmen zu stärken. Meine Ziele umfassten die Implementierung wesentlicher Schutzmassnahmen wie IP-Range-Blocking und Zwei-Faktor-Authentifizierung (2FA).

## Architektur und Softwarequalität
<!-- 
- 2022 Q1 - Skalierung (auch Indien)
- 2022 Q2 - Analytics -> Keine Skalierung & Hazelcast
- 2022 Q3 - Analytics
-->

Nach nur sechs Monaten fand ich mich mit der Aufgabe wieder, über die grosse Architektur nachzudenken. Wie konnten wir die Software so gestalten, dass sie in Zukunft skalierbar ist? Und welche Komponenten des Monolithen sollten extrahiert werden, um den Prozess zu vereinfachen?
Gleichzeitig begannen wir mit der Auslagerung der Softwareentwicklung nach Indien, da die Suche nach lokalen Talenten erfolglos gewesen war. Unsere Kollegen dort begannen parallel eine Neugestaltung der Softwarearchitektur. Ihr Ziel war es, am Ende des ersten Quartals eine überarbeitete Anwendung zu liefern.

Mein unermüdliches Streben nach besserer Softwarequalität führte mich in eine Welt anspruchsvoller Werkzeuge und Strategien. Das erste unter diesen Werkzeugen, das ich einführte, war Sonar, manifestiert durch SonarCloud.

Zu Beginn des zweiten Quartals 2022 begannen wir mit einer gewissen Skepsis hinsichtlich der Ergebnisse aus Indien. Wir erkannten die Notwendigkeit, die Skalierbarkeit der Anwendung selbst zu gewährleisten. Der einfachste Weg, dies zu erreichen, bestand darin, den Teil der Anwendung zu isolieren und zu skalieren, der die meisten Lasten erzeugte. Angesichts unserer cloudbasierten Einrichtung hätte dies unkompliziert sein sollen.
Die einzige Herausforderung trat auf, als wir feststellen mussten, dass mehrere Instanzen der Anwendung den Status laufender Vorgänge teilen mussten. Obwohl ein Refactoring dieses Problem potenziell hätte lösen können, entschieden wir uns für einen technisch spannenderen Ansatz (ja, neue Technologien sind immer spannend).
Dies markierte den Beginn unserer Schwierigkeiten. Wir führten Hazelcast als gemeinsamen Cache ein, und zunächst schien alles einwandfrei zu funktionieren. Doch diese Illusion zerbrach schnell, als wir merkwürdiges Verhalten im Speicher beobachteten und die Testanwendung wiederholt abstürzte. Unsere Lösungssuche führte uns in ein Kaninchenloch, in dem wir unsere völlige Unkenntnis hinsichtlich der Konfiguration und der Auswirkungen anderer Abhängigkeiten, wie z.B. JHipster, entdeckten, was die Situation weiter verkomplizierte.
Glücklicherweise wurde beschlossen, Redis als Lösung für den gemeinsamen Cache auszuprobieren, was sich in unserem Szenario als die richtige Wahl herausstellte. Nicht, weil es Hazelcast grundsätzlich übertraf, sondern einfach durch einen glücklichen Zufall.

Also, nach Monaten der Herausforderungen und dem Fehlen greifbarer Vorteile für den Kunden, begann das dritte Quartal. In dieser Zeit nahm ich mir ein benutzerorientierteres Ziel vor: die Verbesserung der dynamischen Analysefähigkeit. Inspiriert durch das Cube.js-Dashboard, begann ich dieses Projekt.

# Wachstum

## Team
Neben den visionären Personen, die die Grundlage des Startups legten, umfasst die Teamzusammensetzung ein dynamisches Spektrum an Talenten. Drei engagierte Support-Mitarbeiter sorgen für reibungslose Abläufe. Eine Handvoll Projektmanager widmet ihre Bemühungen der Pflege von Kundenbeziehungen und der Orientierung der Kunden auf den richtigen Weg. Vier talentierte Entwickler verstärken das technologische Know-how und bringen ihre Expertise in innovative Lösungen ein.

{{< figure src="./images/summerfest_2023.jpg" alt="Team 2023" class="aligne-left size-medium" >}}

Über geografische Grenzen hinweg erstreckt sich die Wirkung des Teams. In Deutschland vereint das Zentrum eine dreifache Synergie aus Sales und Projektmanagement. Über Kontinente hinweg weitet sich der Einfluss weiter aus, indem auch mit Indien und darüber hinaus zusammengearbeitet wird, was die globale Reichweite des Startups erhöht und eine vielfältige, lebendige Gemeinschaft unterstützt. Diese Kombination von Talenten treibt die Mission des Startups mit einem unerschütterlichen Geist der Zusammenarbeit und Innovation voran.

Die Dynamik unseres Teams hat sich weiterentwickelt, vom anfänglich sehr kleinen und persönlichen Setup hin zu einer kompakten, aber stetig wachsenden Einheit. Trotz dieser Expansion bleibt die Essenz unseres Teams von Intimität erhalten. Wir pflegen weiterhin eine flache Hierarchie, die sicherstellt, dass jede Stimme gehört wird, jede Idee ihren Platz findet und Zusammenarbeit nahtlos fliesst.

## Technologischer Stack
<!-- 
  - Verbesserung der Softwarequalität
    - Queues
    - Terraform
    - MongoDB
    - Redis
    - SonarCloud 
-->

Während unsere Reise in diesem aufstrebenden Unternehmen fortschreitet, wächst die Symphonie modernster Technologien, die unsere innovative Landschaft prägen, immer weiter. Unsere Backend-Operationen, fest verwurzelt in Java und Spring, bilden das Fundament unserer Operationen und stellen eine robuste Grundlage für unsere komplexen Abläufe sicher. Ebenso bleibt React unsere zuverlässige Wahl für die Gestaltung ansprechender Benutzeroberflächen im Frontend.
Ergänzend zu dieser Symphonie spielt SonarCloud eine entscheidende Rolle, indem es eine umfassende Codeanalyse bereitstellt und die Integrität sowie die Qualität unseres Codes gewährleistet. 
Redis, unsere Lösung für gemeinsamen Cache, stellt eine zuverlässige Grundlage für unser Statusmanagement dar und trägt so zur reibungslosen Durchführung unserer Operationen bei.
Die Melodie der Effizienz und Koordination findet ihren Rhythmus in Pub/Sub, unserem gewählten Queue-Mechanismus, der eine nahtlose Kommunikation innerhalb unseres komplexen Netzwerks sicherstellt.
Zusätzlich steht unsere Infrastruktur dank Terraform, unserem bevorzugten Infrastruktur-als-Code-Werkzeug, fest, sodass unsere digitale Architektur anpassungsfähig und zukunftsfähig bleibt.

| Technologie    | Beschreibung           |
| :---:          | :---:                 |
| Java           | Backend               |
| React          | Frontend              |
| GCP            | Cloud                 |
| Cube.js        | Analytics             |
| CP-SAT Solver  | Planungsoptimierung   |
| SonarCloud     | Codeanalyse           |
| Redis          | Gemeinsamer Cache     |
| PubSub         | Queuing               |
| Terraform      | Infrastruktur-als-Code |

## Meine Arbeit
Nach den Herausforderungen durch die vorhergesagte erfolglose Lieferung aus Indien erlebte unsere Partnerschaft eine Transformation. Ihre Rolle änderte sich, und sie konzentrierten sich auf kleinere Aufgaben, die deutlich getrennte Bereiche betrafen. Die Zusammenführung der beiden Scrum-Teams verlief jedoch schrittweise und dauerte fast ein halbes Jahr, bevor wir begannen, gemeinsam zu arbeiten.
Im letzten Quartal 2022 richtete sich mein Fokus stark auf die Gestaltung der Softwarearchitektur und die Unterteilung der Anwendung in verschiedene abgegrenzte Kontexte.

Die sorgfältige Schaffung abgegrenzter Kontexte und die anschliessende Fragmentierung der monolithischen Anwendung in mehrere Dienste bleiben die zentralen Achsen meiner Aufmerksamkeit für die kommenden sechs Monate. Dieses faszinierende Thema bleibt weiterhin mein Fokus, und ich bin wirklich froh, mit dieser Aufgabe betraut worden zu sein. Es ist eine intellektuell stimulierende Herausforderung, und ich bin dankbar, an der Spitze dieser transformierenden Reise zu stehen.