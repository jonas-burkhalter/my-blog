+++ 
draft = false
date = 2023-08-02T22:00:00+02:00
title = "Working for a startup"
description = "A review of the first two years"
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["work", "startup", "computer science", "programming"]
categories = ["work", "startup", "computer science", "programming"]
externalLink = ""
series = ["soobr"]
+++

After my apprenticeship I worked for three years in a small company. Having honed my skills there bustling with seasoned developers, I felt a yearning for greater technical responsibilities. With a team that appreciated my ideas but also challenged them vigorously. Furhtermore the constant shuffle of projects, although instructive, started to feel limiting. Eager for a more immersive experience, I sought a product-based company. The prospect of nurturing a solution from its inception through its evolution, without constant project changes, held a magnetic pull.


# first days at soobr
Luckely I found an interesting position at a startup. 

## soobr
[Soobr](https://www.soobr.com/) is a Swiss technology company that is driving the digital transformation of the cleaning industry.

{{< figure src="./images/soobr_interfaces.png" alt="soobr interfaces" class="aligne-left size-large" >}}

Soobr offers optimized and demand-oriented planning and execution of cleaning routes based on room occupancy data and artificial intelligence. By utilizing existing performance specifications, building data, and sensor data, the actual cleaning requirements are determined.

The combined data results in a dynamic tour planning that is visualized on the building plan and displayed to the cleaning staff on a tablet or smartphone. This allows for the automation of the most time-consuming steps of the cleaning process, resulting in cost savings, transparency, and higher quality.

## team
<!-- 
    - Kaspar
    - Stefan
    - Tobi
    - Tinu
    - Fabio
    - Nicolai
    - Linda
    - Kai
-->
{{< figure src="./images/kickstarter.jpg" alt="team 2021" class="aligne-right size-medium" >}}


At the beginning the team was composed of the minds who founded the startup. With a dynamic structure of two developers, two sales professionals, and two project managers. The team's potency is further augmented by the recent inclusion of a project manager just before my tenure and the imminent arrival of another developer shortly after my own.

All in all it was a time when everyone knew everything that happend. With so few people a personal connection grew between everyone. 

## technology stack
In this burgeoning company, I find myself fully immersed in a symphony of cutting-edge technologies that intricately shape our innovative landscape. At the very core of our backend operations, our reliance lies on Java paired with Spring. Shifting focus to the frontend, we embrace React as our platform of choice.
During the planning process, the CP-SAT Solver takes center stage, optimizing the cleaning efficiently.
When it comes to hosting the application, the embrace of Google Cloud Platform (GCP) encapsulates our entire infrastructure. The inherent cloud capabilities of GCP empower us to scale gracefully, ensuring our operations retain an agile and responsive nature, adept at meeting the ever-evolving demands of our field.
As we venture into the realm of analytics, Cube JS shines with its brilliant capabilities. Its remarkable ability to seamlessly integrate new information and swiftly generate charts leaves us amazed at its potential.

| technology    | description           |
| :---:         | :---:                 |
| java          | backend               |
| react         | frontend              |
| gcp           | cloud                 |
| cube js       | analytics             |
| cp-sat solver | planning optimization |

# my work

## getting to know the product
<!-- 
    - 2021 Q3 
        - getting to know the product
        - emegency planning
        - timezones
    - 2021 Q4
        - security (ip range blocking, 	2-Factor Authentication, ...)
-->

Starting in the third quarter of 2021, the focus was on learning all aspects of the product and getting to know everyone.
My goal for this quarter was to create an emergency planning feature. This feature would enable users to download a PDF version of the roombook, service of schedule, and a simulated planning.
Additionally, we needed to address time zones, as we had customers in Japan. The backend already handled this correctly, so my task was to ensure that all dates were displayed accurately in the frontend.

As the year drew to a close, my primary focus for the last quarter was centered on fortifying our systems with new security measures. My objectives revolved around the implementation of essential safeguards such as IP range blocking and two-factor authentication (2FA).

## architecture and software quality
<!-- 
- 2022 Q1 - scaling (india aswell)
- 2022 Q2 - analytics -> nop scaling & hazelcast
- 2022 Q3 - analytics

-->

After just six months, I found myself tasked with pondering the grand architectural landscape. How could we design the software to ensure scalability in the future? And which components of the monolith should be extracted to simplify the process?
Concurrently, we initiated software development outsourcing to India, as the pursuit of local talent had proven unsuccessful. Our counterparts there embarked on a parallel mission to mine – reimagining the software architecture. Their objective was to deliver a refactored application by the end of the first quarter.

My relentless pursuit of enhanced software quality propels me into a realm of sophisticated tools and strategies. The first among these that I introduced was Sonar, manifested in the form of SonarCloud.

At the start of the second quarter of 2022, we commenced with a certain skepticism regarding the deliverables from India. We realized the need to guarantee the application's scalability ourselves. The simplest route to achieve this was by isolating and scaling the part that create the most loaded of the application. Given our cloud-based setup, this should have been straightforward.
The sole challenge emerged when we encountered the necessity for multiple instances of the application to share the status of ongoing operations. While refactoring could have potentially eradicated this issue, we opted for a more technically captivating approach (yeah new technologies are always fun).
This marked the genesis of our troubles. We introduced Hazelcast as a shared cache, and initially, everything seemed to be functioning seamlessly. However, this illusion was quickly shattered when we observed peculiar memory behavior, and the test application crashed repeatedly. Our pursuit of solutions led us down a rabbit hole, exposing our complete lack of understanding regarding the configuration and the extent of influence of other dependencies, such as JHipster, which further complicated matters.
Fortunately, a decision was made to experiment with Redis as the shared cache solution, which proved to be the right call in our scenario. Not because it inherently outperformed Hazelcast, but simply due to a stroke of luck.

So, after enduring months of challenges and witnessing no tangible benefits for the customer, the third quarter unfolded. During this period, I took a more user-oriented objective: enhancing the dynamic analytics capability. Taking inspiration from the Cube.js dashboard, I embarked on this endeavor.

# growing

## team
In addition to the pioneering individuals who laid the foundation of the startup, the team's composition embraces a dynamic spectrum of talents. Among them, three dedicated support professionals stand strong, ensuring seamless operations. A handful of project managers dedicated their efforts to nurturing customer relationships and guiding them towards the right path. Four skilled developers amplify the technological prowess, channeling their expertise into innovative solutions. 

{{< figure src="./images/summerfest_2023.jpg" alt="team 2023" class="aligne-left size-medium" >}}

Spanning across geographical borders, the team's impact resonates widely. In Germany, the hub boasts a threefold synergy, combining the prowess of sales and project management. Across continents, the scope expands further, as collaborations extend to India and beyond, enhancing the startup's global reach and nurturing its vibrant, diverse community. This combination of talents fuels the startup's mission with an unwavering spirit of collaboration and innovation.

The dynamics of our team have evolved, transitioning from an extremely small and personal setup to what remains a compact yet steadily growing unit. Despite this expansion, our team's essence of intimacy persists. We continue to embrace a flat hierarchy, ensuring that every voice resonates, every idea finds its platform, and collaboration flows seamlessly.

<!-- 
besides the persons creating the startup 
- 3 support
- 4 dev
- 5 project manager
- 3 sales / project managers in germany
- ...
- india
-->

## technology stack
<!-- 
  - improving software quality
    - queues
    - terraform
    - mongo db
    - redis
    - sonarcloud 
-->

As our journey in this burgeoning company continues, the symphony of cutting-edge technologies orchestrating our innovative landscape only grows richer. Our backend operations, firmly rooted in Java and Spring, serve as the bedrock of our operations, ensuring a robust foundation for our intricate operations. Similarly, React remains our steadfast choice for crafting captivating user interfaces on the frontend.
Adding to this symphony, SonarCloud plays a pivotal role by providing comprehensive code analysis, ensuring the integrity and quality of our codebase. 
Redis, our shared cache solution, stands as a reliable foundation for our status management, contributing to the seamlessness of our operations.
The melody of efficiency and coordination finds its rhythm in Pub/Sub, our chosen queuing mechanism, ensuring seamless communication within our intricate network of operations.
Additionally, our infrastructure stands tall thanks to Terraform, which, as our preferred infrastructure-as-code tool, ensures our digital architecture remains adaptable and future-ready.

| technology    | description            |
| :---:         | :---:                  |
| java          | backend                |
| react         | frontend               |
| gcp           | cloud                  |
| cube js       | analytics              |
| cp-sat solver | planning optimization  |
| sonarcloud    | code analysis          |
| redis         | shared cache           |
| pubsub        | queuing                |
| terraform     | infrastructure-as-code |

## my work
Following the challenges posed by the previously unsuccessful delivery from India, our partnership underwent a transformation. Their role pivoted towards handling smaller tasks, focusing on distinctly separated areas. The convergence of the two scrum teams, however, was a gradual process, taking nearly half a year before we started collaborating and working together.
During the concluding quarter of 2022, my attention was keenly directed towards shaping the software architecture and delineating the application into distinct bounded contexts.

The meticulous creation of bounded contexts and the subsequent fragmentation of the monolithic application into several services remain the central axes of my attention for the upcoming six months. This captivating subject continues to hold my focus, and I am genuinely pleased to have been entrusted with its pursuit. It's an intellectually stimulating endeavor, and I am grateful to be at the forefront of this transformative journey. 

<!-- 
- work with india

- 2022 Q4 
    - Bounded Contexts
    - coming together (india)

- 2023 Q1 & 2
    software architect
    env splitup
    terraform

- Q3 
 - software splitup
-->