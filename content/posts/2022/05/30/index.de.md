+++
draft = false
date = 2022-05-30T22:00:00+02:00
title = "Analyse und Bewertung von Softwarearchitekturen"
description = ""
slug = ""
authors = ["Jonas Burkhalter"]
tags = ["BFH", "Informatik", "Programmierung"]
categories = ["BFH", "Informatik", "Programmierung"]
externalLink = ""
series = []
+++

# 1 Grundlagen
In den folgenden Unterabschnitten werden grundlegende Begriffe und Konzepte erläutert.

## 1.1 Arten von Anforderungen
### 1.1.1 Funktionale Anforderungen
Funktionale Anforderungen definieren das Verhalten einer Anwendung oder Komponente. Typischerweise beinhalten diese drei Hauptschritte: Dateneingabe - Verarbeitung - Datenausgabe. Die Verarbeitung kann Aufgaben wie Datenmanipulation, Berechnungen, Speicherung, Ausführen von Geschäftsprozessen oder andere Aktivitäten umfassen.

Dieser Anforderungstyp beschreibt **was** eine Anwendung tut.

Funktionale Anforderungen erläutern dem Softwareentwickler, wie sich eine Anwendung verhalten sollte. Wenn diese Anforderungen nicht erfüllt werden, bedeutet das aus der Sicht des Benutzers, dass die Anwendung nicht richtig funktioniert.

### 1.1.2 Nicht-funktionale Anforderungen
Nicht-funktionale Anforderungen beschreiben die Qualitätsmerkmale einer Anwendung, wie Benutzerfreundlichkeit, Sicherheit, Skalierbarkeit und viele andere.

Während funktionale Anforderungen beschreiben, was eine Anwendung tut, erklären nicht-funktionale Anforderungen **wie** das System dies erreicht.

Wenn nicht-funktionale Anforderungen nicht erfüllt werden, kann die Anwendung weiterhin ihre Kernfunktionen korrekt ausführen. Es kann jedoch zu einer umständlichen Erfahrung für Benutzer oder Softwareentwickler kommen.

## 1.2 Qualitätsmerkmale
Nachfolgend werden einige Qualitätsmerkmale aufgelistet, wobei die Liste nicht vollständig ist.

### 1.2.1 Sicherheit
- Wird sensible Information übertragen oder gespeichert?
- Wer kann auf welche Informationen zugreifen?

### 1.2.2 Kapazität
- Wie viele Daten erzeugt die Anwendung?
- Hängt das Datenvolumen von der Anzahl der Benutzer ab?
- Wie lange werden Daten gespeichert und archiviert?

### 1.2.3 Kompatibilität
- Was sind die Mindestanforderungen an die Hardware?
- Welche Betriebssysteme und Versionen werden unterstützt?

### 1.2.4 Zuverlässigkeit
- Wie hoch ist die Wahrscheinlichkeit, dass die Anwendung über einen bestimmten Zeitraum hinweg wie erwartet funktioniert?

### 1.2.5 Verfügbarkeit
- Wie häufig und wie lange können Ausfälle höchstens auftreten?
- Wie lange können Wartungsfenster dauern?

### 1.2.6 Leistung
- Wie schnell reagiert die Anwendung auf Benutzeraktionen?
- Wie lange dauert die Hintergrundverarbeitung?

### 1.2.7 Skalierbarkeit
- Was ist die höchste Last, unter der die Anwendung noch funktioniert?
- Ab welcher Benutzeranzahl wird die Leistungsanforderung nicht mehr erfüllt?
- Kann die Anwendung horizontal oder vertikal skaliert werden?

### 1.2.8 Benutzerfreundlichkeit
- Wie einfach ist es, die Anwendung zu nutzen?
- Wie effizient kann ein Benutzer die Anwendung bedienen?
- Gibt es verschiedene Benutzertypen?

### 1.2.9 Regulierung
- Gibt es gesetzliche Anforderungen, die eingehalten werden müssen?
- Ist die Einhaltung anderer Standards erforderlich?

### 1.2.10 Umwelt
- In welchen Umgebungen soll die Anwendung laufen?
- Werden separate Umgebungen für Tests, Demos, Reviews usw. verwendet?

### 1.2.11 Wartbarkeit
- Wie lange dauert es, Bugs zu beheben?
- Wie einfach ist es, Änderungen an der Anwendung vorzunehmen?
- Wie viel Aufwand ist erforderlich, um die Anwendung auf eine andere Plattform zu portieren?

### 1.2.12 Verwaltungsfähigkeit
- Wie leicht kann ein Administrator die Anwendung verwalten?
- Wie einfach können Schäden und Fehlinformationen durch Bugs behoben werden?

# 2 Metriken
In den folgenden Unterabschnitten werden verschiedene Metriken für Softwarearchitekturen erklärt.

## 2.1 Hauptsequenzlinie
1994 schlug Robert Martin eine Reihe von Metriken für objektorientiertes Design vor. Diese Metriken nutzen nicht alle Attribute, sondern konzentrieren sich auf die Beziehung zwischen Paketen und Klassen innerhalb eines Projekts.

### 2.1.1 Efferenz-Kopplung (Ce)
Diese Metrik wird verwendet, um die Beziehungen zwischen Klassen zu messen. Sie repräsentiert die Anzahl der Klassen in einem bestimmten Paket, die von Klassen in anderen Paketen abhängen. Sie misst die Verwundbarkeit des Pakets gegenüber Änderungen in externen Paketen.

{{< figure src="./images/efferent-coupling.jpg" alt="efferent-coupling" width="50%" >}}

In der oben gezeigten Abbildung ist zu erkennen, dass Klasse A ausgehende Abhängigkeiten zu 3 anderen Klassen hat; daher beträgt der Ce-Wert für diese Klasse 3.

Ein hoher Ce-Wert weist auf die Instabilität eines Pakets hin. Änderungen in einer der zahlreichen externen Klassen können Änderungen am Paket erfordern. Bevorzugte Ce-Werte liegen im Bereich von 0 bis 20; höhere Werte können Wartungs- und Entwicklungsprobleme verursachen.

### 2.1.2 Afferenz-Kopplung (Ca)
Diese Metrik ist komplementär zur Ce-Metrik und wird verwendet, um eingehende Abhängigkeiten zwischen Paketen zu messen. Sie ermöglicht es uns, die Empfindlichkeit der verbleibenden Pakete gegenüber Änderungen im analysierten Paket zu messen.

{{< figure src="./images/afferent-coupling.jpg" alt="afferent-coupling" width="50%" >}}

In der oben gezeigten Abbildung ist zu erkennen, dass Klasse A nur eine eingehende Abhängigkeit hat (von Klasse X); daher beträgt der Ca-Wert 1.

Ein hoher Ca-Wert weist typischerweise auf eine hohe Stabilität des Komponenten hin. Das liegt daran, dass die Klasse von vielen anderen Klassen abhängt. Sie kann daher nicht wesentlich geändert werden, da solche Änderungen die Wahrscheinlichkeit erhöhen würden, dass sich diese Änderungen verbreiten. Bevorzugte Ca-Werte liegen im Bereich von 0 bis 500.

### 2.1.3 Instabilität (I)
Diese Metrik wird verwendet, um die relative Empfindlichkeit einer Klasse gegenüber Änderungen zu messen. Laut der Definition ist die Instabilität das Verhältnis der ausgehenden Abhängigkeiten (Ce) zu allen Abhängigkeiten des Pakets (Ce + Ca) und nimmt Werte von 0 bis 1 an.

Die Metrik wird durch die folgende Formel definiert:

<p style="text-align:center;">
<math><mn>I</mn><mo>=</mo><mfrac><mn>Ce</mn><mrow><mn>Ce</mn><mo>+</mo><mn>Ca</mn><mrow></mfrac></math>
</p>

Wo: Ce – ausgehende Abhängigkeiten, Ca – eingehende Abhängigkeiten

{{< figure src="./images/instability.jpg" alt="instability" width="50%" >}}

In der oben gezeigten Abbildung ist zu erkennen, dass Klasse A 3 ausgehende und 1 eingehende Abhängigkeit hat. Daher beträgt der I-Wert gemäss der Formel 0,75.

Anhand des Wertes der I-Metrik können wir zwei Typen von Komponenten unterscheiden:
 - Komponenten mit vielen ausgehenden Abhängigkeiten (Ce) und wenigen eingehenden Abhängigkeiten (Ca). Diese haben einen I-Wert nahe 1. Dies deutet darauf hin, dass sie instabiler sind, da viele mögliche Änderungen in den ausgehenden Abhängigkeiten zu Änderungen in der Klasse führen können.
 - Komponenten mit vielen eingehenden Abhängigkeiten (Ca) und wenigen ausgehenden Abhängigkeiten (Ce). Diese haben einen I-Wert nahe 0. Sie sind schwieriger zu ändern, da ihre Verantwortung grösser ist.

Bevorzugte I-Werte sollten in den Bereichen von 0 bis 0,3 oder 0,7 bis 1 liegen. Komponenten sollten entweder sehr stabil oder instabil sein, und Komponenten mit mittlerer Stabilität sollten vermieden werden.

### 2.1.4 Abstraktheit (A)
Diese Metrik wird verwendet, um das Abstraktionsniveau eines Pakets zu messen. Laut Definition ist Abstraktheit die Anzahl der abstrakten Klassen im Paket geteilt durch die Gesamtzahl der Klassen.

Die Metrik wird durch die folgende Formel definiert:

<p style="text-align:center;">
<math><mn>A</mn><mo>=</mo><mfrac><mn>T<sub>abstract</sub></mn><mrow><mn>T<sub>abstract</sub></mn><mo>+</mo><mn>T<sub>concrete</sub></mn><mrow></mfrac></math>
</p>

Wo: T<sub>abstract</sub> – Anzahl der abstrakten Klassen in einem Paket, T<sub>concrete</sub> – Anzahl der konkreten Klassen in einem Paket

Bevorzugte A-Werte sollten extreme Werte nahe 0 oder 1 annehmen. Stabile Pakete (I-Wert nahe 0), die von einer sehr kleinen Anzahl anderer Pakete abhängen, sollten auch abstrakt sein (A-Wert nahe 1). Sehr instabile Pakete (I-Wert nahe 1) sollten andererseits aus konkreten Klassen bestehen (A-Wert nahe 0).

{{< figure src="./images/abstractness.jpg" alt="abstractness" width="50%" >}}

Es ist erwähnenswert, dass die Kombination aus Abstraktheit und Stabilität es Robert Martin ermöglichte, eine These über das Vorhandensein der „Hauptsequenzlinie“ zu formulieren.

Im Optimalfall wird die Instabilität der Klasse durch ihre Abstraktheit ausgeglichen, was zur folgenden Gleichung führt:

<p style="text-align:center;">
<math><mrow><mn>I</mn><mo>+</mo><mn>A</mn><mo>=</mo><mn>1</mn><mrow></math>
</p>

Gut gestaltete Klassen sollten sich entlang dieser Graph-Endpunkte der „Hauptsequenzlinie“ gruppieren.

### 2.1.5 Normalisierte Entfernung von der Hauptsequenz (D)
Diese Metrik wird verwendet, um das Gleichgewicht zwischen Stabilität und Abstraktheit zu messen und wird mit der folgenden Formel berechnet:

<p style="text-align:center;">
<math><mrow><mn>D</mn><mo>=</mo><mo>|</mo><mn>A</mn><mo>+</mo><mn>I</mn><mo>-</mo><mn>1</mn><mo>|</mo><mrow></math>
</p>

Wo: A – Abstraktheit, I – Instabilität

Der Wert der D-Metrik kann wie folgt interpretiert werden. Wenn wir eine gegebene Klasse auf dem Graphen der „Hauptsequenzlinie“ platzieren, ist ihre Entfernung von der „Hauptsequenzlinie“ proportional zum Wert von D.

{{< figure src="./images/normalized-distance-from-main-sequence.jpg" alt="normalized-distance-from-main-sequence" width="50%" >}}

Der Wert der D-Metrik sollte so niedrig wie möglich sein, um Komponenten nahe der „Hauptsequenzlinie“ zu haben. Zwei äusserst ungünstige Fälle werden ebenfalls berücksichtigt:
 - A = 0 und I = 0, ein Paket ist äusserst stabil und starr, was unerwünscht ist, da das Paket sehr unflexibel ist und nicht erweitert werden kann;
 - A = 1 und I = 1, eine eher unmögliche Situation, weil ein völlig abstraktes Paket eine externe Verbindung benötigen würde, damit die Instanz, die die Funktionalität aus den abstrakten Klassen dieses Pakets implementiert, erstellt werden kann.

### 2.1.6 Fazit
Die Metriken von Robert Martin ermöglichen es uns, die Beziehungen zwischen Paketen in einem Projekt einfach zu bewerten und zu bestimmen, ob Änderungen erforderlich sind, um potenzielle Probleme in der Zukunft zu vermeiden.

## 2.2 Chidamber und Kemerer
In ihrer Arbeit mit dem Titel „A metrics suite for object-oriented design“ [1] veröffentlichten Shyam Chidamber und Chris Kemerer 1994 sechs Metriken für objektorientiertes Design.

### 2.2.1 Gewichtete Methoden pro Klasse (WMC)
Dies definiert die Summe aller Methoden in einer Klasse, die durch ihre McCabe-Komplexität dargestellt werden. Klassen mit vielen Methoden sind wahrscheinlich anwendungspezifischer und bieten begrenzte Wiederverwendbarkeit.

### 2.2.2 Tiefe des Vererbungbaums (DIT)
DIT stellt die Tiefe einer Klasse im Vererbungbaum dar, wobei ein Wert von null Wurzelklassen entspricht und mehrere Vererbungen die maximale Tiefe anzeigen. Je tiefer eine Klasse in der Hierarchie steht, desto mehr Methoden und Variablen werden wahrscheinlich geerbt, was sie komplexer macht. Tiefe Bäume deuten auf eine grössere Designkomplexität hin.

### 2.2.3 Anzahl der Kinder (NOC)
NOC definiert die Anzahl der direkten Unterklassen einer Klasse. Je mehr Unterklassen eine Klasse hat, desto mehr Aufwand ist erforderlich, um sicherzustellen, dass Änderungen am Verhalten dieser Unterklassen nicht die Klasse zerstören. Infolgedessen wird es schwieriger, die Klasse zu ändern, was mehr Tests erfordert.

### 2.2.4 Kopplung zwischen Objektklassen (CBO)
Diese Metrik misst die Anzahl der Klassen, mit denen eine Klasse gekoppelt ist. Zwei Klassen sind gekoppelt, wenn Methoden, die in einer Klasse deklariert sind, Methoden oder Instanzvariablen verwenden, die von der anderen Klasse definiert sind. Mehr Kopplung bedeutet, dass der Code schwieriger zu warten ist, da Änderungen in anderen Klassen auch Änderungen in dieser Klasse erfordern können. Daher sind diese Klassen weniger wiederverwendbar und erfordern mehr Testaufwand.

### 2.2.5 Antwort für eine Klasse (RFC)
Sie stellt die Anzahl der Methoden dar, die potenziell von ausserhalb der Klasse aufgerufen werden können. Wenn die Anzahl der Methoden, die in einer Klasse aufgerufen werden können, hoch ist, gilt die Klasse als komplexer und möglicherweise stark mit anderen Klassen gekoppelt. Daher erfordert sie grösseren Test- und Wartungsaufwand.

### 2.2.6 Mangelnde Kohäsion der Methoden (LCOM)
Diese Metrik misst, wie Methoden innerhalb einer Klasse miteinander in Beziehung stehen. Geringe Kohäsion deutet darauf hin, dass die Klasse mehr als eine Verantwortung implementiert.
Here is the translated content in German:

# 3 Werkzeuge
In den folgenden Unterabschnitten werden einige Werkzeuge zur Analyse der Softwarearchitektur vorgestellt.

## 3.1 CodeMR
CodeMR ist ein Analysetool für Softwarequalität und statische Codeanalyse für Java-, Kotlin- und Scala-Projekte. CodeMR visualisiert Kennzahlen und Qualitätsmerkmale auf hoher Ebene (Kopplung, Komplexität, Kohäsion und Grösse) in verschiedenen Ansichten wie Paketstruktur, TreeMap, Sunburst, Abhängigkeits- und Diagrammansichten. CodeMR analysiert den Quellcode auf der lokalen Maschine und speichert alle Analyse-Dateien in einem lokalen Arbeitsverzeichnis.

### 3.1.1 Kennzahlen
CodeMR bietet verschiedene Kennzahlen für die Softwareanalyse. Im Folgenden sind einige der Kennzahlen und deren Erläuterungen aufgeführt.

#### Grösse
Grösse ist eine der einfachsten und häufigsten Formen der Softwareanalyse. Die folgende Liste enthält einige der Kennzahlen, die CodeMR für die Grösse bereitstellt.

- **CLOC (Class Lines of Code)**: Die Anzahl der Codezeilen in einer Klasse.
- **NOM (Number of Methods)**: Die Anzahl der Methoden in einer Klasse.
- **NoCls (Number of Classes)**: Gesamtanzahl der Klassen.
- **NoE (Number of Entities)**: Gesamtanzahl der Schnittstellen und Klassen.
- **nofP (Number of Packages)**: Anzahl der Pakete im Projekt.
- ...

#### Komplexität
Komplexität bezieht sich auf die Schwierigkeit des Verständnisses und beschreibt die Interaktionen zwischen einer Menge von Einheiten. Höhere Komplexität in der Software erhöht das Risiko, unbeabsichtigt Interaktionen zu stören, und somit die Wahrscheinlichkeit, bei Änderungen Fehler einzuführen.

- **WMC (Weighted Method Count)**: Die gewichtete Summe der Methoden einer Klasse und der zyklomatischen Komplexität der Methoden.
- **DIT (Depth of Inheritance Tree)**: Die Position der Klasse im Vererbungshierarchiebaum. Sie hat den Wert null für Wurzel- und nicht vererbte Klassen. Bei mehrfacher Vererbung zeigt die Kennzahl die maximale Tiefe an.
- **RFC (Response For a Class)**: Die Anzahl der Methoden, die von aussen aus der Klasse aufgerufen werden können.
- **SI (Specialization Index)**: Der Spezialisierungsindex misst das Mass, in dem Unterklassen ihre Elternklassen überschreiben.
- ...

#### Kopplung
Kopplung beschreibt die Beziehung zwischen verschiedenen Klassen oder Paketen.

- **NOC (Number of Children)**: Die Anzahl der direkten Unterklassen einer Klasse.
- **CBO (Coupling Between Object Classes)**: Die Anzahl der Klassen, mit denen eine Klasse gekoppelt ist.
- **EC (Efferent Coupling)**: Die Anzahl der Klassen in anderen Paketen, von denen die Klassen im Paket abhängen.
- **AC (Afferent Coupling)**: Die Anzahl der Klassen in anderen Paketen, die von den Klassen innerhalb des Pakets abhängen.
- ...

#### Kohäsion
Kohäsion zeigt, wie gut die Methoden einer Klasse miteinander verwandt sind. Eine hohe Kohäsion ist im Allgemeinen bevorzugt.

- **LCOM (Lack of Cohesion of Methods)**: Zeigt, wie die Methoden innerhalb einer Klasse miteinander in Beziehung stehen.
- **LCAM (Lack of Cohesion Among Methods (1-CAM))**: Misst die Kohäsion basierend auf den Parametertypen der Methoden.
- **LTCC (Lack Of Tight Class Cohesion (1-TCC))**: Misst den Mangel an Kohäsion zwischen den öffentlichen Methoden einer Klasse.
- ...

### 3.1.2 Visualisierung
CodeMR kann Visualisierungen auf Projekt- oder Paketebene durchführen, und es können verschiedene Visualisierungstypen ausgewählt werden. Nachfolgend sind einige Visualisierungstypen aufgeführt.

#### Verteilung
Um die Verteilung einer Kennzahl anzuzeigen, kann ein Diagramm zur Metrikverteilung verwendet werden. Es stellt eine einzelne Kennzahl im gesamten Projekt oder Paket dar.
{{< figure src="./images/codemr-highlevel-overview.PNG" alt="codemr-highlevel-overview" width="50%" >}}

#### Paketübersicht
In der Paketübersicht werden Klassen und Pakete als Blasen dargestellt. Zudem kann das Farbschema basierend auf verschiedenen Kennzahlen gewechselt werden.
{{< figure src="./images/codemr-package-overview.PNG" alt="codemr-package-overview" width="50%" >}}

#### Paketabhängigkeiten
Mit dem Diagramm für Paketabhängigkeiten können alle Abhängigkeiten eines einzelnen Pakets schnell und einfach identifiziert und analysiert werden.
{{< figure src="./images/codemr-package-dependencies.PNG" alt="codemr-package-dependencies" width="50%" >}}

#### Graph
Ein weiteres Diagramm ist das Graph-Diagramm, in dem Klassen als Knoten dargestellt werden, die in Paketen gruppiert sind, und Pfeile die Abhängigkeiten darstellen.

{{< figure src="./images/codemr-graph.PNG" alt="codemr-graph" width="50%" >}}

Visuelle Eigenschaften eines Knotens im Diagramm umfassen dessen Farbe, Grösse und Form. Jedes Qualitätsmerkmal wird verschiedenen physischen Eigenschaften eines Knotens zugeordnet.
{{< figure src="./images/codemr-metric-legend.png" alt="codemr-metric-legend" width="50%" >}}

- **Farbe**: Zeigt die Komplexität der Entität an. Die Komplexität ändert die Farbskala in ein Grün-zu-Rot-Spektrum. Die Farben werden wärmer, je mehr die Komplexität einer Entität zunimmt.
- **Form**: Wenn die Kopplung einer Klasse zunimmt, wird die Form des Knotens kantiger. Eine Klasse mit hoher Kopplung hat mehr Ecken, was auf mehr Interaktionspunkte mit anderen Klassen hinweist. Ein Kreis stellt geringe Kopplung dar, während ein sechsseitiger Stern sehr hohe Kopplung darstellt.
- **Grösse**: Die Fläche der Form stellt die Grösse der Klasse dar; je mehr Codezeilen die Klasse enthält, desto grösser wird der Knoten.

#### Weitere Informationen
In verschiedenen Diagrammen können detaillierte Auswertungen für Klassen oder Pakete angezeigt werden. Wie in der folgenden Abbildung gezeigt, können verschiedene Kennzahlen bewertet werden.
{{< figure src="./images/codemr-metric-detail.PNG" alt="codemr-metric-detail" width="50%" >}}

## 3.2 CodeScene
CodeScene ist ein vielseitiges Tool, das sowohl technische als auch soziale Aspekte des Codes analysiert. Neben Kennzahlen wie Lines of Code berücksichtigt es auch die Häufigkeit von Commits.

### 3.2.1 Visualisierung
Der folgende Abschnitt stellt einige Visualisierungen und Analysen vor, die von CodeScene bereitgestellt werden.

#### Hotspots
Die meisten Codeänderungen erfolgen typischerweise in relativ wenigen Paketen. Eine Hotspot-Analyse identifiziert die Pakete, in denen die meiste Zeit für Änderungen aufgewendet wird.

Hotspots dienen als Ausgangspunkt für die Priorisierung von Produktivitätsverlusten.
{{< figure src="./images/codescene-hotspots.PNG" alt="codescene-hotspots" width="50%" >}}

#### Code-Gesundheit
Die Kennzahl Code Health basiert auf bekannten Mustern, die die Wartungskosten erhöhen und den Code schwerer verständlich machen. Dies erhöht das Risiko von Änderungen und verlangsamt die Wartung und Entwicklung.
Der Code Health Score wird aus verschiedenen Faktoren berechnet, einschliesslich der folgenden:

- **Brain Method**: Eine einzelne Methode, die zu viel Verhalten zentralisiert und zu einem lokalen Hotspot wird.
- **Developer Congestion**: Code, der zu einem Koordinationsengpass wird, wenn mehrere Entwickler parallel daran arbeiten.
- **Don't Repeat Yourself Violations**: CodeScene erkennt doppelte Logik, die in vorhersehbaren Mustern zusammen geändert wird.
- **Primitive Obsession**: Code, der viele eingebaute Primitiven wie Ganzzahlen, Zeichenketten und Fliesskommazahlen verwendet, fehlt oft eine Domänensprache, die die Validierung und Semantik von Funktionsparametern kapselt.
- ...
{{< figure src="./images/codescene-code-health.PNG" alt="codescene-code-health" width="50%" >}}

Diese Abbildung zeigt einen Ausschnitt der Code-Gesundheitsanalyse für das Elasticsearch Showcase.
Code-Gesundheitstrends können automatisch in einer CI/CD-Pipeline überwacht werden.

#### Change Coupling
Change Coupling erkennt, wenn zwei oder mehr Klassen über einen bestimmten Zeitraum hinweg gemeinsam geändert werden. Change Coupling ist an sich weder gut noch schlecht; es hängt davon ab, welche Klassen gekoppelt sind und warum sie sich gemeinsam entwickeln.
Klassen gelten als Change Coupling, wenn regelmässig eine der folgenden Bedingungen erfüllt ist:
- Die Klassen werden im gleichen Commit bearbeitet.
- Die Klassen werden von demselben Entwickler innerhalb eines bestimmten Zeitraums geändert.
- Die Commit-Nachrichten für die Änderungen beziehen sich auf dasselbe Ticket.

{{< figure src="./images/codescene-change-coupling.png" alt="codescene-change-coupling" width="50%" >}}
Diese Abbildung zeigt einen Ausschnitt der Change Coupling-Analyse für das Elasticsearch Showcase.

#### Wissensverteilung
Die Analyse der Wissensverteilung misst verschiedene Aspekte, darunter aber nicht beschränkt auf:

- **Koordinationsengpässe**: Teile des Codes, bei denen mehrere Teams ihre Arbeit koordinieren müssen.
- **Wissensinseln**: Der Grossteil des Codes wurde von einem einzigen Entwickler geschrieben.
- **Komplexer Code von früheren Mitwirkenden**: Repräsentiert Code mit niedriger Code-Gesundheit, wobei der Grossteil dieses Codes von früheren Mitwirkenden geschrieben wurde.

## 3.3 JDepend
JDepend ist ein Open-Source-Tool zur Softwareanalyse für Java. JDepend untersucht Klassen und erzeugt Softwarearchitektur-Metriken für jedes Paket. Mit JDepend kann die Qualität einer Architektur automatisch in Bezug auf Erweiterbarkeit, Wiederverwendbarkeit und Wartbarkeit gemessen werden, wodurch Paketabhängigkeiten effektiv verwaltet und kontrolliert werden.

### 3.3.1 Kennzahlen
JDepend analysiert die folgenden Kennzahlen:
- **Anzahl der Klassen und Schnittstellen**: Die Anzahl der konkreten und abstrakten Klassen (und Schnittstellen) im Paket.
- **Anzahl der konkreten Klassen (CC)**: Die Anzahl der konkreten Klassen im Paket.
- **Anzahl der abstrakten Klassen (AC)**: Die Anzahl der abstrakten Klassen (und Schnittstellen) im Paket.
- **Afferente Kopplungen (Ca)**: Die Anzahl der anderen Pakete, die von den Klassen innerhalb des Pakets abhängen.
- **Efferente Kopplungen (Ce)**: Die Anzahl der anderen Pakete, von denen Klassen im Paket abhängen.
- **Abstraktheit (A)**: Das Verhältnis der Anzahl abstrakter Klassen (und Schnittstellen) zum gesamten Klassensatz im Paket.
- **Instabilität (I)**: Das Verhältnis der efferenten Kopplungen (Ce) zur Summe der Kopplungen (Ce + Ca).
- **Entfernung von der Hauptsequenz (D)**: Der vertikale Abstand eines Pakets von der „Hauptsequenzlinie.“
- **Paketabhängigkeitszyklen**: Listet Pakete mit zyklischen Abhängigkeiten auf.

### 3.3.2 Berichte
JDepend stellt Berichte in Text- oder XML-Dateiformaten zur Verfügung, sowie eine grafische Benutzeroberfläche.

#### Grafische Benutzeroberfläche
{{< figure src="./images/jdepend-ui.png" alt="jdepend-ui" width="50%" >}}
Diese Abbildung zeigt die Standard-Benutzeroberfläche von JDepend.

#### XML-basierter Bericht
{{< figure src="./images/jdepend-xml.PNG" alt="jdepend-xml" width="50%" >}}
Diese Abbildung zeigt einen Ausschnitt eines JDepend XML-Berichts.

# 4 Fazit
Es gibt verschiedene Metriken zur Analyse von nicht funktionalen Anforderungen im Zusammenhang mit Softwarearchitekturen. Die meisten davon beziehen sich auf die Abstraktion und die Kopplung von Klassen. Es gibt keine perfekte Klasse; unterschiedliche Werte werden für verschiedene Arten von Klassen erwartet.

Die drei untersuchten Tools zur automatisierten Auswertung von Softwarearchitektur-Metriken haben alle ihre Vor- und Nachteile.

**CodeScene** liefert leicht verständliche Aussagen über die Qualität der Softwarearchitektur und des Codes. Zudem kann das Tool einfach in eine CI/CD-Pipeline integriert werden, um eine automatisierte Auswertung vorzunehmen. Leider sind die meisten Kennzahlen des Tools eine Mischung aus verschiedenen Metriken, weshalb es schwierig ist, präzise Aussagen über spezifische Aspekte der Softwarearchitektur zu treffen.
**JDepend** ist ein sehr minimalistisches Tool, das sich auf wenige Kennzahlen konzentriert. Die von JDepend erfassten Werte beziehen sich stark auf die von Robert Martin vorgeschlagenen Metriken rund um die „Hauptsequenzlinie“. Leider ist die Nutzung des Tools eher umständlich und die Visualisierung für grössere Projekte kaum brauchbar.
**CodeMR** ermöglicht die einfache Visualisierung vieler Softwarearchitektur-Metriken und erleichtert somit ein schnelles Verständnis der Architektur. Allerdings ist das Tool kaum geeignet für automatisierte Auswertungen in einer CI/CD-Pipeline, da die Auswertungen manuell überprüft und interpretiert werden müssen.

# 4 Conclusion
Es gibt verschiedene Metriken zur Analyse von nicht funktionalen Anforderungen in Bezug auf Softwarearchitektur. Die meisten davon betreffen die Abstraktion und die Kopplung von Klassen. Es gibt keine perfekte Klasse; unterschiedliche Werte werden für unterschiedliche Arten von Klassen erwartet.

Die drei untersuchten Werkzeuge zur automatisierten Auswertung von Softwarearchitektur-Metriken haben sowohl Vor- als auch Nachteile.

**CodeScene** liefert leicht verständliche Einblicke in die Qualität der Softwarearchitektur und des Codes. Das Tool lässt sich problemlos in eine CI/CD-Pipeline integrieren, um eine automatisierte Auswertung vorzunehmen. Leider sind die meisten Werte des Tools eine Kombination verschiedener Metriken, wodurch es schwierig ist, präzise Aussagen über spezifische Aspekte der Softwarearchitektur zu treffen.

**JDepend** ist ein sehr minimalistisches Tool, das sich auf wenige Metriken konzentriert. Die von JDepend erfassten Werte beziehen sich stark auf die von Robert Martin vorgeschlagenen Metriken rund um die "main sequence line". Leider ist die Nutzung des Tools eher mühsam und die Visualisierung für grössere Projekte kaum nützlich.

**CodeMR** ermöglicht eine einfache Visualisierung vieler Software

# Original and Quellen
- [Original Presentation](./original/Analyse%20und%20Bewertung%20von%20Software%20Architekturen.pdf)
- [Original](./original/Informatik_Seminar.pdf)
- [aivosto: Chidamber and Kemerer object-oriented metrics suite](https://www.aivosto.com/project/help/pm-oo-ck.html)
- [codemr: Measure, visualise and improve your code quality](https://www.codemr.co.uk/)
- [codescene: See the invisible. Know your priority](https://codescene.com/)
- [future-processing: Object-oriented metrics by Robert Martin](https://kariera.future-processing.pl/blog/object-oriented-metrics-by-robert-martin/)
- [github - JDepend](https://github.com/clarkware/jdepend)
- [perforce: What are Non Functional Requirements — With Examples](https://www.perforce.com/blog/alm/what-are-non-functional-requirements-examples)
- [scand: Functional vs Non-Functional Requirements: The Definitive Guide](https://scand.com/company/blog/functional-vs-non-functional-requirements)
- S.R. Chidamber and C.F. Kemerer. A metrics suite for object oriented design. IEEE Transactions on Software Engineering, 20(6):476–493, 1994.